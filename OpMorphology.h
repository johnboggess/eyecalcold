#pragma once
#include "opbase.h"
#include "Cv.h"

typedef enum { MORPH_ERODE, MORPH_DILATE, MORPH_OPEN, MORPH_CLOSE, MORPH_GRADIENT, MORPH_TOPHAT, MORPH_BLACKHAT } MORPH_TYPE;
typedef enum { KERNEL_RECT, KERNEL_CROSS, KERNEL_ELLIPSE } KERNEL_SHAPE_TYPE;

#define MORPH_DEFAULT_WIDTH (200)
#define MORPH_DEFAULT_HEIGHT (220)

class COpMorphology : public COpBase
{
public:
	COpMorphology(CMainView *pMain);
	~COpMorphology(void);

	virtual	PTCHAR GetFriendlyName() { return L"Morphology"; }
	virtual int	GetDefaultWidth() { return MORPH_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return MORPH_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pIterations;
	CComboBox	*m_pMorpthType;
	CComboBox	*m_pKernelType;
	CEdit		*m_pIterationsEdit;
	CSliderCtrl	*m_pSizeSlider;
	CEdit		*m_pSizeEdit;
	TCHAR		m_aMorphTypeStr[20];
	TCHAR		m_aMorphKernelStr[20];

		// Save data
	MORPH_TYPE	m_eMorphType;
	int			m_nKernelType;
	int			m_nIterations;
	int			m_nSize;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;
	IplConvKernel *m_pKernel;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters(MORPH_TYPE nMorphType, int nKernelType, int nIterations, int nSize);

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void MorphTypeChange();
	afx_msg void MorphKernelChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnChangeParam1();
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
