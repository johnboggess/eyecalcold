#pragma once
#include "opbase.h"
#include "Cv.h"

#define THRESHOLD_DEFAULT_WIDTH (200)
#define THRESHOLD_DEFAULT_HEIGHT (140)

class COpThreshold : public COpBase
{
public:
	COpThreshold(CMainView *pMain);
	~COpThreshold(void);

	virtual	PTCHAR GetFriendlyName() { return L"Threshold"; }
	virtual int	GetDefaultWidth() { return THRESHOLD_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return THRESHOLD_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pThresholdSlider;
	CSliderCtrl	*m_pMaxValueSlider;
	CEdit		*m_pThresholdEdit;
	CEdit		*m_pMaxValueEdit;
	CComboBox	*m_pThresholdTypeCombo;
	TCHAR		m_aThresholdTypeStr[20];

		// Save data
	int			m_nThresholdType;
	int			m_nMaxValue;
	int			m_nThreshold;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void ThresholdTypeChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
