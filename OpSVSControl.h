#pragma once
#include "opbase.h"
#include "Cv.h"
#include "HighGui.h"

#define SVSControl_DEFAULT_WIDTH (200)
#define SVSControl_DEFAULT_HEIGHT (220)

class COpSVSControl : public COpBase
{
public:
	COpSVSControl(CMainView *pMain);
	~COpSVSControl(void);

	virtual	PTCHAR GetFriendlyName() { return L"SVSControl"; }
	virtual int	GetDefaultWidth() { return SVSControl_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return SVSControl_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CButton		*m_pSetupButton;
	CButton		*m_pForwardButton;
	CButton		*m_pStopButton;
	CButton		*m_pBackButton;
	CButton		*m_pLeftButton;
	CButton		*m_pRightButton;
	CSliderCtrl	*m_pDurationSlider;
	CEdit		*m_pDurationEdit;
	CSliderCtrl	*m_pSpeedSlider;
	CEdit		*m_pSpeedEdit;

		// Save data
	int			m_nDuration;
	int			m_nSpeed;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;
	


	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();
	afx_msg void OnBrowseButton();
	afx_msg void OnForwardButton();
	afx_msg void OnStopButton();
	afx_msg void OnBackButton();
	afx_msg void OnLeftButton();
	afx_msg void OnRightButton();
	afx_msg void OnSetupButton();

	DECLARE_MESSAGE_MAP()
};
