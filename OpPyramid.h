#pragma once
#include "opbase.h"
#include "Cv.h"

#define Pyramid_DEFAULT_WIDTH (200)
#define Pyramid_DEFAULT_HEIGHT (100)

class COpPyramid : public COpBase
{
public:
	COpPyramid(CMainView *pMain);
	~COpPyramid(void);

	virtual	PTCHAR GetFriendlyName() { return L"Pyramid"; }
	virtual int	GetDefaultWidth() { return Pyramid_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Pyramid_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CComboBox	*m_pUpDownCombo;
	TCHAR		m_aUpDownStr[20];
	CSliderCtrl	*m_pLevelsSlider;
	CEdit		*m_pLevelsEdit;

		// Save data
	BOOL	m_fPyrUp;
	int		m_nLevels;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnPyramidChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
