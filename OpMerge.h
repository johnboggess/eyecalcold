#pragma once
#include "opbase.h"
#include "Cv.h"

#define Merge_DEFAULT_WIDTH (100)
#define Merge_DEFAULT_HEIGHT (80)

class COpMerge : public COpBase
{
public:
	COpMerge(CMainView *pMain);
	~COpMerge(void);

	virtual	PTCHAR GetFriendlyName() { return L"Merge"; }
	virtual int	GetDefaultWidth() { return Merge_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Merge_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pSlider;
	CComboBox	*m_pCombo;
	CEdit		*m_pEdit;
	TCHAR		m_aStr[20];
	CButton		*m_pButton;

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
