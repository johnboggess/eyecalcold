#include "StdAfx.h"
#include "OpMatrix.h"
#include "OpBase.h"
#include "Cv.h"
#include "Resource.h"

#define ID_MATRIX_SET_BUTTON (100)

BYTE bMatrixFiller[32];

BEGIN_MESSAGE_MAP(COpMatrix, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_COMMAND(ID_MATRIX_CENTERPT, OnCenterPt)
	ON_COMMAND(ID_MATRIX_CLEARALL, OnClearAll)
	ON_COMMAND(ID_MATRIX_SET_BUTTON, OnSetButton)
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CMatrixEdit, CEdit)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

COpMatrix::COpMatrix(CMainView *pMain) : COpBase(pMain)
{
	m_pSetButton = NULL;
	m_fLoadedData = FALSE;

	m_oLastContextMenuPoint.x = MAX_X_SLOTS / 2;
	m_oLastContextMenuPoint.y = MAX_Y_SLOTS / 2;
	m_oAnchorPoint = m_oLastContextMenuPoint;

	for (int y = 0; y < MAX_Y_SLOTS; y++)
		for (int x = 0; x < MAX_X_SLOTS; x++)	
			m_pEdits[x][y] = NULL;


	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpMatrix::~COpMatrix(void)
{
	if (m_pSetButton)delete m_pSetButton;

	for (int y = 0; y < MAX_Y_SLOTS; y++)
		for (int x = 0; x < MAX_X_SLOTS; x++)	
			if (m_pEdits[x][y]) delete m_pEdits[x][y];

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpMatrix::OnPaint() 
{
	
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (!m_fMinimized)	// Draw grid
	{
		RECT Rect = GetBaseDrawingArea();

			// Remember to change the corresponding values in OnContextMenu function below
		Rect.top += 40;	// Save room for the "Set" button on the top
		
		int NumX = MAX_X_SLOTS;
		int NumY = MAX_Y_SLOTS;
		
		int Height = Rect.bottom - Rect.top;
		int	Width = Rect.right - Rect.left;
		int XBin = Width /  NumX;
		int YBin = Height / NumY;
		
		
		
			// Draw horizontal bars
		for (int i = 0; i < NumY; i++)
		{
			dc.MoveTo(Rect.left, Rect.top + (i * YBin));
			dc.LineTo(Rect.right, Rect.top + (i * YBin));
			
		}

			// Draw vertical bars
		for (int i = 1; i < NumX; i++)
		{
			dc.MoveTo(Rect.left + (i * XBin), Rect.top);
			dc.LineTo(Rect.left + (i * XBin), Rect.bottom);
			
		}

			// Draw rectangle around grid item that is the current anchor point
		CPen Pen(PS_SOLID, 2, RGB(0, 255, 0));
		CPen *pOldPen = dc.SelectObject(&Pen);
		dc.Rectangle(CRect(Rect.left+(m_oAnchorPoint.x * XBin)+2, Rect.top + (m_oAnchorPoint.y * YBin)+2, 
			Rect.left+(m_oAnchorPoint.x * XBin)+XBin, Rect.top + (m_oAnchorPoint.y * YBin) + YBin));
		dc.SelectObject(pOldPen);

		if (!m_pEdits[0][0])	// instantiate an edit box for each square in the grid
		{
			for (int y = 0; y < NumY; y++)
				for (int x = 0; x < NumX; x++)
				{		
					m_pEdits[x][y] = new CMatrixEdit(this);
				
					m_pEdits[x][y]->Create(WS_CHILD | WS_VISIBLE | ES_CENTER, CRect(Rect.left+(x * XBin)+4, Rect.top + (y * YBin)+4, Rect.left+(x * XBin)+XBin-4, Rect.top + (y * YBin) + YBin-4), this, 0);
					m_pEdits[x][y]->ShowWindow(SW_SHOW);

					m_pEdits[x][y]->SetWindowTextW(L"");
					
				}
				
			if (m_fLoadedData)	// Put loaded data into edits so they can be displayed and edited
			{
				m_fLoadedData = FALSE;
				
				for (int y = 0; y < NumY; y++)
					for (int x = 0; x < NumX; x++)
						if (m_pEdits[x][y])
						{
							TCHAR TStr[20];
							
							_stprintf(TStr, L"%3.2g", m_gMatrixData[x][y]);
							if (m_gMatrixData[x][y] == (float) MAXINT)
								m_pEdits[x][y]->SetWindowTextW(L"");
							else
								m_pEdits[x][y]->SetWindowTextW(TStr);
						}
						
			}
			
		}
		else
		{
			for (int y = 0; y < NumY; y++)
				for (int x = 0; x < NumX; x++)
					if (m_pEdits[x][y]) m_pEdits[x][y]->ShowWindow(SW_SHOW);

		}		
	}
	else
	{

		for (int y = 0; y < MAX_Y_SLOTS; y++)
			for (int x = 0; x < MAX_X_SLOTS; x++)
				if (m_pEdits[x][y]) m_pEdits[x][y]->ShowWindow(SW_HIDE);

/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpMatrix::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

	m_pSetButton = CreateOpButton(L"Set", 80, 0, ID_MATRIX_SET_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

/*
	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);
*/

	return 0;	// Must be zero
}

DWORD WINAPI COpMatrix::StartOpThread(void *pData)
{
	COpMatrix *pBase = (COpMatrix *) pData;

	pBase->OpThread();

	return 0;
}

void COpMatrix::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpMatrix::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpMatrix::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpMatrix::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		TCHAR TStr[20];
		int nTemp = MAX_X_SLOTS;
		WriteFile(hFile, &nTemp, sizeof(nTemp), &dwBytesWritten, NULL);
		nTemp = MAX_Y_SLOTS;
		WriteFile(hFile, &nTemp, sizeof(nTemp), &dwBytesWritten, NULL);
		
		for (int y = 0; y < MAX_Y_SLOTS; y++)
			for (int x = 0; x < MAX_X_SLOTS; x++)	
			{
				float fValue = (float) MAXINT;
				if (m_pEdits[x][y])
				{
					m_pEdits[x][y]->GetWindowText(TStr, 10);
				
					if (TStr[0]) _stscanf(TStr, L"%f", &fValue);
					
				}
					else fValue = m_gMatrixData[x][y];  // Edits never instantiated so get the data from the save array
					
				WriteFile(hFile, &fValue, sizeof(fValue), &dwBytesWritten, NULL);
			}

		if (WriteFile(hFile, &m_oAnchorPoint, sizeof(m_oAnchorPoint), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bMatrixFiller, sizeof(bMatrixFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpMatrix::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		TCHAR TStr[20];
		int nMaxXSlots;
		int nMaxYSlots;
		ReadFile(hFile, &nMaxXSlots, sizeof(nMaxXSlots), &dwBytesWritten, NULL);
		ReadFile(hFile, &nMaxYSlots, sizeof(nMaxYSlots), &dwBytesWritten, NULL);

		m_fLoadedData = TRUE;
		
		for (int y = 0; y < nMaxYSlots; y++)
			for (int x = 0; x < nMaxXSlots; x++)	
			{
				float fValue = (float) MAXINT;
				ReadFile(hFile, &fValue, sizeof(fValue), &dwBytesWritten, NULL);

				m_gMatrixData[x][y] = fValue;
			}


		if (ReadFile(hFile, &m_oAnchorPoint, sizeof(m_oAnchorPoint), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bMatrixFiller, sizeof(bMatrixFiller), &dwBytesWritten, NULL);

			GetAndSetKernelData();	// Get the current current in readiness to be sent to outputs

			UpdateSliders();
			UpdateValues();
		}

	}

	return fRetValue;
}

	// We got data from the given input index
void COpMatrix::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpMatrix::ValidateParameters()
{
}

void COpMatrix::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpMatrix::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpMatrix::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
/*	
		IplImage *pImage = (IplImage *) LockData(L"MatrixThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if () {	// Validate Src and Dst images, etc

				SetData(0, DATA_IMAGE, 0, pImage);
				SendData();

			}
				else TitleError(specific error text);

			ReleaseData(L"MatrixThread");  // We're done with the data so it can change if needed
		}
*/
		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}

void COpMatrix::OnContextMenu(CWnd* pWnd, CPoint point)
{
	RECT Rect = GetBaseDrawingArea();

	CPoint ClientPoint = point;
	
	ScreenToClient(&ClientPoint);

	if (ClientPoint.y > Rect.bottom) // Call base OnContextMenu
	{
		COpBase::OnContextMenu(pWnd, point);
	}
		else	// Use matrix-specific context menu
	{
		CMenu Menu;

		if (Menu.LoadMenu(IDR_EDIT_MENU))
		{

			CMenu *pPopup = Menu.GetSubMenu(0);

	//		if (m_fDisabled) pPopup->CheckMenuItem(ID_OP_DISABLE, MF_CHECKED);
	//		else pPopup->CheckMenuItem(ID_OP_DISABLE, MF_UNCHECKED);

			pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
					point.x, point.y,
					this); 

		}
		

		Rect.top += 40;	// Save room for the "Set" button on the top
				
		int NumX = MAX_X_SLOTS;
		int NumY = MAX_Y_SLOTS;
		
		int Height = Rect.bottom - Rect.top;
		int	Width = Rect.right - Rect.left;
		int XBin = Width /  NumX;
		int YBin = Height / NumY;


		CPoint ClientPoint = point;
		
		ScreenToClient(&ClientPoint);
		
		m_oLastContextMenuPoint.x = (ClientPoint.x - Rect.left) / XBin;
		m_oLastContextMenuPoint.y = (ClientPoint.y - Rect.top) / YBin;
	}
		
}

void CMatrixEdit::OnContextMenu(CWnd* pWnd, CPoint point)
{
	if (m_pMatrix) m_pMatrix->OnContextMenu(pWnd, point);
}

void COpMatrix::OnCenterPt()
{
	m_oAnchorPoint = m_oLastContextMenuPoint;
	
	InvalidateRect(NULL);

}

void COpMatrix::OnClearAll()
{
	for (int y = 0; y < MAX_Y_SLOTS; y++)
		for (int x = 0; x < MAX_X_SLOTS; x++)
		{		
			if (m_pEdits[x][y]) m_pEdits[x][y]->SetWindowText(L"");
		}
}

void COpMatrix::GetAndSetKernelData()
{
	int MinX = MAXINT, MaxX = 0;
	int MinY = MAXINT, MaxY = 0;
	
	TCHAR TStr[20];
	
		// Get data from edits
	if (m_pEdits[0][0]) // Get data from edits
	{
		for (int y = 0; y < MAX_Y_SLOTS; y++)
			for (int x = 0; x < MAX_X_SLOTS; x++)	
			{
				if (m_pEdits[x][y])
				{
					m_pEdits[x][y]->GetWindowText(TStr, 10);
					
					if (TStr[0] != 0)
					{
						 if (y < MinY) MinY = y;
						 if (y > MaxY) MaxY = y;
						 if (x < MinX) MinX = x;
						 if (x > MaxX) MaxX = x;
					}
					
					PTCHAR Cp = TStr;
					
						// Get past leading spaces
					while (*Cp && _istspace(*Cp)) Cp++;
					
					if (*Cp) 
					{
						float fNum;
						_stscanf(TStr, L"%f", &fNum);
						m_gMatrixData[x][y] = fNum;
					}
						else m_gMatrixData[x][y] = 0;
				}
			}
	}
		else	// Get data from m_gMatrixData since edits are not yet instantiated
	{
		for (int y = 0; y < MAX_Y_SLOTS; y++)
			for (int x = 0; x < MAX_X_SLOTS; x++)	
			{
				if (m_gMatrixData[x][y] != (float) MAXINT)  // MAXINT designates "unused"
				{
					if (y < MinY) MinY = y;
					if (y > MaxY) MaxY = y;
					if (x < MinX) MinX = x;
					if (x > MaxX) MaxX = x;
				}
			}
	}
			
	KERNEL_TYPE *pKernel = new KERNEL_TYPE;

	if (pKernel && (MaxX > MinX || MaxY > MinY))
	{
		CvMat *pMat = cvCreateMat(MaxY-MinY+1, MaxX-MinX+1, CV_32FC1);
		
		if (pMat)
		{
			for (int y = MinY; y <= MaxY; y++)
				for (int x = MinX; x <= MaxX; x++)	
				{
					CvScalar Scaler = cvScalarAll(m_gMatrixData[x][y]);
					cvSet2D(pMat, y-MinY, x-MinX, Scaler);
				}
				
			pKernel->pKernel = pMat;

			pKernel->Anchor.x = m_oAnchorPoint.x - MinX;
			pKernel->Anchor.y = m_oAnchorPoint.y - MinY;
			
			SetData(0, DATA_KERNEL, sizeof(KERNEL_TYPE), pKernel, TRUE);
		}
	}		
}

void COpMatrix::OnSetButton()
{
	GetAndSetKernelData();
	SendData();
}

void COpMatrix::OpDisable()
{
	COpBase::OpDisable();

	if (!m_fDisabled) // We reenabled
		OnSetButton();
	
}
