//@Hai the same as Max except the value to compare is enterd by slider
#include "StdAfx.h"
#include "OpMaxS.h"
#include "OpBase.h"
#include "Cv.h"
#define MIN_SUB (0)
#define MAX_SUB (255)
BYTE bMaxSFiller[32];

BEGIN_MESSAGE_MAP(COpMaxS, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpMaxS::COpMaxS(CMainView *pMain) : COpBase(pMain)
{
	m_pCompare_ValueSlider = NULL;
	m_pCompare_ValueEdit = NULL;
	m_nNumCompare_Value = 0;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpMaxS::~COpMaxS(void)
{
	if (m_pCompare_ValueSlider)delete m_pCompare_ValueSlider;
	if (m_pCompare_ValueEdit) delete m_pCompare_ValueEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpMaxS::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpMaxS::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/

//md%  put title in here
	CreateOpStatic(L"Compare Value", TRUE, 0);
	m_pCompare_ValueEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pCompare_ValueSlider = CreateOpSlider(0, MIN_SUB, MAX_SUB);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpMaxS::StartOpThread(void *pData)
{
	COpMaxS *pBase = (COpMaxS *) pData;

	pBase->OpThread();

	return 0;
}

void COpMaxS::UpdateSliders()
{
	m_pCompare_ValueSlider->SetPos(m_nNumCompare_Value);

}
void COpMaxS::UpdateValues()
{
	TCHAR TStr[1000];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

	_itot(m_nNumCompare_Value, TStr, 10);
	m_pCompare_ValueEdit->SetWindowTextW(TStr);


}


void COpMaxS::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpMaxS::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (WriteFile(hFile, &m_nNumCompare_Value, sizeof(m_nNumCompare_Value), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bMaxSFiller, sizeof(bMaxSFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpMaxS::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (ReadFile(hFile, &m_nNumCompare_Value, sizeof(m_nNumCompare_Value), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bMaxSFiller, sizeof(bMaxSFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpMaxS::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpMaxS::ValidateParameters()
{
}

void COpMaxS::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pCompare_ValueSlider) 
		m_nNumCompare_Value = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpMaxS::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpMaxS::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpMaxS::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpMaxS::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"MaxSThread", DATA_IMAGE, 0);

		if (pImage) 
		{
				cvMaxS(pImage,m_nNumCompare_Value,pImage);
			if (pImage)
			{
				SetData(0, DATA_IMAGE, 0, pImage, FALSE);
				SendData();
			}
			ReleaseData(L"AbsDifSThread");  // We're done with the data so it can change if needed
		}
		
		WaitForSingleObject(m_hThreadEvent, INFINITE);
	}
}
