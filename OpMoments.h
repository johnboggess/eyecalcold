#pragma once
#include "opbase.h"
#include "Cv.h"

#define Moments_DEFAULT_WIDTH (200)
#define Moments_DEFAULT_HEIGHT (340)

class COpMoments : public COpBase
{
public:
	COpMoments(CMainView *pMain);
	~COpMoments(void);

	virtual	PTCHAR GetFriendlyName() { return L"Moments"; }
	virtual int	GetDefaultWidth() { return Moments_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Moments_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CEdit		*m_pM00Edit;
	CEdit		*m_pM10Edit;
	CEdit		*m_pM01Edit;
	CEdit		*m_pM20Edit;
	CEdit		*m_pM11Edit;
	CEdit		*m_pM02Edit;
	CEdit		*m_pM30Edit;
	CEdit		*m_pM21Edit;
	CEdit		*m_pM12Edit;
	CEdit		*m_pM03Edit;

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	CvMoments	m_CvMoments;


	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
