#pragma once
#include "opbase.h"
#include "Cv.h"

#define HierarchicalMatch_DEFAULT_WIDTH (180)
#define HierarchicalMatch_DEFAULT_HEIGHT (120)

class COpHierarchicalMatch : public COpBase
{
public:
	COpHierarchicalMatch(CMainView *pMain);
	~COpHierarchicalMatch(void);

	virtual	PTCHAR GetFriendlyName() { return L"Hierarchi-Match"; }
	virtual int	GetDefaultWidth() { return HierarchicalMatch_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return HierarchicalMatch_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CEdit		*m_pMatchEdit;
	CSliderCtrl	*m_pThresholdSlider;
	CEdit		*m_pThresholdEdit;
	

		// Save data
	double m_MatchValue;
	int	m_nThreshold;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	CvMoments	m_CvHierarchicalMatch;


	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
