#pragma once
#include "opbase.h"
#include "Cv.h"

#define InRangeS_DEFAULT_WIDTH (200)
#define InRangeS_DEFAULT_HEIGHT (120)

class COpInRangeS : public COpBase
{
public:
	COpInRangeS(CMainView *pMain);
	~COpInRangeS(void);

	virtual	PTCHAR GetFriendlyName() { return L"InRangeS"; }
	virtual int	GetDefaultWidth() { return InRangeS_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return InRangeS_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pUpperSlider;
	CSliderCtrl	*m_pLowerSlider;
	CEdit *m_pUpperEdit;
	CEdit *m_pLowerEdit;

		// Save data
	int m_nUpperShift;
	int m_nLowerShift;
	
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
//@hai takes out	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
