#pragma once
#include "opbase.h"
#include "Cv.h"

#define StereoDepth_DEFAULT_WIDTH (200)
#define StereoDepth_DEFAULT_HEIGHT (305)

class COpStereoDepth : public COpBase
{
public:
	COpStereoDepth(CMainView *pMain);
	~COpStereoDepth(void);

	virtual	PTCHAR GetFriendlyName() { return L"StereoDepth"; }
	virtual int	GetDefaultWidth() { return StereoDepth_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return StereoDepth_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_ppreFilterSize;
	CSliderCtrl	*m_ppreFilterCap;
	CSliderCtrl	*m_pSADWindowSize;
	CSliderCtrl	*m_pminDisparity;
	CSliderCtrl	*m_pnumberOfDisparities;
	CSliderCtrl	*m_ptextureThreshold;
	CSliderCtrl	*m_puniquenessRatio;

	CEdit	*m_ppreFilterSizeEdit;
	CEdit	*m_ppreFilterCapEdit;
	CEdit	*m_pSADWindowSizeEdit;
	CEdit	*m_pminDisparityEdit;
	CEdit	*m_pnumberOfDisparitiesEdit;
	CEdit	*m_ptextureThresholdEdit;
	CEdit	*m_puniquenessRatioEdit;

			// Save data
	int m_npreFilterSize;
	int m_npreFilterCap;
	int m_nSADWindowSize;
	int m_nminDisparity;
	int m_nnumberOfDisparities;
	int m_ntextureThreshold;
	int m_nuniquenessRatio;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
