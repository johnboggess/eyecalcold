#include "StdAfx.h"
#include "OpAnaglyph.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bAnaglyphFiller[32];

#define ANAGLYPH_DEFAULT_SHIFT (14)
#define ANAGLYPH_SHIFT_MIN (0)
#define ANAGLYPH_SHIFT_MAX (200)


BEGIN_MESSAGE_MAP(COpAnaglyph, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpAnaglyph::COpAnaglyph(CMainView *pMain) : COpBase(pMain)
{

	m_pHorzSlider = NULL;
	m_pVertSlider = NULL;
	m_pHorzEdit = NULL;
	m_pVertEdit = NULL;

	m_nHorizontalShift = 0;
	m_nVerticalShift = 0;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpAnaglyph::~COpAnaglyph(void)
{
	if (m_pHorzSlider)delete m_pHorzSlider;
	if (m_pVertSlider)delete m_pVertSlider;
	if (m_pHorzEdit) delete m_pHorzEdit;
	if (m_pVertEdit) delete m_pVertEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpAnaglyph::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpAnaglyph::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
	CreateOpStatic(L"horizontal", TRUE, 0);
	m_pHorzEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pHorzSlider = CreateOpSlider(0, ANAGLYPH_SHIFT_MIN, ANAGLYPH_SHIFT_MAX);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"vertical", TRUE, 0);
	m_pVertEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pVertSlider = CreateOpSlider(0, ANAGLYPH_SHIFT_MIN, ANAGLYPH_SHIFT_MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpAnaglyph::StartOpThread(void *pData)
{
	COpAnaglyph *pBase = (COpAnaglyph *) pData;

	pBase->OpThread();

	return 0;
}

void COpAnaglyph::UpdateSliders()
{
	m_pHorzSlider->SetPos(-1);
	m_pVertSlider->SetPos(-1);
	m_pHorzSlider->SetPos(m_nHorizontalShift);
	m_pVertSlider->SetPos(m_nVerticalShift);

}
void COpAnaglyph::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

	_itot(m_nHorizontalShift, TStr, 10);
	m_pHorzEdit->SetWindowTextW(TStr);
	_itot(m_nVerticalShift, TStr, 10);
	m_pVertEdit->SetWindowTextW(TStr);


}


void COpAnaglyph::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpAnaglyph::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nHorizontalShift, sizeof(m_nHorizontalShift), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nVerticalShift, sizeof(m_nVerticalShift), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bAnaglyphFiller, sizeof(bAnaglyphFiller), &dwBytesWritten, NULL);
		}
	}

	return fRetValue;
}

BOOL COpAnaglyph::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nHorizontalShift, sizeof(m_nHorizontalShift), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nVerticalShift, sizeof(m_nVerticalShift), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bAnaglyphFiller, sizeof(bAnaglyphFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}

	}

	return fRetValue;
}

	// We got data from the given input index
void COpAnaglyph::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpAnaglyph::ValidateParameters()
{
}

void COpAnaglyph::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pHorzSlider) 
		m_nHorizontalShift = pSlider->GetPos();
	else if (pSlider == m_pVertSlider) 
		m_nVerticalShift = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpAnaglyph::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpAnaglyph::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpAnaglyph::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpAnaglyph::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pLeftImage = (IplImage *) LockData(L"AnaglyphThread", DATA_IMAGE, 0);

		IplImage *pRightImage = (IplImage *) LockData(L"AnaglyphThread", DATA_IMAGE, 1);

		if (pLeftImage && pRightImage) 
		{

				// Shift left image a little
			cvSetImageROI(pLeftImage, cvRect(m_nHorizontalShift, 0, pLeftImage->width-m_nHorizontalShift, pLeftImage->height));
			IplImage *pNewLeftImage = cvCreateImage(cvSize(pLeftImage->width + (m_nHorizontalShift * 2), pLeftImage->height + (m_nHorizontalShift * 2)), pLeftImage->depth, pLeftImage->nChannels);
			cvCopyMakeBorder(pLeftImage, pNewLeftImage, cvPoint(m_nHorizontalShift, 0), IPL_BORDER_CONSTANT, cvScalarAll(0));
			cvResetImageROI(pLeftImage);
			cvSetImageROI(pNewLeftImage, cvRect(m_nHorizontalShift, 0, pLeftImage->width, pLeftImage->height));
			pLeftImage = cvCloneImage(pNewLeftImage);
			cvReleaseImage(&pNewLeftImage);
		
			if (pLeftImage->depth == pRightImage->depth && pLeftImage->nChannels == pRightImage->nChannels
				&& cvGetSize(pLeftImage).height == cvGetSize(pRightImage).height				
				&& cvGetSize(pLeftImage).width == cvGetSize(pRightImage).width) {	// Validate Src and Dst images, etc

				IplImage *pNewImage = cvCreateImage(cvGetSize(pRightImage), pRightImage->depth, 3);
				
				IplImage *pBlue = cvCreateImage(cvGetSize(pRightImage), pRightImage->depth, 1);
				IplImage *pGreen = cvCreateImage(cvGetSize(pRightImage), pRightImage->depth, 1);
				IplImage *pRed = cvCreateImage(cvGetSize(pRightImage), pRightImage->depth, 1);
				
				cvSplit(pLeftImage, NULL, NULL, pRed, NULL);
				cvSplit(pRightImage, pBlue, pGreen, NULL, NULL);
				
				cvMerge(pBlue, pGreen, pRed, NULL, pNewImage);

				SetData(0, DATA_IMAGE, 0, pNewImage, TRUE);
				SendData();
				
				cvReleaseImage(&pBlue);
				cvReleaseImage(&pGreen);
				cvReleaseImage(&pRed);

			}
				else TitleError(L"images must be of same depth, size and number of channels");

			ReleaseData(L"AnaglyphThread");  // We're done with the data so it can change if needed
			ReleaseData(L"AnaglyphThread");  // We're done with the data so it can change if needed
		}
			else 
		{
			TitleError(L"Left and right images are required");
			
			if (pLeftImage) ReleaseData(L"AnaglyphThread");
			if (pRightImage) ReleaseData(L"AnaglyphThread");
		}
		
		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
