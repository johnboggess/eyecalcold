#pragma once

#include "cv.h"

typedef enum { GET_IMAGE_LEFT, GET_IMAGE_RIGHT } GET_IMAGE_TYPE;

typedef enum { MOVE_STOP, MOVE_FORWARD, MOVE_BACKWARD, MOVE_LEFT, MOVE_RIGHT } MOVE_TYPE;

typedef enum { CAMERA_DEST_LEFT, CAMERA_DEST_RIGHT, CAMERA_DEST_BOTH } CAMERA_DEST_TYPE;
class CMySocket : public CSocket
{
public:
	CMySocket();
	~CMySocket();
	
	int Receive(void* lpBuf, int nBufLen, int nFlags = 0);

};
class CSVSInterface
{
public:
	CSVSInterface(void);
	~CSVSInterface(void);
	
	BOOL Initialize();
	void RetryInitialize();
	IplImage *GetImage(GET_IMAGE_TYPE eImage);
	BOOL MoveRobot(MOVE_TYPE eMove, DWORD dwSpeed, DWORD dwDuration);
	BOOL SendCommand(CAMERA_DEST_TYPE eCameraDest, char *CmdStr, char *pResultBuf, int nResultBufLen, int *pBytesReturned);

protected:
	int		m_nStartupResult;
	CMySocket m_oSocketLeft;
	CMySocket m_oSocketRight;
	BOOL	m_fSVSConnected;
	DWORD	m_dwLastConnectTryTime;
	HANDLE	m_hLeftSocketMutex;
	HANDLE	m_hRightSocketMutex;


};

