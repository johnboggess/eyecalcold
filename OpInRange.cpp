//@Hai check if the pixels in an image(source0) fall within a particular range (source1;source2)
specifi ed range
#include "StdAfx.h"
#include "OpInRange.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bInRangeFiller[32];

BEGIN_MESSAGE_MAP(COpInRange, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
END_MESSAGE_MAP()

COpInRange::COpInRange(CMainView *pMain) : COpBase(pMain)
{

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpInRange::~COpInRange(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpInRange::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpInRange::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CCombo *pCombo = m_pCombo;
	if (pCombo)
	{
//		int Ndx = pCombo->InRangeString(L"");
//		pCombo->SetItemData(Ndx, );
//		Ndx = pCombo->InRangeString(L"");
//		pCombo->SetItemData(Ndx, );
	}

//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

	UpdateSliders();
	UpdateValues();
*/

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpInRange::StartOpThread(void *pData)
{
	COpInRange *pBase = (COpInRange *) pData;

	pBase->OpThread();

	return 0;
}

void COpInRange::UpdateSliders()
{
//	m_pSluder->SetPos(m_n);

}
void COpInRange::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpInRange::InRangeItemsToContextMenu(CMenu *pMenu)
{
//	InRangeMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpInRange::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
/*	
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bInRangeFiller, sizeof(bInRangeFiller), &dwBytesWritten, NULL);
		}
*/
	}

	return fRetValue;
}

BOOL COpInRange::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
/*	
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bInRangeFiller, sizeof(bInRangeFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
*/		
	}

	return fRetValue;
}

	// We got data from the given input index
void COpInRange::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpInRange::ValidateParameters()
{
}

void COpInRange::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
/*
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
*/
}

void COpInRange::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpInRange::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage0 = (IplImage *) LockData(L"AbsDiffThread", DATA_IMAGE, 0);
		IplImage *pImage1 = (IplImage *) LockData(L"AbsDiffThread", DATA_IMAGE, 1);
		IplImage *pImage2 = (IplImage *) LockData(L"AbsDiffThread", DATA_IMAGE, 2);
			if (pImage0 && pImage1&&pImage2)  	// Validate Src and Dst images, etc
			{
				if (pImage1->width == pImage0->width&&
					pImage0->width ==pImage2->width&&
					pImage1->height == pImage0->height&&
					pImage0->height== pImage2->height&&
					pImage0->depth == pImage1->depth&&
					pImage1->depth==pImage2->depth&& 
	//				(pImage1->depth== IPL_DEPTH_8U|| pImage1->depth==IPL_DEPTH_8S)&&
					pImage0->nChannels == pImage1->nChannels&&
					pImage1->nChannels==pImage2->nChannels)
				{		
					IplImage *pDest = cvCreateImage(cvGetSize(pImage0),8, 1);
					cvInRange(pImage0, pImage1,pImage2, pDest);
					if (pDest)
					{
						SetData(0, DATA_IMAGE, 0, pDest, TRUE);
						SendData();
					}
			     }
				else TitleError(L"Src and Dest images must be the same size, depth, and channels");
			}
			else 
				TitleError(L"Operation requires three images");
		ReleaseData(L"AbsDiffThread");
		ReleaseData(L"AbsDiffThread"); 
		ReleaseData(L"AbsDiffThread");// We're done with the data so it can change if needed	
	}
		WaitForSingleObject(m_hThreadEvent, INFINITE);
	
}