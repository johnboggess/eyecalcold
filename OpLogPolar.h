#pragma once
#include "opbase.h"
#include "Cv.h"

#define LogPolar_DEFAULT_WIDTH (200)
#define LogPolar_DEFAULT_HEIGHT (100)

class COpLogPolar : public COpBase
{
public:
	COpLogPolar(CMainView *pMain);
	~COpLogPolar(void);

	virtual	PTCHAR GetFriendlyName() { return L"LogPolar"; }
	virtual int	GetDefaultWidth() { return LogPolar_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return LogPolar_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pScaleSlider;
	CEdit *m_pScaleEdit;
	CButton *m_pInverseButton;

		// Save data
	int		m_nScale;
	int		m_nInverse;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnInverseButton();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
