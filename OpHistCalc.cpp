#include "StdAfx.h"
#include "OpHistCalc.h"
#include "OpBase.h"
#include "Cv.h"

#define MIN_DIMS (1)
#define MAX_DIMS (2)
#define NUM_HIST_BINS (255)

BYTE bHistCalcFiller[32];

BEGIN_MESSAGE_MAP(COpHistCalc, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpHistCalc::COpHistCalc(CMainView *pMain) : COpBase(pMain)
{
	m_pDimensionsSlider = NULL;
	m_pDimensionsEdit = NULL;
	m_nNumDimensions = 1;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpHistCalc::~COpHistCalc(void)
{
	if (m_pDimensionsSlider)delete m_pDimensionsSlider;
	if (m_pDimensionsEdit) delete m_pDimensionsEdit;
	
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpHistCalc::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpHistCalc::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
	CreateOpStatic(L"", TRUE, 0);
	m_pDimensionsEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pDimensionsSlider = CreateOpSlider(0, MIN_DIMS, MAX_DIMS);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpHistCalc::StartOpThread(void *pData)
{
	COpHistCalc *pBase = (COpHistCalc *) pData;

	pBase->OpThread();

	return 0;
}

void COpHistCalc::UpdateSliders()
{
	m_pDimensionsSlider->SetPos(m_nNumDimensions);

}
void COpHistCalc::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

	_itot(m_nNumDimensions, TStr, 10);
	m_pDimensionsEdit->SetWindowTextW(TStr);


}


void COpHistCalc::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpHistCalc::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (WriteFile(hFile, &m_nNumDimensions, sizeof(m_nNumDimensions), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bHistCalcFiller, sizeof(bHistCalcFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpHistCalc::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (ReadFile(hFile, &m_nNumDimensions, sizeof(m_nNumDimensions), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bHistCalcFiller, sizeof(bHistCalcFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpHistCalc::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpHistCalc::ValidateParameters()
{
}

void COpHistCalc::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pDimensionsSlider) 
		m_nNumDimensions = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpHistCalc::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpHistCalc::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpHistCalc::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpHistCalc::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"HistCalcThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (pImage->nChannels == 1) {	// Validate Src and Dst images, etc
				
				int h_bins = NUM_HIST_BINS;
				float h_ranges[] = { 0, 255 };
				
				int hist_size[] = { h_bins, h_bins };
				float *ranges[] = { h_ranges, h_ranges };
				IplImage *Images[] = { pImage, pImage };
				
				CvHistogram *pHist = cvCreateHist(m_nNumDimensions, hist_size, CV_HIST_ARRAY, ranges, 1);

				if (pHist)
				{
					cvCalcHist(Images, pHist, 0, 0);

					SetData(0, DATA_HISTOGRAM, 0, pHist, TRUE);
					SendData();
				}
			}
				else TitleError(L"Source image must be single channel");

			ReleaseData(L"HistCalcThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
