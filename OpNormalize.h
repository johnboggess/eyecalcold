#pragma once
#include "opbase.h"
#include "Cv.h"

#define Normalize_DEFAULT_WIDTH (200)
#define Normalize_DEFAULT_HEIGHT (150)

class COpNormalize : public COpBase
{
public:
	COpNormalize(CMainView *pMain);
	~COpNormalize(void);

	virtual	PTCHAR GetFriendlyName() { return L"Normalize"; }
	virtual int	GetDefaultWidth() { return Normalize_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Normalize_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pNormalizeTypeCombo;
	CSliderCtrl	*m_paSlider;
	CSliderCtrl	*m_pbSlider;
	CEdit *m_paEdit;
	CEdit *m_pbEdit;

		// Save data
	int			m_nNormalizeType;
	int m_naShift;
	int m_nbShift;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void NormalizeTypeChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
