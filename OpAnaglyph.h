#pragma once
#include "opbase.h"
#include "Cv.h"

#define Anaglyph_DEFAULT_WIDTH (200)
#define Anaglyph_DEFAULT_HEIGHT (120)

class COpAnaglyph : public COpBase
{
public:
	COpAnaglyph(CMainView *pMain);
	~COpAnaglyph(void);

	virtual	PTCHAR GetFriendlyName() { return L"Anaglyph"; }
	virtual int	GetDefaultWidth() { return Anaglyph_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Anaglyph_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pHorzSlider;
	CSliderCtrl	*m_pVertSlider;
	CEdit *m_pHorzEdit;
	CEdit *m_pVertEdit;

		// Save data
	int m_nHorizontalShift;
	int m_nVerticalShift;
	
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
