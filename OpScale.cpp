#include "StdAfx.h"
#include "OpScale.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bScaleFiller[32];

#define MIN_SCALE (0)
#define MAX_SCALE (2048)

#define MIN_FINE_SCALE (0)
#define MAX_FINE_SCALE (10)

BEGIN_MESSAGE_MAP(COpScale, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
END_MESSAGE_MAP()

COpScale::COpScale(CMainView *pMain) : COpBase(pMain)
{
	m_pScaleSlider = NULL;
	m_pFineScaleSlider = NULL;
	m_pScaleEdit = NULL;
	
	m_nScale = 1;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpScale::~COpScale(void)
{
	if (m_pScaleSlider)delete m_pScaleSlider;
	if (m_pFineScaleSlider)delete m_pFineScaleSlider;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpScale::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpScale::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	CreateOpStatic(L"scale", TRUE, 0);
	m_pScaleEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pScaleSlider = CreateOpSlider(0, MIN_SCALE, MAX_SCALE);

	CreateOpStatic(L"fine tune", TRUE, 0);
	m_pFineScaleSlider = CreateOpSlider(0, MIN_FINE_SCALE, MAX_FINE_SCALE);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpScale::StartOpThread(void *pData)
{
	COpScale *pBase = (COpScale *) pData;

	pBase->OpThread();

	return 0;
}

void COpScale::UpdateSliders()
{
	int nMod = m_nScale % 10;

	m_pScaleSlider->SetPos(m_nScale - nMod );

	m_pFineScaleSlider->SetPos(nMod);

}
void COpScale::UpdateValues()
{
	TCHAR TStr[10];

	_itot(m_nScale, TStr, 10);
	m_pScaleEdit->SetWindowTextW(TStr);


}


void COpScale::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpScale::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (WriteFile(hFile, &m_nScale, sizeof(m_nScale), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bScaleFiller, sizeof(bScaleFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpScale::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (ReadFile(hFile, &m_nScale, sizeof(m_nScale), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bScaleFiller, sizeof(bScaleFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpScale::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpScale::ValidateParameters()
{
}

void COpScale::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	m_nScale = m_pScaleSlider->GetPos() + m_pFineScaleSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpScale::OnChange()
{
	
}

void COpScale::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"ScaleThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (1) {	// Validate Src and Dst images, etc

				cvScale(pImage, pImage, m_nScale);

				SetData(0, DATA_IMAGE, 0, pImage);
				SendData();

			}
				else TitleError(L"");

			ReleaseData(L"ScaleThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
