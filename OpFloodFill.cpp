#include "StdAfx.h"
#include "OpFloodFill.h"
#include "OpBase.h"
#include "Cv.h"

#define MIN_DIFF_SIZE (0)
#define MAX_DIFF_SIZE (255)

#define FLOOD_X_ID (100)
#define FLOOD_Y_ID (101)
#define FLOOD_R_ID (102)
#define FLOOD_G_ID (103)
#define FLOOD_B_ID (104)
#define ID_FIXED_RANGE_BUTTON (105)
#define ID_DIAGONAL_BUTTON (106)

#define SEED_COLOR_X (140)
#define SEED_COLOR_Y (30)
#define SEED_COLOR_WIDTH (20)
#define SEED_COLOR_HEIGHT (20)

BYTE bFloodFillFiller[32];

BEGIN_MESSAGE_MAP(COpFloodFill, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_WM_TIMER()
	ON_EN_CHANGE(FLOOD_X_ID, OnEditChange)
	ON_EN_CHANGE(FLOOD_Y_ID, OnEditChange)
	ON_EN_CHANGE(FLOOD_R_ID, OnEditChange)
	ON_EN_CHANGE(FLOOD_G_ID, OnEditChange)
	ON_EN_CHANGE(FLOOD_B_ID, OnEditChange)
	ON_COMMAND(ID_FIXED_RANGE_BUTTON, OnFixedRangeButton)
	ON_COMMAND(ID_DIAGONAL_BUTTON, OnDiagonalButton)
END_MESSAGE_MAP()

COpFloodFill::COpFloodFill(CMainView *pMain) : COpBase(pMain)
{
	m_pLoDiffSlider = NULL;
	m_pHiDiffSlider = NULL;
	m_pLoDiffEdit = NULL;
	m_pHiDiffEdit = NULL;
	m_pPointXEdit = NULL;
	m_pPointYEdit = NULL;
	m_pValueRed = NULL;
	m_pValueGreen = NULL;
	m_pValueBlue = NULL;
	m_oLoDiff = cvScalarAll(0);
	m_oHiDiff = cvScalarAll(0);
	m_oSeedPoint.x = 0;
	m_oSeedPoint.y = 0;
	m_oNewValue = cvScalarAll(0);
	m_fEnableChange = TRUE;
	m_fFixedRange = FALSE;
	m_fDiagonal = FALSE;
	m_pFixedRangeCheck = NULL;
	m_pDiagonalCheck = NULL;


	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

	m_hThread = NULL;

}

COpFloodFill::~COpFloodFill(void)
{

	if (m_pLoDiffSlider) delete m_pLoDiffSlider;
	if (m_pHiDiffSlider) delete m_pHiDiffSlider;
	if (m_pPointXEdit) delete m_pPointXEdit;
	if (m_pPointYEdit) delete m_pPointYEdit;
	if (m_pValueRed) delete m_pValueRed;
	if (m_pValueGreen) delete m_pValueGreen;
	if (m_pValueBlue) delete m_pValueBlue;
	if (m_pLoDiffEdit) delete m_pLoDiffEdit;
	if (m_pHiDiffEdit) delete m_pHiDiffEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpFloodFill::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*md%
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}
	else	// Draw non-minimizes stuff
	{

		RECT Rect = GetBaseDrawingArea();

		IplImage *pImage = (IplImage *) LockData(L"FloodFillPaint", DATA_IMAGE, 0);

		if (pImage)
		{
			CvSize Size = cvGetSize(pImage);

			CvScalar Data;
			if (m_oSeedPoint.x >= 0 && m_oSeedPoint.y >= 0 && m_oSeedPoint.x < Size.width && m_oSeedPoint.y < Size.height)
			{		
				Data = cvGet2D(pImage, m_oSeedPoint.y, m_oSeedPoint.x);

				CBrush Brush(RGB(Data.val[2], Data.val[1], Data.val[0]));
				CBrush *pOldBrush = dc.SelectObject(&Brush);
				dc.Rectangle(SEED_COLOR_X, SEED_COLOR_Y, SEED_COLOR_X+SEED_COLOR_WIDTH, SEED_COLOR_Y+SEED_COLOR_HEIGHT);

				dc.SelectObject(pOldBrush);
			}

			ReleaseData(L"FloodFill Paint");
		}

	}

}

int COpFloodFill::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	CreateOpStatic(L"seed point", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"X", TRUE, PARAM_EDIT_WIDTH);
	m_pPointXEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, FALSE, FLOOD_X_ID);
	CreateOpStatic(L"Y", TRUE, PARAM_EDIT_WIDTH);
	m_pPointYEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, FALSE, FLOOD_Y_ID);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"new value", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"R", TRUE, 15);
	m_pValueRed = CreateOpEdit(PARAM_EDIT_WIDTH+20, FALSE, FLOOD_R_ID);
	CreateOpStatic(L"G", TRUE, 15);
	m_pValueGreen = CreateOpEdit(PARAM_EDIT_WIDTH+20, FALSE, FLOOD_G_ID);
	CreateOpStatic(L"B", TRUE, 15);
	m_pValueBlue = CreateOpEdit(PARAM_EDIT_WIDTH+20, FALSE, FLOOD_B_ID);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"lo diff", TRUE, 0);
	m_pLoDiffEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pLoDiffSlider = CreateOpSlider(0, MIN_DIFF_SIZE, MAX_DIFF_SIZE);
	CreateOpStatic(L"hi diff", TRUE, 0);
	m_pHiDiffEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pHiDiffSlider = CreateOpSlider(0, MIN_DIFF_SIZE, MAX_DIFF_SIZE);
	m_pFixedRangeCheck = CreateOpButton(L"Fixed", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);
	m_pDiagonalCheck = CreateOpButton(L"Diagonal", 80, BS_AUTOCHECKBOX, ID_DIAGONAL_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpFloodFill::StartOpThread(void *pData)
{
	COpFloodFill *pBase = (COpFloodFill *) pData;

	pBase->OpThread();

	return 0;
}

void COpFloodFill::UpdateSliders()
{
	m_pLoDiffSlider->SetPos((int) m_oLoDiff.val[0]);
	m_pHiDiffSlider->SetPos((int) m_oHiDiff.val[0]);

	if (m_fFixedRange) m_pFixedRangeCheck->SetCheck(BST_CHECKED);
	if (m_fDiagonal) m_pDiagonalCheck->SetCheck(BST_CHECKED);

}
void COpFloodFill::UpdateValues()
{
	TCHAR TStr[20];

	m_fEnableChange = FALSE;	// Don't call OnEditChange

	_itot((int)m_oLoDiff.val[0], TStr, 10);
	m_pLoDiffEdit->SetWindowTextW(TStr);

	_itot((int)m_oHiDiff.val[0], TStr, 10);
	m_pHiDiffEdit->SetWindowTextW(TStr);

	_itot(m_oSeedPoint.x, TStr, 10);
	m_pPointXEdit->SetWindowTextW(TStr);

	_itot(m_oSeedPoint.y, TStr, 10);
	m_pPointYEdit->SetWindowTextW(TStr);

	_itot((int)m_oNewValue.val[2], TStr, 10);
	m_pValueRed->SetWindowTextW(TStr);
	_itot((int)m_oNewValue.val[1], TStr, 10);
	m_pValueGreen->SetWindowTextW(TStr);
	_itot((int)m_oNewValue.val[0], TStr, 10);
	m_pValueBlue->SetWindowTextW(TStr);

	m_fEnableChange = TRUE;	

}


void COpFloodFill::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpFloodFill::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_oLoDiff, sizeof(m_oLoDiff), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_oHiDiff, sizeof(m_oHiDiff), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_oSeedPoint, sizeof(m_oSeedPoint), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_fFixedRange, sizeof(m_fFixedRange), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_fDiagonal, sizeof(m_fDiagonal), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_oNewValue, sizeof(m_oNewValue), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bFloodFillFiller, sizeof(bFloodFillFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpFloodFill::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_oLoDiff, sizeof(m_oLoDiff), &dwBytesWritten, NULL);

		ReadFile(hFile, &m_oHiDiff, sizeof(m_oHiDiff), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_oSeedPoint, sizeof(m_oSeedPoint), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_fFixedRange, sizeof(m_fFixedRange), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_fDiagonal, sizeof(m_fDiagonal), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_oNewValue, sizeof(m_oNewValue), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bFloodFillFiller, sizeof(bFloodFillFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpFloodFill::NotifyNewDataIndex(int InputIndex)
{
	CvPoint *pPoint = (CvPoint *)LockData(L"FloodFill NotifyNewDataIndex", DATA_POINT, 0);

	if (pPoint)
	{
		CvPoint NewPoint = *pPoint;

		ReleaseData(L"FloodFill NotifyNewDataIndex");
		
		if (m_oSeedPoint.x != NewPoint.x || m_oSeedPoint.y != NewPoint.y)
		{
			m_oSeedPoint = NewPoint;

			UpdateValues();
			InvalidateRect(CRect(SEED_COLOR_X, SEED_COLOR_Y, SEED_COLOR_X+SEED_COLOR_WIDTH, SEED_COLOR_Y+SEED_COLOR_HEIGHT));

		}

	}

	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpFloodFill::ValidateParameters()
{

}

void COpFloodFill::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	CvScalar oLoDiff;
//	CvScalar oHiDiff;

	if (pSlider == m_pLoDiffSlider) 
		m_oLoDiff = cvScalarAll(pSlider->GetPos());

	if (pSlider == m_pHiDiffSlider) 
		m_oHiDiff = cvScalarAll(pSlider->GetPos());

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}


void COpFloodFill::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		TCHAR TStr[20];

		IplImage *pImage = (IplImage *) LockData(L"FloodFillThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			IplImage *pSrc = pImage;
			BOOL fDeallocateWhenDone = FALSE;

			int nFlags = 0;

			if (m_fFixedRange) nFlags |= CV_FLOODFILL_FIXED_RANGE;
			if (m_fDiagonal) nFlags |= 8;
				else nFlags |= 4;

			CvSize Size = cvGetSize(pImage);

			if (m_oSeedPoint.x >= Size.width) { m_oSeedPoint.x = 0; UpdateValues(); }
			if (m_oSeedPoint.y >= Size.height) { m_oSeedPoint.y = 0; UpdateValues(); }

			cvFloodFill(pImage, m_oSeedPoint, m_oNewValue, m_oLoDiff, m_oHiDiff, NULL, nFlags);

			SetData(0, DATA_IMAGE, 0, pImage, fDeallocateWhenDone);
			SendData();

			ReleaseData(L"FloodFillThread");  // We're done with the data so it can change if needed

		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}

void COpFloodFill::OnEditChange()
{
	TCHAR TStr[20];

	if (m_fEnableChange)
	{
		if (m_pPointXEdit) {
			m_pPointXEdit->GetWindowText(TStr, sizeof(TStr));
			m_oSeedPoint.x = _ttol(TStr);
		}
		if (m_pPointYEdit) {
			m_pPointYEdit->GetWindowText(TStr, sizeof(TStr));
			m_oSeedPoint.y = _ttol(TStr);
		}
		if (m_pValueRed) {
			m_pValueRed->GetWindowText(TStr, sizeof(TStr));
			m_oNewValue.val[2] = _ttol(TStr);
		}
		if (m_pValueGreen) {
			m_pValueGreen->GetWindowText(TStr, sizeof(TStr));
			m_oNewValue.val[1] = _ttol(TStr);
		}
		if (m_pValueBlue) {
			m_pValueBlue->GetWindowText(TStr, sizeof(TStr));
			m_oNewValue.val[0] = _ttol(TStr);
		}

			// Update color swatch
		InvalidateRect(CRect(SEED_COLOR_X, SEED_COLOR_Y, SEED_COLOR_X+SEED_COLOR_WIDTH, SEED_COLOR_Y+SEED_COLOR_HEIGHT));

		KillTimer(0);
		SetTimer(0, 1000, NULL);
	}


}

void COpFloodFill::OnTimer(UINT)
{
	KillTimer(0);
	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
}

void COpFloodFill::OnFixedRangeButton()
{
	if (m_pFixedRangeCheck->GetCheck() == BST_CHECKED)
		m_fFixedRange = TRUE;
	else
		m_fFixedRange = FALSE;

	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpFloodFill::OnDiagonalButton()
{
	if (m_pDiagonalCheck->GetCheck() == BST_CHECKED)
		m_fDiagonal = TRUE;
	else
		m_fDiagonal = FALSE;

	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}
