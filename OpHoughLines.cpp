#include "StdAfx.h"
#include "OpHoughLines.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bHoughLinesFiller[32];

#define ID_METHOD_COMBO (101)

#define DEGREES_PER_RADIAN (57.2957)

#define MIN_RHO (1)
#define MAX_RHO (20)
#define MIN_THETA (1)
#define MAX_THETA (355)	// Convert to radians later
#define MIN_THRESHOLD (1)
#define MAX_THRESHOLD (1024)
#define MIN_PARAM	(1)
#define MAX_PARAM	(1024)

BEGIN_MESSAGE_MAP(COpHoughLines, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_METHOD_COMBO, OnMethodChange)
END_MESSAGE_MAP()

COpHoughLines::COpHoughLines(CMainView *pMain) : COpBase(pMain)
{

	m_pMethodCombo = NULL;
	m_aMethodStr[0] = 0;
	m_pRhoSlider = NULL;
	m_pRhoEdit = NULL;
	m_pThetaSlider = NULL;
	m_pThetaEdit = NULL;
	m_pThresholdSlider = NULL;
	m_pThresholdEdit = NULL;
	m_pParam1Slider = NULL;
	m_pParam1Edit = NULL;
	m_pParam2Slider = NULL;
	m_pParam2Edit = NULL;

	m_nMethod = CV_HOUGH_STANDARD;
	m_gRho = 1;
	m_gTheta = 1;  // (CV_PI/180;
	m_nThreshold = 100;
	m_gParam1 = 50;
	m_gParam2 = 10;


	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpHoughLines::~COpHoughLines(void)
{
	if (m_pMethodCombo) delete m_pMethodCombo;
	if (m_pRhoSlider) delete m_pRhoSlider;
	if (m_pRhoEdit) delete m_pRhoEdit;
	if (m_pThetaSlider) delete m_pThetaSlider;
	if (m_pThetaEdit) delete m_pThetaEdit;
	if (m_pThresholdSlider) delete m_pThresholdSlider;
	if (m_pThresholdEdit) delete m_pThresholdEdit;
	if (m_pParam1Slider) delete m_pParam1Slider;
	if (m_pParam1Edit) delete m_pParam1Edit;
	if (m_pParam2Slider) delete m_pParam2Slider;
	if (m_pParam2Edit) delete m_pParam2Edit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpHoughLines::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpHoughLines::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_pMethodCombo = CreateOpCombo(0, ID_METHOD_COMBO);

	CComboBox *pCombo = m_pMethodCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"standard");
		pCombo->SetItemData(Ndx, CV_HOUGH_STANDARD);
		Ndx = pCombo->AddString(L"probabilistic");
		pCombo->SetItemData(Ndx, CV_HOUGH_PROBABILISTIC);
		Ndx = pCombo->AddString(L"multi scale");
		pCombo->SetItemData(Ndx, CV_HOUGH_MULTI_SCALE);
	}

	CreateOpStatic(L"rho", TRUE, 0);
	m_pRhoEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pRhoSlider = CreateOpSlider(0, MIN_RHO, MAX_RHO);

	CreateOpStatic(L"theta", TRUE, 0);
	m_pThetaEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pThetaSlider = CreateOpSlider(0, MIN_THETA, MAX_THETA);

	CreateOpStatic(L"threshold", TRUE, 0);
	m_pThresholdEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pThresholdSlider = CreateOpSlider(0, MIN_PARAM, MAX_PARAM);

	CreateOpStatic(L"param 1", TRUE, 0);
	m_pParam1Edit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pParam1Slider = CreateOpSlider(0, MIN_PARAM, MAX_PARAM);

	CreateOpStatic(L"param 2", TRUE, 0);
	m_pParam2Edit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pParam2Slider = CreateOpSlider(0, MIN_PARAM, MAX_PARAM);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpHoughLines::StartOpThread(void *pData)
{
	COpHoughLines *pBase = (COpHoughLines *) pData;

	pBase->OpThread();

	return 0;
}

void COpHoughLines::UpdateSliders()
{
	m_pRhoSlider->SetPos((int)m_gRho);
	m_pThetaSlider->SetPos((int)m_gTheta);
	m_pThresholdSlider->SetPos(m_nThreshold);
	m_pParam1Slider->SetPos((int)m_gParam1);
	m_pParam2Slider->SetPos((int)m_gParam2);
}

void COpHoughLines::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pMethodCombo->GetCount(); Ndx++)
		if (m_pMethodCombo->GetItemData(Ndx) == m_nMethod) break;

	m_pMethodCombo->SetCurSel(Ndx);
	m_pMethodCombo->GetLBText(Ndx, m_aMethodStr);


	_itot((int)m_gRho, TStr, 10);
	m_pRhoEdit->SetWindowTextW(TStr);
//	_itot((int) (m_gTheta / (2 * CV_PI)), TStr, 10);
	_stprintf(TStr, L"%3.2g", (m_gTheta / DEGREES_PER_RADIAN ));
	m_pThetaEdit->SetWindowTextW(TStr);
	_itot(m_nThreshold, TStr, 10);
	m_pThresholdEdit->SetWindowTextW(TStr);
	_itot((int)m_gParam1, TStr, 10);
	m_pParam1Edit->SetWindowTextW(TStr);
	_itot((int)m_gParam2, TStr, 10);
	m_pParam2Edit->SetWindowTextW(TStr);
}


void COpHoughLines::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpHoughLines::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nMethod, sizeof(m_nMethod), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_gRho, sizeof(m_gRho), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_gTheta, sizeof(m_gTheta), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nThreshold, sizeof(m_nThreshold), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_gParam1, sizeof(m_gParam1), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_gParam2, sizeof(m_gParam2), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bHoughLinesFiller, sizeof(bHoughLinesFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpHoughLines::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nMethod, sizeof(m_nMethod), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_gRho, sizeof(m_gRho), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_gTheta, sizeof(m_gTheta), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nThreshold, sizeof(m_nThreshold), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_gParam1, sizeof(m_gParam1), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_gParam2, sizeof(m_gParam2), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bHoughLinesFiller, sizeof(bHoughLinesFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpHoughLines::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpHoughLines::ValidateParameters()
{
}

void COpHoughLines::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pRhoSlider) 
		m_gRho = pSlider->GetPos();
	if (pSlider == m_pThetaSlider) 
		m_gTheta = pSlider->GetPos();
	if (pSlider == m_pThresholdSlider) 
		m_nThreshold = pSlider->GetPos();
	if (pSlider == m_pParam1Slider) 
		m_gParam1 = pSlider->GetPos();
	if (pSlider == m_pParam2Slider) 
		m_gParam2 = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpHoughLines::OnMethodChange()
{

	int Ndx = m_pMethodCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nMethod = m_pMethodCombo->GetItemData(Ndx);
		m_pMethodCombo->GetLBText(Ndx, m_aMethodStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpHoughLines::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"HoughLinesThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (pImage->nChannels == 1 && (pImage->depth & IPL_DEPTH_8U) == IPL_DEPTH_8U) {	// Validate Src and Dst images, etc

				CvMemStorage* storage = cvCreateMemStorage(0);
				CvSeq* lines = 0;

				lines = cvHoughLines2(pImage, storage, m_nMethod, m_gRho, m_gTheta / DEGREES_PER_RADIAN, m_nThreshold, m_gParam1, m_gParam2);

				SetData(0, DATA_IMAGE, 0, pImage);
				
				if (m_nMethod == CV_HOUGH_PROBABILISTIC) SetData(1, DATA_HOUGH_LINES_CART, 0, lines, TRUE, DATA_STORAGE_MEMSTORAGE, storage);
					else SetData(1, DATA_HOUGH_LINES_POLAR, 0, lines, TRUE, DATA_STORAGE_MEMSTORAGE, storage);
				SendData();

			}
				else TitleError(L"Image must be single-channel, binary image of depth 8");

			ReleaseData(L"HoughLinesThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
