#pragma once
#include "opbase.h"
#include "HighGUI.h"

#define DEFAULT_WIDTH (320)
#define DEFAULT_HEIGHT (240)

class COpImage : public COpBase
{
public:
	COpImage(CMainView *pMain);
	~COpImage(void);

	virtual	PTCHAR GetFriendlyName() { return L"Image"; }
	virtual int	GetDefaultWidth() { return DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	BOOL m_fInSizeArea;
	BOOL m_fSizing;
	CvvImage *m_pImage;
	TCHAR m_aImageFileName[MAX_PATH];
	BOOL m_fShowStats;
	BOOL m_fSettingROI;
	BOOL m_fSettingPoints;
	CPoint m_oROIUpperLeft;
	CPoint m_oROILowerRight;
	CvRect m_oROIRect;
	int	m_nMouseCurX;
	int m_nMouseCurY;
	CvPoint m_oClickPoint;
	POINT_LIST_TYPE m_sPointList;
	BOOL m_fDraggingPoint;
	int m_nDragPointNdx;
	IplImage *m_pRawImage;

	CRITICAL_SECTION m_pImageCrit;

		// Drawing requirements
	BOOL	m_fDrawHoughLines;
	BOOL	m_fDrawHoughCircles;

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual void LoadImage();
	virtual void SaveImage();
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);

	afx_msg void OnPaint();
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
	afx_msg void SetROI();
	afx_msg void ResetROI();
	afx_msg void OnShowStats();
	afx_msg void OnSetPoints();
	afx_msg void OnClearPoints();


	DECLARE_MESSAGE_MAP()
};
