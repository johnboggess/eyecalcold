#include "StdAfx.h"
#include "OpIntegral.h"
#include "OpBase.h"
#include "Cv.h"

#define ID_INTEGRAL_TYPE (101)

BYTE bIntegralFiller[32];

BEGIN_MESSAGE_MAP(COpIntegral, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_INTEGRAL_TYPE, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpIntegral::COpIntegral(CMainView *pMain) : COpBase(pMain)
{
	m_eIntegralType = INTEGRAL_SUM;

	m_pCombo = NULL;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpIntegral::~COpIntegral(void)
{
	if (m_pCombo)delete m_pCombo;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpIntegral::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpIntegral::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_pCombo = CreateOpCombo(0, ID_INTEGRAL_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"Sum");
		pCombo->SetItemData(Ndx, INTEGRAL_SUM);
		Ndx = pCombo->AddString(L"Squared Sum");
		pCombo->SetItemData(Ndx, INTEGRAL_SQSUM);
		Ndx = pCombo->AddString(L"Titled");
		pCombo->SetItemData(Ndx, INTEGRAL_TILTED);
	}

//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpIntegral::StartOpThread(void *pData)
{
	COpIntegral *pBase = (COpIntegral *) pData;

	pBase->OpThread();

	return 0;
}

void COpIntegral::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpIntegral::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pCombo->GetCount(); Ndx++)
		if (m_pCombo->GetItemData(Ndx) == m_eIntegralType) break;

	m_pCombo->SetCurSel(Ndx);
/*
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpIntegral::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpIntegral::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_eIntegralType, sizeof(m_eIntegralType), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bIntegralFiller, sizeof(bIntegralFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpIntegral::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_eIntegralType, sizeof(m_eIntegralType), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bIntegralFiller, sizeof(bIntegralFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpIntegral::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpIntegral::ValidateParameters()
{
}

void COpIntegral::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
/*
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
*/
}

void COpIntegral::OnChange()
{

	int Ndx = m_pCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_eIntegralType = (INTEGRAL_TYPE) m_pCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}

}

void COpIntegral::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpIntegral::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpIntegral::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"IntegralThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			CvSize Size = cvGetSize(pImage);
			Size.height++;
			Size.width++;
			
			IplImage *pSumImage = cvCreateImage(Size, IPL_DEPTH_32F, pImage->nChannels);
			
			IplImage *pSqSumImage = NULL;
			IplImage *pTiltedSumImage = NULL;
			IplImage *pOutputImage = pSumImage;
			
			switch (m_eIntegralType)
			{
				case INTEGRAL_SUM:
					break;
				
				case INTEGRAL_SQSUM:
					pSqSumImage = cvCreateImage(Size, IPL_DEPTH_64F, pImage->nChannels);
					pOutputImage = pSqSumImage;
					
					break;
			
				case INTEGRAL_TILTED:
					pTiltedSumImage = cvCreateImage(Size, IPL_DEPTH_32F, pImage->nChannels);
					pOutputImage = pTiltedSumImage;
					break;
			}			

			if (1) {	// Validate Src and Dst images, etc

				cvIntegral(pImage, pSumImage, pSqSumImage, pTiltedSumImage);

				if (m_eIntegralType != INTEGRAL_SUM) cvReleaseImage(&pSumImage);

				SetData(0, DATA_IMAGE, 0, pOutputImage, TRUE);
				SendData();

			}
				else TitleError(L"");

			ReleaseData(L"IntegralThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
