#include "StdAfx.h"
#include "OpSVSControl.h"
#include "OpBase.h"
#include "Cv.h"
#include "HighGui.h"
#include "SVSSetup.h"

BYTE bSVSControlFiller[32];

#define MIN_DURATION (10)
#define MAX_DURATION (100)
#define MIN_SPEED (1)
#define MAX_SPEED (127)

#define DEFAULT_DURATION (10)
#define DEFAULT_SPEED (80)

#define ID_SVS_CONTROL_BROWSE_BUTTON (101)
#define ID_SVS_CONTROL_FORWARD_BUTTON (102)
#define ID_SVS_CONTROL_STOP_BUTTON (104)
#define ID_SVS_CONTROL_BACK_BUTTON (105)
#define ID_SVS_CONTROL_LEFT_BUTTON (106)
#define ID_SVS_CONTROL_RIGHT_BUTTON (107)
#define ID_SVS_CONTROL_SETUP_BUTTON (108)

BEGIN_MESSAGE_MAP(COpSVSControl, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
	ON_COMMAND(ID_SVS_CONTROL_BROWSE_BUTTON, OnBrowseButton)
	ON_COMMAND(ID_SVS_CONTROL_FORWARD_BUTTON, OnForwardButton)
	ON_COMMAND(ID_SVS_CONTROL_STOP_BUTTON, OnStopButton)
	ON_COMMAND(ID_SVS_CONTROL_BACK_BUTTON, OnBackButton)
	ON_COMMAND(ID_SVS_CONTROL_LEFT_BUTTON, OnLeftButton)
	ON_COMMAND(ID_SVS_CONTROL_RIGHT_BUTTON, OnRightButton)
	ON_COMMAND(ID_SVS_CONTROL_SETUP_BUTTON, OnSetupButton)
END_MESSAGE_MAP()

COpSVSControl::COpSVSControl(CMainView *pMain) : COpBase(pMain)
{

	m_pSetupButton = NULL;
	m_pForwardButton = NULL;
	m_pStopButton = NULL;
	m_pBackButton = NULL;
	m_pLeftButton = NULL;
	m_pRightButton = NULL;
	m_pDurationSlider = NULL;
	m_pDurationEdit = NULL;
	m_pSpeedSlider = NULL;
	m_pSpeedEdit = NULL;

	m_nDuration = DEFAULT_DURATION;
	m_nSpeed = DEFAULT_SPEED;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;
	

}

COpSVSControl::~COpSVSControl(void)
{
	if (m_pSetupButton) delete m_pSetupButton;
	if (m_pForwardButton) delete m_pForwardButton;
	if (m_pStopButton) delete m_pStopButton;
	if (m_pBackButton) delete m_pBackButton;
	if (m_pLeftButton) delete m_pLeftButton;
	if (m_pRightButton) delete m_pRightButton;
	if (m_pDurationSlider) delete m_pDurationSlider;
	if (m_pDurationEdit) delete m_pDurationEdit;
	if (m_pSpeedSlider) delete m_pSpeedSlider;
	if (m_pSpeedEdit) delete m_pSpeedEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpSVSControl::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpSVSControl::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

/*
	m_pBrowseButton = CreateOpButton(L"Browse", 0, 0, ID_SVS_CONTROL_BROWSE_BUTTON);
	m_pFileNameEdit = CreateOpEdit(0, TRUE); 
	CreateOpStatic(L"", TRUE, 0);
*/
	CreateOpStatic(L" ", TRUE, 62);
	m_pForwardButton = CreateOpButton(L"Forward", 70, 0, ID_SVS_CONTROL_FORWARD_BUTTON);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L" ", TRUE, 4);
	m_pLeftButton = CreateOpButton(L"Left", 45, 0, ID_SVS_CONTROL_LEFT_BUTTON);
	CreateOpStatic(L" ", TRUE, 10);
	m_pStopButton = CreateOpButton(L"Stop", 45, 0, ID_SVS_CONTROL_STOP_BUTTON);
	CreateOpStatic(L" ", TRUE, 10);
	m_pRightButton = CreateOpButton(L"Right", 45, 0, ID_SVS_CONTROL_RIGHT_BUTTON);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L" ", TRUE, 50);
	m_pBackButton = CreateOpButton(L"Backward", 70, 0, ID_SVS_CONTROL_BACK_BUTTON);

	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"Duration", TRUE, 0);
	m_pDurationEdit = CreateOpEdit(PARAM_EDIT_WIDTH*2, TRUE);
	m_pDurationSlider = CreateOpSlider(0, MIN_DURATION, MAX_DURATION);
//	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"Speed", TRUE, 0);
	m_pSpeedEdit = CreateOpEdit(PARAM_EDIT_WIDTH*2, TRUE);
	m_pSpeedSlider = CreateOpSlider(0, MIN_SPEED, MAX_SPEED);

	CreateOpStatic(L" ", TRUE, 50);
	m_pSetupButton = CreateOpButton(L"Setup", 70, 0, ID_SVS_CONTROL_SETUP_BUTTON);

	UpdateSliders();
	UpdateValues();

/*
	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);
*/

	return 0;	// Must be zero
}

DWORD WINAPI COpSVSControl::StartOpThread(void *pData)
{
	COpSVSControl *pBase = (COpSVSControl *) pData;

	pBase->OpThread();

	return 0;
}

void COpSVSControl::UpdateSliders()
{
	m_pDurationSlider->SetPos(m_nDuration);
	m_pSpeedSlider->SetPos(m_nSpeed);

}
void COpSVSControl::UpdateValues()
{
	TCHAR TStr[10];


	_itot(m_nDuration, TStr, 10);
	m_pDurationEdit->SetWindowTextW(TStr);

	_itot(m_nSpeed, TStr, 10);
	m_pSpeedEdit->SetWindowTextW(TStr);

/*
	if (m_fPlayFlag) m_pPlayButton->SetWindowTextW(L"Pause");
		else m_pPlayButton->SetWindowTextW(L"Play");

	if (m_aFileName[0]) 
	{
		PTCHAR pChar = m_aFileName + _tcslen(m_aFileName) - 1;
		
		while (pChar > m_aFileName && *pChar && *pChar != '\\') pChar--;
		
		if (*pChar == '\\') pChar++;	// Move to first character of file name
		
		TCHAR FileName[MAX_PATH];
		
		_tcscpy(FileName, pChar);
		
		m_pFileNameEdit->SetWindowTextW(FileName);
	}
	else m_pFileNameEdit->SetWindowTextW(L"");
*/
}


void COpSVSControl::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpSVSControl::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nDuration, sizeof(m_nDuration), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nSpeed, sizeof(m_nSpeed), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bSVSControlFiller, sizeof(bSVSControlFiller), &dwBytesWritten, NULL);
		}

		fRetValue = TRUE;
	}

	return fRetValue;
}

BOOL COpSVSControl::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nDuration, sizeof(m_nDuration), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nSpeed, sizeof(m_nSpeed), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bSVSControlFiller, sizeof(bSVSControlFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}

		fRetValue = TRUE;

	}

	return fRetValue;
}

	// We got data from the given input index
void COpSVSControl::NotifyNewDataIndex(int InputIndex)
{
//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpSVSControl::ValidateParameters()
{
}

void COpSVSControl::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{

	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pDurationSlider) 
		m_nDuration = pSlider->GetPos();

	if (pSlider == m_pSpeedSlider) 
		m_nSpeed = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();


}

void COpSVSControl::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpSVSControl::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpSVSControl::OnBrowseButton()
{
/*
	CFileDialog *pDialog = new CFileDialog(FALSE, L".avi", NULL, 0, L"avi|*.avi||", NULL );

	if (pDialog->DoModal() == IDOK) {
		TCHAR TStr[MAX_PATH];

		_tcscpy(m_aFileName, pDialog->GetPathName());

		UpdateValues();	// Display new file name
	}

	delete pDialog;
*/
}

void COpSVSControl::OnForwardButton()
{
	m_pMain->GetInterface()->MoveRobot(MOVE_FORWARD, m_nSpeed, m_nDuration);
}

void COpSVSControl::OnRightButton()
{
	m_pMain->GetInterface()->MoveRobot(MOVE_RIGHT, 60, 20);
}

void COpSVSControl::OnLeftButton()
{
	m_pMain->GetInterface()->MoveRobot(MOVE_LEFT, 60, 20);
}

void COpSVSControl::OnBackButton()
{
	m_pMain->GetInterface()->MoveRobot(MOVE_BACKWARD, m_nSpeed, m_nDuration);
}

void COpSVSControl::OnStopButton()
{
	m_pMain->GetInterface()->MoveRobot(MOVE_STOP, 0, 0);
}

void COpSVSControl::OnSetupButton()
{
	CSVSSetup *pDialog = new CSVSSetup(m_pMain->GetInterface());
	
	pDialog->Create(IDD_SVS_SETUP_DIALOG);
	pDialog->ShowWindow(SW_SHOW);
}

void COpSVSControl::StopBase()
{
/*
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
*/	
}

void COpSVSControl::OpThread()
{
	
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
	}
}
