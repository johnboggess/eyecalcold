#include "StdAfx.h"
#include "OpLogPolar.h"
#include "OpBase.h"
#include "Cv.h"

#define ID_INVERSE_BUTTON (101)

#define MIN_SCALE (1)
#define MAX_SCALE (500)
#define SCALE_DIVISOR (10)

BYTE bLogPolarFiller[32];

BEGIN_MESSAGE_MAP(COpLogPolar, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_COMMAND(ID_INVERSE_BUTTON, OnInverseButton)

END_MESSAGE_MAP()

COpLogPolar::COpLogPolar(CMainView *pMain) : COpBase(pMain)
{
	m_pScaleSlider = NULL;
	m_pScaleEdit = NULL;
	m_pInverseButton = NULL;

	m_nScale = SCALE_DIVISOR;
	m_nInverse = 0;


	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpLogPolar::~COpLogPolar(void)
{
	if (m_pScaleSlider) delete m_pScaleSlider;
	if (m_pScaleEdit) delete m_pScaleEdit;
	if (m_pInverseButton) delete m_pInverseButton;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpLogPolar::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpLogPolar::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	CreateOpStatic(L"Scale factor", TRUE, 0);
	m_pScaleEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pScaleSlider = CreateOpSlider(0, MIN_SCALE, MAX_SCALE);

	m_pInverseButton = CreateOpButton(L"Inverse", 80, BS_AUTOCHECKBOX, ID_INVERSE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpLogPolar::StartOpThread(void *pData)
{
	COpLogPolar *pBase = (COpLogPolar *) pData;

	pBase->OpThread();

	return 0;
}

void COpLogPolar::UpdateSliders()
{
	m_pScaleSlider->SetPos(m_nScale);

}
void COpLogPolar::UpdateValues()
{
	TCHAR TStr[10];

	_stprintf(TStr, L"%3.2f", ((double) m_nScale / SCALE_DIVISOR));
	m_pScaleEdit->SetWindowTextW(TStr);

}


void COpLogPolar::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpLogPolar::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nScale, sizeof(m_nScale), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nInverse, sizeof(m_nInverse), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bLogPolarFiller, sizeof(bLogPolarFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpLogPolar::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nScale, sizeof(m_nScale), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nInverse, sizeof(m_nInverse), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bLogPolarFiller, sizeof(bLogPolarFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpLogPolar::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpLogPolar::ValidateParameters()
{
}

void COpLogPolar::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pScaleSlider) 
		m_nScale = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpLogPolar::OnInverseButton()
{
	if (m_pInverseButton->GetCheck() == BST_CHECKED)
		m_nInverse = CV_WARP_INVERSE_MAP;
	else
		m_nInverse = 0;

	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpLogPolar::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		CvPoint *pCenterPt = (CvPoint *) LockData(L"LogPolarThread", DATA_POINT, 0);
		
		if (pCenterPt)
		{
			IplImage *pImage = (IplImage *) LockData(L"LogPolarThread", DATA_IMAGE, 0);

			if (pImage) 
			{
				IplImage *pDestImage = cvCreateImage(cvGetSize(pImage), pImage->depth, pImage->nChannels);
			
				if (pDestImage) {	// Validate Src and Dst images, etc
					CvPoint2D32f	center;
					
					center.x = (float) pCenterPt->x;
					center.y = (float) pCenterPt->y;

					int Flags = CV_INTER_LINEAR | m_nInverse;

					cvLogPolar(pImage, pDestImage, center, (double) m_nScale / SCALE_DIVISOR, Flags);

					SetData(0, DATA_IMAGE, 0, pDestImage, TRUE);
					SendData();

				}
					else TitleError(L"Unable to allocate destination image");

				ReleaseData(L"LogPolarThread");  // We're done with the data so it can change if needed
			}
			
			ReleaseData(L"LogPolarThread");
		}
			else TitleError(L"Requires center point");
			
		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
