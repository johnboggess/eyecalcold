#include "StdAfx.h"
#include "OpResize.h"
#include "OpBase.h"
#include "Cv.h"

#define ID_INTERPOLATIONTYPE (100)
#define ID_RESIZE_BUTTON (101)

BYTE bResizeFiller[32];

BEGIN_MESSAGE_MAP(COpResize, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_COMMAND(ID_RESIZE_BUTTON, OnResizeButton)
	ON_CBN_SELCHANGE(ID_INTERPOLATIONTYPE, OnInterpolationChange)
END_MESSAGE_MAP()

COpResize::COpResize(CMainView *pMain) : COpBase(pMain)
{
	m_pWidthEdit = NULL;
	m_pHeightEdit = NULL;
	m_pInterpolationCombo = NULL;
	m_aInterpolationStr[0] = 0;

	m_nWidth = 0;
	m_nHeight = 0;
	m_nInterpolation = CV_INTER_LINEAR;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpResize::~COpResize(void)
{
	if (m_pWidthEdit) delete m_pWidthEdit;
	if (m_pHeightEdit) delete m_pHeightEdit;
	if (m_pInterpolationCombo) delete m_pInterpolationCombo;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpResize::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpResize::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_pInterpolationCombo = CreateOpCombo(0, ID_INTERPOLATIONTYPE);

	if (m_pInterpolationCombo)
	{
		int Ndx = m_pInterpolationCombo->AddString(L"nearest neighbor");
		m_pInterpolationCombo->SetItemData(Ndx, CV_INTER_NN);
		Ndx = m_pInterpolationCombo->AddString(L"bilinear");
		m_pInterpolationCombo->SetItemData(Ndx, CV_INTER_LINEAR);
		Ndx = m_pInterpolationCombo->AddString(L"pixel area re-sampling");
		m_pInterpolationCombo->SetItemData(Ndx, CV_INTER_AREA);
		Ndx = m_pInterpolationCombo->AddString(L"inter cubic");
		m_pInterpolationCombo->SetItemData(Ndx, CV_INTER_CUBIC);
	}

	CreateOpStatic(L"width", FALSE, 50);
	m_pWidthEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3*2, FALSE);
	CreateOpStatic(L"", FALSE, 0);
	CreateOpStatic(L"", FALSE, 0);
	CreateOpStatic(L"", FALSE, 0);

	CreateOpStatic(L"height", FALSE, 50);
	m_pHeightEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3*2, FALSE);
	CreateOpStatic(L"", FALSE, 0);
	CreateOpStatic(L"", FALSE, 0);
	CreateOpStatic(L"", FALSE, 0);

	CreateOpButton(L"Resize", 100, BS_DEFPUSHBUTTON, ID_RESIZE_BUTTON);
	
	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpResize::StartOpThread(void *pData)
{
	COpResize *pBase = (COpResize *) pData;

	pBase->OpThread();

	return 0;
}

void COpResize::UpdateSliders()
{

}
void COpResize::UpdateValues()
{
	TCHAR TStr[10];

	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pInterpolationCombo->GetCount(); Ndx++)
		if (m_pInterpolationCombo->GetItemData(Ndx) == m_nInterpolation) break;

	m_pInterpolationCombo->SetCurSel(Ndx);
	m_pInterpolationCombo->GetLBText(Ndx, m_aInterpolationStr);

	_itot(m_nWidth, TStr, 10);
	m_pWidthEdit->SetWindowTextW(TStr);
	_itot(m_nHeight, TStr, 10);
	m_pHeightEdit->SetWindowTextW(TStr);

}


void COpResize::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpResize::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nWidth, sizeof(m_nWidth), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nHeight, sizeof(m_nHeight), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nInterpolation, sizeof(m_nInterpolation), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bResizeFiller, sizeof(bResizeFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpResize::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nWidth, sizeof(m_nWidth), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nHeight, sizeof(m_nHeight), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nInterpolation, sizeof(m_nInterpolation), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bResizeFiller, sizeof(bResizeFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpResize::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpResize::ValidateParameters()
{
}

void COpResize::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
}

void COpResize::OnInterpolationChange()
{
	
	int Ndx = m_pInterpolationCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nInterpolation = (int) m_pInterpolationCombo->GetItemData(Ndx);
		m_pInterpolationCombo->GetLBText(Ndx, m_aInterpolationStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpResize::OnResizeButton()
{
	TCHAR TStr[20];
	m_pWidthEdit->GetWindowText(TStr, 20);
	m_nWidth = _ttoi(TStr);

	m_pHeightEdit->GetWindowText(TStr, 20);
	m_nHeight = _ttoi(TStr);
	
	ValidateParameters();
	
	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
}

void COpResize::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"ResizeThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (m_nWidth && m_nHeight && (m_nWidth != pImage->width || m_nHeight != pImage->height))
			{
//				IplImage *pConvertImage = cvCreateImage(cvSize(pImage->width, pImage->height), IPL_DEPTH_16U, pImage->nChannels);
//cvScale(pImage, pConvertImage, 255);

				IplImage *pNewImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), pImage->depth, pImage->nChannels);

				if (pNewImage)
				{
					cvResize(pImage, pNewImage, m_nInterpolation);
					SetData(0, DATA_IMAGE, 0, pNewImage, TRUE);
					SendData();
				}

				TitleError(L"");
			}
				else TitleError(L"Src and Dest images are the same size");
				
				ReleaseData(L"ResizeThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
