#pragma once
#include "opbase.h"
#include "Cv.h"

#define MatchTemplate_DEFAULT_WIDTH (200)
#define MatchTemplate_DEFAULT_HEIGHT (80)

class COpMatchTemplate : public COpBase
{
public:
	COpMatchTemplate(CMainView *pMain);
	~COpMatchTemplate(void);

	virtual	PTCHAR GetFriendlyName() { return L"MatchTemplate"; }
	virtual int	GetDefaultWidth() { return MatchTemplate_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return MatchTemplate_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pMatchMethodCombo;

		// Save data
	int	m_nMatchMethod;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
