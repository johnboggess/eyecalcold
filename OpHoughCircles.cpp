#include "StdAfx.h"
#include "OpHoughCircles.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bHoughCirclesFiller[32];

#define ID_METHOD_COMBO (101)

#define DEGREES_PER_RADIAN (57.2957)

#define MIN_DP (1)
#define MAX_DP (10)
#define MIN_DIST (1)
#define MAX_DIST (1024)	
#define MIN_PARAM	(1)
#define MAX_PARAM	(1024)
#define MIN_RADIUS	(0)
#define MAX_RADIUS	(1024)

BEGIN_MESSAGE_MAP(COpHoughCircles, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_METHOD_COMBO, OnMethodChange)
END_MESSAGE_MAP()

COpHoughCircles::COpHoughCircles(CMainView *pMain) : COpBase(pMain)
{

	m_pDpSlider = NULL;
	m_pDpEdit = NULL;
	m_pMinDistSlider = NULL;
	m_pMinDistEdit = NULL;
	m_pParam1Slider = NULL;
	m_pParam1Edit = NULL;
	m_pParam2Slider = NULL;
	m_pParam2Edit = NULL;
	m_pMinRadiusSlider = NULL;
	m_pMinRadiusEdit = NULL;
	m_pMaxRadiusSlider = NULL;
	m_pMaxRadiusEdit = NULL;

		// Save data
	m_gDp = 1;
	m_gMinDist = 1;
	m_gParam1 = 100;
	m_gParam2 = 300;
	m_nMinRadius = 0;
	m_nMaxRadius = 0;


	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpHoughCircles::~COpHoughCircles(void)
{
	if (m_pDpSlider) delete m_pDpSlider;
	if (m_pDpEdit) delete m_pDpEdit;
	if (m_pMinDistSlider) delete m_pMinDistSlider;
	if (m_pMinDistEdit) delete m_pMinDistEdit;
	if (m_pParam1Slider) delete m_pParam1Slider;
	if (m_pParam1Edit) delete m_pParam1Edit;
	if (m_pParam2Slider) delete m_pParam2Slider;
	if (m_pParam2Edit) delete m_pParam2Edit;
	if (m_pMinRadiusSlider) delete m_pMinRadiusSlider;
	if (m_pMinRadiusEdit) delete m_pMinRadiusEdit;
	if (m_pMaxRadiusSlider) delete m_pMaxRadiusSlider;
	if (m_pMaxRadiusEdit) delete m_pMaxRadiusEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpHoughCircles::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpHoughCircles::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	CreateOpStatic(L"dp", TRUE, 0);
	m_pDpEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pDpSlider = CreateOpSlider(0, MIN_DP, MAX_DP);

	CreateOpStatic(L"min dist", TRUE, 0);
	m_pMinDistEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pMinDistSlider = CreateOpSlider(0, MIN_DIST, MAX_DIST);

	CreateOpStatic(L"param 1", TRUE, 0);
	m_pParam1Edit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pParam1Slider = CreateOpSlider(0, MIN_PARAM, MAX_PARAM);

	CreateOpStatic(L"param 2", TRUE, 0);
	m_pParam2Edit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pParam2Slider = CreateOpSlider(0, MIN_PARAM, MAX_PARAM);

	CreateOpStatic(L"min radius", TRUE, 0);
	m_pMinRadiusEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pMinRadiusSlider = CreateOpSlider(0, MIN_RADIUS, MAX_RADIUS);

	CreateOpStatic(L"max radius", TRUE, 0);
	m_pMaxRadiusEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pMaxRadiusSlider = CreateOpSlider(0, MIN_RADIUS, MAX_RADIUS);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpHoughCircles::StartOpThread(void *pData)
{
	COpHoughCircles *pBase = (COpHoughCircles *) pData;

	pBase->OpThread();

	return 0;
}

void COpHoughCircles::UpdateSliders()
{
	m_pDpSlider->SetPos((int)m_gDp);
	m_pMinDistSlider->SetPos((int)m_gMinDist);
	m_pParam1Slider->SetPos((int)m_gParam1);
	m_pParam2Slider->SetPos((int)m_gParam2);
	m_pMinRadiusSlider->SetPos((int)m_nMinRadius);
	m_pMaxRadiusSlider->SetPos((int)m_nMaxRadius);
}

void COpHoughCircles::UpdateValues()
{
	TCHAR TStr[10];

	_itot((int)m_gDp, TStr, 10);
	m_pDpEdit->SetWindowTextW(TStr);
	_itot((int) m_gMinDist, TStr, 10);
	m_pMinDistEdit->SetWindowTextW(TStr);
	_itot((int)m_gParam1, TStr, 10);
	m_pParam1Edit->SetWindowTextW(TStr);
	_itot((int)m_gParam2, TStr, 10);
	m_pParam2Edit->SetWindowTextW(TStr);
	_itot((int)m_nMinRadius, TStr, 10);
	m_pMinRadiusEdit->SetWindowTextW(TStr);
	_itot((int)m_nMaxRadius, TStr, 10);
	m_pMaxRadiusEdit->SetWindowTextW(TStr);
}


void COpHoughCircles::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpHoughCircles::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_gDp, sizeof(m_gDp), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_gMinDist, sizeof(m_gMinDist), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_gParam1, sizeof(m_gParam1), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_gParam2, sizeof(m_gParam2), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nMinRadius, sizeof(m_nMinRadius), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nMaxRadius, sizeof(m_nMaxRadius), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bHoughCirclesFiller, sizeof(bHoughCirclesFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpHoughCircles::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_gDp, sizeof(m_gDp), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_gMinDist, sizeof(m_gMinDist), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_gParam1, sizeof(m_gParam1), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_gParam2, sizeof(m_gParam2), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nMinRadius, sizeof(m_nMinRadius), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nMaxRadius, sizeof(m_nMaxRadius), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bHoughCirclesFiller, sizeof(bHoughCirclesFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpHoughCircles::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpHoughCircles::ValidateParameters()
{
}

void COpHoughCircles::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pDpSlider) 
		m_gDp = pSlider->GetPos();
	if (pSlider == m_pMinDistSlider) 
		m_gMinDist = pSlider->GetPos();
	if (pSlider == m_pParam1Slider) 
		m_gParam1 = pSlider->GetPos();
	if (pSlider == m_pParam2Slider) 
		m_gParam2 = pSlider->GetPos();
	if (pSlider == m_pMinRadiusSlider) 
		m_nMinRadius = pSlider->GetPos();
	if (pSlider == m_pMaxRadiusSlider) 
		m_nMaxRadius = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpHoughCircles::OnMethodChange()
{
}

void COpHoughCircles::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"HoughCirclesThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (pImage->nChannels == 1 && (pImage->depth & IPL_DEPTH_8U) == IPL_DEPTH_8U) {	// Validate Src and Dst images, etc

				CvMemStorage* storage = cvCreateMemStorage(0);
				CvSeq* circles = 0;

				circles = cvHoughCircles(pImage, storage, CV_HOUGH_GRADIENT, m_gDp, m_gMinDist, m_gParam1, m_gParam2, m_nMinRadius, m_nMaxRadius);

				SetData(0, DATA_IMAGE, 0, pImage);
				SetData(1, DATA_HOUGH_CIRCLES, 0, circles, TRUE, DATA_STORAGE_MEMSTORAGE, storage);
				SendData();

			}
				else TitleError(L"Image must be single-channel, binary image of depth 8");

			ReleaseData(L"HoughCirclesThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
