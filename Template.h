#pragma once
#include "opbase.h"
#include "Cv.h"

#define TEMPLATE_DEFAULT_WIDTH (200)
#define TEMPLATE_DEFAULT_HEIGHT (220)

class COpTemplate : public COpBase
{
public:
	COpTemplate(CMainView *pMain);
	~COpTemplate(void);

	virtual	PTCHAR GetFriendlyName() { return L"Template"; }
	virtual int	GetDefaultWidth() { return TEMPLATE_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return TEMPLATE_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pSlider;
	CComboBox	*m_pCombo;
	CEdit		*m_pEdit;
	TCHAR		m_aStr[20];
	CButton		*m_pButton;

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
