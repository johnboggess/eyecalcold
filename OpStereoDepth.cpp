#include "StdAfx.h"
#include "OpStereoDepth.h"
#include "OpBase.h"
#include "Cv.h"
#include "Highgui.h"

BYTE bStereoDepthFiller[32];

#define DEFAULT_FILTER_SIZE (33)
#define DEFAULT_PREFILTER_CAP (33)
#define DEFAULT_SAD_WINDOW (33)
#define DEFAULT_MIN_DISPARITY (-64)
#define DEFAULT_NUM_DISPARITIES (256)
#define DEFAULT_TEXTURE_THRESHOLD (10)
#define DEFAULT_UNIQUENESS_RATIO (15)


BEGIN_MESSAGE_MAP(COpStereoDepth, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpStereoDepth::COpStereoDepth(CMainView *pMain) : COpBase(pMain)
{

	m_npreFilterSize=DEFAULT_FILTER_SIZE;
	m_npreFilterCap=DEFAULT_PREFILTER_CAP;
	m_nSADWindowSize=DEFAULT_SAD_WINDOW;
	m_nminDisparity=DEFAULT_MIN_DISPARITY;
	m_nnumberOfDisparities=DEFAULT_NUM_DISPARITIES;
	m_ntextureThreshold=DEFAULT_TEXTURE_THRESHOLD;
	m_nuniquenessRatio=DEFAULT_UNIQUENESS_RATIO;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpStereoDepth::~COpStereoDepth(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpStereoDepth::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpStereoDepth::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
	
	CreateOpStatic(L"prefilter size", TRUE, 0);
	m_ppreFilterSizeEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_ppreFilterSize = CreateOpSlider(0, 5, 255);

	CreateOpStatic(L"prefilter cap", TRUE, 0);
	m_ppreFilterCapEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_ppreFilterCap = CreateOpSlider(0, 1, 63);

	CreateOpStatic(L"SAD window size", TRUE, 0);
	m_pSADWindowSizeEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pSADWindowSize = CreateOpSlider(0, 5, 100);

	CreateOpStatic(L"min disparity", TRUE, 0);
	m_pminDisparityEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pminDisparity = CreateOpSlider(0, -256, 2);

	CreateOpStatic(L"num disparities", TRUE, 0);
	m_pnumberOfDisparitiesEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pnumberOfDisparities = CreateOpSlider(0, 16, 512);

	CreateOpStatic(L"texture threshold", TRUE, 0);
	m_ptextureThresholdEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_ptextureThreshold = CreateOpSlider(0, 0, 50);

	CreateOpStatic(L"uniqueness ratio", TRUE, 0);
	m_puniquenessRatioEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_puniquenessRatio = CreateOpSlider(0, 0, 50);


//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpStereoDepth::StartOpThread(void *pData)
{
	COpStereoDepth *pBase = (COpStereoDepth *) pData;

	pBase->OpThread();

	return 0;
}

void COpStereoDepth::UpdateSliders()
{

	m_ppreFilterSize->SetPos(m_npreFilterSize);
	m_ppreFilterCap->SetPos(m_npreFilterCap);
	m_pSADWindowSize->SetPos(m_nSADWindowSize);
	m_pminDisparity->SetPos(m_nminDisparity);
	m_pnumberOfDisparities->SetPos(m_nnumberOfDisparities);
	m_ptextureThreshold->SetPos(m_ntextureThreshold);
	m_puniquenessRatio->SetPos(m_nuniquenessRatio);

}
void COpStereoDepth::UpdateValues()
{
	TCHAR TStr[10];


	_itot(m_npreFilterSize, TStr, 10);
	m_ppreFilterSizeEdit->SetWindowTextW(TStr);

	_itot(m_npreFilterCap, TStr, 10);
	m_ppreFilterCapEdit->SetWindowTextW(TStr);

	_itot(m_nSADWindowSize, TStr, 10);
	m_pSADWindowSizeEdit->SetWindowTextW(TStr);

	_itot(m_nminDisparity, TStr, 10);
	m_pminDisparityEdit->SetWindowTextW(TStr);

	_itot(m_nnumberOfDisparities, TStr, 10);
	m_pnumberOfDisparitiesEdit->SetWindowTextW(TStr);

	_itot(m_ntextureThreshold, TStr, 10);
	m_ptextureThresholdEdit->SetWindowTextW(TStr);

	_itot(m_nuniquenessRatio, TStr, 10);
	m_puniquenessRatioEdit->SetWindowTextW(TStr);


}


void COpStereoDepth::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpStereoDepth::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);
/*
		if (WriteFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bStereoDepthFiller, sizeof(bStereoDepthFiller), &dwBytesWritten, NULL);
		}
*/
fRetValue = TRUE;

	}

	return fRetValue;
}

BOOL COpStereoDepth::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);
/*
		if (ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bStereoDepthFiller, sizeof(bStereoDepthFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
*/
fRetValue = TRUE;
	}

	return fRetValue;
}

	// We got data from the given input index
void COpStereoDepth::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpStereoDepth::ValidateParameters()
{
}

void COpStereoDepth::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_ppreFilterSize) 
		m_npreFilterSize = pSlider->GetPos();

	if (pSlider == m_ppreFilterCap) 
		m_npreFilterCap = pSlider->GetPos();

	if (pSlider == m_pSADWindowSize) 
		m_nSADWindowSize = pSlider->GetPos();

	if (pSlider == m_pminDisparity) 
		m_nminDisparity = pSlider->GetPos();

	if (pSlider == m_pnumberOfDisparities) 
		m_nnumberOfDisparities = pSlider->GetPos();

	if (pSlider == m_ptextureThreshold) 
		m_ntextureThreshold = pSlider->GetPos();

	if (pSlider == m_puniquenessRatio) 
		m_nuniquenessRatio = pSlider->GetPos();

	if (m_nSADWindowSize % 2 == 0) m_nSADWindowSize++;	// Must be odd

	if (m_npreFilterSize % 2 == 0) m_npreFilterSize++;	// Must be odd

	if (m_nnumberOfDisparities % 16 != 0) 
		m_nnumberOfDisparities += 16-((m_nnumberOfDisparities % 16));

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpStereoDepth::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpStereoDepth::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpStereoDepth::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpStereoDepth::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImageLOrig = (IplImage *) LockData(L"StereoDepthThread", DATA_IMAGE, 0);
		IplImage *pImageROrig = (IplImage *) LockData(L"StereoDepthThread", DATA_IMAGE, 1);
		CALIBRATION_TYPE *pCalib = (CALIBRATION_TYPE *) LockData(L"StereoRectifyThread", DATA_CALIBRATION, 0);

		if (pImageLOrig && pImageROrig && pCalib) 
		{

			IplImage *pImageL = cvCreateImage(cvGetSize(pImageLOrig), pImageLOrig->depth, 1);
			cvCvtColor(pImageLOrig, pImageL, CV_BGR2GRAY);
			IplImage *pImageR = cvCreateImage(cvGetSize(pImageROrig), pImageROrig->depth, 1);
			cvCvtColor(pImageROrig, pImageR, CV_BGR2GRAY);

	        CvStereoBMState *BMState = cvCreateStereoBMState();
			BMState->preFilterSize=m_npreFilterSize;
			BMState->preFilterCap=m_npreFilterCap;
			BMState->SADWindowSize=m_nSADWindowSize;
			BMState->minDisparity=m_nminDisparity;
			BMState->numberOfDisparities=m_nnumberOfDisparities;
			BMState->textureThreshold=m_ntextureThreshold;
			BMState->uniquenessRatio=m_nuniquenessRatio;

			IplImage *pDepthImage = cvCreateImage(cvGetSize(pImageL), IPL_DEPTH_16S, 3);
			IplImage *pImageLR = cvCreateImage(cvGetSize(pImageL), pImageL->depth, pImageL->nChannels);
			IplImage *pImageRR = cvCreateImage(cvGetSize(pImageR), pImageR->depth, pImageR->nChannels);
			IplImage *pDisp = cvCreateImage(cvGetSize(pImageL), IPL_DEPTH_16S, 1);
			IplImage *pvDisp = cvCreateImage(cvGetSize(pImageL), IPL_DEPTH_8U, 1);
//        CvMat* pDisp = cvCreateMat( pImageL->height, pImageL->width, CV_16S );
//        CvMat* pvDisp = cvCreateMat( pImageL->height, pImageL->width, CV_8U );

			CvMat *mx1 = cvCreateMat(pImageL->height, pImageL->width, CV_32F);
			CvMat *my1 = cvCreateMat(pImageL->height, pImageL->width, CV_32F);
			CvMat *mx2 = cvCreateMat(pImageR->height, pImageR->width, CV_32F);
			CvMat *my2 = cvCreateMat(pImageR->height, pImageR->width, CV_32F);
            CvMat _M1 = cvMat(3, 3, CV_64F, pCalib->CamMatrix1);
            CvMat _M2 = cvMat(3, 3, CV_64F, pCalib->CamMatrix2);
            CvMat _D1 = cvMat(1, 5, CV_64F, pCalib->DistCoeff1);
            CvMat _D2 = cvMat(1, 5, CV_64F, pCalib->DistCoeff2);
            CvMat _R = cvMat(3, 3, CV_64F, pCalib->Rotation);
            CvMat _T = cvMat(3, 1, CV_64F, pCalib->Translation);
			CvMat _R1 = cvMat(3, 3, CV_64F, pCalib->Rectify1);
			CvMat _R2 = cvMat(3, 3, CV_64F, pCalib->Rectify2);
            CvMat _P1 = cvMat(3, 4, CV_64F, pCalib->Project1);
            CvMat _P2 = cvMat(3, 4, CV_64F, pCalib->Project2);
            CvMat _Q = cvMat(4, 4, CV_64F, pCalib->Disparity);

			// Bouguet's method			
			cvInitUndistortRectifyMap(&_M1, &_D1, &_R1, &_P1, mx1, my1);
			cvInitUndistortRectifyMap(&_M2, &_D2, &_R2, &_P2, mx2, my2);

			// Hartley's method			
			//	cvInitUndistortRectifyMap(&_M1, &_D1, &_R1, &_M1, mx1, my1);
			//	cvInitUndistortRectifyMap(&_M2, &_D2, &_R2, &_M2, mx2, my2);

				// Rectify images
			cvRemap(pImageL, pImageLR, mx1, my1);
			cvRemap(pImageR, pImageRR, mx2, my2);

            cvFindStereoCorrespondenceBM( pImageLR, pImageRR, pDisp, BMState);
            cvNormalize( pDisp, pvDisp, 0, 256, CV_MINMAX );

//md%		cvReprojectImageTo3D(pvDisp, pDepthImage, &_Q);
			cvReprojectImageTo3D(pDisp, pDepthImage, &_Q);


			cvReleaseStereoBMState(&BMState);
			cvReleaseMat( &mx1 );
			cvReleaseMat( &my1 );
			cvReleaseMat( &mx2 );
			cvReleaseMat( &my2 );
			cvReleaseImage( &pImageL);
			cvReleaseImage( &pImageR);
			cvReleaseImage( &pImageLR );
			cvReleaseImage( &pImageRR );
			cvReleaseImage( &pDisp );
			cvReleaseImage( &pvDisp );

			IplImage *pDepthImage2 = cvCreateImage(cvGetSize(pDepthImage), IPL_DEPTH_16S, 1);
			
//md%
//cvNormalize(pDepthImage, pDepthImage2, 0, 32000, CV_MINMAX);

			cvSplit(pDepthImage, NULL, NULL, pDepthImage2, NULL);



			cvReleaseImage(&pDepthImage);

			SetData(0, DATA_IMAGE, 0, pDepthImage2, TRUE);
//			SetData(0, DATA_IMAGE, 0, pvDisp, TRUE);
			SendData();

			ReleaseData(L"StereoDepthThread");  // We're done with the data so it can change if needed
			ReleaseData(L"StereoDepthThread");  // We're done with the data so it can change if needed
			ReleaseData(L"StereoDepthThread");  // We're done with the data so it can change if needed
		}
			else
		{
			if (pImageLOrig) ReleaseData(L"StereoDepthThread");  // We're done with the data so it can change if needed
			if (pImageROrig) ReleaseData(L"StereoDepthThread");  // We're done with the data so it can change if needed
			if (pCalib) ReleaseData(L"StereoDepthThread");  // We're done with the data so it can change if needed
			
			TitleError(L"Need left and right images and calib");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
