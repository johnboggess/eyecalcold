#pragma once
#include "opbase.h"
#include "Cv.h"

#define Integral_DEFAULT_WIDTH (180)
#define Integral_DEFAULT_HEIGHT (80)

typedef enum { INTEGRAL_SUM, INTEGRAL_SQSUM, INTEGRAL_TILTED } INTEGRAL_TYPE;

class COpIntegral : public COpBase
{
public:
	COpIntegral(CMainView *pMain);
	~COpIntegral(void);

	virtual	PTCHAR GetFriendlyName() { return L"Integral"; }
	virtual int	GetDefaultWidth() { return Integral_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Integral_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pCombo;

		// Save data
	INTEGRAL_TYPE	m_eIntegralType;
	
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
