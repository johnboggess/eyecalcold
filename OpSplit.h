#pragma once
#include "opbase.h"
#include "Cv.h"

#define Split_DEFAULT_WIDTH (140)
#define Split_DEFAULT_HEIGHT (60)

class COpSplit : public COpBase
{
public:
	COpSplit(CMainView *pMain);
	~COpSplit(void);

	virtual	PTCHAR GetFriendlyName() { return L"Split"; }
	virtual int	GetDefaultWidth() { return Split_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Split_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CComboBox	*m_pSplitChannelCombo;
	//TCHAR		m_aSplitChannelStr[20];

		// Save data
	int			m_nSplitChannel;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();
	void StopBase();

	afx_msg void SplitChannelChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
