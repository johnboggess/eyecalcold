#pragma once
#include "opbase.h"
#include "Cv.h"

#define DCT_DEFAULT_WIDTH (120)
#define DCT_DEFAULT_HEIGHT (80)

class COpDCT : public COpBase
{
public:
	COpDCT(CMainView *pMain);
	~COpDCT(void);

	virtual	PTCHAR GetFriendlyName() { return L"DCT"; }
	virtual int	GetDefaultWidth() { return DCT_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return DCT_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pCombo;

		// Save data
	BOOL	m_fForward;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
