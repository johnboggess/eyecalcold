#include "StdAfx.h"
#include "OpPyramid.h"
#include "OpBase.h"
#include "Cv.h"

#define ID_PYRAMID_TYPE (100)

#define MAX_LEVELS (8)

BYTE bPyramidFiller[32];

BEGIN_MESSAGE_MAP(COpPyramid, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_PYRAMID_TYPE, OnPyramidChange)
END_MESSAGE_MAP()

COpPyramid::COpPyramid(CMainView *pMain) : COpBase(pMain)
{
	m_pUpDownCombo = NULL;
	m_aUpDownStr[0] = 0;
	m_pLevelsSlider = NULL;
	m_pLevelsEdit = NULL;

	m_fPyrUp = TRUE;
	m_nLevels = 1;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpPyramid::~COpPyramid(void)
{
	if (m_pUpDownCombo)delete m_pUpDownCombo;
	if (m_pLevelsSlider) delete m_pLevelsSlider;
	if (m_pLevelsEdit) delete m_pLevelsEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpPyramid::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpPyramid::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_pUpDownCombo = CreateOpCombo(0, ID_PYRAMID_TYPE);

	if (m_pUpDownCombo)
	{
		int Ndx = m_pUpDownCombo->AddString(L"PyrUp");
		m_pUpDownCombo->SetItemData(Ndx, 1);
		Ndx = m_pUpDownCombo->AddString(L"PryDown");
		m_pUpDownCombo->SetItemData(Ndx, 0);
	}

	CreateOpStatic(L"levels", TRUE, 0);
	m_pLevelsEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pLevelsSlider = CreateOpSlider(0, 1, MAX_LEVELS);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpPyramid::StartOpThread(void *pData)
{
	COpPyramid *pBase = (COpPyramid *) pData;

	pBase->OpThread();

	return 0;
}

void COpPyramid::UpdateSliders()
{
	m_pLevelsSlider->SetPos(m_nLevels);

}
void COpPyramid::UpdateValues()
{
	TCHAR TStr[10];

	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pUpDownCombo->GetCount(); Ndx++)
		if (m_pUpDownCombo->GetItemData(Ndx) == m_fPyrUp) break;

	m_pUpDownCombo->SetCurSel(Ndx);
	m_pUpDownCombo->GetLBText(Ndx, m_aUpDownStr);

	_ltot(m_nLevels, TStr, 10);
	m_pLevelsEdit->SetWindowText(TStr);
}


void COpPyramid::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpPyramid::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_fPyrUp, sizeof(m_fPyrUp), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nLevels, sizeof(m_nLevels), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bPyramidFiller, sizeof(bPyramidFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpPyramid::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_fPyrUp, sizeof(m_fPyrUp), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nLevels, sizeof(m_nLevels), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bPyramidFiller, sizeof(bPyramidFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpPyramid::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpPyramid::ValidateParameters()
{
	if (m_fPyrUp && m_nLevels > 4) 
	{
		m_nLevels = 4;
		
		UpdateValues();
	}
}

void COpPyramid::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pLevelsSlider) 
	{
		m_nLevels = pSlider->GetPos();

	}

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpPyramid::OnPyramidChange()
{
	
	int Ndx = m_pUpDownCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_fPyrUp = (BOOL) m_pUpDownCombo->GetItemData(Ndx);
		m_pUpDownCombo->GetLBText(Ndx, m_aUpDownStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpPyramid::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"PyramidThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (1) {	// Validate Src and Dst images, etc
				IplImage *pDest = NULL;

				for (int i = 0; i < m_nLevels; i++)
				{
					int nWidth = pImage->width;
					int nHeight = pImage->height;

					if (m_fPyrUp) { nWidth *= 2; nHeight *= 2; }
						else { nWidth /= 2; nHeight /= 2; }
				
					if ((m_fPyrUp && std::abs(nWidth - pImage->width*2) == nWidth % 2 &&
							std::abs(nHeight - pImage->height*2) == nHeight % 2)
						|| (!m_fPyrUp && std::abs(nWidth*2 - pImage->width) <= 2 &&
							std::abs(nHeight*2 - pImage->height) <= 2))
					{
						pDest = cvCreateImage(cvSize(nWidth, nHeight), pImage->depth, pImage->nChannels);

						if (pDest)
						{
							if (m_fPyrUp)
								cvPyrUp(pImage, pDest);
							else
								cvPyrDown(pImage, pDest);

							if (i != 0) cvReleaseImage(&pImage);
							
							pImage = pDest;
						}
						
					}
						else TitleError(L"Src image is incorrect width or height");

				}

				if (pDest)
				{
					SetData(0, DATA_IMAGE, 0, pDest, TRUE);
					SendData();
				}
			}
				else TitleError(L"");

			ReleaseData(L"PyramidThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
