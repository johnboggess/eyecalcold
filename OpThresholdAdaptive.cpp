#include "StdAfx.h"
#include "OpThresholdAdaptive.h"
#include "Cv.h"

#define ID_AdaptiveTYPE (104)
#define ID_ThresholdTYPE (105)

BYTE bThresholdAdaptiveFiller[24];

BEGIN_MESSAGE_MAP(COpThresholdAdaptive, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_AdaptiveTYPE, AdaptiveTypeChange)
	ON_CBN_SELCHANGE(ID_ThresholdTYPE, ThresholdTypeChange)

END_MESSAGE_MAP()

COpThresholdAdaptive::COpThresholdAdaptive(CMainView *pMain) : COpBase(pMain)
{
	m_pThresholdTypeCombo = NULL;
	m_pAdaptiveTypeCombo = NULL;
	m_pMaxValueSlider = NULL;
	m_pMaxValueEdit = NULL;
	m_pBlockSizeSlider = NULL;
	m_pBlockSizeEdit = NULL;
	m_pSubtractionConstantSlider = NULL;
	m_pSubtractionConstantEdit = NULL;
	m_aThresholdTypeStr[0] = 0;
	m_aAdaptiveTypeStr[0] = 0;

	m_nThresholdType = CV_THRESH_BINARY;
	m_nAdaptiveMethod = CV_ADAPTIVE_THRESH_MEAN_C;
	m_nMaxValue = 255;
	m_nBlockSize = 3;
	m_nSubtractionConstant = 5;

	m_hThread = NULL;

}

COpThresholdAdaptive::~COpThresholdAdaptive(void)
{
	if (m_pThresholdTypeCombo) delete m_pThresholdTypeCombo;
	if (m_pAdaptiveTypeCombo) delete m_pAdaptiveTypeCombo;
	if (m_pMaxValueSlider) delete m_pMaxValueSlider;
	if (m_pMaxValueEdit) delete m_pMaxValueEdit;
	if (m_pBlockSizeSlider) delete m_pBlockSizeSlider;
	if (m_pBlockSizeEdit) delete m_pBlockSizeEdit;
	if (m_pSubtractionConstantSlider) delete m_pSubtractionConstantSlider;
	if (m_pSubtractionConstantEdit) delete m_pSubtractionConstantEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpThresholdAdaptive::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aThresholdAdaptiveTypeStr, _tcslen(m_aThresholdAdaptiveTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpThresholdAdaptive::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_pThresholdTypeCombo = CreateOpCombo(0, ID_ThresholdTYPE);

	if (m_pThresholdTypeCombo)
	{
		int Ndx = m_pThresholdTypeCombo->AddString(L"Binary");
		m_pThresholdTypeCombo->SetItemData(Ndx, CV_THRESH_BINARY);
		Ndx = m_pThresholdTypeCombo->AddString(L"Inverted");
		m_pThresholdTypeCombo->SetItemData(Ndx, CV_THRESH_BINARY_INV);
	}

	m_pAdaptiveTypeCombo = CreateOpCombo(0, ID_AdaptiveTYPE);

	if (m_pAdaptiveTypeCombo)
	{
		int Ndx = m_pAdaptiveTypeCombo->AddString(L"Equal weighting");
		m_pAdaptiveTypeCombo->SetItemData(Ndx, CV_ADAPTIVE_THRESH_MEAN_C);
		Ndx = m_pAdaptiveTypeCombo->AddString(L"Gaussian weighting");
		m_pAdaptiveTypeCombo->SetItemData(Ndx, CV_ADAPTIVE_THRESH_GAUSSIAN_C);
	}

	CreateOpStatic(L"max value", TRUE, 0);
	m_pMaxValueEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pMaxValueSlider = CreateOpSlider(0, 1, 255);

	CreateOpStatic(L"block size", TRUE, 0);
	m_pBlockSizeEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pBlockSizeSlider = CreateOpSlider(0, 3, 20);

	CreateOpStatic(L"subtraction constant", TRUE, 0);
	m_pSubtractionConstantEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pSubtractionConstantSlider = CreateOpSlider(0, -20, 20);


	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpThresholdAdaptive::StartOpThread(void *pData)
{
	COpThresholdAdaptive *pThresholdAdaptive = (COpThresholdAdaptive *) pData;

	pThresholdAdaptive->OpThread();

	return 0;
}

void COpThresholdAdaptive::UpdateSliders()
{
	m_pBlockSizeSlider->SetPos(m_nBlockSize);
	m_pMaxValueSlider->SetPos(m_nMaxValue);
	m_pSubtractionConstantSlider->SetPos(m_nSubtractionConstant);
}

void COpThresholdAdaptive::UpdateValues()
{
	TCHAR TStr[10];

	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pThresholdTypeCombo->GetCount(); Ndx++)
		if (m_pThresholdTypeCombo->GetItemData(Ndx) == m_nThresholdType) break;

	m_pThresholdTypeCombo->SetCurSel(Ndx);
	m_pThresholdTypeCombo->GetLBText(Ndx, m_aThresholdTypeStr);

	for (Ndx = 0; Ndx < m_pAdaptiveTypeCombo->GetCount(); Ndx++)
		if (m_pAdaptiveTypeCombo->GetItemData(Ndx) == m_nThresholdType) break;

	m_pAdaptiveTypeCombo->SetCurSel(Ndx);
	m_pAdaptiveTypeCombo->GetLBText(Ndx, m_aAdaptiveTypeStr);

	if (m_nBlockSize % 2 != 1) m_nBlockSize--;

	_itot(m_nMaxValue, TStr, 10);
	m_pMaxValueEdit->SetWindowTextW(TStr);
	_itot(m_nBlockSize, TStr, 10);
	m_pBlockSizeEdit->SetWindowTextW(TStr);
	_itot(m_nSubtractionConstant, TStr, 10);
	m_pSubtractionConstantEdit->SetWindowTextW(TStr);

}


void COpThresholdAdaptive::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpThresholdAdaptive::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nThresholdType, sizeof(m_nThresholdType), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nAdaptiveMethod, sizeof(m_nAdaptiveMethod), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nBlockSize, sizeof(m_nBlockSize), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nSubtractionConstant, sizeof(m_nSubtractionConstant), &dwBytesWritten, NULL);
		
		if (WriteFile(hFile, &m_nMaxValue, sizeof(m_nMaxValue), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bThresholdAdaptiveFiller, sizeof(bThresholdAdaptiveFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpThresholdAdaptive::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nThresholdType, sizeof(m_nThresholdType), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nAdaptiveMethod, sizeof(m_nAdaptiveMethod), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nBlockSize, sizeof(m_nBlockSize), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nSubtractionConstant, sizeof(m_nSubtractionConstant), &dwBytesWritten, NULL);
		
		if (ReadFile(hFile, &m_nMaxValue, sizeof(m_nMaxValue), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bThresholdAdaptiveFiller, sizeof(bThresholdAdaptiveFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpThresholdAdaptive::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpThresholdAdaptive::ValidateParameters()
{
}

void COpThresholdAdaptive::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pBlockSizeSlider) 
	{
		m_nBlockSize = pSlider->GetPos();

	}
	else if (pSlider == m_pMaxValueSlider) 
	{
		m_nMaxValue = pSlider->GetPos();

	}
	else if (pSlider == m_pSubtractionConstantSlider) 
	{
		m_nSubtractionConstant = pSlider->GetPos();

	}

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpThresholdAdaptive::ThresholdTypeChange()
{
	
	int Ndx = m_pThresholdTypeCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nThresholdType = m_pThresholdTypeCombo->GetItemData(Ndx);
		m_pThresholdTypeCombo->GetLBText(Ndx, m_aThresholdTypeStr);


		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpThresholdAdaptive::AdaptiveTypeChange()
{
	
	int Ndx = m_pAdaptiveTypeCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nAdaptiveMethod = m_pAdaptiveTypeCombo->GetItemData(Ndx);
		m_pAdaptiveTypeCombo->GetLBText(Ndx, m_aAdaptiveTypeStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpThresholdAdaptive::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"ThresholdAdaptiveThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (pImage->nChannels == 1)
			{
				int nBlockSize = m_nBlockSize;
				int nMaxValue = m_nMaxValue;
				int nSubtractionConstant = m_nSubtractionConstant;

				IplImage *pSrc = pImage;

					// Need separate destination buffer
				pSrc = cvCloneImage(pImage);

				cvAdaptiveThreshold(pSrc, pImage, nMaxValue, m_nAdaptiveMethod, m_nThresholdType, nBlockSize, nSubtractionConstant);

				cvReleaseImage(&pSrc);

				SetData(0, DATA_IMAGE, 0, pImage);
				SendData();

				ReleaseData(L"ThresholdAdaptiveThread");  // We're done with the data so it can change if needed
			}
				else TitleError(L"Src image must be single channel");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
