#pragma once
#include "opbase.h"
#include "Cv.h"

#define AndS_DEFAULT_WIDTH (200)
#define AndS_DEFAULT_HEIGHT (80)

class COpAndS : public COpBase
{
public:
	COpAndS(CMainView *pMain);
	~COpAndS(void);

	virtual	PTCHAR GetFriendlyName() { return L"AndS"; }
	virtual int	GetDefaultWidth() { return AndS_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return AndS_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pAnd_ValueSlider;
	CEdit		*m_pAnd_ValueEdit;

		// Save data
	int	m_nNumAnd_Value;
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
