#pragma once
#include "opbase.h"
#include "Cv.h"

#define Laplace_DEFAULT_WIDTH (200)
#define Laplace_DEFAULT_HEIGHT (80)

class COpLaplace : public COpBase
{
public:
	COpLaplace(CMainView *pMain);
	~COpLaplace(void);

	virtual	PTCHAR GetFriendlyName() { return L"Laplace"; }
	virtual int	GetDefaultWidth() { return Laplace_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Laplace_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pApertureSlider;
	CEdit		*m_pApertureEdit;

		// Save data
	int	m_nAperture;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
