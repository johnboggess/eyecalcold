#pragma once
#include "opbase.h"
#include "Cv.h"

#define ConvertScaleAbs_DEFAULT_WIDTH (200)
#define ConvertScaleAbs_DEFAULT_HEIGHT (120)

class COpConvertScaleAbs : public COpBase
{
public:
	COpConvertScaleAbs(CMainView *pMain);
	~COpConvertScaleAbs(void);

	virtual	PTCHAR GetFriendlyName() { return L"ConvertScaleAbs"; }
	virtual int	GetDefaultWidth() { return ConvertScaleAbs_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return ConvertScaleAbs_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pScaleAbsSlider;
	CSliderCtrl	*m_pShiftAbsSlider;
	CEdit		*m_pScaleAbsEdit;
	CEdit		*m_pShiftAbsEdit;

		// Save data
	int m_nScaleAbsShift;
	int m_nShiftAbsShift;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
