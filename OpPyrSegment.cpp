#include "StdAfx.h"
#include "OpPyrSegment.h"
#include "OpBase.h"
#include "Cv.h"

#define MIN_LEVEL (1)
#define MAX_LEVEL (8)

BYTE bPyrSegmentFiller[32];

BEGIN_MESSAGE_MAP(COpPyrSegment, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()

COpPyrSegment::COpPyrSegment(CMainView *pMain) : COpBase(pMain)
{
	m_pLevelSlider = NULL;
	m_pLevelEdit = NULL;
	m_pThresh1Slider = NULL;
	m_pThresh1Edit = NULL;
	m_pThresh2Slider = NULL;
	m_pThresh2Edit = NULL;

		// Save data
	m_nLevel = 1;
	m_nThreshold1 = 150;
	m_nThreshold2 = 30;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpPyrSegment::~COpPyrSegment(void)
{
	if (m_pLevelSlider) delete m_pLevelSlider;
	if (m_pLevelEdit) delete m_pLevelEdit;
	if (m_pThresh1Slider) delete m_pThresh1Slider;
	if (m_pThresh1Edit) delete m_pThresh1Edit;
	if (m_pThresh2Slider) delete m_pThresh2Slider;
	if (m_pThresh2Edit) delete m_pThresh2Edit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpPyrSegment::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpPyrSegment::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	CreateOpStatic(L"level", TRUE, 0);
	m_pLevelEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pLevelSlider = CreateOpSlider(0, MIN_LEVEL, MAX_LEVEL);

	CreateOpStatic(L"threshold 1", TRUE, 0);
	m_pThresh1Edit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pThresh1Slider = CreateOpSlider(0, 1, 255);

	CreateOpStatic(L"threshold 2", TRUE, 0);
	m_pThresh2Edit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pThresh2Slider = CreateOpSlider(0, 1, 50);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpPyrSegment::StartOpThread(void *pData)
{
	COpPyrSegment *pBase = (COpPyrSegment *) pData;

	pBase->OpThread();

	return 0;
}

void COpPyrSegment::UpdateSliders()
{
	m_pLevelSlider->SetPos(m_nLevel);
	m_pThresh1Slider->SetPos((int) m_nThreshold1);
	m_pThresh2Slider->SetPos((int) m_nThreshold2);

}
void COpPyrSegment::UpdateValues()
{
	TCHAR TStr[10];


	_itot(m_nLevel, TStr, 10);
	m_pLevelEdit->SetWindowTextW(TStr);
	_itot((int) m_nThreshold1, TStr, 10);
	m_pThresh1Edit->SetWindowTextW(TStr);
	_itot((int) m_nThreshold2, TStr, 10);
	m_pThresh2Edit->SetWindowTextW(TStr);
}


void COpPyrSegment::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpPyrSegment::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nLevel, sizeof(m_nLevel), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nThreshold1, sizeof(m_nThreshold1), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nThreshold2, sizeof(m_nThreshold2), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bPyrSegmentFiller, sizeof(bPyrSegmentFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpPyrSegment::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nLevel, sizeof(m_nLevel), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nThreshold1, sizeof(m_nThreshold1), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nThreshold2, sizeof(m_nThreshold2), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bPyrSegmentFiller, sizeof(bPyrSegmentFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpPyrSegment::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpPyrSegment::ValidateParameters()
{
}

void COpPyrSegment::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pLevelSlider) 
		m_nLevel = pSlider->GetPos();

	if (pSlider == m_pThresh1Slider) 
		m_nThreshold1 = pSlider->GetPos();

	if (pSlider == m_pThresh2Slider) 
		m_nThreshold2 = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpPyrSegment::OnChange()
{
	
}

void COpPyrSegment::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"PyrSegmentThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (pImage->depth == IPL_DEPTH_8U) {	// Validate Src and Dst images, etc
				IplImage *pNewImage = cvCreateImage(cvGetSize(pImage), pImage->depth, pImage->nChannels);

				if (pNewImage)
				{
					CvMemStorage *Storage = cvCreateMemStorage();
					CvSeq *pComp = NULL;
					
					cvPyrSegmentation(pImage, pNewImage, Storage, &pComp, m_nLevel, m_nThreshold1, m_nThreshold2);

					cvReleaseMemStorage (&Storage);
										
					SetData(0, DATA_IMAGE, 0, pNewImage, TRUE);
					SendData();
				}
			}
				else TitleError(L"Images must be 8-bit");

			ReleaseData(L"PyrSegmentThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
