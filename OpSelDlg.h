#pragma once


// COpSelDlg dialog

class COpSelDlg : public CDialog
{
	DECLARE_DYNAMIC(COpSelDlg)

public:
	COpSelDlg(CPoint DialogXy, int *pSelectionIndex, PTCHAR ItemList, CWnd* pParent = NULL);   // standard constructor
	virtual ~COpSelDlg();

// Dialog Data
	enum { IDD = IDD_OP_SELECTION };

protected:
	PTCHAR m_pItemList;
	int *m_pSelectionIndex;
	CPoint m_oDialogXy;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg BOOL OnInitDialog( );

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeOpEdit();
	afx_msg void OnLbnDblclkOpList();
};
