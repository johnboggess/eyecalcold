#include "StdAfx.h"
#include "OpCanny.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bCannyFiller[32];

BEGIN_MESSAGE_MAP(COpCanny, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
END_MESSAGE_MAP()

COpCanny::COpCanny(CMainView *pMain) : COpBase(pMain)
{
	m_pLowThreshSlider = NULL;
	m_pLowThreshEdit = NULL;
	m_pHiThreshSlider = NULL;
	m_pHiThreshEdit = NULL;
	m_pApertureSlider = NULL;
	m_pApertureEdit = NULL;

		// Save data
	m_nLowThresh = 1;
	m_nHiThresh = 2;
	m_nAperture = 3;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpCanny::~COpCanny(void)
{
	if (m_pLowThreshSlider) delete m_pLowThreshSlider;
	if (m_pLowThreshEdit) delete m_pLowThreshEdit;
	if (m_pHiThreshSlider) delete m_pHiThreshSlider;
	if (m_pHiThreshEdit) delete m_pHiThreshEdit;
	if (m_pApertureSlider) delete m_pApertureSlider;
	if (m_pApertureEdit) delete m_pApertureEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpCanny::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpCanny::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	CreateOpStatic(L"low threshold", TRUE, 0);
	m_pLowThreshEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pLowThreshSlider = CreateOpSlider(0, 0, 1024);
//	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"high threshold", TRUE, 0);
	m_pHiThreshEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pHiThreshSlider = CreateOpSlider(0, 0, 1024);
//	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"aperture", TRUE, 0);
	m_pApertureEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pApertureSlider = CreateOpSlider(0, 3, 7);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpCanny::StartOpThread(void *pData)
{
	COpCanny *pBase = (COpCanny *) pData;

	pBase->OpThread();

	return 0;
}

void COpCanny::UpdateSliders()
{
	m_pLowThreshSlider->SetPos(m_nLowThresh);
	m_pHiThreshSlider->SetPos(m_nHiThresh);
	m_pApertureSlider->SetPos(m_nAperture);

}
void COpCanny::UpdateValues()
{
	TCHAR TStr[10];

	_itot(m_nLowThresh, TStr, 10);
	m_pLowThreshEdit->SetWindowTextW(TStr);

	_itot(m_nHiThresh, TStr, 10);
	m_pHiThreshEdit->SetWindowTextW(TStr);

	_itot(m_nAperture, TStr, 10);
	m_pApertureEdit->SetWindowTextW(TStr);

}


void COpCanny::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpCanny::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nLowThresh, sizeof(m_nLowThresh), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nHiThresh, sizeof(m_nHiThresh), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nAperture, sizeof(m_nAperture), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bCannyFiller, sizeof(bCannyFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpCanny::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nLowThresh, sizeof(m_nLowThresh), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nHiThresh, sizeof(m_nHiThresh), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nAperture, sizeof(m_nAperture), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bCannyFiller, sizeof(bCannyFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpCanny::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpCanny::ValidateParameters()
{
	if (m_nAperture %2 == 0) m_nAperture++;	// must be odd
	
	if (m_nAperture < 3) m_nAperture = 3;
	
}

void COpCanny::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pLowThreshSlider) 
		m_nLowThresh = pSlider->GetPos();

	if (pSlider == m_pHiThreshSlider) 
		m_nHiThresh = pSlider->GetPos();

	if (pSlider == m_pApertureSlider) 
		m_nAperture = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpCanny::OnChange()
{
}

void COpCanny::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"CannyThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (pImage->nChannels == 1) {	// Validate Src and Dst images, etc

				IplImage *pNewImage = cvCreateImage(cvGetSize(pImage), IPL_DEPTH_8U, pImage->nChannels);
				
				if (pNewImage)
				{
					cvCanny(pImage, pNewImage, m_nLowThresh, m_nHiThresh, m_nAperture);

					SetData(0, DATA_IMAGE, 0, pNewImage, TRUE);
					SendData();
				}
			}
				else TitleError(L"Image must be single channel");

			ReleaseData(L"CannyThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
