#pragma once
#include "opbase.h"
#include "Cv.h"
	#include "cvaux.h"
	#include "highgui.h"
	#include <stdio.h>

#define CodeBookSub_DEFAULT_WIDTH (180)
#define CodeBookSub_DEFAULT_HEIGHT (100)
#define CHANNELS 3






class COpCodeBookSub : public COpBase
{
public:
	COpCodeBookSub(CMainView *pMain);
	~COpCodeBookSub(void);

	virtual	PTCHAR GetFriendlyName() { return L"CodeBookSub"; }
	virtual int	GetDefaultWidth() { return CodeBookSub_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return CodeBookSub_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:

	CComboBox	*m_pSubTypeCombo;
	CComboBox	*m_pElementTypeCombo;

	// Save data
	int	SubType;
	int ElementType;

	unsigned cbBounds;
	bool firsttime;
	int runtimes;
	int minMod;
	int maxMod;
	int c, n, nframes ;
	int nframesToLearnBG ;
	IplImage *yuvImage ;
	IplImage *ImaskCodeBook ;
	IplImage *ImaskCodeBookCC ;

	bool update_bg_model;
	CvBGStatModel * bg_model;
	CvBGCodeBookModel* model;

	// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void SubTypeChange();
	afx_msg void ElementTypeChange();
	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP();

	
};