#include "StdAfx.h"
#include "OpFindContour.h"
#include "OpBase.h"
#include "Cv.h"

#define ID_FIND_CONTOUR_MODE (101)
#define ID_FIND_CONTOUR_METHOD (102)

#define MAX_CONTOURS (1000)

BYTE bFindContourFiller[32];

BEGIN_MESSAGE_MAP(COpFindContour, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_FIND_CONTOUR_MODE, OnModeChange)
	ON_CBN_SELCHANGE(ID_FIND_CONTOUR_METHOD, OnMethodChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpFindContour::COpFindContour(CMainView *pMain) : COpBase(pMain)
{
	m_pModeCombo = NULL;
	m_pMethodCombo = NULL;
	m_pNumContoursEdit = NULL;

	m_eMode = CV_RETR_LIST;
	m_eMethod = CV_CHAIN_APPROX_SIMPLE;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpFindContour::~COpFindContour(void)
{
	if (m_pModeCombo)delete m_pModeCombo;
	if (m_pMethodCombo)delete m_pMethodCombo;
	if (m_pNumContoursEdit) delete m_pNumContoursEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpFindContour::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpFindContour::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_pModeCombo = CreateOpCombo(0, ID_FIND_CONTOUR_MODE);

	CComboBox *pCombo = m_pModeCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"external");
		pCombo->SetItemData(Ndx, CV_RETR_EXTERNAL);
		Ndx = pCombo->AddString(L"list");
		pCombo->SetItemData(Ndx, CV_RETR_LIST);
		Ndx = pCombo->AddString(L"ccomp");
		pCombo->SetItemData(Ndx, CV_RETR_CCOMP);
		Ndx = pCombo->AddString(L"tree");
		pCombo->SetItemData(Ndx, CV_RETR_TREE);
	}

	m_pMethodCombo = CreateOpCombo(0, ID_FIND_CONTOUR_METHOD);

	pCombo = m_pMethodCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"chain code");
		pCombo->SetItemData(Ndx, CV_CHAIN_CODE);
		Ndx = pCombo->AddString(L"chain approx none");
		pCombo->SetItemData(Ndx, CV_CHAIN_APPROX_NONE);
		Ndx = pCombo->AddString(L"chain approx simple");
		pCombo->SetItemData(Ndx, CV_CHAIN_APPROX_SIMPLE);
		Ndx = pCombo->AddString(L"chain approx TC89_L1");
		pCombo->SetItemData(Ndx, CV_CHAIN_APPROX_TC89_L1);
		Ndx = pCombo->AddString(L"chain approx TC89_KCOS");
		pCombo->SetItemData(Ndx, CV_CHAIN_APPROX_TC89_KCOS);
		Ndx = pCombo->AddString(L"link runs");
		pCombo->SetItemData(Ndx, CV_LINK_RUNS);
	}

	CreateOpStatic(L"num contours found", TRUE, 0);
	CreateOpStatic(L" ", FALSE, 60);
	m_pNumContoursEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3*2, TRUE);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpFindContour::StartOpThread(void *pData)
{
	COpFindContour *pBase = (COpFindContour *) pData;

	pBase->OpThread();

	return 0;
}

void COpFindContour::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpFindContour::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pModeCombo->GetCount(); Ndx++)
		if (m_pModeCombo->GetItemData(Ndx) == m_eMode) break;

	m_pModeCombo->SetCurSel(Ndx);

	for (Ndx = 0; Ndx < m_pMethodCombo->GetCount(); Ndx++)
		if (m_pMethodCombo->GetItemData(Ndx) == m_eMethod) break;

	m_pMethodCombo->SetCurSel(Ndx);

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpFindContour::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpFindContour::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_eMode, sizeof(m_eMode), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_eMethod, sizeof(m_eMethod), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bFindContourFiller, sizeof(bFindContourFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpFindContour::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_eMode, sizeof(m_eMode), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_eMethod, sizeof(m_eMethod), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bFindContourFiller, sizeof(bFindContourFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpFindContour::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpFindContour::ValidateParameters()
{
}

void COpFindContour::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpFindContour::OnModeChange()
{
	int Ndx = m_pModeCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_eMode = (CvContourRetrievalMode) m_pModeCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpFindContour::OnMethodChange()
{
	int Ndx = m_pMethodCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_eMethod = (CvChainApproxMethod) m_pMethodCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpFindContour::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpFindContour::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpFindContour::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"FindContourThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (pImage->nChannels == 1) {	// Validate Src and Dst images, etc

				CvMemStorage *pStorage = cvCreateMemStorage();
				CvSeq *pContours = NULL;
				
				int NumContour = cvFindContours(pImage, pStorage, &pContours, sizeof(CvContour), m_eMode, m_eMethod);

				TCHAR TStr[20];
				
				_itot(NumContour, TStr, 10);
				m_pNumContoursEdit->SetWindowTextW(TStr);
				m_pNumContoursEdit->UpdateWindow();

				if (NumContour < MAX_CONTOURS)
				{
					if (pContours)
					{
						SetData(0, DATA_CONTOUR, 0, pContours, TRUE, DATA_STORAGE_MEMSTORAGE, pStorage);
						SendData();
					}
				}
					else // too many contours
				{
					TitleError(L"Too many contours");
					cvReleaseMemStorage(&pStorage);
				}
			}
				else TitleError(L"Source image must be single-channel binary");

			ReleaseData(L"FindContourThread");  // We're done with the data so it can change if needed
		}
			else TitleError(L"Requires binary source image");

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
