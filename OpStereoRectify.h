#pragma once
#include "opbase.h"
#include "Cv.h"

#define StereoRectify_DEFAULT_WIDTH (200)
#define StereoRectify_DEFAULT_HEIGHT (100)

class COpStereoRectify : public COpBase
{
public:
	COpStereoRectify(CMainView *pMain);
	~COpStereoRectify(void);

	virtual	PTCHAR GetFriendlyName() { return L"StereoRectify"; }
	virtual int	GetDefaultWidth() { return StereoRectify_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return StereoRectify_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pViewCombo;

		// Save data
	int			m_nCurView;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChangeView();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
