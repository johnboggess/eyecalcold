#pragma once
#include "opbase.h"
#include "Cv.h"

#define Matrix_DEFAULT_WIDTH (200)
#define Matrix_DEFAULT_HEIGHT (200)

#define MAX_X_SLOTS 5
#define MAX_Y_SLOTS 5


class COpMatrix : public COpBase
{
friend class CMatrixEdit;

public:
	COpMatrix(CMainView *pMain);
	~COpMatrix(void);

	virtual	PTCHAR GetFriendlyName() { return L"Matrix"; }
	virtual int	GetDefaultWidth() { return Matrix_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Matrix_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pSlider;
	CComboBox	*m_pCombo;
	CEdit		*m_pEdit;
	TCHAR		m_aStr[20];
	CButton		*m_pSetButton;

		// Save data
	CMatrixEdit *m_pEdits[MAX_X_SLOTS][MAX_Y_SLOTS];
	CvPoint		m_oAnchorPoint;
	
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;
	CvPoint		m_oLastContextMenuPoint;
	BOOL		m_fLoadedData;
	double		m_gMatrixData[MAX_X_SLOTS][MAX_Y_SLOTS];

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();
	void GetAndSetKernelData();


	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnCenterPt();
	afx_msg void OnClearAll();
	afx_msg void OnSetButton();
	afx_msg	virtual void OpDisable();

	DECLARE_MESSAGE_MAP()
};


class CMatrixEdit : public CEdit {
public:
	CMatrixEdit(COpMatrix *pMatrix) { CEdit(); m_pMatrix = pMatrix; }
	~CMatrixEdit(void) {  }

protected:
	COpMatrix *m_pMatrix;
	
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);

	DECLARE_MESSAGE_MAP()
};
