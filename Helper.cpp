#include "StdAfx.h"
#include "Cv.h"

CvSeq *CopyContour(CvSeq *pSeq, CvSeq *pHPrev, CvSeq *pVPrev, CvMemStorage *pStorage)
{
	if (pStorage)
	{
		CvSeq *pNewSeq = cvCloneSeq(pSeq, pStorage);

		if (pNewSeq)
		{
			if (pHPrev) pNewSeq->h_prev = pHPrev;
			if (pVPrev) pNewSeq->v_prev = pVPrev;
			
			if( pSeq->h_next) pNewSeq->h_next = CopyContour(pSeq->h_next, pNewSeq, pVPrev, pStorage);
			if( pSeq->v_next) pNewSeq->v_next = CopyContour(pSeq->v_next, NULL, pNewSeq, pStorage);
		}
		
		return pNewSeq;
	}
		else return NULL;
}
