// SVSSetup.cpp : implementation file
//

#include "stdafx.h"
#include "EyeCalc.h"
#include "SVSSetup.h"


// CSVSSetup dialog

IMPLEMENT_DYNAMIC(CSVSSetup, CDialog)

CSVSSetup::CSVSSetup(CSVSInterface *pInterface, CWnd* pParent /*=NULL*/)
	: CDialog(CSVSSetup::IDD, pParent)
{
	m_pInterface = pInterface;
}

CSVSSetup::~CSVSSetup()
{
}

void CSVSSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSVSSetup, CDialog)
	ON_BN_CLICKED(IDC_SUBMIT_BUTTON, &CSVSSetup::OnBnClickedSubmitButton)
END_MESSAGE_MAP()


// CSVSSetup message handlers

void CSVSSetup::OnEnChangeCommandEdit()
{
}

void CSVSSetup::OnBnClickedSubmitButton()
{
	CEdit *pEdit = (CEdit *) GetDlgItem(IDC_COMMAND);
	TCHAR LineBuf[4096];
	int nBytesReturned;
	char ResultBuf[4096];
	CAMERA_DEST_TYPE eCameraDest;

	pEdit->GetWindowText(LineBuf, sizeof(LineBuf));
	
	char CommandStr[1024];
	
	wcstombs(CommandStr, LineBuf, sizeof(CommandStr));
	
	char cCameraDest = tolower(CommandStr[0]);
	
	if (strstr(CommandStr, "test"))
	{
		DWORD dwThreadID;
		CreateThread(NULL, 0, StartRobotThread, this, 0, &dwThreadID);

	}
		else
	if (cCameraDest == 'l' || cCameraDest == 'r' || cCameraDest == 'b')
	{
		strcpy(CommandStr, CommandStr+1);	// Overwrite camera destination
		
		switch (cCameraDest)
		{
			case 'l':
				eCameraDest = CAMERA_DEST_LEFT;
				break;
			case 'r':
				eCameraDest = CAMERA_DEST_RIGHT;
				break;
			case 'b':
				eCameraDest = CAMERA_DEST_BOTH;
				break;
		}
		
		m_pInterface->SendCommand(eCameraDest, CommandStr, ResultBuf, sizeof(ResultBuf), &nBytesReturned);

		CEdit *pResultEdit = (CEdit *) GetDlgItem(IDC_COMMAND_RESULT);

		if (nBytesReturned > 0)
		{
			ResultBuf[nBytesReturned] = 0;
	
			mbstowcs(LineBuf, ResultBuf, sizeof(LineBuf)*sizeof(TCHAR));
			
			if (LineBuf[0]) pResultEdit->SetWindowTextW(LineBuf);
		}
			else pResultEdit->SetWindowTextW(L"No result received");
	}
		else MessageBox(L"Please precede command with camera destination: 'l', 'r', or 'b'");
			
	pEdit->SetWindowText(L"");
}


void CSVSSetup::GoToSign()
{
	char ResultBuf[4096];
	int nBytesReturned;

	BOOL fDone = FALSE;
	
	while (!fDone)
	{
		m_pInterface->SendCommand(CAMERA_DEST_LEFT, "vb0", ResultBuf, sizeof(ResultBuf), &nBytesReturned);

		if (nBytesReturned)
		{
			char *Cp = ResultBuf;
			int Count = 0;
			int NumPixels;
			int x1, x2, y1, y2;
				
			sscanf(ResultBuf, "##vb0 %d", &Count);
				
				// Find 
			while (*Cp && *Cp != '\n') Cp++;
			if (*Cp && *Cp == '\n') Cp++;	 // Move past \n
			
			sscanf(Cp,"%d - %d %d %d %d", &NumPixels, &x1, &x2, &y1, &y2);

			if (x2-x1 > 100)
			{
				fDone = true;
				m_pInterface->MoveRobot(MOVE_STOP, 0, 0);
				m_pInterface->MoveRobot(MOVE_BACKWARD, 60, 5);
				
			}
				else
			{
				int xCenter = (x1 + x2) / 2;
				
				if (xCenter < 160 - 20)
				{
					m_pInterface->MoveRobot(MOVE_LEFT, 50, 10);
//					m_pInterface->MoveRobot(MOVE_FORWARD, 60, 20);
					
				
				}
					else
				if (xCenter > 160 + 20)
				{
					m_pInterface->MoveRobot(MOVE_RIGHT, 50, 10);
//					m_pInterface->MoveRobot(MOVE_FORWARD, 60, 20);

				}
					else m_pInterface->MoveRobot(MOVE_FORWARD, 60, 20);

			} // else
		}  // if nBytesReturned
	} // while
	//	m_pInterface->SendCommand(CAMERA_DEST_LEFT, "", ResultBuf, sizeof(ResultBuf), &nBytesReturned);

}

void CSVSSetup::TestRobot()
{
	int nn[16];
	
	char ResultBuf[4096];
	int nBytesReturned;

	BOOL fDone = FALSE;
	
	while (!fDone)
	{
		GoToSign();

		m_pInterface->SendCommand(CAMERA_DEST_LEFT, "nb0", ResultBuf, sizeof(ResultBuf), &nBytesReturned);

			// Find /r/n
		char *Cp = ResultBuf;
		while (*Cp && *Cp != '\n') Cp++;
		if (*Cp && *Cp == '\n') Cp++;	 // Move past \n

		sscanf(Cp, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", 
			&nn[0], &nn[1], &nn[2], &nn[3], &nn[4], &nn[5], &nn[6], &nn[7], &nn[8], &nn[9], &nn[10], &nn[11], &nn[12], &nn[13], &nn[14], &nn[15]);

		int nMax = 0, iMax = 0;
		for (int i = 0; i < 16; i++)
			if (nn[i] > nMax) { nMax = nn[i], iMax = i; }
			
		if (iMax == 0 || iMax == 0x0e) // turn right
		{
			m_pInterface->MoveRobot(MOVE_RIGHT, 50, 60);
		}
			else
		if (iMax == 1 || iMax == 0x0f) // turn left
		{
			m_pInterface->MoveRobot(MOVE_LEFT, 50, 60);
		}
			else
		if (iMax == 2 || iMax == 3) // stop
		{
			m_pInterface->MoveRobot(MOVE_LEFT, 50, 60);
			m_pInterface->MoveRobot(MOVE_LEFT, 50, 60);
			m_pInterface->MoveRobot(MOVE_LEFT, 50, 60);
			m_pInterface->MoveRobot(MOVE_LEFT, 50, 60);
			fDone = true;		
		}
	}
}

DWORD WINAPI CSVSSetup::StartRobotThread(void *pData)
{
	CSVSSetup *pBase = (CSVSSetup *) pData;

	pBase->TestRobot();

	return 0;

}
