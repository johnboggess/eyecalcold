#pragma once
#include "opbase.h"
#include "Cv.h"

#define CentralMoments_DEFAULT_WIDTH (200)
#define CentralMoments_DEFAULT_HEIGHT (250)

class COpCentralMoments : public COpBase
{
public:
	COpCentralMoments(CMainView *pMain);
	~COpCentralMoments(void);

	virtual	PTCHAR GetFriendlyName() { return L"CentralMoments"; }
	virtual int	GetDefaultWidth() { return CentralMoments_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return CentralMoments_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CEdit		*m_pMu20Edit;
	CEdit		*m_pMu11Edit;
	CEdit		*m_pMu02Edit;
	CEdit		*m_pMu30Edit;
	CEdit		*m_pMu21Edit;
	CEdit		*m_pMu12Edit;
	CEdit		*m_pMu03Edit;

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	CvMoments	m_CvCentralMoments;


	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
