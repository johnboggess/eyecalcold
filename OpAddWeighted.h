#pragma once
#include "opbase.h"
#include "Cv.h"

#define AddWeighted_DEFAULT_WIDTH (200)
#define AddWeighted_DEFAULT_HEIGHT (180)

class COpAddWeighted : public COpBase
{
public:
	COpAddWeighted(CMainView *pMain);
	~COpAddWeighted(void);

	virtual	PTCHAR GetFriendlyName() { return L"AddWeighted"; }
	virtual int	GetDefaultWidth() { return AddWeighted_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return AddWeighted_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pAlphaSlider;
	CSliderCtrl	*m_pBetaSlider;
	CSliderCtrl	*m_pGammaSlider;
	CEdit *m_pAlphaEdit;
	CEdit *m_pBetaEdit;
	CEdit *m_pGammaEdit;

		// Save data
	double m_nAlphaShift;
	double m_nBetaShift;
	int m_nGammaShift;
	int help_Alpha;
	int help_Beta;
	
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
//@hai takes out	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
