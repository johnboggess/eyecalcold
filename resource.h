//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EyeCalc.rc
//
#define IDD_OP_SELECTION                9
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_EyeComputeTYPE              129
#define IDD_DIALOG1                     130
#define IDR_OPBASE_MENU                 131
#define IDD_SVS_SETUP_DIALOG            133
#define IDR_EDIT_MENU                   134
#define IDC_OP_LIST                     1003
#define IDC_OP_EDIT                     1004
#define IDC_COMMAND_RESULT              1006
#define IDC_COMMAND                     1007
#define IDC_SUBMIT_BUTTON               1008
#define ID_CLOSE_HELLO                  32771
#define ID_CLOSE_THERE                  32772
#define ID_CLOSE_EXIT                   32773
#define ID_FILE_SAVE32774               32774
#define ID_FILE_LOAD                    32775
#define ID_FILE_NEW32776                32776
#define ID_DELETE_BASE                  32777
#define ID_Menu                         32778
#define ID_OP_DISABLE                   32779
#define ID_OP_CHANGE                    32780
#define ID_CLOSE_SHOWDATA               32781
#define ID_OP_SHOWDATA                  32782
#define ID_CLOSE_SHOWERROR              32783
#define ID_SHOW_ERROR                   32784
#define ID_CENTERPT                     32785
#define ID_CENTERPT_CENTERPTR           32786
#define ID_CENTERPT_CLEARALL            32787
#define ID_Menu32788                    32788
#define ID_MATRIX_CENTERPT              32789
#define ID_MATRIX_CLEARALL              32790
#define ID_FILE_SAVEAS                  32791
#define ID_FILE_CONNECTSVS              32792

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32793
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
