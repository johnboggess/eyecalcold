#pragma once

#include "resource.h"
#include "SVSInterface.h"

// CSVSSetup dialog

class CSVSSetup : public CDialog
{
	DECLARE_DYNAMIC(CSVSSetup)

public:
	CSVSSetup(CSVSInterface *pInterface, CWnd* pParent = NULL);   // standard constructor
	virtual ~CSVSSetup();

// Dialog Data
	enum { IDD = IDD_SVS_SETUP_DIALOG };

protected:
	CSVSInterface *m_pInterface;

	static DWORD WINAPI StartRobotThread(void *pData);
	void TestRobot();
	void GoToSign();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeCommandEdit();
	afx_msg void OnBnClickedSubmitButton();
};
