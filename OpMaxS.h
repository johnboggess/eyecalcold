#pragma once
#include "opbase.h"
#include "Cv.h"

#define MaxS_DEFAULT_WIDTH (200)
#define MaxS_DEFAULT_HEIGHT (80)

class COpMaxS : public COpBase
{
public:
	COpMaxS(CMainView *pMain);
	~COpMaxS(void);

	virtual	PTCHAR GetFriendlyName() { return L"MaxS"; }
	virtual int	GetDefaultWidth() { return MaxS_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return MaxS_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pCompare_ValueSlider;
	CEdit		*m_pCompare_ValueEdit;

		// Save data
	int	m_nNumCompare_Value;
	CvScalar	scalar;
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
