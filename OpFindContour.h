#pragma once
#include "opbase.h"
#include "Cv.h"

#define FindContour_DEFAULT_WIDTH (200)
#define FindContour_DEFAULT_HEIGHT (130)

class COpFindContour : public COpBase
{
public:
	COpFindContour(CMainView *pMain);
	~COpFindContour(void);

	virtual	PTCHAR GetFriendlyName() { return L"FindContour"; }
	virtual int	GetDefaultWidth() { return FindContour_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return FindContour_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pModeCombo;
	CComboBox	*m_pMethodCombo;
	CEdit		*m_pNumContoursEdit;

		// Save data
	CvContourRetrievalMode	m_eMode;
	CvChainApproxMethod	m_eMethod;
	
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnModeChange();
	afx_msg void OnMethodChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
