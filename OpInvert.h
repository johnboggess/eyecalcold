#pragma once
#include "opbase.h"
#include "Cv.h"

#define Invert_DEFAULT_WIDTH (140)
#define Invert_DEFAULT_HEIGHT (60)

class COpInvert : public COpBase
{
public:
	COpInvert(CMainView *pMain);
	~COpInvert(void);

	virtual	PTCHAR GetFriendlyName() { return L"Invert"; }
	virtual int	GetDefaultWidth() { return Invert_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Invert_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CComboBox	*m_pInvertTypeCombo;
	TCHAR		m_aInvertTypeStr[20];

		// Save data
	int			m_nInvertType;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void InvertTypeChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
