#pragma once
#include "opbase.h"
#include "Cv.h"

#define FLOOD_DEFAULT_WIDTH (200)
#define FLOOD_DEFAULT_HEIGHT (240)

class COpFloodFill : public COpBase
{
public:
	COpFloodFill(CMainView *pMain);
	~COpFloodFill(void);

	virtual	PTCHAR GetFriendlyName() { return L"FloodFill"; }
	virtual int	GetDefaultWidth() { return FLOOD_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return FLOOD_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pLoDiffSlider;
	CSliderCtrl	*m_pHiDiffSlider;
	CEdit		*m_pLoDiffEdit;
	CEdit		*m_pHiDiffEdit;
	CEdit		*m_pPointXEdit;
	CEdit		*m_pPointYEdit;
	CEdit		*m_pValueRed;
	CEdit		*m_pValueGreen;
	CEdit		*m_pValueBlue;
	CButton		*m_pFixedRangeCheck;
	CButton		*m_pDiagonalCheck;

		// Save data
	CvScalar	m_oLoDiff;
	CvScalar	m_oHiDiff;
	CvPoint		m_oSeedPoint;
	CvScalar	m_oNewValue;
	BOOL		m_fFixedRange;
	BOOL		m_fDiagonal;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;
	BOOL		m_fEnableChange;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void MorphTypeChange();
	afx_msg void MorphKernelChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnChangeParam1();
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnEditChange();
	afx_msg void OnTimer(UINT);
	afx_msg void OnFixedRangeButton();
	afx_msg void OnDiagonalButton();


	DECLARE_MESSAGE_MAP()
};
