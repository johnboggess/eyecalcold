#pragma once
#include "opbase.h"
#include "Cv.h"

#define XorS_DEFAULT_WIDTH (200)
#define XorS_DEFAULT_HEIGHT (80)

class COpXorS : public COpBase
{
public:
	COpXorS(CMainView *pMain);
	~COpXorS(void);

	virtual	PTCHAR GetFriendlyName() { return L"XorS"; }
	virtual int	GetDefaultWidth() { return XorS_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return XorS_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pSubtract_ValueSlider;
	CEdit		*m_pSubtract_ValueEdit;

		// Save data
	int	m_nNumSubtract_Value;
	CvScalar	scalar;
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
