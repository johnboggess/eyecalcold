// EyeCompute.h : main header file for the EyeCompute application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CEyeComputeApp:
// See EyeCompute.cpp for the implementation of this class
//

class CEyeComputeApp : public CWinApp
{
public:
	CEyeComputeApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

public:
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CEyeComputeApp theApp;