#pragma once
#include "opbase.h"
#include "Cv.h"
#include "HighGui.h"

#define VideoFile_DEFAULT_WIDTH (200)
#define VideoFile_DEFAULT_HEIGHT (220)

class COpVideoFile : public COpBase
{
public:
	COpVideoFile(CMainView *pMain);
	~COpVideoFile(void);

	virtual	PTCHAR GetFriendlyName() { return L"VideoFile"; }
	virtual int	GetDefaultWidth() { return VideoFile_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return VideoFile_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CEdit		*m_pFileNameEdit;
	CButton		*m_pBrowseButton;
	CButton		*m_pPlayButton;
	CButton		*m_pStopButton;
	CButton		*m_pNextButton;
	CButton		*m_pPrevButton;
	CSliderCtrl	*m_pDelaySlider;
	CEdit		*m_pDelayEdit;

		// Save data
	TCHAR		m_aFileName[MAX_PATH];
	int			m_nDelay;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;
	BOOL		m_fPlayFlag;
	BOOL		m_fStopFlag;
	CvCapture	*m_pCapture;


	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();
	afx_msg void OnBrowseButton();
	afx_msg void OnPlayButton();
	afx_msg void OnStopButton();
	afx_msg void OnNextButton();
	afx_msg void OnPrevButton();

	DECLARE_MESSAGE_MAP()
};
