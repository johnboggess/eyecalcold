#pragma once
#include "opbase.h"
#include "Cv.h"

#define ThresholdAdaptive_DEFAULT_WIDTH (200)
#define ThresholdAdaptive_DEFAULT_HEIGHT (220)

class COpThresholdAdaptive : public COpBase
{
public:
	COpThresholdAdaptive(CMainView *pMain);
	~COpThresholdAdaptive(void);

	virtual	PTCHAR GetFriendlyName() { return L"Adaptive"; }
	virtual int	GetDefaultWidth() { return ThresholdAdaptive_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return ThresholdAdaptive_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CComboBox	*m_pThresholdTypeCombo;
	TCHAR		m_aThresholdTypeStr[30];
	CComboBox	*m_pAdaptiveTypeCombo;
	TCHAR		m_aAdaptiveTypeStr[30];
	CSliderCtrl	*m_pMaxValueSlider;
	CEdit		*m_pMaxValueEdit;
	CSliderCtrl	*m_pBlockSizeSlider;
	CEdit		*m_pBlockSizeEdit;
	CSliderCtrl	*m_pSubtractionConstantSlider;
	CEdit		*m_pSubtractionConstantEdit;

		// Save data
	int			m_nThresholdType;
	int			m_nAdaptiveMethod;
	int			m_nMaxValue;
	int			m_nBlockSize;
	int			m_nSubtractionConstant;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void ThresholdTypeChange();
	afx_msg void AdaptiveTypeChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
