#include "StdAfx.h"
#include "OpDrawContour.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bDrawContourFiller[32];
/*
#define DRAW_CONTOUR_WIDTH (512)
#define DRAW_CONTOUR_HEIGHT (512)
*/
//md%
#define DRAW_CONTOUR_WIDTH (640)
#define DRAW_CONTOUR_HEIGHT (480)


BEGIN_MESSAGE_MAP(COpDrawContour, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpDrawContour::COpDrawContour(CMainView *pMain) : COpBase(pMain)
{

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpDrawContour::~COpDrawContour(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpDrawContour::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpDrawContour::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpDrawContour::StartOpThread(void *pData)
{
	COpDrawContour *pBase = (COpDrawContour *) pData;

	pBase->OpThread();

	return 0;
}

void COpDrawContour::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpDrawContour::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpDrawContour::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpDrawContour::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);
fRetValue = TRUE;
/*
		if (WriteFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bDrawContourFiller, sizeof(bDrawContourFiller), &dwBytesWritten, NULL);
		}
*/
	}

	return fRetValue;
}

BOOL COpDrawContour::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

fRetValue = TRUE;
//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);
/*
		if (ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bDrawContourFiller, sizeof(bDrawContourFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
*/
	}

	return fRetValue;
}

	// We got data from the given input index
void COpDrawContour::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpDrawContour::ValidateParameters()
{
}

void COpDrawContour::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpDrawContour::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpDrawContour::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpDrawContour::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpDrawContour::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		CvSeq *pContour = (CvSeq *) LockData(L"DrawContourThread", DATA_CONTOUR, 0);

		if (pContour) 
		{
			if (1) {	// Validate Src and Dst images, etc
					// Calculate bounding rectangle of all contours in list
/*					
				CvSeq *pSeq = pContour;
				
				int x2 = 0, y2 = 0;
				
				while (pSeq)
				{
					CvRect Rect = cvBoundingRect(pSeq, 1);
					
					if (Rect.width+Rect.x > x2) x2 = Rect.width+Rect.x;
					if (Rect.height+Rect.y > y2) y2 = Rect.height+Rect.y;
					
					pSeq = pSeq->h_next;
				}
*/
				
				IplImage *pImage = cvCreateImage(cvSize(1024,1024), IPL_DEPTH_8U, 1);

				cvZero(pImage);
				
				cvDrawContours(pImage, pContour, cvScalarAll(255), cvScalarAll(255), 100);

				SetData(0, DATA_IMAGE, 0, pImage, TRUE);
				SendData();

			}
				else TitleError(L"");

			ReleaseData(L"DrawContourThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
