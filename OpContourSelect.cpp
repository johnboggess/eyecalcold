#include "StdAfx.h"
#include "OpContourSelect.h"
#include "OpBase.h"
#include "Cv.h"

#define SELECT_CONTOUR_MIN 0
#define SELECT_CONTOUR_MAX 100

#define SELECT_CONTOUR_SIZE_MIN (0)
#define SELECT_CONTOUR_SIZE_MAX (20)


BYTE bSelectContourFiller[32];

BEGIN_MESSAGE_MAP(COpSelectContour, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpSelectContour::COpSelectContour(CMainView *pMain) : COpBase(pMain)
{

	m_pSelectSlider = NULL;
	m_pSelectEdit = NULL;
	m_pSizeSlider = NULL;
	m_pSizeEdit = NULL;

	m_nCurContour = 0;
	m_nMinSize = 0;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpSelectContour::~COpSelectContour(void)
{
	if (m_pSelectSlider) delete m_pSelectSlider;
	if (m_pSelectEdit) delete m_pSelectEdit;
	if (m_pSizeSlider) delete m_pSizeSlider;
	if (m_pSizeEdit) delete m_pSizeEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpSelectContour::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpSelectContour::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
//	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"contour", TRUE, 0);
	m_pSelectEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pSelectSlider = CreateOpSlider(0, SELECT_CONTOUR_MIN, SELECT_CONTOUR_MAX);

	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"min size", TRUE, 0);
	m_pSizeEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pSizeSlider = CreateOpSlider(0, SELECT_CONTOUR_SIZE_MIN, SELECT_CONTOUR_SIZE_MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpSelectContour::StartOpThread(void *pData)
{
	COpSelectContour *pBase = (COpSelectContour *) pData;

	pBase->OpThread();

	return 0;
}

void COpSelectContour::UpdateSliders()
{
	m_pSelectSlider->SetPos(m_nCurContour);
	m_pSizeSlider->SetPos(m_nMinSize);

}
void COpSelectContour::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

	_itot(m_nCurContour, TStr, 10);
	m_pSelectEdit->SetWindowTextW(TStr);

	_itot(m_nMinSize, TStr, 10);
	m_pSizeEdit->SetWindowTextW(TStr);


}


void COpSelectContour::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpSelectContour::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nMinSize, sizeof(m_nMinSize), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nCurContour, sizeof(m_nCurContour), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bSelectContourFiller, sizeof(bSelectContourFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpSelectContour::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nMinSize, sizeof(m_nMinSize), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nCurContour, sizeof(m_nCurContour), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bSelectContourFiller, sizeof(bSelectContourFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpSelectContour::NotifyNewDataIndex(int InputIndex)
{
	CvSeq *pContour = (CvSeq *) LockData(L"SelectContourThread", DATA_CONTOUR, 0);

	int Count = 0;
	
	while (pContour && pContour->h_next)
	{
		Count++;
		pContour = pContour->h_next;
	}

	m_pSelectSlider->SetRangeMax(Count, TRUE);

	ReleaseData(L"SelectContourThread");  // We're done with the data so it can change if needed

	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpSelectContour::ValidateParameters()
{
}

void COpSelectContour::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pSelectSlider) 
		m_nCurContour = pSlider->GetPos();

	if (pSlider == m_pSizeSlider) 
	{
		m_nMinSize = pSlider->GetPos();

		CvSeq *pContour = (CvSeq *) LockData(L"SelectContourThread", DATA_CONTOUR, 0);

		if (pContour) 
		{
			CvSeq *pNewContour = pContour;

				// Count the contours that have this size or greater
			CvRect Rect = cvBoundingRect(pNewContour, 1);
			int Count = 0;
			
			while (pNewContour)
			{
				if (Rect.height > m_nMinSize*10 || Rect.width > m_nMinSize*10) Count++;
			 
				pNewContour = pNewContour->h_next;
			
				if (pNewContour) Rect = cvBoundingRect(pNewContour, 1);
			}

			m_pSelectSlider->SetRangeMax(Count, TRUE);

			ReleaseData(L"SelectContourThread");  // We're done with the data so it can change if needed
		}
	}

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpSelectContour::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpSelectContour::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpSelectContour::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpSelectContour::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		CvSeq *pContour = (CvSeq *) LockData(L"SelectContourThread", DATA_CONTOUR, 0);

		if (pContour) 
		{
			if (1) {	// Validate Src and Dst images, etc
				CvSeq *pNewContour = pContour;
				
				int Count = 0;
				do
				{
					if (pNewContour)
					{
						CvRect Rect = cvBoundingRect(pNewContour,1);
						
						while (pNewContour && Rect.height < m_nMinSize*10 && Rect.width < m_nMinSize*10)
						{
							pNewContour = pNewContour->h_next;
						
							if (pNewContour) Rect = cvBoundingRect(pNewContour, 1);
						}
						
						Count++;
						
						if (Count < m_nCurContour && pNewContour) pNewContour = pNewContour->h_next;
					}
				} while (pNewContour && Count < m_nCurContour);
				
				if (pNewContour)
				{
					CvMemStorage *pStorage = cvCreateMemStorage();
					
					pNewContour = cvCloneSeq(pNewContour, pStorage);

					SetData(0, DATA_CONTOUR, 0, pNewContour, TRUE, DATA_STORAGE_MEMSTORAGE, pStorage);
					SendData();
				}
			}
				else TitleError(L"");

			ReleaseData(L"SelectContourThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
