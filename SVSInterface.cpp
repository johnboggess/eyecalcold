#include "StdAfx.h"
#include "SVSInterface.h"
#include "HighGUI.h"
#include "OpBase.h"

#define IMG_HEADER_SIZE (10)
//#define SVS_IP_ADDRESS L"169.254.0.10"
#define SVS_IP_ADDRESS L"216.249.120.20"
#define SVS_LEFT_PORT 10001
#define SVS_RIGHT_PORT 10002

#define RETRY_DELAY (2000)

CSVSInterface::CSVSInterface(void)
{
	m_fSVSConnected = FALSE;
	m_dwLastConnectTryTime = 0;
	
	WSADATA wsaData;
	WORD version;
	
	version = MAKEWORD(2, 0);
	
	m_nStartupResult = WSAStartup(version, &wsaData);

	m_hLeftSocketMutex = CreateMutex(NULL, FALSE, L"");
	m_hRightSocketMutex = CreateMutex(NULL, FALSE, L"");


}

CSVSInterface::~CSVSInterface(void)
{
	m_oSocketLeft.Close();
	m_oSocketRight.Close();

	WSACleanup();
	
	CloseHandle(m_hLeftSocketMutex);

}

int CMySocket::Receive(void* lpBuf, int nBufLen, int nFlags)
{
	if (m_pbBlocking != NULL)
	{
		WSASetLastError(WSAEINPROGRESS);
		return  FALSE;
	}
	int nResult;
	int nTimeout = 2000;
	while (nTimeout && ((nResult = CAsyncSocket::Receive(lpBuf, nBufLen, nFlags)) == SOCKET_ERROR))
	{
		if (GetLastError() == WSAEWOULDBLOCK)
		{
			Sleep(1);
//			if (!PumpMessages(FD_READ))
//				return SOCKET_ERROR;
		}
		else
			return SOCKET_ERROR;
			
		nTimeout--;
	}
	return nResult;

}

CMySocket::CMySocket() { CSocket(); }

CMySocket::~CMySocket() { /*~CSocket();*/ }

BOOL CSVSInterface::Initialize()
{
	BOOL fRetValue = FALSE;
	
	if (m_nStartupResult == 0)	// no error
	{
		char ResultBuf[1024];
		
		if (m_oSocketLeft.Create(0, SOCK_STREAM))
		{
			if (m_oSocketLeft.Connect(SVS_IP_ADDRESS, SVS_LEFT_PORT))
			{
			
				int CharsSent = m_oSocketLeft.Send("V", 1);
				
				Sleep(10);

				int CharsReceived = m_oSocketLeft.Receive(ResultBuf, sizeof(ResultBuf));
			
				if (CharsSent && CharsReceived) 
				{
					fRetValue = true;

//					m_oSocketLeft.Send("c", 1);	// Set resolution to 640x480
//					m_oSocketLeft.Receive(ResultBuf, sizeof(ResultBuf));
					
				}
			
			}
			else	// Socket connection issue
			{
				DWORD Error = GetLastError();

				m_oSocketLeft.Close();
			} 
		
		}
		else	// Socket creation issue
		{
		} 

		if (fRetValue && m_oSocketRight.Create(0, SOCK_STREAM))
		{
			fRetValue = FALSE;
			
			if (m_oSocketRight.Connect(SVS_IP_ADDRESS, SVS_RIGHT_PORT))
			{
			
				int CharsSent = m_oSocketRight.Send("V", 1);

				Sleep(10);
				
				int CharsReceived = m_oSocketRight.Receive(ResultBuf, sizeof(ResultBuf));
			
				if (CharsSent && CharsReceived) fRetValue = true;
			
			}
			else	// Socket connection issue
			{
				DWORD Error = GetLastError();

				m_oSocketRight.Close();
			} 
		
		}
		else	// Socket creation issue
		{
		} 

	}
	
	m_fSVSConnected = fRetValue;
	
	return fRetValue;
}

IplImage *CSVSInterface::GetImage(GET_IMAGE_TYPE eImage)
{
	IplImage *pImage = NULL;

	if (m_fSVSConnected)
	{
		CMySocket *pSocket;
		
		if (eImage == GET_IMAGE_RIGHT)
		{
			WaitForSingleObject(m_hRightSocketMutex, INFINITE);
			pSocket = &m_oSocketRight;
		}
		else
		{
			WaitForSingleObject(m_hLeftSocketMutex, INFINITE);
			pSocket = &m_oSocketLeft;
		}
		uchar ResultBuf[128];
		
		Log(LOG_SOCKETS, L"Sending I");
		
		int CharsSent = pSocket->Send("I", 1);

		Log(LOG_SOCKETS, L"Sent %d characters", CharsSent);

		int CharsReceived = pSocket->Receive(ResultBuf, IMG_HEADER_SIZE);

		while (CharsReceived == -1) {
			Log(LOG_SOCKETS, L"Retrying");
		
			CharsReceived = pSocket->Receive(ResultBuf, IMG_HEADER_SIZE);
		}

		Log(LOG_SOCKETS, L"Received %d characters", CharsReceived);

		DWORD dwImageSize = ResultBuf[6] + ResultBuf[7] * 256 + ResultBuf[8] * 65536L + ResultBuf[9]*16777216L;

		if (CharsReceived > 0 && dwImageSize)
		{
			if (ResultBuf[0] = '#' && ResultBuf[1] == '#' && ResultBuf[2] == 'I')
			{
				CvMat *pMat = cvCreateMat(1, dwImageSize * 2, CV_8U);
				
				uchar *pData = cvPtr1D(pMat, 0);

				if (pData)
				{
					DWORD dwIndex = 0;
					BYTE bTimeout = 255;
					do {		
						Log(LOG_SOCKETS, L"Getting %d characters", dwImageSize-dwIndex);
			
						CharsReceived = pSocket->Receive(pData + dwIndex, dwImageSize-dwIndex);

						Log(LOG_SOCKETS, L"Got %d characters", CharsReceived);

						dwIndex += CharsReceived;
						bTimeout--;
					} while (dwIndex != dwImageSize && bTimeout);
					
					if (!bTimeout)
					{
						int a = 5;
					}

					Log(LOG_SOCKETS, L"Image Acquired %x", pImage);
					
					pImage = cvDecodeImage(pMat);
				}
				
				if (!pImage)
				{
					int a = 6;
				}
				
				cvReleaseMat(&pMat);
				
			}
				else
			{
				int a = 6;
			}
			
		}

		if (eImage == GET_IMAGE_LEFT)
			ReleaseMutex(m_hLeftSocketMutex);
		else
			ReleaseMutex(m_hRightSocketMutex);
	}
		else  // Retry connecting every so often
	{
	}
	
	return pImage;
}

void CSVSInterface::RetryInitialize()
{

	if (!m_fSVSConnected)
	{
		if (!m_dwLastConnectTryTime || GetTickCount() - m_dwLastConnectTryTime > RETRY_DELAY)
		{
			Initialize();

			m_dwLastConnectTryTime = GetTickCount();
			
		}
	}
}

	// Forward speed = 0 - 7F, Backward speed = 0xFF - 0x81, duration 0 = infinite, otherwise 10ms
BOOL CSVSInterface::MoveRobot(MOVE_TYPE eMove, DWORD dwSpeed, DWORD dwDuration)
{
	BOOL fRetValue = FALSE;


	DWORD Result = WaitForSingleObject(m_hLeftSocketMutex, INFINITE);

	char LeftSpeed, RightSpeed, Duration = dwDuration;
	char ControlStr[10];
	char ResultBuf[10];
	
	
	switch (eMove)
	{
		case MOVE_FORWARD:
			LeftSpeed = RightSpeed = (char) dwSpeed;
			break;
		case MOVE_STOP:
			LeftSpeed = RightSpeed = 0;
			break;
		case MOVE_BACKWARD:
			LeftSpeed = RightSpeed = (char) 255-dwSpeed;
			break;
		case MOVE_RIGHT:
			LeftSpeed = (char) dwSpeed; 
			RightSpeed = (char) 255-dwSpeed;
			break;
		case MOVE_LEFT:
			RightSpeed = (char) dwSpeed; 
			LeftSpeed = (char) 255-dwSpeed;
			break;

	}
	
	ControlStr[0] = 'M';
	ControlStr[1] = LeftSpeed;
	ControlStr[2] = RightSpeed;
	ControlStr[3] = (char) Duration;
	 
	int CharsSent = m_oSocketLeft.Send(ControlStr, 4);
		
	int CharsReceived = m_oSocketLeft.Receive(ResultBuf, sizeof(ResultBuf));
	
	if (CharsSent && CharsReceived) fRetValue = true;

	if (fRetValue)
	{
		fRetValue = FALSE;
/*md%		
		int CharsSent = m_oSocketLeft.Send(MoveStr, strlen(MoveStr));
			
		int CharsReceived = m_oSocketLeft.Receive(ResultBuf, sizeof(ResultBuf));
*/
		if (CharsSent && CharsReceived) fRetValue = true;
	}

	ReleaseMutex(m_hLeftSocketMutex);

	return fRetValue;
}

BOOL CSVSInterface::SendCommand(CAMERA_DEST_TYPE eCameraDest, char *CmdStr, char *pResultBuf, int nResultBufLen, int *pBytesReturned)
{
	BOOL fRetValue = FALSE;
	int CharsReceived = 0;

	if (eCameraDest == CAMERA_DEST_LEFT)
		WaitForSingleObject(m_hLeftSocketMutex, INFINITE);
	else if (eCameraDest == CAMERA_DEST_RIGHT)
		WaitForSingleObject(m_hRightSocketMutex, INFINITE);
	else // both cameras
	{
		WaitForSingleObject(m_hLeftSocketMutex, INFINITE);
		WaitForSingleObject(m_hRightSocketMutex, INFINITE);
	}
	
	int CharsSent = 0;

	if (eCameraDest == CAMERA_DEST_LEFT)
		CharsSent = m_oSocketLeft.Send(CmdStr, strlen(CmdStr));
	else if (eCameraDest == CAMERA_DEST_RIGHT)
		CharsSent = m_oSocketRight.Send(CmdStr, strlen(CmdStr));
	else   // send to both cameras
	{
		CharsSent = m_oSocketLeft.Send(CmdStr, strlen(CmdStr));
		CharsSent = m_oSocketRight.Send(CmdStr, strlen(CmdStr));
	}
		
	if (pResultBuf)
	{
		if (eCameraDest == CAMERA_DEST_LEFT)
			CharsReceived = m_oSocketLeft.Receive(pResultBuf, nResultBufLen);
		else if (eCameraDest == CAMERA_DEST_RIGHT)
			CharsReceived = m_oSocketRight.Receive(pResultBuf, nResultBufLen);
		else   // send to both cameras
		{
			int CharsReceived1 = m_oSocketLeft.Receive(pResultBuf, nResultBufLen);
			int CharsReceived2 = m_oSocketRight.Receive(pResultBuf+CharsReceived1, nResultBufLen);
			pResultBuf[CharsReceived1] = '\n';
			
			CharsReceived = CharsReceived1 + CharsReceived2;
		}

	}

	if (pBytesReturned)
	{
		if (CharsReceived > 0) *pBytesReturned = CharsReceived;
			else *pBytesReturned = 0;
		
	}
	
	if (eCameraDest == CAMERA_DEST_LEFT)
		ReleaseMutex(m_hLeftSocketMutex);
	else if (eCameraDest == CAMERA_DEST_RIGHT)
		ReleaseMutex(m_hRightSocketMutex);
	else // both cameras
	{
		ReleaseMutex(m_hLeftSocketMutex);
		ReleaseMutex(m_hRightSocketMutex);
	}

	if (CharsSent) fRetValue = true;
	
	return fRetValue;
}