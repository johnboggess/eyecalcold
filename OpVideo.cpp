#include "StdAfx.h"
#include "OpVideo.h"
#include "OpBase.h"
#include "Cv.h"
#include "HighGui.h"

BYTE bVideoFiller[32];

#define MIN_DELAY (0)
#define MAX_DELAY (5000)

#define MAX_AVI_FPS (10)

#define ID_PAUSE_BUTTON (101)
#define ID_SAVE_BUTTON (102)
#define ID_BROWSE_BUTTON (103)
#define ID_VIDEO_SOURCE (104)

BEGIN_MESSAGE_MAP(COpVideo, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_VIDEO_SOURCE, OnVideoSourceChange)
	ON_COMMAND(ID_PAUSE_BUTTON, OnPauseButton)
	ON_COMMAND(ID_SAVE_BUTTON, OnSaveButton)
	ON_COMMAND(ID_BROWSE_BUTTON, OnBrowseButton)
END_MESSAGE_MAP()

COpVideo::COpVideo(CMainView *pMain) : COpBase(pMain)
{
	m_pSourceCombo = NULL;
	m_pFileNameEdit = NULL;
	m_pBrowseButton = NULL;
	m_pDelaySlider = NULL;
	m_pDelayEdit = NULL;
	m_pPauseButton = NULL;
	m_pSaveButton = NULL;
	m_pFPSEdit = NULL;

	m_nDelay = 0;
	m_fPaused = FALSE;
	m_fSaveToFile = FALSE;
	m_aSaveFileName[0] = 0;
	m_eVideoSource = VIDEO_SOURCE_NONE;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpVideo::~COpVideo(void)
{
	if (m_pSourceCombo) delete m_pSourceCombo;
	if (m_pFileNameEdit) delete m_pFileNameEdit;
	if (m_pBrowseButton) delete m_pBrowseButton;
	if (m_pDelaySlider) delete m_pDelaySlider;
	if (m_pDelayEdit) delete m_pDelayEdit;
	if (m_pPauseButton) delete m_pPauseButton;
	if (m_pSaveButton) delete m_pSaveButton;
	if (m_pFPSEdit) delete m_pFPSEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);	// Tell thread that new data is available
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpVideo::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpVideo::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_pSourceCombo = CreateOpCombo(0, ID_VIDEO_SOURCE);

	CComboBox *pCombo = m_pSourceCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"Camera 0");
		pCombo->SetItemData(Ndx, VIDEO_SOURCE_0);
		Ndx = pCombo->AddString(L"Camera 1");
		pCombo->SetItemData(Ndx, VIDEO_SOURCE_1);
		Ndx = pCombo->AddString(L"SVS Left");
		pCombo->SetItemData(Ndx, VIDEO_SOURCE_SVS_LEFT);
		Ndx = pCombo->AddString(L"SVS Right");
		pCombo->SetItemData(Ndx, VIDEO_SOURCE_SVS_RIGHT);
	}

	CreateOpStatic(L"Delay", TRUE, 0);
	m_pDelayEdit = CreateOpEdit(PARAM_EDIT_WIDTH*2, TRUE);
	m_pDelaySlider = CreateOpSlider(0, MIN_DELAY, MAX_DELAY);
	CreateOpStatic(L"", TRUE, 0);
	m_pSaveButton = CreateOpButton(L"Save to file", 100, BS_AUTOCHECKBOX, ID_SAVE_BUTTON);
	m_pBrowseButton = CreateOpButton(L"Browse", 0, 0, ID_BROWSE_BUTTON);
	m_pFileNameEdit = CreateOpEdit(0, TRUE); 

	CreateOpStatic(L" ", FALSE, 45);
	m_pPauseButton = CreateOpButton(L"Pause", 70, 0, ID_PAUSE_BUTTON);
	CreateOpStatic(L" ", FALSE, 20);
	m_pFPSEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpVideo::StartOpThread(void *pData)
{
	COpVideo *pBase = (COpVideo *) pData;

	pBase->OpThread();

	return 0;
}

void COpVideo::UpdateSliders()
{
	m_pDelaySlider->SetPos(m_nDelay);

}
void COpVideo::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pSourceCombo->GetCount(); Ndx++)
		if (m_pSourceCombo->GetItemData(Ndx) == m_eVideoSource) break;

	m_pSourceCombo->SetCurSel(Ndx);
//	m_pSourceCombo->GetLBText(Ndx, m_aStr);

	_itot(m_nDelay, TStr, 10);
	m_pDelayEdit->SetWindowTextW(TStr);

	if (m_fSaveToFile) m_pSaveButton->SetCheck(BST_CHECKED);
		else m_pSaveButton->SetCheck(BST_UNCHECKED);

	if (m_fPaused) m_pPauseButton->SetWindowTextW(L"Play");
		else m_pPauseButton->SetWindowTextW(L"Pause");
		
	if (m_aSaveFileName[0]) 
	{
		PTCHAR pChar = m_aSaveFileName + _tcslen(m_aSaveFileName) - 1;
		
		while (pChar > m_aSaveFileName && *pChar && *pChar != '\\') pChar--;
		
		if (*pChar == '\\') pChar++;	// Move to first character of file name
		
		TCHAR FileName[MAX_PATH];
		
		_tcscpy(FileName, pChar);
		
		m_pFileNameEdit->SetWindowTextW(FileName);
	}
	else m_pFileNameEdit->SetWindowTextW(L"");
}


void COpVideo::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpVideo::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nDelay, sizeof(m_nDelay), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_fPaused, sizeof(m_fPaused), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_eVideoSource, sizeof(m_eVideoSource), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_fSaveToFile, sizeof(m_fSaveToFile), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_aSaveFileName, sizeof(m_aSaveFileName), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bVideoFiller, sizeof(bVideoFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpVideo::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nDelay, sizeof(m_nDelay), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_fPaused, sizeof(m_fPaused), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_eVideoSource, sizeof(m_eVideoSource), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_fSaveToFile, sizeof(m_fSaveToFile), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_aSaveFileName, sizeof(m_aSaveFileName), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bVideoFiller, sizeof(bVideoFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
			
			SetEvent(m_hThreadEvent);	// May need to update video source

		
		}

	}
	return fRetValue;
}

	// We got data from the given input index
void COpVideo::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpVideo::ValidateParameters()
{
}

void COpVideo::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pDelaySlider) 
		m_nDelay = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpVideo::OnVideoSourceChange()
{

	int Ndx = m_pSourceCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_eVideoSource = (VIDEO_SOURCE_TYPE) m_pSourceCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}

}

void COpVideo::OnPauseButton()
{

	if (m_fPaused) // starting again
	{
		SetEvent(m_hThreadEvent);	// Tell thread to start again
		m_pPauseButton->SetWindowText(L"Pause");
	}
		else	// pausing
	{
		m_pPauseButton->SetWindowText(L"Play");
	}
	
	m_fPaused = !m_fPaused;

}

void COpVideo::OnSaveButton()
{

	m_fSaveToFile = !m_fSaveToFile;
}

void COpVideo::OnBrowseButton()
{
	CFileDialog *pDialog = new CFileDialog(FALSE, L".avi", NULL, 0, L"avi|*.avi||", NULL );

	if (pDialog->DoModal() == IDOK) {
		TCHAR TStr[MAX_PATH];

		_tcscpy(m_aSaveFileName, pDialog->GetPathName());

		UpdateValues();	// Display new file name
	}

	delete pDialog;

}

void COpVideo::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpVideo::OpThread()
{
	m_fThreadRunning = true;

	VIDEO_SOURCE_TYPE eCurSource = VIDEO_SOURCE_NONE;

	CvCapture *pCapture = NULL;
	CvVideoWriter *pWriter = NULL;
	BOOL	fDeallocateWhenDone = FALSE;

	int PrevFrameTime = 0;
	DWORD dwStartTime = 0;
	int fps = 0;
	int fpsCounter = 0;
	
	while (m_fThreadRunning)
	{
		IplImage *pImage = NULL;

		if (m_eVideoSource != eCurSource)
		{
			switch(eCurSource)	// Close down old source
			{
				case VIDEO_SOURCE_NONE:
					break;
				case VIDEO_SOURCE_0:
				case VIDEO_SOURCE_1:
					if (pCapture) cvReleaseCapture(&pCapture);
					break;
				case VIDEO_SOURCE_SVS_LEFT:
				case VIDEO_SOURCE_SVS_RIGHT:
					break;
			}

			switch(m_eVideoSource)	// Start new source
			{
				case VIDEO_SOURCE_NONE:
					break;
				case VIDEO_SOURCE_0:
					pCapture = cvCaptureFromCAM(0);
					fDeallocateWhenDone = FALSE;
					break;
				case VIDEO_SOURCE_1:
					pCapture = cvCaptureFromCAM(1);
					fDeallocateWhenDone = FALSE;
					break;
				case VIDEO_SOURCE_SVS_LEFT:
					fDeallocateWhenDone = TRUE;
					break;
				case VIDEO_SOURCE_SVS_RIGHT:
					fDeallocateWhenDone = TRUE;
					break;
			}
			
			eCurSource = m_eVideoSource;
		}
			
		switch(m_eVideoSource)	// Get the next frame
		{
			case VIDEO_SOURCE_NONE:
				break;
			case VIDEO_SOURCE_0:
			case VIDEO_SOURCE_1:
				if (pCapture) pImage = cvQueryFrame(pCapture);
				break;
			case VIDEO_SOURCE_SVS_LEFT:
				pImage = m_pMain->GetInterface()->GetImage(GET_IMAGE_LEFT);	// left image
				break;
			case VIDEO_SOURCE_SVS_RIGHT:
				pImage = m_pMain->GetInterface()->GetImage(GET_IMAGE_RIGHT);	// right image
				break;
		}
				

		if (pImage) 
		{
			if (!fpsCounter)
			{
				dwStartTime = GetTickCount();
				fpsCounter = 1;
			}
			else if (fpsCounter == 1)
			{
				PrevFrameTime = GetTickCount()-dwStartTime;
				dwStartTime = GetTickCount();
				
				fpsCounter = 2;
			}
			else
			{
				double gDiv = ((PrevFrameTime + (GetTickCount()-dwStartTime)) / 2);
				
				if (gDiv)
				{
					fps = (int) (1000 / gDiv);
				
					if (!fps) fps = 1;
					
					TCHAR TStr[10];
					
					_itot(fps, TStr, 10);
					m_pFPSEdit->SetWindowTextW(TStr);
					m_pFPSEdit->UpdateWindow();
				}
								
				fpsCounter = 0;
			}

			if (m_fSaveToFile)	// Check for save-to-file functionality
			{
				if (!pWriter && fps)
				{
					if (m_aSaveFileName[0])
					{
						char FileName[MAX_PATH];
						
						wcstombs(FileName, m_aSaveFileName, MAX_PATH);
						
						pWriter = cvCreateVideoWriter(FileName, 0, fps, cvGetSize(pImage));
					}
					else TitleError(L"Save file name required");
					
					if (!pWriter) TitleError(L"Error creating AVI writer");
				}
			}
				else	// Check to see if we want to stop writing to file
			{
				if (pWriter)	// We are currently writing, so stop
				{
					cvReleaseVideoWriter(&pWriter);
					pWriter = NULL;
				}
				
				TitleError(L"");
			}

			if (1) {	
				if (m_fSaveToFile && pWriter)
					cvWriteFrame(pWriter, pImage);

				SetData(0, DATA_IMAGE, 0, pImage, fDeallocateWhenDone);
				SendData();

			}
				else TitleError(L"");

			ReleaseData(L"VideoThread");  // We're done with the data so it can change if needed
			
		
		}
			else 
		{
			TitleError(L"Unable to connect to video source");
			
			Sleep(1000);
		}
			
		if (m_fThreadRunning)
		{
			if (m_fPaused || m_eVideoSource == VIDEO_SOURCE_NONE)
				WaitForSingleObject(m_hThreadEvent, INFINITE);
			else if (m_nDelay) 
				Sleep(m_nDelay);
		}
	}
	
	if (pWriter)	// We are currently writing, so stop
	{
		cvReleaseVideoWriter(&pWriter);
		pWriter = NULL;
	}

}
