// ChildView.cpp : implementation of the CMainView class
//

#include "stdafx.h"
#include "EyeCalc.h"
#include "ChildView.h"
#include "SVSInterface.h"
#include "OpBase.h"
#include "HighGUI.h"
#include "OpSelDlg.h"
#include "OpImage.h"
#include "OpSmooth.h"
#include "OpMorphology.h"
#include "OpFloodFill.h"
#include "OpThreshold.h"
#include "OpConvertColor.h"
#include "OpThresholdAdaptive.h"
#include "OpResize.h"
#include "OpPyramid.h"
#include "OpTranspose.h"
#include "OpAdd.h"
#include "OpPyrSegment.h"
#include "OpSobel.h"
#include "OpScale.h"
#include "OpLaplace.h"
#include "OpCanny.h"
#include "OpHoughLines.h"
#include "OpHoughCircles.h"
#include "OpWarpPerspective.h"
#include "OpLogPolar.h"
#include "OpMatrix.h"
#include "OpFilter2D.h"
#include "OpVideo.h"
#include "OpVideoFile.h"
#include "OpSVSControl.h"
#include "OpIntegral.h"
#include "OpDCT.h"
#include "OpHistEqualize.h"
#include "OpHistDisplay.h"
#include "OpHistCalc.h"
#include "OpAnaglyph.h"
#include "OpBackProject.h"
#include "OpMatchTemplate.h"
#include "OpFindContour.h"
#include "OpMerge.h"
#include "OpDrawContour.h"
#include "OpContourSelect.h"
#include "OpApproxPoly.h"
#include "OpMoments.h"
#include "OpStereoCalib.h"
#include "OpStereoRectify.h"
#include "OpStereoDepth.h"
#include "OpAbs.h"
#include "OpAbsDiff.h"
#include "OpAbsDiffS.h"
#include "OpAddS.h"
#include "OpAnd.h"
#include "OpAndS.h"
#include "OpAddWeighted.h"
#include "OpConvertScale.h"
#include "OpConvertScaleAbs.h"
#include "OpCopy.h"
#include "OpDiv.h"
#include "OpFlip.h"
#include "OpInRange.h"
#include "OpMul.h"
#include "OpMax.h"
#include "OpMaxS.h"
#include "OpMin.h"
#include "OpMinS.h"
#include "OpTryout.h"
#include "OpCmp.h"
#include "OpCmpS.h"
#include "OpInRangeS.h"
#include "OpNot.h"
#include "OpNormalize.h"
#include "OpOr.h"
#include "OpOrS.h"
#include "OpRepeat.h"
#include "OpSplit.h"
#include "OpSub.h"
#include "OpSubS.h"
#include "OpSubRS.h"
#include "OpXor.h"
#include "OpXorS.h"
#include "OpInvert.h"
#include "OpCentralMoments.h"
#include "OpNormalizedCentralMoments.h"
#include "OpHuMoments.h"
#include "OpMatchShapes.h"
#include "OpHierarchicalMatch.h"
#include "OpCalcPGH.h"
#include "OpBackGroundSub.h"
#include "OpFlip.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define ID_TAB_CTRL (101)
#define TAB_HEIGHT (20)
#define NUM_TABS (10)

#define OP_DIALOG_HEIGHT (250)

/*extern "C" const PTCHAR g_aOpList = L"Image|Morphology|FloodFill|ReSize|Smooth|Threshold|ConvertColor|ThresholdAdaptive|\
Pyramid|Transpose|Abs|Add|PyrSegment|Sobel|Scale|Laplace|Canny|HoughLines|HoughCircles|WarpPerspective|LogPolar|Matrix|\
Filter2D|Video|VideoFile|SVSControl|Integral|DCT|HistEqualize|HistDisplay|HistCalc|Anaglyph|BackProject|\
MatchTemplate|ContourFind|Merge|ContourDraw|ContourSelect|ApproxPoly|Moments|StereoCalib|StereoRectify|\
StereoDepth|AbsDiff|AbsDiffS|AddS|And|AndS|AddWeighted|ConvertScale|ConvertScaleAbs|Copy|Divide|Flip|InRange|Multiply|\
Max|MaxS|Min|MinS|Tryout|Compare|CompareS|InRangeS|Not|Normalize|Or|OrS|Repeat|Split|Subtract|SubtractS|SubtractRS|\
Xor|XorS|Invert|CentralMoments|NormalizedCMoments|HuMoments|MatchShapes|HierarchiMatch|CalcPGH|BackGroundSub";*/
extern "C" const PTCHAR g_aOpList = L"Image|Morphology|FloodFill|ReSize|Smooth|Threshold|ConvertColor|ThresholdAdaptive|\
Pyramid|Add|PyrSegment|Sobel|Scale|Laplace|Canny|HoughLines|HoughCircles|WarpPerspective|LogPolar|Matrix|\
Filter2D|Video|VideoFile|SVSControl|Integral|DCT|HistEqualize|HistDisplay|HistCalc|Anaglyph|BackProject|\
MatchTemplate|ContourFind|Merge|ContourDraw|ContourSelect|ApproxPoly|Moments|StereoCalib|StereoRectify|\
StereoDepth|Flip";
// CMainView

int MyError(int status, const char* func_name, const char* err_msg, const char* file_name, int line, void *);


void CMainView::InitVars()
{
	memset(pOpBases, 0, sizeof(pOpBases));
	m_nNumBases = 0;
	m_pDeletedOpBase = NULL;
	dwMaxUniqueID = 1;
	m_fBoardChanged = FALSE;
	m_fDragging = FALSE;
	m_nXScroll = 0;
	
}

CMainView::CMainView()
{
	InitVars();

	m_pTabs = NULL;
	m_aSaveFileName[0] = 0;
	
	cvRedirectError(MyError);
}

CMainView::~CMainView()
{
}


BEGIN_MESSAGE_MAP(CMainView, CWnd)
	ON_WM_PAINT()
	ON_WM_CHAR()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_HSCROLL()
	ON_COMMAND(ID_FILE_SAVE32774, &CMainView::OnFileSave32774)
	ON_COMMAND(ID_FILE_SAVEAS, OnFileSaveAs)
	ON_COMMAND(ID_FILE_LOAD, &CMainView::OnFileLoad)
	ON_COMMAND(ID_FILE_NEW32776, &CMainView::OnFileNew32776)
	ON_COMMAND(ID_FILE_CONNECTSVS, OnConnectSVS)
	ON_NOTIFY(TCN_SELCHANGE, ID_TAB_CTRL, OnTabCtrl)
	
END_MESSAGE_MAP()



// CMainView message handlers

BOOL CMainView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

int CMainView::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
//md%
//	m_oSVS.Initialize();

	m_pTabs = new CTabCtrl();
	
	TC_ITEM TcItem;
	
	CDC *pDc = GetDC();
	
	int nWidth = GetDeviceCaps(pDc->m_hDC, HORZRES);
	
	ReleaseDC(pDc);
	
	m_pTabs->Create(WS_CHILD | WS_VISIBLE | TCS_TABS, CRect(0, 0, nWidth, TAB_HEIGHT), this, ID_TAB_CTRL);
	
	for (int i = 0; i < NUM_TABS; i++)
	{
		PTCHAR pTStr = new TCHAR[4];
		
		_stprintf(pTStr, L"%d", i+1);
		TcItem.mask = TCIF_TEXT;
		TcItem.cchTextMax = _tcslen(pTStr) * sizeof(TCHAR) + 1;
		TcItem.pszText = pTStr;

		m_pTabs->InsertItem(i, &TcItem);
	}
	
	m_pTabs->SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	m_pTabs->ShowWindow(SW_SHOW);
	
	return 0;
}


void CMainView::OnClose()
{
	CWnd::OnClose();
}

void CMainView::DestroyAllOps()
{
	int nNumBases = m_nNumBases;
	
	COpBase *pTempOpBases[MAX_OPBASES];

	for (int i = 0; i < nNumBases; i++)
	{
		pTempOpBases[i] = pOpBases[i];
		
		pOpBases[i]->StopBase();
	}

	for (int i = 0; i < nNumBases; i++)
	{
		pTempOpBases[i]->DeleteBase(FALSE);
		
		delete pTempOpBases[i];
	}
}

void CMainView::OnDestroy()
{
	DestroyAllOps();
	
	CWnd::OnDestroy();
}


// CInputDlg message handlers

void CMainView::OnPaint() 
{
	static BOOL fLoaded = FALSE;

	if (!fLoaded) { 
		_tcscpy(m_aSaveFileName, L"C:\\Users\\haivo\\Desktop\\Job\\1.cvs");
		LoadFile(m_aSaveFileName);
		fLoaded = TRUE; 
	}

	CPaintDC dc(this); // device context for painting

	if (m_pDeletedOpBase) { m_pDeletedOpBase->DestroyWindow(); m_pDeletedOpBase = NULL; }


	//iterate through operators, draw connections
	for (int i = 0; i < m_nNumBases; i++){
			// Draw connection INPUTS
		for (int j = 0; j < pOpBases[i]->GetNumConnections(CONNECT_INPUT); j++) {
			dc.MoveTo(pOpBases[i]->GetConnectionPoint(CONNECT_INPUT, j, 1), pOpBases[i]->GetConnectionPoint(CONNECT_INPUT, j, 0));

			COpBase *pDst = pOpBases[i]->GetConnection(CONNECT_INPUT, j);

			if (pDst) {
				for (int k = 0; k < pDst->GetNumConnections(CONNECT_OUTPUT); k++)
					if (pDst->GetConnection(CONNECT_OUTPUT, k) == pOpBases[i]) {
						dc.LineTo(pDst->GetConnectionPoint(CONNECT_OUTPUT, k, 1), pDst->GetConnectionPoint(CONNECT_OUTPUT, k, 0));
						break;
					}
			}
		}
	}

		// Draw page vertical indicators
		
	RECT Rect;
	int MaxX = dc.GetDeviceCaps(HORZRES);
	
	GetClientRect(&Rect);

	CPen Pen(PS_DASH, 1, RGB(0, 0, 0));
	CPen *pOldPen = dc.SelectObject(&Pen);

	int X = MaxX + (m_nXScroll % MaxX);
	
	dc.MoveTo(X, 0);
	dc.LineTo(X, Rect.bottom);

	dc.SelectObject(pOldPen);

}

void CMainView::OnChar( UINT nChar, UINT a, UINT b)
{
	return;
}

COpBase *CMainView::CreateOperator(int nOperatorIndex, RECT Rect) {

	COpBase *pBase = NULL;

	switch (nOperatorIndex) {
		case OP_IMAGE: // 0 - Image
			pBase = new COpImage(this);
			break;
		case 1: // Morphology
			pBase = new COpMorphology(this);
			break;
		case 2: // FloodFill
			pBase = new COpFloodFill(this);
			break;
		case 3: // Resize
			pBase = new COpResize(this);
			break;
		case 4: // Smooth
			pBase = new COpSmooth(this);
			break;
		case 5: // Threshold
			pBase = new COpThreshold(this);
			break;
		case 6: // ConvertColor
			pBase = new COpConvertColor(this);
			break;
		case 7: // Threshold adaptive
			pBase = new COpThresholdAdaptive(this);
			break;
		case 8: // Pyramid
			pBase = new COpPyramid(this);
			break;
		case 9: // Transpose
			//pBase = new COpTranspose(this);
			break;
		case 10: // Abs
			//pBase = new COpAbs(this);
			break;
		case 11: // Add
			pBase = new COpAdd(this);
			break;
		case 12: // PyrSegment
			pBase = new COpPyrSegment(this);
			break;
		case 13: // Sobel
			pBase = new COpSobel(this);
			break;
		case 14: // Scale
			pBase = new COpScale(this);
			break;
		case 15: // Laplace
			pBase = new COpLaplace(this);
			break;
		case 16: // Canny
			pBase = new COpCanny(this);
			break;
		case 17: // Hough lines
			pBase = new COpHoughLines(this);
			break;
		case 18: // Hough circles
			pBase = new COpHoughCircles(this);
			break;
		case 19: // Warp perspective
			pBase = new COpWarpPerspective(this);
			break;
		case 20: // LogPolar
			pBase = new COpLogPolar(this);
			break;
		case 21: // Matrix
			pBase = new COpMatrix(this);
			break;
		case 22: // Filter 2D
			pBase = new COpFilter2D(this);
			break;
		case 23: // Video
			pBase = new COpVideo(this);
			break;
		case 24: // VideoFile
			pBase = new COpVideoFile(this);
			break;
		case 25: // SVSControl
			pBase = new COpSVSControl(this);
			break;
		case 26: // Integral
			pBase = new COpIntegral(this);
			break;
		case 27: // DCT
			pBase = new COpDCT(this);
			break;
		case 28: // HistEqualize
			pBase = new COpHistEqualize(this);
			break;
		case 29: // HistDisplay
			pBase = new COpHistDisplay(this);
			break;
		case 30: // HistCalc
			pBase = new COpHistCalc(this);
			break;
		case 31: // Anaglyph
			pBase = new COpAnaglyph(this);
			break;
		case 32: // BackProject
			pBase = new COpBackProject(this);
			break;
		case 33: // MatchTemplate
			pBase = new COpMatchTemplate(this);
			break;
		case 34: // FindContour
			pBase = new COpFindContour(this);
			break;
		case 35: // Merge
			pBase = new COpMerge(this);
			break;
		case 36: // DrawContour
			pBase = new COpDrawContour(this);
			break;
		case 37: // SelectContour
			pBase = new COpSelectContour(this);
			break;
		case 38: // ApproxPoly
			pBase = new COpApproxPoly(this);
			break;
		case 39: // Moments
			pBase = new COpMoments(this);
			break;
		case 40: // StereoCalib
			pBase = new COpStereoCalib(this);
			break;
		case 41: // StereoRectify
			pBase = new COpStereoRectify(this);
			break;
		case 42: // StereoDepth
			pBase = new COpStereoDepth(this);
			break;
		case 43: // Flip
			pBase = new COpFlip(this);
			break;
		/*case 43: // AbsDiff
			//pBase = new COpAbsDiff(this);
			break;
		case 44: // AbsDiffS
			//pBase = new COpAbsDiffS(this);
			break;
		case 45: // AddS
			//pBase = new COpAddS(this);
			break;
		case 46: // And
			//pBase = new COpAnd(this);
			break;
		case 47: // AndS
			//pBase = new COpAndS(this);
			break;
		case 48: // AddWeighted
			//pBase = new COpAddWeighted(this);
			break;
		case 49: // ConvertScale
			//pBase = new COpConvertScale(this);
			break;
		case 50: // ConvertScaleAbs
			//pBase = new COpConvertScaleAbs(this);
			break;
		case 51: // Copy
			//pBase = new COpCopy(this);
			break;
		case 52: // Division
			//pBase = new COpDiv(this);
			break;
		case 53: // Flip
			//pBase = new COpFlip(this);
			break;
		case 54: // InRange
			//pBase = new COpInRange(this);
			break;
		case 55: // Multiply
			//pBase = new COpMul (this);
			break;
		case 56: // Max
			//pBase = new COpMax (this);
			break;
		case 57: // MaxS
			//pBase = new COpMaxS (this);
			break;
		case 58: // Min
			//pBase = new COpMin (this);
			break;
		case 59: // MinS
			//pBase = new COpMinS (this);
			break;
		case 60: // Tryout
			//pBase = new COpTryout (this);
			break;
		case 61: // Compare
			//pBase = new COpCmp (this);
			break;
		case 62: // CompareS
			//pBase = new COpCmpS (this);
			break;
		case 63: // InRangeS
			//pBase = new COpInRangeS (this);
			break;
		case 64: // Not
			//pBase = new COpNot (this);
			break;
		case 65: // Normalize
			//pBase = new COpNormalize (this);
			break;
		case 66: // Or
			//pBase = new COpOr (this);
			break;
		case 67: // OrS
			//pBase = new COpOrS (this);
			break;
		case 68: // Repeat
			//pBase = new COpRepeat (this);
			break;
		case 69: // Split
			//pBase = new COpSplit (this);
			break;
		case 70: // Sub
			//pBase = new COpSub (this);
			break;
			break;
		case 71: // SubS
			//pBase = new COpSubS (this);
			break;
			break;
		case 72: // SubRS
			//pBase = new COpSubRS (this);
			break;
		case 73: // Xor
			//pBase = new COpXor (this);
			break;
		case 74: // XorS
			//pBase = new COpXorS (this);
			break;
		case 75: // Invert
			//pBase = new COpInvert (this);
			break;
		case 76: // Central Moments
			//pBase = new COpCentralMoments (this);
			break;
		case 77: // Normalized Central Moments
			//pBase = new COpNormalizedCentralMoments (this);
			break;
		case 78: // Hu Moments
			//pBase = new COpHuMoments (this);
			break;
		case 79: // Match Shapes
			//pBase = new COpMatchShapes (this);
			break;
		case 80: //Hierarchical Match
			//pBase = new COpHierarchicalMatch (this);
			break;
		case 81: //Pairwise Geometrical Histograms
			//pBase = new COpCalcPGH (this);
			break;
		case 82: //Back Ground Subtraction using Averaging mode
			//pBase = new COpBackGroundSub (this);
			break;*/
		default:
			pBase = new COpBase(this);
	}

	if (!Rect.right) {  // Get width and height from object itself
		Rect.right = Rect.left + pBase->GetDefaultWidth();
		Rect.bottom = Rect.top + pBase->GetDefaultHeight();
	}
	
	if (pBase && pBase->Create(NULL, L"OpBase", WS_CHILD | WS_VISIBLE, Rect, this, 1))
	{
		pBase->ShowWindow(SW_SHOW);

		pOpBases[m_nNumBases] = pBase;
		m_nNumBases++;

		pBase->SetUniqueID(dwMaxUniqueID);
		pBase->SetOperator(nOperatorIndex);

		dwMaxUniqueID++;

	}

	m_fBoardChanged = TRUE;

	return pBase;
}

// ChildView.cpp : implementation file
//
	// Create new OpBase object
void CMainView::OnLButtonDblClk( UINT nFlags, CPoint point )
{
	if (m_nNumBases < MAX_OPBASES) {

		int OperatorNdx;

		CPoint DialogPoint = point;

		ClientToScreen(&DialogPoint);
		
		RECT Rect;
		
		GetClientRect(&Rect);
		
		if (DialogPoint.y > Rect.bottom - OP_DIALOG_HEIGHT) DialogPoint.y = Rect.bottom - OP_DIALOG_HEIGHT;

		COpSelDlg *pDialog = new COpSelDlg(DialogPoint, &OperatorNdx, g_aOpList);

		if (pDialog->DoModal() == IDOK) {
			COpBase *pBase = CreateOperator(OperatorNdx, CRect(point.x, point.y, 0, 0));  //point.x+BASE_OBJECT_WIDTH, point.y+BASE_OBJECT_HEIGHT));

		}

		delete pDialog;
		
	}

	return;
}

BOOL CMainView::DeleteOpBase(COpBase *pBase)
{
	BOOL fRetValue = FALSE;

	m_pDeletedOpBase = pBase;

	for (int i = 0; i < m_nNumBases; i++)  // Find this base in our list
	{
		if (pOpBases[i] == pBase){
			for (int j = i; j < m_nNumBases; j++)	// Remove this OpBase from our list
				pOpBases[j] = pOpBases[j+1];

			m_nNumBases--;

			fRetValue = TRUE;
		}
	}

	return fRetValue;
}

void CMainView::OnFileSaveAs()
{
	CFileDialog *pDialog = new CFileDialog(FALSE, L".cvf", NULL, 0, L"cvf|*.cvf||", NULL );

	if (pDialog->DoModal() == IDOK) {
		_tcscpy(m_aSaveFileName, pDialog->GetPathName());
	
		OnFileSave32774();	// Save with new filename
	}
}

void CMainView::OnFileSave32774()
{
	HANDLE hFile = CreateFile(m_aSaveFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dwBytesWritten;

	if (hFile != INVALID_HANDLE_VALUE) {
		WriteFile(hFile, &m_nXScroll, sizeof(m_nXScroll), &dwBytesWritten, NULL);
			// Save Number of bases instantiated
		WriteFile(hFile, &m_nNumBases, sizeof(m_nNumBases), &dwBytesWritten, NULL);
			// Save rectangles for all bases
		for (int i = 0; i < m_nNumBases; i++)  // Find this base in our list
		{
			RECT Rect;
			int OperatorNdx = pOpBases[i]->GetOperator();

			pOpBases[i]->GetMaximizedWindowRect(&Rect);

			RECT ParentRect;

			GetClientRect(&ParentRect);
			ClientToScreen(&ParentRect);

			Rect.top -= ParentRect.top;
			Rect.bottom -= ParentRect.top;

			WriteFile(hFile, &Rect, sizeof(Rect), &dwBytesWritten, NULL);
			WriteFile(hFile, &OperatorNdx, sizeof(OperatorNdx), &dwBytesWritten, NULL);
		}

		WriteFile(hFile, &dwMaxUniqueID, sizeof(dwMaxUniqueID), &dwBytesWritten, NULL);

		// Save all base-specific data
		for (int i = 0; i < m_nNumBases; i++)  // Find this base in our list
			pOpBases[i]->SaveToStream(hFile);

		CloseHandle(hFile);

		m_fBoardChanged = FALSE;
	}
	else MessageBox(L"Unable to save to file", L"Error", MB_OK);
}

void CMainView::LoadFile(PTCHAR pFileName)
{
	HANDLE hFile = CreateFile(pFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD dwBytesWritten;

	for (int i = 0; i < m_nNumBases; i++)
		pOpBases[i]->DestroyWindow();
	
	InitVars();

	if (hFile != INVALID_HANDLE_VALUE) {
		int NumBases;

		ReadFile(hFile, &m_nXScroll, sizeof(m_nXScroll), &dwBytesWritten, NULL);

		ReadFile(hFile, &NumBases, sizeof(m_nNumBases), &dwBytesWritten, NULL);
		for (int i = 0; i < NumBases; i++)  // Find this base in our list
		{
			RECT Rect;
			int OperatorNdx;

			ReadFile(hFile, &Rect, sizeof(Rect), &dwBytesWritten, NULL);
			ReadFile(hFile, &OperatorNdx, sizeof(OperatorNdx), &dwBytesWritten, NULL);

			CreateOperator(OperatorNdx, Rect);

		}

		ReadFile(hFile, &dwMaxUniqueID, sizeof(dwMaxUniqueID), &dwBytesWritten, NULL);

		for (int i = 0; i < NumBases; i++)  // Find this base in our list

			pOpBases[i]->LoadFromStream(hFile);


		for (int i = 0; i < NumBases; i++)  // Find this base in our list
			if (pOpBases[i]->GetUniqueID() > dwMaxUniqueID) dwMaxUniqueID = pOpBases[i]->GetUniqueID() + 1;

			// Tell the bases to connect with their outputs
		for (int i = 0; i < NumBases; i++)  // Find this base in our list
			pOpBases[i]->ConnectOutputs();

		for (int i = 0; i < NumBases; i++)  // Tell the bases with no inputs to initiate sending data
			if (!pOpBases[i]->GetNumInputs())
					pOpBases[i]->SendData();


		CloseHandle(hFile);

			// Update the current tab selected
		RECT Rect;
		GetClientRect(&Rect);
		m_pTabs->SetCurSel(-m_nXScroll / Rect.right);

		m_fBoardChanged = FALSE;

		InvalidateRect(NULL);	// Redraw main screen
	}
	else MessageBox(L"Unable to load from file", L"Error", MB_OK);

}

void CMainView::OnFileLoad()
{

	if (!m_fBoardChanged || MessageBox(L"Are you sure you want to load over the current board?", L"Warning", MB_YESNO) == IDYES) 
	{
		CFileDialog *pDialog = new CFileDialog(TRUE, L".cvf", NULL, 0, L"cvf|*.cvf||", NULL );

		if (pDialog->DoModal() == IDOK) {
			_tcscpy(m_aSaveFileName, pDialog->GetPathName());

			LoadFile(m_aSaveFileName);
		}
	}
}

void CMainView::OnFileNew32776()
{

	if (MessageBox(L"Are you sure you want to clear the current board?", L"Warning", MB_YESNO) == IDYES) 
	{
		DestroyAllOps();
		
		InitVars();

		InvalidateRect(NULL);
	}
}

COpBase *CMainView::FindUniqueID(DWORD UniqueID)
{
	for (int i = 0; i < m_nNumBases; i++)
		if (pOpBases[i]->GetUniqueID() == UniqueID)
			return pOpBases[i];

	return NULL;
}

int MyError(int status, const char* func_name, const char* err_msg, const char* file_name, int line, void *)
{
	TCHAR TStr[1024];
	TCHAR TStr2[1024];
	
	mbstowcs(TStr, func_name, 1024);
	mbstowcs(TStr2, err_msg, 1024);
	
	_tcscat(TStr, L": ");
	_tcscat(TStr, TStr2);
	
	MessageBox(NULL, TStr, L"Error", MB_OK);
	
	throw;
	
	return NULL;
}

void CMainView::OnMouseMove( UINT nFlags, CPoint point )
{
	if (m_fDragging)
	{
		int XScroll = point.x-m_oDragStartPoint.x;
		
		if (m_nXScroll + XScroll > 0) XScroll = 0; 
		
		if (XScroll)
		{
			ScrollWindow(XScroll, 0);
			m_oDragStartPoint.x += XScroll;
			UpdateWindow();
			
			m_nXScroll += XScroll;
		}
	}
	
}

void CMainView::OnLButtonDown( UINT nFlags, CPoint point )
{
	m_fDragging = TRUE;
	m_oDragStartPoint = point;
	
	m_pTabs->ShowWindow(SW_HIDE);

	SetCapture();
}

void CMainView::OnLButtonUp( UINT nFlags, CPoint point )
{
	m_fDragging = FALSE;

	InvalidateRect(NULL);
	UpdateWindow();
	
	ReleaseCapture();
	
	m_pTabs->SetWindowPos(NULL, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOSIZE);	// Move tab control with scroll
	m_pTabs->ShowWindow(SW_SHOW);

	RECT Rect;
	GetClientRect(&Rect);
	m_pTabs->SetCurSel(-m_nXScroll / Rect.right);


}

void CMainView::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
/*
	if (nSBCode == SB_THUMBTRACK)
	{
		int XScroll = -nPos;
		
		ScrollWindow(XScroll, 0);
		UpdateWindow();
		
		m_nXScroll -= XScroll;

	}
*/
}

void CMainView::OnTabCtrl( NMHDR * pNotifyStruct, LRESULT * result )
{
	int nSel = m_pTabs->GetCurSel();
	
	RECT Rect;
	
	GetClientRect(&Rect);

	CDC *pDc = GetDC();
	int MaxX = pDc->GetDeviceCaps(HORZRES);
	ReleaseDC(pDc);
	
	int nNewScroll = -(nSel * MaxX);
	
	int nDelta = nNewScroll - m_nXScroll;
	
	ScrollWindow(nDelta, 0);
	UpdateWindow();
	
	m_nXScroll += nDelta;

	return;
}

void CMainView::OnConnectSVS()
{
	m_oSVS.Initialize();

}

#define LOG_FILENAME "\\EyeCompute.log"
const DWORD LogEnabled =  0; // LOG_SOCKETS; //LOG_DATA; // | LOG_LOCK;

void Log(DWORD LogID, PTCHAR Format, ...)
{
	static BOOL fInitialized = FALSE;
	static CRITICAL_SECTION LogSection;
	static FILE *Fp = NULL;
	static DWORD dwStartTime = 0;
	TCHAR TStr[2048];
	char  Str[2048];

	if (!fInitialized && LogEnabled)
	{
		InitializeCriticalSection(&LogSection);
		fInitialized = TRUE;
	
		mbstowcs(TStr, LOG_FILENAME, strlen(LOG_FILENAME));
		DeleteFile(TStr);

		Fp = fopen(LOG_FILENAME, "at");
		fputc('\n\n\n', Fp);
		fclose(Fp);

		dwStartTime = GetTickCount();

	}

	if (LogID & LogEnabled)
	{
		EnterCriticalSection(&LogSection);

		va_list Args;

		va_start(Args, Format);

		Fp = fopen(LOG_FILENAME, "at");

		fprintf(Fp, "%ld: ", GetTickCount()-dwStartTime);

		_vstprintf(TStr, Format, Args);

		wcstombs(Str, TStr, _tcslen(TStr)+1);
		
		fputs(Str, Fp);
		fputc('\n', Fp);

		fclose(Fp);

		va_end(Args);

		LeaveCriticalSection(&LogSection);
	}
}

 