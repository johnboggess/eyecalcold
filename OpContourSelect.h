#pragma once
#include "opbase.h"
#include "Cv.h"

#define SelectContour_DEFAULT_WIDTH (140)
#define SelectContour_DEFAULT_HEIGHT (120)

class COpSelectContour : public COpBase
{
public:
	COpSelectContour(CMainView *pMain);
	~COpSelectContour(void);

	virtual	PTCHAR GetFriendlyName() { return L"SelectContour"; }
	virtual int	GetDefaultWidth() { return SelectContour_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return SelectContour_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pSelectSlider;
	CEdit		*m_pSelectEdit;
	CSliderCtrl	*m_pSizeSlider;
	CEdit		*m_pSizeEdit;

		// Save data
	int	m_nCurContour;
	int m_nMinSize;
	
		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
