#pragma once
#include "opbase.h"
#include "Cv.h"

#define HoughLines_DEFAULT_WIDTH (200)
#define HoughLines_DEFAULT_HEIGHT (260)

class COpHoughLines : public COpBase
{
public:
	COpHoughLines(CMainView *pMain);
	~COpHoughLines(void);

	virtual	PTCHAR GetFriendlyName() { return L"HoughLines"; }
	virtual int	GetDefaultWidth() { return HoughLines_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return HoughLines_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CComboBox	*m_pMethodCombo;
	TCHAR		m_aMethodStr[20];
	CSliderCtrl	*m_pRhoSlider;
	CEdit		*m_pRhoEdit;
	CSliderCtrl	*m_pThetaSlider;
	CEdit		*m_pThetaEdit;
	CSliderCtrl	*m_pThresholdSlider;
	CEdit		*m_pThresholdEdit;
	CSliderCtrl	*m_pParam1Slider;
	CEdit		*m_pParam1Edit;
	CSliderCtrl	*m_pParam2Slider;
	CEdit		*m_pParam2Edit;

		// Save data
	int		m_nMethod;
	double	m_gRho;
	double	m_gTheta;
	int		m_nThreshold;
	double	m_gParam1;
	double	m_gParam2;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnMethodChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
