#pragma once
#include "opbase.h"
#include "Cv.h"

#define Transpose_DEFAULT_WIDTH (100)
#define Transpose_DEFAULT_HEIGHT (80)

class COpTranspose : public COpBase
{
public:
	COpTranspose(CMainView *pMain);
	~COpTranspose(void);

	virtual	PTCHAR GetFriendlyName() { return L"Transpose"; }
	virtual int	GetDefaultWidth() { return Transpose_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Transpose_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
// (Hai)	virtual void StopBase();


protected:
// (Hai)	CSliderCtrl	*m_pSlider;
// (Hai)	CComboBox	*m_pCombo;
// (Hai)	CEdit		*m_pEdit;
// (Hai)	TCHAR		m_aStr[20];
// (Hai)	CButton		*m_pButton;

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
// (Hai)	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
