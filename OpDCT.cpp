#include "StdAfx.h"
#include "OpDCT.h"
#include "OpBase.h"
#include "Cv.h"

#define ID_DCT_DIRECTION (101)

BYTE bDCTFiller[32];

BEGIN_MESSAGE_MAP(COpDCT, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_DCT_DIRECTION, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpDCT::COpDCT(CMainView *pMain) : COpBase(pMain)
{
	m_fForward = TRUE;
	m_pCombo = NULL;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpDCT::~COpDCT(void)
{
	if (m_pCombo)delete m_pCombo;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpDCT::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpDCT::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_pCombo = CreateOpCombo(0, ID_DCT_DIRECTION);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"Forward");
		pCombo->SetItemData(Ndx, TRUE);
		Ndx = pCombo->AddString(L"Inverse");
		pCombo->SetItemData(Ndx, FALSE);
	}

//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpDCT::StartOpThread(void *pData)
{
	COpDCT *pBase = (COpDCT *) pData;

	pBase->OpThread();

	return 0;
}

void COpDCT::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpDCT::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pCombo->GetCount(); Ndx++)
		if (m_pCombo->GetItemData(Ndx) == m_fForward) break;

	m_pCombo->SetCurSel(Ndx);


//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpDCT::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpDCT::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_fForward, sizeof(m_fForward), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bDCTFiller, sizeof(bDCTFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpDCT::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_fForward, sizeof(m_fForward), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bDCTFiller, sizeof(bDCTFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpDCT::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpDCT::ValidateParameters()
{
}

void COpDCT::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
/*
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
*/
}

void COpDCT::OnChange()
{

	int Ndx = m_pCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_fForward = (BOOL) m_pCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpDCT::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpDCT::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpDCT::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"DCTThread", DATA_IMAGE, 0);

		if (pImage)
		{
			if (pImage->nChannels == 1) 
			{
		 
				CvSize Size;
				
				Size = cvGetSize(pImage);
				
				IplImage *p32Image = cvCreateImage(Size, IPL_DEPTH_32F, pImage->nChannels);
				
				cvCvtScale(pImage, p32Image);
				
				IplImage *pDestImage = cvCloneImage(p32Image);
			
				if (pDestImage) {	// Validate Src and Dst images, etc

					int nFlags = CV_DXT_FORWARD;
					
					if (!m_fForward) nFlags = CV_DXT_INVERSE;

					cvDCT(p32Image, pDestImage, nFlags);

					cvReleaseImage(&p32Image);
					
					SetData(0, DATA_IMAGE, 0, pDestImage, TRUE);
					SendData();

				}
					else TitleError(L"");
			}
				else TitleError(L"Image must be single channel");
			
			ReleaseData(L"DCTThread");  // We're done with the data so it can change if needed
		}
			else TitleError(L"");

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
