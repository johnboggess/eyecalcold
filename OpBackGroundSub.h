#pragma once
#include "opbase.h"
#include "Cv.h"

#define BackGroundSub_DEFAULT_WIDTH (200)
#define BackGroundSub_DEFAULT_HEIGHT (120)

class COpBackGroundSub : public COpBase
{
public:
	COpBackGroundSub(CMainView *pMain);
	~COpBackGroundSub(void);

	virtual	PTCHAR GetFriendlyName() { return L"BckgrndSub"; }
	virtual int	GetDefaultWidth() { return BackGroundSub_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return BackGroundSub_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
		CSliderCtrl	*m_pHighSlider;
		CSliderCtrl	*m_pLowSlider;
		CEdit *m_pHighEdit;
		CEdit *m_pLowEdit;
		int first ;

		// Save data
		int m_nHighShift;
		int m_nLowShift;

		IplImage *IavgF,*IdiffF, *IprevF, *IhiF, *IlowF;
		IplImage *Iscratch,*Iscratch2;
		//Float, 1-channel images
		//		
		IplImage *Igray1,*Igray2, *Igray3;
		IplImage *Ilow1, *Ilow2, *Ilow3;
		IplImage *Ihi1, *Ihi2, *Ihi3;
		// Byte, 1-channel image
		//		
		IplImage *Imaskt;
		//Counts number of images learned for averaging later.		
		//
		double Icount;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	void	DeallocateImages();
	void	backgroundDiff(IplImage *I,IplImage *Imask);
	void	setHighThreshold( float scale );
	void	setLowThreshold( float scale );
	void	createModelsfromStats(float hight,float low);
	void	accumulateBackground( IplImage *I ,int fps);
	void	AllocateImages( IplImage* I );

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
