#pragma once
#include "opbase.h"
#include "Cv.h"

#define MatchShapes_DEFAULT_WIDTH (140)
#define MatchShapes_DEFAULT_HEIGHT (120)

class COpMatchShapes : public COpBase
{
public:
	COpMatchShapes(CMainView *pMain);
	~COpMatchShapes(void);

	virtual	PTCHAR GetFriendlyName() { return L"MatchShapes"; }
	virtual int	GetDefaultWidth() { return MatchShapes_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return MatchShapes_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pMethodCombo;
	CEdit		*m_MatchShapes;

		// Save data
	double		matchshapes;
	int			m_nMethod;


		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;



	void	UpdateValues();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};

