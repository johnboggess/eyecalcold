#include "StdAfx.h"
#include "OpStereoCalib.h"
#include "OpBase.h"
#include "Cv.h"
#include <vector>
#include "cxmisc.h"
#include "highgui.h"

#define LOAD_CALIB

#define DEFULT_NUM_CAPTURES (14)
#define CALIB_WAIT_TIME (2000)
#define CALIB_POINTS_LEFT_NAME "CalibLeftPoints.xml"
#define CALIB_POINTS_RIGHT_NAME "CalibRightPoints.xml"
#define CALIB_INFO_NAME "CalibInformation.xml"
#define CALIB_FILENAME "CalibData.bin"

#define ID_CALIB_START_BUTTON (101)
#define ID_CALIB_LOAD_BUTTON (102)

#define SAVE_IMAGES 

BYTE bStereoCalibFiller[32];

using namespace std;

BEGIN_MESSAGE_MAP(COpStereoCalib, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
	ON_COMMAND(ID_CALIB_START_BUTTON, OnStartButton)
	ON_COMMAND(ID_CALIB_LOAD_BUTTON, OnLoadButton)
END_MESSAGE_MAP()

COpStereoCalib::COpStereoCalib(CMainView *pMain) : COpBase(pMain)
{

	m_pNumCapturesEdit = NULL;
	m_pCurCapturesEdit = NULL;
	m_pNumChessboardRowsEdit = NULL;
	m_pNumChessboardColsEdit = NULL;
	m_pStartButton = NULL;
	m_pLoadButton = NULL;
	m_pStatusEdit = NULL;


	m_nNumCaptures = DEFULT_NUM_CAPTURES;
	m_nNumChessboardRows = 8;
	m_nNumChessboardCols = 6;

	m_nCurrentCapture = 0;
	m_fStarted = FALSE;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpStereoCalib::~COpStereoCalib(void)
{
	if (m_pNumCapturesEdit) delete m_pNumCapturesEdit;
	if (m_pCurCapturesEdit) delete m_pCurCapturesEdit;
	if (m_pNumChessboardRowsEdit) delete m_pNumChessboardRowsEdit;
	if (m_pNumChessboardColsEdit) delete m_pNumChessboardColsEdit;
	if (m_pStartButton) delete m_pStartButton;
	if (m_pLoadButton) delete m_pLoadButton;
	if (m_pStatusEdit) delete m_pStatusEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpStereoCalib::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpStereoCalib::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
	CreateOpStatic(L"number of captures", FALSE, 140);
	m_pNumCapturesEdit = CreateOpEdit(PARAM_EDIT_WIDTH, FALSE);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"chessboard rows", FALSE, 140);
	m_pNumChessboardRowsEdit = CreateOpEdit(PARAM_EDIT_WIDTH, FALSE);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"chessboard colums", FALSE, 140);
	m_pNumChessboardColsEdit = CreateOpEdit(PARAM_EDIT_WIDTH, FALSE);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);


//	m_pSlider = CreateOpSlider(0, MIN, MAX);

	CreateOpStatic(L" ", TRUE, 46);
	m_pStartButton = CreateOpButton(L"Start", 80, BS_PUSHBUTTON, ID_CALIB_START_BUTTON);

	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"current capture", FALSE, 140);
	m_pCurCapturesEdit = CreateOpEdit(PARAM_EDIT_WIDTH, FALSE);

	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"Status", FALSE, 60);
	m_pStatusEdit = CreateOpEdit(100, FALSE);
	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L" ", TRUE, 46);
	m_pLoadButton = CreateOpButton(L"Load", 80, BS_PUSHBUTTON, ID_CALIB_LOAD_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpStereoCalib::StartOpThread(void *pData)
{
	COpStereoCalib *pBase = (COpStereoCalib *) pData;

	pBase->OpThread();

	return 0;
}

void COpStereoCalib::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpStereoCalib::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

	_itot(m_nNumCaptures, TStr, 10);
	m_pNumCapturesEdit->SetWindowTextW(TStr);
	_itot(m_nCurrentCapture, TStr, 10);
	m_pCurCapturesEdit->SetWindowTextW(TStr);
	_itot(m_nNumChessboardRows, TStr, 10);
	m_pNumChessboardRowsEdit->SetWindowTextW(TStr);
	_itot(m_nNumChessboardCols, TStr, 10);
	m_pNumChessboardColsEdit->SetWindowTextW(TStr);


}


void COpStereoCalib::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpStereoCalib::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nNumChessboardRows, sizeof(m_nNumChessboardRows), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nNumChessboardCols, sizeof(m_nNumChessboardCols), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nNumCaptures, sizeof(m_nNumCaptures), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bStereoCalibFiller, sizeof(bStereoCalibFiller), &dwBytesWritten, NULL);
		}
	}

	return fRetValue;
}

BOOL COpStereoCalib::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nNumChessboardRows, sizeof(m_nNumChessboardRows), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nNumChessboardCols, sizeof(m_nNumChessboardCols), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nNumCaptures, sizeof(m_nNumCaptures), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bStereoCalibFiller, sizeof(bStereoCalibFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}

	}

	return fRetValue;
}

	// We got data from the given input index
void COpStereoCalib::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpStereoCalib::ValidateParameters()
{
}

void COpStereoCalib::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpStereoCalib::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpStereoCalib::OnStartButton()
{
	if (!m_fStarted) {
		m_pStartButton->SetWindowTextW(L"Stop");

		TCHAR TStr[20];
		
		m_pNumCapturesEdit->GetWindowText(TStr, sizeof(TStr));
		m_nNumCaptures = _ttoi(TStr);
		m_pNumChessboardRowsEdit->GetWindowText(TStr, sizeof(TStr));
		m_nNumChessboardRows = _ttoi(TStr);
		m_pNumChessboardColsEdit->GetWindowText(TStr, sizeof(TStr));
		m_nNumChessboardCols = _ttoi(TStr);
		
		m_nCurrentCapture = 0;
		_itot(m_nCurrentCapture, TStr, 10);
		m_pCurCapturesEdit->SetWindowTextW(TStr);
		m_pCurCapturesEdit->UpdateWindow();

		m_ChessboardPoints[0].clear();
		m_ChessboardPoints[1].clear();

		Sleep(CALIB_WAIT_TIME);
	}
	else 
		m_pStartButton->SetWindowTextW(L"Start");
	
	m_fStarted = !m_fStarted;

	SetEvent(m_hThreadEvent);	// Tell thread to start
}

void COpStereoCalib::OnLoadButton()
{

#ifdef LOAD_CALIB
	FILE *Fp = fopen(CALIB_FILENAME, "rb");

	if (Fp)
	{
		CALIBRATION_TYPE *pCalib = (CALIBRATION_TYPE *) new BYTE [sizeof(CALIBRATION_TYPE)];
		if (pCalib)
		{
			fread(pCalib, 1, sizeof(CALIBRATION_TYPE), Fp);
			m_pStatusEdit->SetWindowText(L"Loaded");
			m_pStatusEdit->UpdateWindow();

			SetData(0, DATA_CALIBRATION, sizeof(CALIBRATION_TYPE), pCalib, TRUE);
			SendData();
		}
		
		fclose(Fp);
	}
	else
	{
		m_pStatusEdit->SetWindowText(L"No data");
	    m_pStatusEdit->UpdateWindow();
	}
#else

	CvMat *pMatLeft = (CvMat *) cvLoad(CALIB_POINTS_LEFT_NAME);
	CvMat *pMatRight = (CvMat *) cvLoad(CALIB_POINTS_RIGHT_NAME);

	int nNumCaptures, nNumChessboardRows, nNumChessboardCols;
	CvSize ImageSize;

	CvFileStorage *fs = cvOpenFileStorage(CALIB_INFO_NAME, 0, CV_STORAGE_READ);
	nNumCaptures = cvReadIntByName(fs, 0, "NumCaptures");
	nNumChessboardRows = cvReadIntByName(fs, 0, "ChessboardRows");
	nNumChessboardCols = cvReadIntByName(fs, 0, "ChessboardCols");
	ImageSize.width = cvReadIntByName(fs, 0, "ImageWidth");
	ImageSize.height = cvReadIntByName(fs, 0, "ImageHeight");
	cvReleaseFileStorage(&fs);

	if (pMatLeft && pMatRight)
	{
		m_pStatusEdit->SetWindowText(L"Loaded");
	    m_pStatusEdit->UpdateWindow();
		
		Calibrate(nNumCaptures, nNumChessboardRows, nNumChessboardCols, pMatLeft, pMatRight, ImageSize);
		
		cvReleaseMat(&pMatLeft);
		cvReleaseMat(&pMatRight);

	}
		else  // No data found
	{
		m_pStatusEdit->SetWindowText(L"No data");
	    m_pStatusEdit->UpdateWindow();
	}
#endif
}

void COpStereoCalib::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpStereoCalib::OpThread()
{
    vector<CvPoint2D32f> Corners[2];
    int CornersFound = 0;
    char FileName[MAX_PATH];
    CvSize ImageSize;

	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		if (m_fStarted)
		{
			Sleep(CALIB_WAIT_TIME);
			MessageBeep(MB_ICONEXCLAMATION);

			while (!CornersFound)
			{		
				IplImage *pImage1 = (IplImage *) LockData(L"StereoCalibThread", DATA_IMAGE, 0);
				IplImage *pImage2 = (IplImage *) LockData(L"StereoCalibThread", DATA_IMAGE, 1);

				if (pImage1 && pImage2) 
				{
			
					int count[2];
	
					ImageSize = cvGetSize(pImage1);
					
						// Process left image
					Corners[0].clear();
					Corners[0].resize((m_nNumChessboardRows-1) * (m_nNumChessboardCols-1));
					CornersFound = cvFindChessboardCorners( pImage1, cvSize(m_nNumChessboardCols-1, m_nNumChessboardRows-1),
						&Corners[0][0], &count[0], CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE);

						// Process right image
					if (CornersFound) 
					{
						Corners[1].clear();
						Corners[1].resize((m_nNumChessboardRows-1) * (m_nNumChessboardCols-1));
						CornersFound = cvFindChessboardCorners( pImage2, cvSize(m_nNumChessboardCols-1, m_nNumChessboardRows-1),
							&Corners[1][0], &count[1], CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_NORMALIZE_IMAGE);
					}
					
					if (CornersFound)
					{
							// Process left image
						int N = m_ChessboardPoints[0].size();
							// Make room for these new points
						m_ChessboardPoints[0].resize(N + (m_nNumChessboardRows-1) * (m_nNumChessboardCols-1));

						IplImage *pLeftImage = cvCreateImage(cvGetSize(pImage1), IPL_DEPTH_8U, 1);

						cvCvtColor(pImage1, pLeftImage, CV_BGR2GRAY);
						cvFindCornerSubPix(pLeftImage, &Corners[0][0], count[0], cvSize(11, 11), cvSize(-1, -1), 
							cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 30, 0.01));

						cvReleaseImage(&pLeftImage);

							// Copy these corner points to our master list
						copy(Corners[0].begin(), Corners[0].end(), m_ChessboardPoints[0].begin() + N);

#ifdef SAVE_IMAGES
						sprintf(FileName, "Calib_Left_%d.jpg", m_nCurrentCapture);
						cvSaveImage(FileName, pImage1);
#endif

						cvDrawChessboardCorners( pImage1, cvSize(m_nNumChessboardCols-1, m_nNumChessboardRows-1), &Corners[0][0],
							count[0], CornersFound );
					
						SetData(0, DATA_IMAGE, 0, pImage1);
						SendData();

							// Process right image
						N = m_ChessboardPoints[1].size();
							// Make room for these new points
						m_ChessboardPoints[1].resize(N + (m_nNumChessboardRows-1) * (m_nNumChessboardCols-1));

						IplImage *pRightImage = cvCreateImage(cvGetSize(pImage2), IPL_DEPTH_8U, 1);
						cvCvtColor(pImage2, pRightImage, CV_BGR2GRAY);
						cvFindCornerSubPix(pRightImage, &Corners[1][0], count[1], cvSize(11, 11), cvSize(-1, -1), 
							cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 30, 0.01));
		
						cvReleaseImage(&pRightImage);

							// Copy these corner points to our master list
						copy(Corners[1].begin(), Corners[1].end(), m_ChessboardPoints[1].begin() + N);

#ifdef SAVE_IMAGES
						sprintf(FileName, "Calib_Right_%d.jpg", m_nCurrentCapture);
						cvSaveImage(FileName, pImage2);
#endif

					}
						else	// Allow calibrator to see images
					{
						SetData(0, DATA_IMAGE, 0, pImage1);
						SendData();
					}
					
					ReleaseData(L"StereoCalibThread");  // We're done with the data so it can change if needed
					ReleaseData(L"StereoCalibThread");  // We're done with the data so it can change if needed
				}
					else 
				{
					if (pImage1) ReleaseData(L"StereoCalibThread");  // We're done with the data so it can change if needed
					if (pImage2) ReleaseData(L"StereoCalibThread");  // We're done with the data so it can change if needed

					TitleError(L"Requires left and right input images");
				}
			}
			
			CornersFound = 0;
			m_nCurrentCapture++;
			TCHAR TStr[20];
			_itot(m_nCurrentCapture, TStr, 10);
			m_pCurCapturesEdit->SetWindowTextW(TStr);
			m_pCurCapturesEdit->UpdateWindow();
			
			if (m_nCurrentCapture == m_nNumCaptures) 
			{
				// Save points to file
				int N = m_ChessboardPoints[0].size();
				CvMat LeftCorners = cvMat(1, N, CV_32FC2, &m_ChessboardPoints[0][0]);
				cvSave(CALIB_POINTS_LEFT_NAME, &LeftCorners);
				N = m_ChessboardPoints[1].size();
				CvMat RightCorners = cvMat(1, N, CV_32FC2, &m_ChessboardPoints[1][0]);
				cvSave(CALIB_POINTS_RIGHT_NAME, &RightCorners);
				
				CvFileStorage *fs = cvOpenFileStorage(CALIB_INFO_NAME, 0, CV_STORAGE_WRITE);
				cvWriteInt(fs, "NumCaptures", m_nNumCaptures);
				cvWriteInt(fs, "ChessboardRows", m_nNumChessboardRows);
				cvWriteInt(fs, "ChessboardCols", m_nNumChessboardCols);
				cvWriteInt(fs, "ImageWidth", ImageSize.width);
				cvWriteInt(fs, "ImageHeight", ImageSize.height);
				cvReleaseFileStorage(&fs);
				
				Calibrate(m_nNumCaptures, m_nNumChessboardRows, m_nNumChessboardCols, &LeftCorners, &RightCorners, ImageSize);
			
				OnStartButton();	// Stop acquiring
			}
		}
		else 
		{
			m_nCurrentCapture = 0;
			
			WaitForSingleObject(m_hThreadEvent, INFINITE);
		}
	}
}

void COpStereoCalib::Calibrate(int NumImages, int nChessboardRows, int nChessboardCols, CvMat *pLeftCorners, CvMat *pRightCorners, CvSize ImageSize)
{
	int TotalCorners = (nChessboardRows-1) * (nChessboardCols-1);
//md%1
	const float squareSize = 26.f;	// It's easier to call each square one unit
    double M1[3][3], M2[3][3], D1[5], D2[5];
    double R[3][3], T[3], E[3][3], F[3][3];
    CvMat _M1 = cvMat(3, 3, CV_64F, M1 );
    CvMat _M2 = cvMat(3, 3, CV_64F, M2 );
    CvMat _D1 = cvMat(1, 5, CV_64F, D1 );
    CvMat _D2 = cvMat(1, 5, CV_64F, D2 );
    CvMat _R = cvMat(3, 3, CV_64F, R );
    CvMat _T = cvMat(3, 1, CV_64F, T );
    CvMat _E = cvMat(3, 3, CV_64F, E );
    CvMat _F = cvMat(3, 3, CV_64F, F );

	vector<CvPoint3D32f> objectPoints;
	vector<int> npoints;
    vector<CvPoint2D32f> points[2];
	
	points[0].resize(NumImages * TotalCorners);
	points[1].resize(NumImages * TotalCorners);
	for (int i = 0; i < NumImages * TotalCorners; i++)
	{
		CvScalar Scalar = cvGet1D(pLeftCorners, i);
		points[0][i].x = Scalar.val[0];
		points[0][i].y = Scalar.val[1];

		Scalar = cvGet1D(pRightCorners, i);
		points[1][i].x = Scalar.val[0];
		points[1][i].y = Scalar.val[1];
		
	}
	

	objectPoints.resize(NumImages * TotalCorners);
	for (int i = 0; i < nChessboardRows-1; i++)
		for (int j = 0; j < nChessboardCols-1; j++)
			objectPoints[i*(nChessboardCols-1) + j] = cvPoint3D32f(i*squareSize, j*squareSize, 0);
	for (int i = 1; i < NumImages; i++)
		copy(objectPoints.begin(), objectPoints.begin() + TotalCorners, objectPoints.begin() + i * TotalCorners);
		
    npoints.resize(NumImages,TotalCorners);
    int N = NumImages*TotalCorners;
    CvMat _objectPoints = cvMat(1, N, CV_32FC3, &objectPoints[0] );
    CvMat _imagePoints1 = cvMat(1, N, CV_32FC2, &points[0][0] );
    CvMat _imagePoints2 = cvMat(1, N, CV_32FC2, &points[1][0] );
    
//md%    
cvSave("Points1_C.xml", &_imagePoints1);
cvSave("Points2_C.xml", &_imagePoints2);
    
    CvMat _npoints = cvMat(1, npoints.size(), CV_32S, &npoints[0] );
    cvSetIdentity(&_M1);
    cvSetIdentity(&_M2);
    cvZero(&_D1);
    cvZero(&_D2);
    
	m_pStatusEdit->SetWindowText(L"Calibrating...");
    m_pStatusEdit->UpdateWindow();
    
    cvStereoCalibrate( &_objectPoints, &_imagePoints1,
	    &_imagePoints2, &_npoints,
		&_M1, &_D1, &_M2, &_D2,
		ImageSize, &_R, &_T, &_E, &_F,
		cvTermCriteria(CV_TERMCRIT_ITER+
			CV_TERMCRIT_EPS, 100, 1e-5),
			CV_CALIB_FIX_ASPECT_RATIO +
			CV_CALIB_ZERO_TANGENT_DIST +
			CV_CALIB_SAME_FOCAL_LENGTH );

		// Now calculate rectification matrices
	double R1[3][3], R2[3][3], P1[3][4], P2[3][4], Q[4][4];
	CvMat _R1 = cvMat(3, 3, CV_64F, R1);
	CvMat _R2 = cvMat(3, 3, CV_64F, R2);
    CvMat _P1 = cvMat(3, 4, CV_64F, P1);
    CvMat _P2 = cvMat(3, 4, CV_64F, P2);
    CvMat _Q = cvMat(4, 4, CV_64F, Q);
  // Bouguet's method
    cvStereoRectify( &_M1, &_M2, &_D1, &_D2, ImageSize, &_R, &_T,
        &_R1, &_R2, &_P1, &_P2, &_Q, 0);
//       &_R1, &_R2, &_P1, &_P2, &_Q, CV_CALIB_ZERO_DISPARITY);
cvSave("M1_C.xml", &_M1);
cvSave("M2_C.xml", &_M2);
cvSave("D1_C.xml", &_D1);
cvSave("D2_C.xml", &_D2);
cvSave("R_C.xml", &_R);
cvSave("T_C.xml", &_T);
cvSave("R1_C.xml", &_R1);
cvSave("R2_C.xml", &_R2);
cvSave("P1_C.xml", &_P1);
cvSave("P2_C.xml", &_P2);

CvScalar Scalar = cvGet1D(&_D1, 0);
Scalar = cvGet1D(&_D1, 0);

//md%
#if 0  // Hartley's method
    double H1[3][3], H2[3][3], iM[3][3];
    CvMat _H1 = cvMat(3, 3, CV_64F, H1);
    CvMat _H2 = cvMat(3, 3, CV_64F, H2);
    CvMat _iM = cvMat(3, 3, CV_64F, iM);
    cvZero(&_R1);
    cvZero(&_R2);
//Just to show you could have independently used F
//    if( useUncalibrated == 2 )
//        cvFindFundamentalMat( &_imagePoints1, &_imagePoints2, &_F);
    cvStereoRectifyUncalibrated( &_imagePoints1, &_imagePoints2, &_F, ImageSize, &_H1, &_H2, 3);
    cvInvert(&_M1, &_iM);
    cvMatMul(&_H1, &_M1, &_R1);
    cvMatMul(&_iM, &_R1, &_R1);
    cvInvert(&_M2, &_iM);
    cvMatMul(&_H2, &_M2, &_R2);
    cvMatMul(&_iM, &_R2, &_R2);
#endif

	// Fill the calibration structure with this calcualted data
	CALIBRATION_TYPE *pCalib = (CALIBRATION_TYPE *) new BYTE[sizeof(CALIBRATION_TYPE)];
	
	pCalib->ImageHeight = ImageSize.height;
	pCalib->ImageWidth = ImageSize.width;
	
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			CvScalar Scalar = cvGet2D(&_M1, i, j);
			pCalib->CamMatrix1[i][j] = Scalar.val[0];

			Scalar = cvGet2D(&_M2, i, j);
			pCalib->CamMatrix2[i][j] = Scalar.val[0];

			Scalar = cvGet2D(&_R, i, j);
			pCalib->Rotation[i][j] = Scalar.val[0];

			Scalar = cvGet2D(&_E, i, j);
			pCalib->Essential[i][j] = Scalar.val[0];

			Scalar = cvGet2D(&_F, i, j);
			pCalib->Fundamental[i][j] = Scalar.val[0];

			Scalar = cvGet2D(&_R1, i, j);
			pCalib->Rectify1[i][j] = Scalar.val[0];

			Scalar = cvGet2D(&_R2, i, j);
			pCalib->Rectify2[i][j] = Scalar.val[0];
			
		}
		
	for (int i = 0; i < 5; i++)
	{
		CvScalar Scalar = cvGet1D(&_D1, i);
		pCalib->DistCoeff1[i] = Scalar.val[0];

		Scalar = cvGet1D(&_D2, i);
		pCalib->DistCoeff2[i] = Scalar.val[0];
		
		if (i < 3)
		{
			Scalar = cvGet1D(&_T, i);
			pCalib->Translation[i] = Scalar.val[0];
		}
	}

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 4; j++)
		{
			CvScalar Scalar = cvGet2D(&_P1, i, j);
			pCalib->Project1[i][j] = Scalar.val[0];

			Scalar = cvGet2D(&_P2, i, j);
			pCalib->Project2[i][j] = Scalar.val[0];
		}

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
		{
			CvScalar Scalar = cvGet2D(&_Q, i, j);
			pCalib->Disparity[i][j] = Scalar.val[0];
		}
		
	FILE *Fp = fopen(CALIB_FILENAME, "wb");
	fwrite(pCalib, 1, sizeof(CALIBRATION_TYPE), Fp);
	fclose(Fp);	
		
	SetData(0, DATA_CALIBRATION, sizeof(CALIBRATION_TYPE), pCalib, TRUE);
	SendData();

// CALIBRATION QUALITY CHECK
// because the output fundamental matrix implicitly
// includes all the output information,
// we can check the quality of calibration using the
// epipolar geometry constraint: m2^t*F*m1=0
    vector<CvPoint3D32f> lines[2];

    points[0].resize(N);
    points[1].resize(N);
    _imagePoints1 = cvMat(1, N, CV_32FC2, &points[0][0] );
    _imagePoints2 = cvMat(1, N, CV_32FC2, &points[1][0] );
    lines[0].resize(N);
    lines[1].resize(N);
    CvMat _L1 = cvMat(1, N, CV_32FC3, &lines[0][0]);
    CvMat _L2 = cvMat(1, N, CV_32FC3, &lines[1][0]);
//Always work in undistorted space
    cvUndistortPoints( &_imagePoints1, &_imagePoints1,
        &_M1, &_D1, 0, &_M1 );
    cvUndistortPoints( &_imagePoints2, &_imagePoints2,
        &_M2, &_D2, 0, &_M2 );
    cvComputeCorrespondEpilines( &_imagePoints1, 1, &_F, &_L1 );
    cvComputeCorrespondEpilines( &_imagePoints2, 2, &_F, &_L2 );
    double avgErr = 0;
    for(int i = 0; i < N; i++ )
    {
        double err = fabs(points[0][i].x*lines[1][i].x +
            points[0][i].y*lines[1][i].y + lines[1][i].z)
            + fabs(points[1][i].x*lines[0][i].x +
            points[1][i].y*lines[0][i].y + lines[0][i].z);
        avgErr += err;
    }
//    printf( "avg err = %g\n", avgErr/(nframes*n) );

	TCHAR TStr[100];
	_stprintf(TStr, L"Calibrated %-6.2f", avgErr/(NumImages*TotalCorners));
	m_pStatusEdit->SetWindowText(TStr);
    m_pStatusEdit->UpdateWindow();
    
    

}