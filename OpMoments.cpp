#include "StdAfx.h"
#include "OpMoments.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bMomentsFiller[32];

BEGIN_MESSAGE_MAP(COpMoments, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpMoments::COpMoments(CMainView *pMain) : COpBase(pMain)
{
	m_pM00Edit = NULL;
	m_pM10Edit = NULL;
	m_pM01Edit = NULL;
	m_pM20Edit = NULL;
	m_pM11Edit = NULL;
	m_pM02Edit = NULL;
	m_pM30Edit = NULL;
	m_pM21Edit = NULL;
	m_pM12Edit = NULL;
	m_pM03Edit = NULL;

	memset(&m_CvMoments, 0, sizeof(m_CvMoments));

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpMoments::~COpMoments(void)
{
	if (m_pM00Edit) delete m_pM00Edit;
	if (m_pM10Edit) delete m_pM10Edit;
	if (m_pM01Edit) delete m_pM01Edit;
	if (m_pM20Edit) delete m_pM20Edit;
	if (m_pM11Edit) delete m_pM11Edit;
	if (m_pM02Edit) delete m_pM02Edit;
	if (m_pM30Edit) delete m_pM30Edit;
	if (m_pM21Edit) delete m_pM21Edit;
	if (m_pM12Edit) delete m_pM12Edit;
	if (m_pM03Edit) delete m_pM03Edit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpMoments::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpMoments::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
	CreateOpStatic(L"m00", FALSE, 35);
	m_pM00Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m10", FALSE, 35);
	m_pM10Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m01", FALSE, 35);
	m_pM01Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m20", FALSE, 35);
	m_pM20Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m11", FALSE, 35);
	m_pM11Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m02", FALSE, 35);
	m_pM02Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m30", FALSE, 35);
	m_pM30Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m21", FALSE, 35);
	m_pM21Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m12", FALSE, 35);
	m_pM12Edit = CreateOpEdit(0, TRUE);
	CreateOpStatic(L"m03", FALSE, 35);
	m_pM03Edit = CreateOpEdit(0, TRUE);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpMoments::StartOpThread(void *pData)
{
	COpMoments *pBase = (COpMoments *) pData;

	pBase->OpThread();

	return 0;
}

void COpMoments::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpMoments::UpdateValues()
{
	TCHAR TStr[128];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);

	if (m_pM00Edit)
	{
		_stprintf(TStr, L"%8.6f", m_CvMoments.m00);				
		m_pM00Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m10);				
		m_pM10Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m01);				
		m_pM01Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m20);				
		m_pM20Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m11);				
		m_pM11Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m02);				
		m_pM02Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m30);				
		m_pM30Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m21);				
		m_pM21Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m12);				
		m_pM12Edit->SetWindowText(TStr);
		_stprintf(TStr, L"%8.6f", m_CvMoments.m03);				
		m_pM03Edit->SetWindowText(TStr);
	}
}


void COpMoments::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpMoments::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;
/*
//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bMomentsFiller, sizeof(bMomentsFiller), &dwBytesWritten, NULL);
		}

*/
fRetValue = TRUE;
	}

	return fRetValue;
}

BOOL COpMoments::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);
/*
		if (ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bMomentsFiller, sizeof(bMomentsFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
*/
fRetValue = TRUE;

	}

	return fRetValue;
}

	// We got data from the given input index
void COpMoments::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpMoments::ValidateParameters()
{
}

void COpMoments::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpMoments::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpMoments::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpMoments::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpMoments::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		CvSeq *pContour = (CvSeq *) LockData(L"MomentsThread", DATA_CONTOUR, 0);

		if (pContour) 
		{
			if (1) {	// Validate Src and Dst images, etc

				cvMoments(pContour, &m_CvMoments);

				UpdateValues();				


			}
				else TitleError(L"");

			ReleaseData(L"MomentsThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
