#pragma once
#include "opbase.h"
#include "Cv.h"

#define Mul_DEFAULT_WIDTH (100)
#define Mul_DEFAULT_HEIGHT (80)

class COpMul : public COpBase
{
public:
	COpMul(CMainView *pMain);
	~COpMul(void);

	virtual	PTCHAR GetFriendlyName() { return L"Multiply"; }
	virtual int	GetDefaultWidth() { return Mul_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Mul_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
