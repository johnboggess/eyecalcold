// ChildView.h : interface of the CMainView class
//

#include "SVSInterface.h"

#pragma once

class COpBase;

#define MAX_OPBASES (100)
#define MAX_SCROLL_WIDTH (10000)

#define OP_IMAGE (0)

// CMainView window

class CMainView : public CWnd
{
// Construction
public:
	CMainView();

// Attributes
public:
	int GetNumBases() { return m_nNumBases; }
	COpBase *GetOpBase(int Index) { if (Index < MAX_OPBASES) return pOpBases[Index]; else return NULL; }
	BOOL DeleteOpBase(COpBase *pBase);
	COpBase *FindUniqueID(DWORD UniqueID);
	COpBase *CreateOperator(int nOperatorIndex, RECT Rect);
	CSVSInterface *GetInterface() { return &m_oSVS; }
	// Operations
public:

// Overrides
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual int OnCreate( LPCREATESTRUCT lpCreateStruct );


// Implementation
public:
	virtual ~CMainView();

	// Generated message map functions
protected:
	void InitVars();

	COpBase *pOpBases[MAX_OPBASES];
	int m_nNumBases;
	COpBase *m_pDeletedOpBase;
	DWORD	dwMaxUniqueID;
	BOOL	m_fBoardChanged;
	BOOL	m_fDragging;
	int		m_nXScroll;
	CPoint	m_oDragStartPoint;
	CTabCtrl *m_pTabs;
	CSVSInterface m_oSVS;
	TCHAR	m_aSaveFileName[MAX_PATH];

	void DestroyAllOps();
	void LoadFile(PTCHAR pFileName);
	
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg void OnChar( UINT, UINT, UINT );
	afx_msg void OnLButtonDblClk( UINT nFlags, CPoint point );
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnTabCtrl( NMHDR * pNotifyStruct, LRESULT * result );

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFileSave32774();
	afx_msg void OnFileSaveAs();
	afx_msg void OnFileLoad();
	afx_msg void OnFileNew32776();
	afx_msg void OnConnectSVS();
};

#pragma once


