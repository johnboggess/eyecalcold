#pragma once
#include "opbase.h"
#include "Cv.h"

#define Scale_DEFAULT_WIDTH (200)
#define Scale_DEFAULT_HEIGHT (120)

class COpScale : public COpBase
{
public:
	COpScale(CMainView *pMain);
	~COpScale(void);

	virtual	PTCHAR GetFriendlyName() { return L"Scale"; }
	virtual int	GetDefaultWidth() { return Scale_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Scale_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pScaleSlider;
	CSliderCtrl *m_pFineScaleSlider;
	CEdit *m_pScaleEdit;

		// Save data
	int		m_nScale;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
