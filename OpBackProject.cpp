#include "StdAfx.h"
#include "OpBackProject.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bBackProjectFiller[32];

BEGIN_MESSAGE_MAP(COpBackProject, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpBackProject::COpBackProject(CMainView *pMain) : COpBase(pMain)
{

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpBackProject::~COpBackProject(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpBackProject::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpBackProject::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpBackProject::StartOpThread(void *pData)
{
	COpBackProject *pBase = (COpBackProject *) pData;

	pBase->OpThread();

	return 0;
}

void COpBackProject::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpBackProject::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpBackProject::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpBackProject::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;
fRetValue = TRUE;
/*
//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bBackProjectFiller, sizeof(bBackProjectFiller), &dwBytesWritten, NULL);
		}
*/
	}

	return fRetValue;
}

BOOL COpBackProject::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

fRetValue = TRUE;
/*
//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bBackProjectFiller, sizeof(bBackProjectFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
*/
	}

	return fRetValue;
}

	// We got data from the given input index
void COpBackProject::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpBackProject::ValidateParameters()
{
}

void COpBackProject::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpBackProject::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpBackProject::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpBackProject::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpBackProject::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"BackProjectThread", DATA_IMAGE, 0);
		IplImage *pImageHist = (IplImage *) LockData(L"BackProjectThread", DATA_IMAGE, 1);

		if (pImage && pImageHist) 
		{
			if (cvGetSize(pImage).height == cvGetSize(pImageHist).height
				&& cvGetSize(pImage).width == cvGetSize(pImageHist).width) {	// Validate Src and Dst images, etc

				IplImage *hsv = cvCreateImage(cvGetSize(pImageHist), 8, 3);
				IplImage *h_plane = cvCreateImage(cvGetSize(pImageHist), pImageHist->depth, 1);
				IplImage *s_plane = cvCreateImage(cvGetSize(pImageHist), pImageHist->depth, 1);
				IplImage *v_plane = cvCreateImage(cvGetSize(pImageHist), pImageHist->depth, 1);

				CvSize DstSize = cvGetSize(pImage);

/* for CalcBackProjectPatch
CvSize patch_size = cvSize(25, 25);
DstSize.width -= patch_size.width - 1;
DstSize.height -= patch_size.height - 1;
*/

				IplImage *pBackProjectImage = cvCreateImage(DstSize, pImage->depth, 1);
// for CalcBackProjectPatch
//				IplImage *pBackProjectImage = cvCreateImage(DstSize, IPL_DEPTH_32F, 1);
				
				IplImage *planes[] = { h_plane, s_plane };
				int h_bins = 30, s_bins = 32;
				int hist_size[] = {h_bins, s_bins};
				float h_ranges[] = {0, 180 };
				float s_ranges[] = {0, 255 };
				float *ranges[] = { h_ranges, s_ranges };

					// Convert to HSV
				cvCvtColor(pImageHist, hsv, CV_BGR2HSV);	
					// Split the planes
				cvCvtPixToPlane(hsv, h_plane, s_plane, v_plane, 0);

				CvHistogram *hist;
				
				hist = cvCreateHist(2, hist_size, CV_HIST_ARRAY, ranges, 1);

				cvCalcHist(planes, hist, 0, 0);
				
				cvCvtColor(pImage, hsv, CV_BGR2HSV);	
					// Split the planes
				cvCvtPixToPlane(hsv, h_plane, s_plane, v_plane, 0);
				
				cvCalcBackProject(planes, pBackProjectImage, hist);

// for CalcBackProjectPatch
//				cvCalcBackProjectPatch(planes, pBackProjectImage, patch_size, hist, CV_COMP_CORREL, 1);

				cvReleaseImage(&hsv);
				cvReleaseImage(&h_plane);
				cvReleaseImage(&s_plane);
				cvReleaseImage(&v_plane);

				SetData(0, DATA_IMAGE, 0, pBackProjectImage, TRUE);
				SendData();

			}
				else 
			{
				TitleError(L"All source and histogram images must be the same size");
				
			}

			ReleaseData(L"BackProjectThread");  // We're done with the data so it can change if needed
			ReleaseData(L"BackProjectThread");  // We're done with the data so it can change if needed
		}
			else
		{
			TitleError(L"Need src image and histogram image");
			if (pImage) ReleaseData(L"BackProjectThread");  // We're done with the data so it can change if needed
			if (pImageHist) ReleaseData(L"BackProjectThread");  // We're done with the data so it can change if needed
		}
		
		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
