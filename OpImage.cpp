#include "StdAfx.h"
#include "OpImage.h"
#include "HighGui.h"
#include "Cv.h"

#define ID_LOAD_IMAGE (100)
#define ID_IMAGE_SET_ROI (101)
#define ID_IMAGE_RESET_ROI (102)
#define ID_IMAGE_SHOW_STATS (103)
#define ID_IMAGE_SET_POINTS (104)
#define ID_IMAGE_CLEAR_POINTS (105)
#define ID_SAVE_IMAGE (106)

#define CROSSHAIR_LEN (5)

#define POINT_DELETE_DIST (10)
#define POINT_DRAG_DIST (20)

BYTE bImageFiller[128];

BEGIN_MESSAGE_MAP(COpImage, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_COMMAND(ID_LOAD_IMAGE, LoadImage)
	ON_COMMAND(ID_SAVE_IMAGE, SaveImage)
	ON_COMMAND(ID_IMAGE_SET_ROI, SetROI)
	ON_COMMAND(ID_IMAGE_RESET_ROI, ResetROI)
	ON_COMMAND(ID_IMAGE_SHOW_STATS, OnShowStats)
	ON_COMMAND(ID_IMAGE_SET_POINTS, OnSetPoints)
	ON_COMMAND(ID_IMAGE_CLEAR_POINTS, OnClearPoints)


END_MESSAGE_MAP()

COpImage::COpImage(CMainView *pMain) : COpBase(pMain)
{
	m_fInSizeArea = FALSE;
	m_fSizing = FALSE;
	*m_aImageFileName = 0;
	m_fShowStats = FALSE;
	m_fSettingROI = FALSE;
	m_fSettingPoints = FALSE;
	m_sPointList.nNumPoints = 0;
	m_fDraggingPoint = FALSE;
	m_nDragPointNdx = 0;
	
	m_pRawImage = NULL;

		// Drawing requirements
	m_fDrawHoughLines = FALSE;
	m_fDrawHoughCircles = FALSE;

	m_oROIUpperLeft.x = 0;
	m_oROIUpperLeft.y = 0;
	m_oROILowerRight.x = 0;
	m_oROILowerRight.y = 0;
	m_oROIRect = cvRect(0, 0, 0, 0);
	m_oClickPoint = cvPoint(0, 0);

	m_nMouseCurX = 0;
	m_nMouseCurY = 0;

	m_pImage = new CvvImage();

	InitializeCriticalSection(&m_pImageCrit);

}

COpImage::~COpImage(void)
{
	if (m_pRawImage) cvReleaseImage(&m_pRawImage);

	DeleteCriticalSection(&m_pImageCrit);
}

BOOL COpImage::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

//	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		NULL /*::LoadCursor(NULL, IDC_ARROW)*/, reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void COpImage::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	if (!m_pImage->GetImage() || m_fShowStats) PaintBase(&dc, TRUE); // Draw base with title
		else PaintBase(&dc, FALSE);  // Draw base first

	RECT Rect;
	RECT ImageRect;

	GetClientRect(&Rect);

	Rect.top += 0;
	Rect.left += LEFT_MARGIN + 0;
	Rect.right -= LEFT_MARGIN + 0;
	Rect.bottom -= 0;


	if (m_fShowStats) Rect.bottom -= TITLE_HEIGHT;	// Compensate for title

	ImageRect = Rect;

	m_pImage->DrawToHDC(dc, &ImageRect);

	if (m_fSettingROI)
	{
		dc.Rectangle(m_oROIUpperLeft.x, m_oROIUpperLeft.y, m_oROILowerRight.x, m_oROILowerRight.y);
	}

	if (m_fShowStats) {
		CFont Font;
		TCHAR TStr[1024];

		Font.CreatePointFont(90, L"TimesNewRoman", &dc);
		CFont *pOldFont = dc.SelectObject(&Font);
		Rect.left += 5;
		Rect.top = Rect.bottom + 3;
		Rect.bottom = Rect.top + TITLE_HEIGHT;

		RECT CurPointRect = GetBaseDrawingArea();

		int nCurX = (int) ((m_nMouseCurX - CurPointRect.left) * (float) m_pImage->Width() / (CurPointRect.right-CurPointRect.left));
		int nCurY = (int) ((m_nMouseCurY - CurPointRect.top) * (float) m_pImage->Height() / (CurPointRect.bottom-CurPointRect.top));

		CvScalar Data = cvScalarAll(0);
		if (nCurX >= 0 && nCurY >= 0 && nCurX < m_pImage->Width() && nCurY < m_pImage->Height())
		{
			EnterCriticalSection(&m_pImageCrit);
			Data = cvGet2D(m_pRawImage, nCurY, nCurX);
//			Data = cvGet2D(m_pImage->GetImage(), nCurY, nCurX);
			LeaveCriticalSection(&m_pImageCrit);
		}

		_stprintf(TStr, L"%d x %d   x=%d, y=%d    r=%d, g=%d, b=%d", m_pImage->Width(), m_pImage->Height(), nCurX, nCurY, (int) Data.val[2], (int) Data.val[1], (int) Data.val[0]);
		dc.DrawText(TStr, -1, &Rect, DT_LEFT);
		dc.SelectObject(pOldFont);

	}
	
	if (m_fSettingPoints)  // Draw all points selected so far
	{
		CPen Pen(PS_SOLID, 1, RGB(255, 0, 0));
		CPen *pOldPen = dc.SelectObject(&Pen);

		double xScale = (double) (ImageRect.right-ImageRect.left) / m_pImage->Width();
		double yScale = (double) (ImageRect.bottom-ImageRect.top) / m_pImage->Height();

		for (int j = 0; j < m_sPointList.nNumPoints; j++)
		{
			int x = (int) (m_sPointList.PointList[j].x * xScale);
			int y = (int) (m_sPointList.PointList[j].y * yScale);
			
			dc.MoveTo(ImageRect.left+x-CROSSHAIR_LEN, y);
			dc.LineTo(ImageRect.left+x+CROSSHAIR_LEN, y);
			dc.MoveTo(ImageRect.left+x, y-CROSSHAIR_LEN);
			dc.LineTo(ImageRect.left+x, y+CROSSHAIR_LEN);			
			
		}

		dc.SelectObject(pOldPen);
	}
	
	if (m_fDrawHoughLines)  // Need to overlay hough lines on image
	{
		BOOL fPolar = TRUE;	// Use polar coords
		
		CvSeq *pLines = (CvSeq *) LockData(L"Image paint", DATA_HOUGH_LINES_POLAR, 0);

		if (!pLines) 
		{
			pLines = (CvSeq *) LockData(L"Image paint", DATA_HOUGH_LINES_CART, 0);
			fPolar = FALSE;	// Cartesian coords
		}
		
		if (pLines)
		{
			CPen Pen(PS_SOLID, 1, RGB(0, 0, 255));
			CPen *pOldPen = dc.SelectObject(&Pen);
			
			double xScale = (double) (ImageRect.right-ImageRect.left) / m_pImage->Width();
			double yScale = (double) (ImageRect.bottom-ImageRect.top) / m_pImage->Height();

			for (int i = 0; i < pLines->total; i++)
			{
				float *line = (float *) cvGetSeqElem(pLines, i);
				
				CvPoint P1, P2;
				
				if (fPolar)
				{
					double rho = line[0];
					double theta = line[1];
					
					double a = cos(theta), b = sin(theta);
					double x0 = a*rho, y0 = b*rho;
					P1.x = cvRound(x0 + 1000*(-b));
					P1.y = cvRound(y0 + 1000*(a));
					P2.x = cvRound(x0 - 1000*(-b));
					P2.y = cvRound(y0 - 1000*(a));
					
				}
					else
				{
					CvPoint *pPoint = (CvPoint *) line;
					
					P1 = pPoint[0];
					P2 = pPoint[1];
				}
				
				// cvLine(m_pImage->GetImage(), P1, P2, cvScalarAll(0));
				
				dc.MoveTo(ImageRect.left + (int) (P1.x * xScale), (int) (P1.y * yScale));
				dc.LineTo(ImageRect.left + (int) (P2.x * xScale), (int) (P2.y * yScale));
			}
			
			ReleaseData(L"Image paint");
			
			dc.SelectObject(pOldPen);
		}
	}

	if (m_fDrawHoughCircles)  // Need to overlay hough lines on image
	{
		CvSeq *pCircles = (CvSeq *) LockData(L"Image paint", DATA_HOUGH_CIRCLES, 0);

		if (pCircles)
		{
			CPen Pen(PS_SOLID, 1, RGB(0, 0, 255));
			CPen *pOldPen = dc.SelectObject(&Pen);
			
			double xScale = (double) (ImageRect.right-ImageRect.left) / (double) m_pImage->Width();
			double yScale = (double) (ImageRect.bottom-ImageRect.top) / (double) m_pImage->Height();

			for (int i = 0; i < pCircles->total; i++)
			{
				float *circle = (float *) cvGetSeqElem(pCircles, i);
				
				CvPoint pCenter = cvPoint(cvRound(circle[0]), cvRound(circle[1]));
				
				int radius = cvRound(circle[2]);
				int x1 = pCenter.x - radius, y1 = pCenter.y - radius;
				int x2 = pCenter.x + radius, y2 = pCenter.y + radius;
				
//				dc.Ellipse((int) (ImageRect.left + (x1 * xScale)),(int) (y1 *yScale), 
//					(int) (ImageRect.left + (x2 * xScale)), (int) (y2 * yScale) );
				dc.Arc((int) (ImageRect.left + (x1 * xScale)),(int) (y1 *yScale), 
					(int) (ImageRect.left + (x2 * xScale)), (int) (y2 * yScale), 
					(int) (ImageRect.left + (pCenter.x * xScale)),(int) (pCenter.y * yScale) - 10,
					(int) (ImageRect.left + (pCenter.x * xScale) + 1),(int) (pCenter.y * yScale) - 10);
			}
			
			ReleaseData(L"Image paint");
			
			dc.SelectObject(pOldPen);
		}
	}

}  

void COpImage::OnMouseMove( UINT nFlags, CPoint point )
{
	RECT Rect;
	static HCURSOR hOldCursor = NULL;

	if (m_fSettingROI && m_oROIUpperLeft.x)
	{
		RECT Rect = GetBaseDrawingArea();

		m_oROILowerRight = point;
		InvalidateRect(CRect(m_oROIUpperLeft.x-10, m_oROIUpperLeft.y-10, point.x+10, point.y+10));
	}
	else
	if (m_fSizing)
	{
		RECT ParentRect;

		GetClientRect(&Rect);
		ClientToScreen(&Rect);
		GetParent()->GetClientRect(&ParentRect);
		GetParent()->ClientToScreen(&ParentRect);

		ClientToScreen(&point);

		int Width = point.x-Rect.left+ParentRect.left;
		int Height = point.y - Rect.top;

		if (Width < GetMinWidth()) Width = GetMinWidth();
		if (Height < GetMinHeight()) Height = GetMinHeight();

			MoveWindow(Rect.left-ParentRect.left, Rect.top-ParentRect.top, Width, Height);

		UpdateWindow();
		GetParent()->InvalidateRect(NULL);
		GetParent()->UpdateWindow();
	}
	else if (m_fDragging || m_fConnecting)
	{
		COpBase::OnMouseMove(nFlags, point);
	}
	else if (m_fSettingPoints) 
	{
	}
	else  // Check for resizing
	{
		m_nMouseCurX = point.x;
		m_nMouseCurY = point.y;

		GetClientRect(&Rect);

		if (m_fShowStats) InvalidateRect(CRect(0, Rect.bottom - TITLE_HEIGHT, Rect.right, Rect.bottom));

		if (point.x > Rect.right - RIGHT_MARGIN - 20 && point.y > Rect.bottom - 20) 
		{
			if (!m_fInSizeArea)
			{
				HCURSOR hCursor = LoadCursor(NULL, IDC_SIZENWSE);

				hOldCursor = SetCursor(hCursor);

				m_fInSizeArea = TRUE;
			}

			if (point.y > Rect.bottom - 2)	// We're going out of the window so put the cursor back
			{
				SetCursor(hOldCursor);
				m_fInSizeArea = FALSE;

			}
		}
		else 
		{
			
			SetCursor(LoadCursor(NULL, IDC_ARROW));
			m_fInSizeArea = FALSE;
		}
	}
}

void COpImage::OnLButtonDown( UINT nFlags, CPoint point )
{
	if (!m_fSettingROI && m_fInSizeArea) // Resizing
	{
		m_fSizing = TRUE;
		SetCapture();
	}
	else if (m_fSettingROI)
	{
		m_oROIUpperLeft = point;
	}
	else 
	{
		if (m_sPointList.nNumPoints)
		{
				// Check to see if we're near a point - might be wanting to drag it
			RECT Rect = GetBaseDrawingArea();

				// Calculate actual image point
	//		double hRatio = (double)  m_pImage->Width() / (Rect.right - Rect.left);
	//		double vRatio = (double) m_pImage->Height() / (Rect.bottom - Rect.top);

			int ScaledX = (int) ((point.x - Rect.left) * (float) m_pImage->Width() / (Rect.right-Rect.left));
			int ScaledY = (int) ((point.y - Rect.top) * (float) m_pImage->Height() / (Rect.bottom-Rect.top));

			int i = 0;
			for (i = 0; i < m_sPointList.nNumPoints; i++)
				if (abs(m_sPointList.PointList[i].x - ScaledX) < POINT_DRAG_DIST && abs(m_sPointList.PointList[i].y - ScaledY) < POINT_DRAG_DIST) break;
				
			if (i < m_sPointList.nNumPoints)	// We found one close enough so we're going to drag it
			{
				m_fDraggingPoint = TRUE;
				m_nDragPointNdx = i;
			}
		}
		
		if (!m_fDraggingPoint) COpBase::OnLButtonDown(nFlags, point);  // Moving or connecting
	}
}

void COpImage::OnLButtonUp( UINT nFlags, CPoint point )
{
	if (!m_fSettingROI && m_fSizing) 
	{
		m_fSizing = FALSE;
		ReleaseCapture();
		GetParent()->InvalidateRect(NULL);
	}
	else 
	if (m_fSettingROI)
	{
		if (m_pImage) 
		{
			RECT Rect = GetBaseDrawingArea();

			double hRatio = (double)  m_pImage->Width() / (Rect.right - Rect.left);
			double vRatio = (double) m_pImage->Height() / (Rect.bottom - Rect.top);

			m_oROIUpperLeft.x -= Rect.left;
			point.x -= Rect.left;

//			cvRectangle(m_pImage->GetImage(), cvPoint(m_oROIUpperLeft.x * hRatio, m_oROIUpperLeft.y * vRatio),
//				cvPoint(point.x * hRatio, point.y * vRatio)	, cvScalarAll(0));


			int Height = m_pImage->GetImage()->height;
//			int Y = (int) m_pImage->Height() - ((point.y - Rect.top) * vRatio);
			int Y = (int) ((m_oROIUpperLeft.y - Rect.top) * vRatio);

			m_oROIRect = cvRect((int) (m_oROIUpperLeft.x * hRatio), Y, (int) ((point.x-m_oROIUpperLeft.x) * hRatio), 
					(int) ((point.y-m_oROIUpperLeft.y) * vRatio));
			cvSetImageROI(m_pImage->GetImage(), m_oROIRect);
			
				// This next line is required in order to get the ROI to show up-side right 
			m_pImage->CopyOf(cvCloneImage(m_pImage->GetImage()));
		}

		m_oROIUpperLeft.x = 0;
		m_oROIUpperLeft.y = 0;
		m_fSettingROI = false;
		InvalidateRect(NULL);
		SetData(0, DATA_IMAGE, 0, m_pImage->GetImage());
		SendData();	// Update our outputs with this new ROI

	}
	else if (m_fDraggingPoint)	// We're finishing up dragging a point around
	{
		RECT Rect = GetBaseDrawingArea();
		
			// Calculate actual image point
		int ScaledX = (int) ((point.x - Rect.left) * (float) m_pImage->Width() / (Rect.right-Rect.left));
		int ScaledY = (int) ((point.y - Rect.top) * (float) m_pImage->Height() / (Rect.bottom-Rect.top));

		if (abs(m_sPointList.PointList[m_nDragPointNdx].x - ScaledX) < POINT_DELETE_DIST && abs(m_sPointList.PointList[m_nDragPointNdx].y - ScaledY) < POINT_DELETE_DIST)
		{	// delete point
			m_sPointList.nNumPoints--;
			for (int j = m_nDragPointNdx; j < m_sPointList.nNumPoints; j++)
				m_sPointList.PointList[j] = m_sPointList.PointList[j+1];
		}
			else // change point
		{
				// Change dragged point in point list
			m_sPointList.PointList[m_nDragPointNdx].x = ScaledX;
			m_sPointList.PointList[m_nDragPointNdx].y = ScaledY;
		}
		
		SetData(1, DATA_POINT_LIST, sizeof(m_sPointList), &m_sPointList);
		SendData();

		InvalidateRect(NULL);

		m_fDraggingPoint = FALSE;
	}
	else // not sizing or setting ROI
	{

		if (!m_fConnecting)
		{
			RECT Rect = GetBaseDrawingArea();

				// Calculate actual image point
			double hRatio = (double)  m_pImage->Width() / (Rect.right - Rect.left);
			double vRatio = (double) m_pImage->Height() / (Rect.bottom - Rect.top);

			int ScaledX = (int) ((point.x - Rect.left) * (float) m_pImage->Width() / (Rect.right-Rect.left));
			int ScaledY = (int) ((point.y - Rect.top) * (float) m_pImage->Height() / (Rect.bottom-Rect.top));

			if (m_fSettingPoints)
			{
				if (m_sPointList.nNumPoints < MAX_POINTS)
				{
					m_sPointList.PointList[m_sPointList.nNumPoints].x = ScaledX;
					m_sPointList.PointList[m_sPointList.nNumPoints].y = ScaledY;
					m_sPointList.nNumPoints++;
				}
		
				SetData(1, DATA_POINT_LIST, sizeof(m_sPointList), &m_sPointList);
				
				InvalidateRect(NULL); //CRect(point.x - CROSSHAIR_LEN, point.y - CROSSHAIR_LEN, 
//					point.x + CROSSHAIR_LEN, point.y + CROSSHAIR_LEN));
			}
				else // Single point
			{
				m_oClickPoint.x = ScaledX;
				m_oClickPoint.y = ScaledY;
			
				SetData(1, DATA_POINT, sizeof(m_oClickPoint), &m_oClickPoint);
			}
			SendData();
		}

		COpBase::OnLButtonUp(nFlags, point);
	}
}

void COpImage::AddItemsToContextMenu(CMenu *pMenu)
{
	AddMenuItem(pMenu, L"", 0, TRUE);  // Separator
	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
	AddMenuItem(pMenu, L"Save Image", ID_SAVE_IMAGE);
	AddMenuItem(pMenu, L"Show Stats", ID_IMAGE_SHOW_STATS);
	
	CMenu *pSubMenu = new CMenu;

	if (pSubMenu)
	{
		if (pSubMenu->CreateMenu())
		{
			AddMenuSubMenu(pMenu, pSubMenu, L"Points");

			AddMenuItem(pSubMenu, L"Set Points", ID_IMAGE_SET_POINTS);
			AddMenuItem(pSubMenu, L"Clear Points", ID_IMAGE_CLEAR_POINTS);

			if (m_fSettingPoints) 
				pSubMenu->CheckMenuItem(ID_IMAGE_SET_POINTS, MF_CHECKED);

		}
	}

	pSubMenu = new CMenu;

	if (pSubMenu)
	{
		if (pSubMenu->CreateMenu())
		{
			AddMenuSubMenu(pMenu, pSubMenu, L"ROI");

			AddMenuItem(pSubMenu, L"Set ROI", ID_IMAGE_SET_ROI);
			AddMenuItem(pSubMenu, L"Reset ROI", ID_IMAGE_RESET_ROI);

			if (m_fSettingROI) 
				pSubMenu->CheckMenuItem(ID_IMAGE_SET_ROI, MF_CHECKED);
		}
	}


	if (m_fShowStats) 
		pMenu->CheckMenuItem(ID_IMAGE_SHOW_STATS, MF_CHECKED);


}


void COpImage::LoadImage()
{
	CFileDialog *pDialog = new CFileDialog(TRUE, L".jpg", NULL, 0, L"jpg;png;bmp|*.jpg;*.png;*.bmp||", NULL );

	if (pDialog->DoModal() == IDOK) {
		char Str[MAX_PATH];

		_tcscpy(m_aImageFileName, pDialog->GetPathName());
		wcstombs(Str, m_aImageFileName, _tcslen(m_aImageFileName)+1);

		m_pImage->Load(Str);

		OnClearPoints();	// Clear any marked points we might have from before

		SetData(0, DATA_IMAGE, 0, m_pImage->GetImage());
		SendData();

		InvalidateRect(NULL);
	}

	delete pDialog;

}

void COpImage::SaveImage()
{
	CFileDialog *pDialog = new CFileDialog(FALSE, L".jpg", NULL, 0, L"jpg;png;bmp|*.jpg;*.png;*.bmp||", NULL );

	if (pDialog->DoModal() == IDOK) {
		char Str[MAX_PATH];

		_tcscpy(m_aImageFileName, pDialog->GetPathName());
		wcstombs(Str, m_aImageFileName, _tcslen(m_aImageFileName)+1);

		m_pImage->Save(Str);
	}

	delete pDialog;

}

BOOL COpImage::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		int FileNameLen = (_tcslen(m_aImageFileName) + 1) * sizeof(TCHAR);

		WriteFile(hFile, &m_fShowStats, sizeof(m_fShowStats), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_oROIRect, sizeof(m_oROIRect), &dwBytesWritten, NULL);
		WriteFile(hFile, &FileNameLen, sizeof(FileNameLen), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_fSettingPoints, sizeof(m_fSettingPoints), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_sPointList, sizeof(m_sPointList), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_oClickPoint, sizeof(m_oClickPoint), &dwBytesWritten, NULL);

		if (WriteFile(hFile, m_aImageFileName, FileNameLen, &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bImageFiller, sizeof(bImageFiller), &dwBytesWritten, NULL);
		}
	}

	return fRetValue;
}

BOOL COpImage::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		int FileNameLen;

		ReadFile(hFile, &m_fShowStats, sizeof(m_fShowStats), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_oROIRect, sizeof(m_oROIRect), &dwBytesWritten, NULL);
		ReadFile(hFile, &FileNameLen, sizeof(FileNameLen), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_fSettingPoints, sizeof(m_fSettingPoints), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_sPointList, sizeof(m_sPointList), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_oClickPoint, sizeof(m_oClickPoint), &dwBytesWritten, NULL);

		if (ReadFile(hFile, m_aImageFileName, FileNameLen, &dwBytesWritten, NULL))
		{
			char Str[MAX_PATH];

			wcstombs(Str, m_aImageFileName, _tcslen(m_aImageFileName)+1);

			if (m_pImage->Load(Str))
			{
				if (m_oROIRect.width) 
				{
					cvSetImageROI(m_pImage->GetImage(), m_oROIRect);
						// This next line is required in order to get the ROI to show up-side right 
					m_pImage->CopyOf(cvCloneImage(m_pImage->GetImage()));
				}
				
				SetData(0, DATA_IMAGE, 0, m_pImage->GetImage()); // This is our output data
				if (m_sPointList.nNumPoints) 
					SetData(1, DATA_POINT_LIST, sizeof(POINT_LIST_TYPE), &m_sPointList);
			}

			ReadFile(hFile, bImageFiller, sizeof(bImageFiller), &dwBytesWritten, NULL);

			fRetValue = TRUE;
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpImage::NotifyNewDataIndex(int InputIndex)
{
	IplImage *pImage = (IplImage *) LockData(L"Image NotifyNewDataIndex", DATA_IMAGE, 0);
	if (pImage) 
	{
		m_pImage->CopyOf(pImage);

		EnterCriticalSection(&m_pImageCrit);

		if (m_pRawImage) cvReleaseImage(&m_pRawImage);

		m_pRawImage = cvCloneImage(pImage);  // Keep a copy for displaying values in OnPaint

		LeaveCriticalSection(&m_pImageCrit);

		if (m_sPointList.nNumPoints) 
			SetData(1, DATA_POINT_LIST, sizeof(POINT_LIST_TYPE), &m_sPointList);

		SendData();	// Since we don't do any processing, pass the data along to our outputs

		RECT Rect = GetBaseDrawingArea();

		InvalidateRect(&Rect, FALSE);

		ReleaseData(L"Image NotifyNewDataIndex");
	}
	
	if (LockData(L"Image NotifyNewDataIndex", DATA_HOUGH_LINES_POLAR, 0) ||
		LockData(L"Image NotifyNewDataIndex", DATA_HOUGH_LINES_CART, 0))
	{
		m_fDrawHoughLines = TRUE;
	
		ReleaseData(L"Image NotifyNewDataIndex");
	}
		else m_fDrawHoughLines = FALSE;

	if (LockData(L"Image NotifyNewDataIndex", DATA_HOUGH_CIRCLES, 0))
	{
		m_fDrawHoughCircles = TRUE;
	
		ReleaseData(L"Image NotifyNewDataIndex");
	}
		else m_fDrawHoughCircles = FALSE;

}

void COpImage::SetROI()
{
	m_fSettingROI = !m_fSettingROI;

}

void COpImage::ResetROI()
{
	if (m_pImage) 
	{
		char Str[MAX_PATH];

		wcstombs(Str, m_aImageFileName, _tcslen(m_aImageFileName)+1);

		m_pImage->Load(Str);
	}
	
	m_oROIRect.width = 0;	// Signal that the ROI is reset
	InvalidateRect(NULL);
	SendData();	// Update our outputs with this new ROI
}

void COpImage::OnShowStats()
{
	m_fShowStats = !m_fShowStats;
	InvalidateRect(NULL);
}

void COpImage::OnSetPoints()
{
	m_fSettingPoints = !m_fSettingPoints;
}

void COpImage::OnClearPoints()
{
	m_sPointList.nNumPoints = 0;
	
	InvalidateRect(NULL);
}

