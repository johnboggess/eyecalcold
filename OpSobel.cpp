#include "StdAfx.h"
#include "OpSobel.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bSobelFiller[32];

BEGIN_MESSAGE_MAP(COpSobel, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
END_MESSAGE_MAP()

COpSobel::COpSobel(CMainView *pMain) : COpBase(pMain)
{
	m_pXOrderSlider = NULL;
	m_pXOrderEdit = NULL;
	m_pYOrderSlider = NULL;
	m_pYOrderEdit = NULL;
	m_pApertureSlider = NULL;
	m_pApertureEdit = NULL;

		// Save data
	m_nXOrder = 1;
	m_nYOrder = 0;
	m_nAperture = 3;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpSobel::~COpSobel(void)
{
	if (m_pXOrderSlider) delete m_pXOrderSlider;
	if (m_pXOrderEdit) delete m_pXOrderEdit;
	if (m_pYOrderSlider) delete m_pYOrderSlider;
	if (m_pYOrderEdit) delete m_pYOrderEdit;
	if (m_pApertureSlider) delete m_pApertureSlider;
	if (m_pApertureEdit) delete m_pApertureEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpSobel::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpSobel::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	CreateOpStatic(L"x order", TRUE, 0);
	m_pXOrderEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pXOrderSlider = CreateOpSlider(0, 0, 2);
//	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"y order", TRUE, 0);
	m_pYOrderEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pYOrderSlider = CreateOpSlider(0, 0, 2);
//	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L"aperture", TRUE, 0);
	m_pApertureEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pApertureSlider = CreateOpSlider(0, 1, 7);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpSobel::StartOpThread(void *pData)
{
	COpSobel *pBase = (COpSobel *) pData;

	pBase->OpThread();

	return 0;
}

void COpSobel::UpdateSliders()
{
	m_pXOrderSlider->SetPos(m_nXOrder);
	m_pYOrderSlider->SetPos(m_nYOrder);
	m_pApertureSlider->SetPos(m_nAperture);

}
void COpSobel::UpdateValues()
{
	TCHAR TStr[10];

	_itot(m_nXOrder, TStr, 10);
	m_pXOrderEdit->SetWindowTextW(TStr);

	_itot(m_nYOrder, TStr, 10);
	m_pYOrderEdit->SetWindowTextW(TStr);

	_itot(m_nAperture, TStr, 10);
	m_pApertureEdit->SetWindowTextW(TStr);

}


void COpSobel::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpSobel::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nXOrder, sizeof(m_nXOrder), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nYOrder, sizeof(m_nYOrder), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nAperture, sizeof(m_nAperture), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bSobelFiller, sizeof(bSobelFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpSobel::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nXOrder, sizeof(m_nXOrder), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nYOrder, sizeof(m_nYOrder), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nAperture, sizeof(m_nAperture), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bSobelFiller, sizeof(bSobelFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpSobel::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpSobel::ValidateParameters()
{
	if (m_nAperture %2 == 0) m_nAperture++;	// must be odd
	
	if (m_nXOrder == 0 && m_nYOrder == 0) m_nXOrder = 1;
	
	if (m_nAperture == 3 && m_nXOrder + m_nYOrder > 1) { m_nXOrder = 1; m_nYOrder = 0; }
}

void COpSobel::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pXOrderSlider) 
		m_nXOrder = pSlider->GetPos();

	if (pSlider == m_pYOrderSlider) 
		m_nYOrder = pSlider->GetPos();

	if (pSlider == m_pApertureSlider) 
		m_nAperture = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpSobel::OnChange()
{
}

void COpSobel::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"SobelThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (1) {	// Validate Src and Dst images, etc

				int Depth = pImage->depth;
				if (Depth == IPL_DEPTH_8U)Depth = IPL_DEPTH_16S;  // to avoid overflow
				
				IplImage *pNewImage = cvCreateImage(cvGetSize(pImage), Depth, pImage->nChannels);
				
				if (pNewImage)
				{
					int nAperture = m_nAperture;
					
					if (nAperture == 3) nAperture = CV_SCHARR;  // Faster better for 3x3
					
					cvSobel(pImage, pNewImage, m_nXOrder, m_nYOrder, nAperture);

					SetData(0, DATA_IMAGE, 0, pNewImage, TRUE);
					SendData();
				}
			}
				else TitleError(L"");

			ReleaseData(L"SobelThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
