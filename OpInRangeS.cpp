//@Hai the same as InRange but the range is entered by slider

#include "StdAfx.h"
#include "OpInRangeS.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bInRangeSFiller[32];





BEGIN_MESSAGE_MAP(COpInRangeS, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpInRangeS::COpInRangeS(CMainView *pMain) : COpBase(pMain)
{

	m_pUpperSlider = NULL;
	m_pLowerSlider = NULL;
	m_pUpperEdit = NULL;
	m_pLowerEdit = NULL;

	m_nUpperShift = 0;
	m_nLowerShift = 0;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpInRangeS::~COpInRangeS(void)
{
	if (m_pUpperSlider)delete m_pUpperSlider;
	if (m_pLowerSlider)delete m_pLowerSlider;
	if (m_pUpperEdit) delete m_pUpperEdit;
	if (m_pLowerEdit) delete m_pLowerEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpInRangeS::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpInRangeS::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
	CreateOpStatic(L"Upper", TRUE, 0);
	m_pUpperEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE); //@hai changes to get more space to display the slider value
	m_pUpperSlider = CreateOpSlider(0, 0, 255);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"Lower", TRUE, 0);
	m_pLowerEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pLowerSlider = CreateOpSlider(0, 0, 255);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpInRangeS::StartOpThread(void *pData)
{
	COpInRangeS *pBase = (COpInRangeS *) pData;

	pBase->OpThread();

	return 0;
}

void COpInRangeS::UpdateSliders()
{
	m_pUpperSlider->SetPos(-1);
	m_pLowerSlider->SetPos(-1);
	m_pUpperSlider->SetPos(m_nUpperShift);
	m_pLowerSlider->SetPos(m_nLowerShift);

}
void COpInRangeS::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

	_itot(m_nUpperShift, TStr, 10);
	m_pUpperEdit->SetWindowTextW(TStr);
	_itot(m_nLowerShift, TStr, 10);
	m_pLowerEdit->SetWindowTextW(TStr);


}


void COpInRangeS::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpInRangeS::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nUpperShift, sizeof(m_nUpperShift), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nLowerShift, sizeof(m_nLowerShift), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bInRangeSFiller, sizeof(bInRangeSFiller), &dwBytesWritten, NULL);
		}
	}

	return fRetValue;
}

BOOL COpInRangeS::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nUpperShift, sizeof(m_nUpperShift), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nLowerShift, sizeof(m_nLowerShift), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bInRangeSFiller, sizeof(bInRangeSFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}

	}

	return fRetValue;
}

	// We got data from the given input index
void COpInRangeS::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpInRangeS::ValidateParameters()
{
}

void COpInRangeS::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pUpperSlider) 
	{
		m_nUpperShift = pSlider->GetPos();
		if (m_nUpperShift < m_nLowerShift)
			m_nLowerShift = m_nUpperShift;
	}
	else if (pSlider == m_pLowerSlider) 
	{
		m_nLowerShift = pSlider->GetPos();
		if (m_nUpperShift < m_nLowerShift)
			m_nUpperShift=m_nLowerShift;
	}


	ValidateParameters();

	UpdateValues();
	UpdateSliders();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpInRangeS::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

//void COpInRangeS::OnButton()
//{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
//}

void COpInRangeS::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpInRangeS::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage0 = (IplImage *) LockData(L"AbsDiffThread", DATA_IMAGE, 0);
			if (pImage0 )  	// Validate Src and Dst images, etc
			{
					IplImage *pDest = cvCreateImage(cvGetSize(pImage0),8, 1);
					cvInRangeS(pImage0,cvScalarAll((double)m_nLowerShift),cvScalarAll((double)m_nUpperShift), pDest);
					if (pDest)
					{
						SetData(0, DATA_IMAGE, 0, pDest, TRUE);
						SendData();
					}
			}
			else 
				TitleError(L"Invalid input");
		ReleaseData(L"AbsDiffThread");// We're done with the data so it can change if needed	
	}
		WaitForSingleObject(m_hThreadEvent, INFINITE);
	
}
