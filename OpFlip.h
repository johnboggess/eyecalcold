#pragma once
#include "opbase.h"
#include "Cv.h"

#define Flip_DEFAULT_WIDTH (140)
#define Flip_DEFAULT_HEIGHT (60)

class COpFlip : public COpBase
{
public:
	COpFlip(CMainView *pMain);
	~COpFlip(void);

	virtual	PTCHAR GetFriendlyName() { return L"Flip"; }
	virtual int	GetDefaultWidth() { return Flip_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Flip_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CComboBox	*m_pFlipTypeCombo;
	TCHAR		m_aFlipTypeStr[20];

		// Save data
	int			m_nFlipType;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void FlipTypeChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
