// OpBase.cpp : implementation file
//

#include "stdafx.h"
#include "EyeCalc.h"
#include "OpBase.h"
#include "OpSelDlg.h"
#include "cxtypes.h"
#include "cxcore.h"

BYTE bFiller[128];

extern "C" const PTCHAR g_aOpList;

// COpBase

IMPLEMENT_DYNAMIC(COpBase, CWnd)

COpBase::COpBase(CMainView *pAMain)
{
	m_pMain = pAMain;

	m_fDragging = FALSE;
	m_fConnecting = FALSE;
	m_nNumInputs = 0;
	m_nNumOutputs = 0;
	m_oOldEndPoint.x = 0;
	m_oOldEndPoint.y = 0;
	m_nOperator = 0;
	m_dwUniqueID = 0;
	m_fDisabled = FALSE;
	m_nCurControlY = 0;
	m_nCurControlX = 0;
	m_fMinimized = FALSE;
	m_bNumControls = 0;
	m_fTitleBarShowing = TRUE;
	m_dwInputLockCount = 0;
	m_pErrorText[0] = 0;
	m_fErrorFlag = FALSE;


	memset(&m_sOutputData, 0, sizeof(m_sOutputData));
	memset(m_sInputDataCache, 0, sizeof(m_sInputDataCache));
	memset(m_sInputDataWorkingCopy, 0, sizeof(m_sInputDataWorkingCopy));
	memset(m_fDeallocateWhenDone, 0, sizeof(m_fDeallocateWhenDone));
	memset(m_pInputData, 0, sizeof(m_pInputData));

	m_hDataInUseMutex = CreateMutex(NULL, FALSE, L"OpDataInUse");
	m_hNewInputDataMutex = CreateMutex(NULL, FALSE, L"OpNewInputData");
}

COpBase::~COpBase()
{
	CloseHandle(m_hDataInUseMutex);
	CloseHandle(m_hNewInputDataMutex);

}


BEGIN_MESSAGE_MAP(COpBase, CWnd)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_DELETE_BASE, DeleteBasePrompt)
	ON_COMMAND(ID_OP_DISABLE, OpDisable)
	ON_COMMAND(ID_OP_CHANGE, OpChangeOperator)
	ON_COMMAND(ID_OP_SHOWDATA, OpShowData)
	ON_COMMAND(ID_SHOW_ERROR, OnShowError)
END_MESSAGE_MAP()

RECT COpBase::GetBaseDrawingArea()
{
	RECT Rect;

	GetClientRect(&Rect);

	Rect.top += 1;
	Rect.left += LEFT_MARGIN;
	Rect.right -= RIGHT_MARGIN;

	if (m_fTitleBarShowing)
		Rect.bottom -= TITLE_HEIGHT + 3;
	else
		Rect.bottom -= 3;  // No title bar showing

	return Rect;
}

	// Note be sure to update GetBaseDrawingArea when changing location and/or size of base painting
void COpBase::PaintBase(CPaintDC *pDc, BOOL fDisplayTitle)
{
	RECT Rect;

	GetClientRect(&Rect);

	CBrush MyBrush(OPBASE_BACKCOLOR);

	Rect.left += LEFT_MARGIN;
	Rect.right -= RIGHT_MARGIN;

	CBrush *pOldBrush = pDc->SelectObject(&MyBrush);

	if (fDisplayTitle)
	{
		pDc->RoundRect(&Rect, CPoint(10, 10));
		pDc->SelectObject(pOldBrush);
	}

//	int Inc = (Rect.bottom - Rect.top)/ (MAX_INPUTS * LEFT_MARGIN);
	int Inc = 0;

	for (int i = 0; i < MAX_INPUTS; i++) {
			// NOTE: If you change how the inputs are drawn, be sure to check that it doesn't break OnLButtonDblClk
			//			which is used to delete connections.  Also check GetConnectionPoint function
		pDc->Ellipse(Rect.left-LEFT_MARGIN, Rect.top+i*(LEFT_MARGIN+Inc), Rect.left-2, Rect.top+i*(LEFT_MARGIN+Inc)+LEFT_MARGIN);

		CBrush GoodConnectionBrush(GOOD_CONNECTION_COLOR);

		CBrush *pOld2 = pDc->SelectObject(&GoodConnectionBrush);

		if (i < m_nNumInputs) pDc->FloodFill(Rect.left-LEFT_MARGIN + 2, Rect.top+i*(LEFT_MARGIN+Inc)+2, CONNECTION_COLOR);

		pDc->SelectObject(pOld2);
	}

	for (int i = 0; i < MAX_OUTPUTS; i++) {
			// NOTE: If you change how the outputs are drawn, be sure to check that it doesn't break OnLButtonDblClk
			//			which is used to delete connections
		pDc->Ellipse(Rect.right+2, Rect.bottom-i*(RIGHT_MARGIN+Inc)-(3*RIGHT_MARGIN), Rect.right+RIGHT_MARGIN, Rect.bottom-i*(RIGHT_MARGIN+Inc)-(2*RIGHT_MARGIN));

		CBrush GoodConnectionBrush(GOOD_CONNECTION_COLOR);

		CBrush *pOld2 = pDc->SelectObject(&GoodConnectionBrush);

		if ((MAX_OUTPUTS-i-1) < m_nNumOutputs) pDc->FloodFill(Rect.right+4, Rect.bottom-i*(RIGHT_MARGIN+Inc)-(3 * RIGHT_MARGIN)+2, CONNECTION_COLOR);

		pDc->SelectObject(pOld2);

	}

	if (fDisplayTitle) 
	{
		m_fTitleBarShowing = TRUE;

		COLORREF BackColor = TITLE_BACK_COLOR;

		if (m_fDisabled) BackColor = TITLE_DISABLED_COLOR;

		if (m_fErrorFlag) BackColor = TITLE_ERROR_COLOR;

			// Display title
		GetClientRect(&Rect);
		pDc->MoveTo(Rect.left+LEFT_MARGIN, Rect.bottom - TITLE_HEIGHT - 2);
		pDc->LineTo(Rect.right-RIGHT_MARGIN, Rect.bottom - TITLE_HEIGHT - 2);
		CBrush TitleBrush(BackColor);
		pDc->SelectObject(&TitleBrush);
		pDc->FloodFill(Rect.left+LEFT_MARGIN + 2, Rect.bottom - TITLE_HEIGHT + 4, BORDER_COLOR);
		Rect.top = Rect.bottom - TITLE_HEIGHT;
		pDc->SetBkColor(BackColor);
		pDc->DrawText(GetFriendlyName(), _tcslen(GetFriendlyName()), &Rect, DT_CENTER);
	}
	else m_fTitleBarShowing = FALSE;

	pDc->SelectObject(pOldBrush);
}


void COpBase::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	PaintBase(&dc, TRUE);
}

void COpBase::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CMenu Menu;

	if (Menu.LoadMenu(IDR_OPBASE_MENU))
	{

		CMenu *pPopup = Menu.GetSubMenu(0);

		if (m_fDisabled) pPopup->CheckMenuItem(ID_OP_DISABLE, MF_CHECKED);
		else pPopup->CheckMenuItem(ID_OP_DISABLE, MF_UNCHECKED);

		AddItemsToContextMenu(pPopup);

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
				point.x, point.y,
				this); 

	}

	return;
}

void COpBase::AddItemsToContextMenu(CMenu *pMenu)
{
}

void COpBase::AddMenuItem(CMenu *pMenu, PTCHAR ItemStr, WORD wID, BOOL fSeparator)
{
	UINT nNumItems = pMenu->GetMenuItemCount();
	MENUITEMINFO Info;

	Info.cbSize = sizeof(Info);
	Info.fMask = MIIM_DATA | MIIM_TYPE | MIIM_ID;
	if (fSeparator) Info.fType = MFT_SEPARATOR;
		else Info.fType = MFT_STRING;
	Info.fState = MFS_ENABLED;
	Info.hSubMenu = NULL;
	Info.hbmpChecked = NULL;
	Info.hbmpUnchecked = NULL;

	Info.wID = wID;
	Info.dwTypeData = ItemStr;
	Info.cch = _tcslen(ItemStr);

	pMenu->InsertMenuItemW(nNumItems, &Info, TRUE); 
}

void COpBase::AddMenuSubMenu(CMenu *pMenu, CMenu *pSubMenu, PTCHAR SubMenuNameStr)
{
	UINT nNumItems = pMenu->GetMenuItemCount();
	MENUITEMINFO Info;

	Info.cbSize = sizeof(Info);
	Info.fMask = MIIM_DATA | MIIM_TYPE | MIIM_ID | MIIM_SUBMENU;
	Info.fType = MFT_STRING;
	Info.fState = MFS_ENABLED;
	Info.hSubMenu = pSubMenu->m_hMenu;
	Info.hbmpChecked = NULL;
	Info.hbmpUnchecked = NULL;

	Info.wID = 0;
	Info.dwTypeData = SubMenuNameStr;
	Info.cch = _tcslen(SubMenuNameStr);

	pMenu->InsertMenuItemW(nNumItems, &Info, TRUE); 
}

CComboBox *COpBase::CreateOpCombo(int nWidth, UINT uID)
{
	RECT Rect = GetBaseDrawingArea();

	Rect.left += CONTROL_BORDER;
	Rect.right -= CONTROL_BORDER;

	if (m_nCurControlX < Rect.left) m_nCurControlX = Rect.left;

	CComboBox *pCombo = new CComboBox();

	if (pCombo)
	{
		m_pControlList[m_bNumControls++] = pCombo;

		int nControlWidth = nWidth;

		if (!nControlWidth) nControlWidth = Rect.right - m_nCurControlX;

		pCombo->Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 
			CRect(m_nCurControlX, m_nCurControlY + (CTRL_GAP / 2), m_nCurControlX + nControlWidth, m_nCurControlY + (CTRL_GAP / 2)+ COMBO_HEIGHT), this, uID);

		if (!nWidth) { m_nCurControlY += CTRL_GAP + COMBO_HEIGHT; m_nCurControlX = Rect.left; }
		else m_nCurControlX += nWidth;

	}

	return pCombo;
}

CStatic *COpBase::CreateOpStatic(PTCHAR pText, BOOL fCenter, int nWidth)
{
	CStatic *pStatic = NULL;

	RECT Rect = GetBaseDrawingArea();

	Rect.left += CONTROL_BORDER;
	Rect.right -= CONTROL_BORDER;

	if (pText == L"")	// Blank line
	{
		m_nCurControlY += CTRL_GAP;
		m_nCurControlX = Rect.left;
	}
	else if (pText[0] == ' ')	// spacer
	{
		m_nCurControlX += nWidth;
	}
	else
	{
		if (m_nCurControlX < Rect.left) m_nCurControlX = Rect.left;

		pStatic = new CStatic;

		if (pStatic)
		{
			m_pControlList[m_bNumControls++] = pStatic;

			int nControlWidth = nWidth;

			if (!nControlWidth) nControlWidth = Rect.right- m_nCurControlX;

			DWORD dwStyle = WS_CHILD | WS_VISIBLE;
			
			if (fCenter) dwStyle |= SS_CENTER;

			pStatic->Create(pText, dwStyle,
				CRect(m_nCurControlX, m_nCurControlY + TEXT_GAP, m_nCurControlX + nControlWidth, m_nCurControlY+TEXT_GAP+TEXT_HEIGHT), this, 0);
			
			if (!nWidth) { m_nCurControlY += CTRL_GAP; m_nCurControlX = Rect.left; }
			else m_nCurControlX += nWidth;
		}
	}

	return pStatic;
}

CSliderCtrl *COpBase::CreateOpSlider(int nWidth, int RangeMin, int RangeMax, UINT uID)
{
	RECT Rect = GetBaseDrawingArea();

	Rect.left += CONTROL_BORDER;
	Rect.right -= CONTROL_BORDER;

	if (m_nCurControlX < Rect.left) m_nCurControlX = Rect.left;

	CSliderCtrl *pSlider = new CSliderCtrl();

	if (pSlider)
	{
		m_pControlList[m_bNumControls++] = pSlider;

		int nControlWidth = nWidth;

		if (!nControlWidth) nControlWidth = Rect.right - m_nCurControlX;

		Rect.top += 10;

		pSlider->Create(TBS_HORZ | TBS_AUTOTICKS | TBS_BOTTOM, 
			CRect(m_nCurControlX , m_nCurControlY + CTRL_GAP, m_nCurControlX + nControlWidth, m_nCurControlY+CTRL_GAP + SLIDER_HEIGHT), this, uID);

		pSlider->SetRange(RangeMin, RangeMax);
		pSlider->ShowWindow(SW_SHOW);

		if (!nWidth) { m_nCurControlY += CTRL_GAP + SLIDER_HEIGHT; m_nCurControlX = Rect.left; }
		else m_nCurControlX += nWidth;
	}

	return pSlider;
}

CEdit *COpBase::CreateOpEdit(int nWidth, BOOL fReadOnly, UINT uID)
{
	RECT Rect = GetBaseDrawingArea();

	Rect.left += CONTROL_BORDER;
	Rect.right -= CONTROL_BORDER;

	if (m_nCurControlX < Rect.left) m_nCurControlX = Rect.left;

	CEdit *pEdit = new CEdit;

	if (pEdit)
	{
		m_pControlList[m_bNumControls++] = pEdit;

		int nControlWidth = nWidth;

		if (!nControlWidth) nControlWidth = Rect.right - m_nCurControlX;

		pEdit->Create(WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL, 
			CRect(m_nCurControlX, m_nCurControlY + CTRL_GAP, m_nCurControlX + nControlWidth, m_nCurControlY + CTRL_GAP + EDIT_HEIGHT), this, uID);
		if (fReadOnly) pEdit->SetReadOnly(TRUE);
//		pEdit->SetWindowTextW(L"0");
		pEdit->ShowWindow(SW_SHOW);

		if (!nWidth) { m_nCurControlY += CTRL_GAP + EDIT_HEIGHT; m_nCurControlX = Rect.left; }
		else m_nCurControlX += nWidth;

	}

	return pEdit;
}

CButton *COpBase::CreateOpButton(PTCHAR pCaption, int nWidth, UINT uStyle, UINT uID)
{
	RECT Rect = GetBaseDrawingArea();

	Rect.left += CONTROL_BORDER;
	Rect.right -= CONTROL_BORDER;

	if (m_nCurControlX < Rect.left) m_nCurControlX = Rect.left;

	CButton *pButton = new CButton;

	if (pButton)
	{
		m_pControlList[m_bNumControls++] = pButton;

		int nControlWidth = nWidth;

		if (!nControlWidth) nControlWidth = Rect.right - m_nCurControlX;

		pButton->Create(pCaption, uStyle, 
			CRect(m_nCurControlX, m_nCurControlY + CTRL_GAP, m_nCurControlX + nControlWidth, m_nCurControlY + CTRL_GAP + BUTTON_HEIGHT), this, uID);
		pButton->ShowWindow(SW_SHOW);

		if (!nWidth) { m_nCurControlY += CTRL_GAP + BUTTON_HEIGHT; m_nCurControlX = Rect.left; }
		else m_nCurControlX += nWidth;

	}

	return pButton;
}

void COpBase::OnMouseMove( UINT nFlags, CPoint point )
{
	RECT Rect;
	RECT ParentRect;

	GetWindowRect(&Rect);
	GetParent()->GetWindowRect(&ParentRect);

	if (m_fDragging) {

		CPoint ScreenPoint = point;
		ClientToScreen(&ScreenPoint);

		MoveWindow(ScreenPoint.x -ParentRect.left - m_oOldPoint.x, ScreenPoint.y-ParentRect.top - m_oOldPoint.y, Rect.right-Rect.left, Rect.bottom-Rect.top);

		GetWindowRect(&Rect);
//		if (Rect.right > ParentRect.right + (Rect.right-Rect.left) / 4) {	// Right side of screen - Delete operator
		if (Rect.top < -((Rect.bottom-Rect.top) / 4)) {	// Right side of screen - Delete operator
			StopBase();
			DeleteBase(FALSE);
		}

		GetParent()->InvalidateRect(NULL);
	}
	else
	if (m_fConnecting) 
	{
		ClientToScreen(&point);

		int x1 = m_oOldEndPoint.x;
		int y1 = m_oOldEndPoint.y;
		int x2 = m_oConnectPoint.x;
		int y2 = m_oConnectPoint.y - ParentRect.top - 2;
		int temp;

		if (x1 > x2) { temp = x1; x1 = x2; x2 = temp; }
		if (y1 > y2) { temp = y1; y1 = y2; y2 = temp; }

		x1 -= 10;
		y1 -= 10;
		x2 += 10;
		y2 += 10;

		GetParent()->InvalidateRect(CRect(x1, y1, x2, y2));
		GetParent()->UpdateWindow();

		CDC *pDC = GetParent()->GetDC();

		pDC->MoveTo(m_oConnectPoint.x, m_oConnectPoint.y - ParentRect.top - 2);
		pDC->LineTo(point.x, point.y - ParentRect.top);

		m_oOldEndPoint.x = point.x;
		m_oOldEndPoint.y = point.y - ParentRect.top;
	}
}

void COpBase::OnLButtonDown( UINT nFlags, CPoint point )
{
	RECT Rect;

	GetClientRect(&Rect);

		// Don't allow dragging from the margins as those are for inputs and outputs
	if (point.x > LEFT_MARGIN && point.x < Rect.right - RIGHT_MARGIN) {
		m_oOldPoint = point;

		m_fDragging = TRUE;

	}
	else
	{
		m_fConnecting = TRUE;
		m_oConnectPoint = point;
		ClientToScreen(&m_oConnectPoint);
		
		if (point.x < LEFT_MARGIN) m_eConnectingType = CONNECT_INPUT;
		else m_eConnectingType = CONNECT_OUTPUT;
	}

	SetCapture();

}

void COpBase::OnLButtonUp( UINT nFlags, CPoint point )
{
	ReleaseCapture();

	if (m_fConnecting) {  // See if we connected to anything
			// First erase current line
		GetParent()->InvalidateRect(NULL);
		GetParent()->UpdateWindow();

		ClientToScreen(&point);

			// Now check to see if we are connected to another COpBase
		for (int i = 0; i < m_pMain->GetNumBases(); i++)
			if (m_pMain->GetOpBase(i) && (m_pMain->GetOpBase(i)->IsInput(point) && m_pMain->GetOpBase(i) != this && m_eConnectingType == CONNECT_OUTPUT))
			{
				if (m_pMain->GetOpBase(i)->ConnectBases(CONNECT_INPUT, this))
				{
					ConnectBases(CONNECT_OUTPUT, m_pMain->GetOpBase(i));
					
					m_pMain->GetOpBase(i)->NotifyNewData(this);	// Tell the new base that we have data for it
				}
			}
			else
			if (m_pMain->GetOpBase(i) && (m_pMain->GetOpBase(i)->IsOutput(point) && m_pMain->GetOpBase(i) != this && m_eConnectingType == CONNECT_INPUT)) { // Yes, we're trying to connect
				if (m_pMain->GetOpBase(i)->ConnectBases(CONNECT_OUTPUT, this))
				{
					ConnectBases(CONNECT_INPUT, m_pMain->GetOpBase(i));

					NotifyNewData(m_pMain->GetOpBase(i));	// Tell ourselves that we have a new input
				}
				break;
			}

			// Draw new connections
		GetParent()->InvalidateRect(NULL);
		GetParent()->UpdateWindow();

	}

	m_fDragging = FALSE;
	m_fConnecting = FALSE;

}


	// Is this point on one of our inputs?
BOOL COpBase::IsInput(CPoint point)
{
	BOOL fRetValue = FALSE;

	RECT Rect;

	GetClientRect(&Rect);
	ClientToScreen(&Rect);

	if (point.x > Rect.left && point.x < (Rect.left + Rect.right) / 2 && point.y > Rect.top && point.y < Rect.bottom) {
		fRetValue = true;
	}

	return fRetValue;
}

	// Is this point on one of our outputs?
BOOL COpBase::IsOutput(CPoint point)
{
	BOOL fRetValue = FALSE;

	RECT Rect;

	GetClientRect(&Rect);
	ClientToScreen(&Rect);

	if (point.x < Rect.right && point.x > (Rect.left + Rect.right) / 2 && point.y > Rect.top && point.y < Rect.bottom) {
		fRetValue = true;
	}

	return fRetValue;
}

BOOL COpBase::ConnectBases(ConnectionType eConnection, COpBase *pNewBase) 
{
	BOOL fRetValue = FALSE;

	if (eConnection == CONNECT_INPUT && m_nNumInputs < MAX_INPUTS) {
		int i;
			// Determine if we'r already connected
		for (i = 0; i < m_nNumInputs; i++)
			if (m_pInputs[i] == pNewBase) break;

		if (i >= m_nNumInputs) { // Not connected so OK to connect
			m_pInputs[m_nNumInputs] = pNewBase;
			m_nNumInputs++;

			fRetValue = TRUE;
		}
	}
	else if (eConnection == CONNECT_OUTPUT && m_nNumOutputs < MAX_OUTPUTS) {
			// Determine if we'r already connected
		int i;
		for (i = 0; i < m_nNumOutputs; i++)
			if (m_pOutputs[i] == pNewBase) break;

		if (i >= m_nNumOutputs) {
			m_pOutputs[m_nNumOutputs] = pNewBase;
			m_nNumOutputs++;

			fRetValue = TRUE;
		}
	}

	return fRetValue;
}

void COpBase::DeallocateData(void *pData, DataType eDataType, StorageType eDataStorageType, void *pDataStorage)
{
	if (eDataType == DATA_IMAGE) {
		Log(LOG_DATA, L"      %s %x deallocating image %x", GetFriendlyName(), this, pData);

		cvReleaseImage((IplImage **) &pData);
	}
	else if (eDataType == DATA_KERNEL)
	{
		Log(LOG_DATA, L"      %s %x deallocating kernel %x", GetFriendlyName(), this, pData);

		KERNEL_TYPE *pKernel = (KERNEL_TYPE *) pData;
		
		cvReleaseMat(&pKernel->pKernel);
		delete pKernel;
	}
	else if (eDataType == DATA_HISTOGRAM)
	{
		Log(LOG_DATA, L"      %s %x deallocating histogram %x", GetFriendlyName(), this, pData);

		CvHistogram *pHist = (CvHistogram *) pData;
		
		cvReleaseHist(&pHist);
	}
	else if (eDataType == DATA_CONTOUR)
	{
		Log(LOG_DATA, L"      %s %x deallocating contour %x", GetFriendlyName(), this, pData);

		cvReleaseMemStorage((CvMemStorage **) &pDataStorage);

	}

		// NOTE: THIS SHOULD BE THE LAST IN THE ELSE-IF LIST BECAUSE IT IGNORES eDataType	
	else if (eDataStorageType == DATA_STORAGE_MEMSTORAGE)
	{
		Log(LOG_DATA, L"      %s %x deallocating seq %x", GetFriendlyName(), this, pData);

		cvReleaseMemStorage((CvMemStorage **) &pDataStorage);
	}

	else
		delete[] pData;
}

BOOL COpBase::DisconnectBases(COpBase *pCurBase)
{
	BOOL fRetValue = FALSE;

		// See if this is an input
	for (int i = 0; i < m_nNumInputs; i++)
		if (m_pInputs[i] == pCurBase) {

			if (m_sInputDataCache[i].bNumItems) // We have a cache, delete the allocated memory
			{
				for (int k = 0; k < m_sInputDataCache[i].bNumItems; k++)
				{
					DeallocateData(m_sInputDataCache[i].pData[k], m_sInputDataCache[i].eDataType[k], 
							m_sInputDataCache[i].eDataStorageType[k], m_sInputDataCache[i].pDataStorage[k]);
				}							
			}

			for (int j = i; j < m_nNumInputs-1; j++)	// Delete connection from input list
			{
				m_pInputs[j] = m_pInputs[j+1];
				m_pInputData[j] = m_pInputData[j+1];
				m_sInputDataCache[j] = m_sInputDataCache[j+1];
			}

			m_nNumInputs--;

				// Zero out the last now-unused element
			m_pInputs[m_nNumInputs] = NULL;
			m_pInputData[m_nNumInputs] = NULL;
			m_sInputDataCache[m_nNumInputs].bNumItems = 0;

//md%
NotifyNewDataIndex(0);	// Tell ourselves that our input data has changed (eg. been deleted)			

			fRetValue = TRUE;
			break;
		}

	if (!fRetValue)
	{
			// See if this is an output
		for (int i = 0; i < m_nNumOutputs; i++)
			if (m_pOutputs[i] == pCurBase) {

				for (int j = i; j < m_nNumOutputs-1; j++)	// Delete connection from input list
					m_pOutputs[j] = m_pOutputs[j+1];

				m_nNumOutputs--;

				fRetValue = TRUE;
				break;
			}

	}

	return fRetValue;
}

int	COpBase::GetConnectionPoint(ConnectionType eConnectType, BYTE bConnectIndex, BYTE X) {

	RECT Rect;
	RECT ParentRect;

	GetClientRect(&Rect);
	ClientToScreen(&Rect);

	GetParent()->GetWindowRect(&ParentRect);

	if (eConnectType == CONNECT_INPUT) { // Input
		if (X) return Rect.left - ParentRect.left;
		else return Rect.top+bConnectIndex*(LEFT_MARGIN)+(LEFT_MARGIN / 2)- 2 - ParentRect.top;
	}
	else // Output
	{
		if (X) return Rect.right - 3 - ParentRect.left;
		else return Rect.bottom-(MAX_OUTPUTS-bConnectIndex)*(RIGHT_MARGIN) - (2 * RIGHT_MARGIN) - ParentRect.top;
	}
}

	// Return the requested OpBase * for the given ConnectionType and index
COpBase *COpBase::GetConnection(ConnectionType eConnectType, BYTE bConnectIndex) 
{
	if (eConnectType == CONNECT_INPUT && bConnectIndex < m_nNumInputs) return m_pInputs[bConnectIndex];
	else if (eConnectType == CONNECT_OUTPUT && bConnectIndex < m_nNumOutputs) return m_pOutputs[bConnectIndex];

	return NULL;
}

	// Delete connections
void COpBase::OnLButtonDblClk( UINT nFlags, CPoint point ) {
	RECT Rect;

	GetClientRect(&Rect);

	if (point.x < LEFT_MARGIN) {
		int Ndx = point.y / LEFT_MARGIN;

		if (Ndx < m_nNumInputs) {  // Delete input

			COpBase *pConnectedBase = m_pInputs[Ndx];
			DisconnectBases(m_pInputs[Ndx]);
			if (pConnectedBase->DisconnectBases(this))
				GetParent()->InvalidateRect(NULL);
		}
	}
	else if (point.x > Rect.right - RIGHT_MARGIN) {

		int Ndx = MAX_OUTPUTS - ((Rect.bottom - point.y - (2 * RIGHT_MARGIN)) / RIGHT_MARGIN) - 1;

		if (Ndx < m_nNumOutputs) {  // Delete output
			COpBase *pConnectedBase = m_pOutputs[Ndx];
			DisconnectBases(m_pOutputs[Ndx]);
			if (pConnectedBase->DisconnectBases(this))
				GetParent()->InvalidateRect(NULL);

		}
	}
	else  // Maximize/Minimize
	{
		if (!m_fMinimized)	// Minimize
		{
			MinimizeOperator(TRUE);
		}
		else	// Maximize
		{
			MinimizeOperator(FALSE);
			MinimizeOperator(TRUE);	// These two lines are necessary for some reason to ensure that the newly maximized window is topmost
			MinimizeOperator(FALSE);

		}

	}

	m_fDragging = FALSE;
	m_fConnecting = FALSE;

	ReleaseCapture();
}

void COpBase::DeleteBasePrompt()
{
	DeleteBase(TRUE);
}

void COpBase::DeleteBase(BOOL fPrompt)
{
	int nResponse = IDYES;

	if (fPrompt) 
		nResponse = MessageBox(L"Are you sure you wish to delete this operator?", L"Warning", MB_YESNO);

	if (nResponse == IDYES)
	{
		CMainView *pParent = (CMainView *) GetParent();

		pParent->DeleteOpBase(this);  // Remove this OpBase from the main screen's list

		for (int i = 0; i < m_nNumInputs; i++) {
			m_pInputs[i]->DisconnectBases(this);
			m_pInputs[i] = NULL;
		}

		m_nNumInputs = 0;

		for (int i = 0; i < m_nNumOutputs; i++)
		{
			m_pOutputs[i]->DisconnectBases(this);
			m_pOutputs[i] = 0;
		}

		m_nNumOutputs = 0;

		m_fDragging = FALSE;
		m_fConnecting = FALSE;

		ReleaseCapture();

		pParent->InvalidateRect(NULL);
	}
}

BOOL COpBase::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;
	DWORD dwBytesWritten;

//	WriteFile(hFile, &m_nNumInputs, sizeof(m_nNumInputs), &dwBytesWritten, NULL);
	WriteFile(hFile, &m_nNumOutputs, sizeof(m_nNumOutputs), &dwBytesWritten, NULL);
	WriteFile(hFile, &m_nOperator, sizeof(m_nOperator), &dwBytesWritten, NULL);
	WriteFile(hFile, &m_dwUniqueID, sizeof(m_dwUniqueID), &dwBytesWritten, NULL);
		// Save the UniqueIDs for all our outputs
	for (int i = 0; i < m_nNumOutputs; i++)
	{
		DWORD UniqueID = m_pOutputs[i]->GetUniqueID();

		WriteFile(hFile, &UniqueID, sizeof(UniqueID), &dwBytesWritten, NULL);
	}

	WriteFile(hFile, &m_fDisabled, sizeof(m_fDisabled), &dwBytesWritten, NULL);
	WriteFile(hFile, &m_fMinimized, sizeof(m_fMinimized), &dwBytesWritten, NULL);

		// Make room for enhancements
	WriteFile(hFile, bFiller, sizeof(bFiller), &dwBytesWritten, NULL);

	fRetValue = TRUE;

	return fRetValue;
}


BOOL COpBase::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;
	DWORD dwBytesWritten;

	m_nNumInputs = 0;	// This will get set later (see ConnectOutputs);

//	ReadFile(hFile, &m_nNumInputs, sizeof(m_nNumInputs), &dwBytesWritten, NULL);
	ReadFile(hFile, &m_nNumOutputs, sizeof(m_nNumOutputs), &dwBytesWritten, NULL);
	ReadFile(hFile, &m_nOperator, sizeof(m_nOperator), &dwBytesWritten, NULL);
	ReadFile(hFile, &m_dwUniqueID, sizeof(m_dwUniqueID), &dwBytesWritten, NULL);
		// Read the UniqueIDs for all our outputs and save for later (see ConnectOutputs)
	for (int i = 0; i < m_nNumOutputs; i++)
	{
		DWORD UniqueID;

		ReadFile(hFile, &UniqueID, sizeof(UniqueID), &dwBytesWritten, NULL);

		m_dwOutputIDs[i] = UniqueID;
	}

	ReadFile(hFile, &m_fDisabled, sizeof(m_fDisabled), &dwBytesWritten, NULL);
	ReadFile(hFile, &m_fMinimized, sizeof(m_fMinimized), &dwBytesWritten, NULL);

			// Make room for enhancements
	ReadFile(hFile, bFiller, sizeof(bFiller), &dwBytesWritten, NULL);

	if (m_fMinimized) MinimizeOperator(TRUE);

	fRetValue = TRUE;

	return fRetValue;
}

	// Use list of UniqueIDs loaded from stream to instantiate old connections
BOOL COpBase::ConnectOutputs()
{
	BOOL fRetValue = FALSE;

	int NumOutputs = m_nNumOutputs;

	m_nNumOutputs = 0;
	for (int i = 0; i < NumOutputs; i++)
	{
		COpBase *pBase = m_pMain->FindUniqueID(m_dwOutputIDs[i]);	

		if (pBase) {
			pBase->ConnectBases(CONNECT_INPUT, this);
			ConnectBases(CONNECT_OUTPUT, pBase);
		}
	}

	fRetValue = TRUE;

	return fRetValue;
}

	// Tell all our outputs that new data is ready
void COpBase::SendData()
{
	Log(LOG_DATA, L"%s %x sending new data to %d outputs (%x) - Ptr:%lx, Len:%ld, Type:%d, Num:%d", 
		GetFriendlyName(), this, m_nNumOutputs, m_pOutputs[0], m_sOutputData.pData[0], m_sOutputData.dwDataSize[0], m_sOutputData.eDataType[0], m_sOutputData.bNumItems);

	for (int i = 0; i < m_nNumOutputs; i++)
	{
		m_pOutputs[i]->NotifyNewData(this);

		m_pOutputData[i] = NULL;
	}

//md%	m_pOutputData[0] = &m_sOutputData;  // The first output gets our original copy

}

	// We're being told that new data is available from the given base
void COpBase::NotifyNewData(COpBase *pBase) 
{ 
	Log(LOG_LOCK, L"NotifyNewData waiting for NewInputDataMutex mutex");

	WaitForSingleObject(m_hNewInputDataMutex, INFINITE);  // Wait until it is safe to change our input data cache

	Log(LOG_LOCK, L"NotifyNewData received mutex");

	// Find out the index of this input data
	int i;
	for (i = 0; i < m_nNumInputs; i++)
		if (m_pInputs[i] == pBase) break;

	if (i < m_nNumInputs) 
	{
		DataStruct *pData = m_pInputs[i]->GetData();

		Log(LOG_DATA, L"   NotifyNewData: %s %x receiving new data from input %d - Ptr:%lx, Len:%ld, Type:%d, Num:%d", 
			GetFriendlyName(), this, i, pData->pData[0], pData->dwDataSize[0], pData->eDataType[0], pData->bNumItems);

		if (m_sInputDataCache[i].bNumItems) // We already have a cache for this input - deallocate all this memory
		{

			for (int j = 0; j < m_sInputDataCache[i].bNumItems; j++)
				DeallocateData(m_sInputDataCache[i].pData[j], m_sInputDataCache[i].eDataType[j], 
					m_sInputDataCache[i].eDataStorageType[j], m_sInputDataCache[i].pDataStorage[j]);

			memset(&m_sInputDataCache[i], 0, sizeof(m_sInputDataCache[i]));
		}

		m_pInputData[i] = pData;

		// Create cache of this data
		m_sInputDataCache[i].bNumItems = pData->bNumItems;
		for (int j = 0; j < pData->bNumItems; j++)
		{
			m_sInputDataCache[i].dwDataSize[j] = pData->dwDataSize[j];
			m_sInputDataCache[i].eDataType[j] = pData->eDataType[j];
			m_sInputDataCache[i].eDataStorageType[j] = pData->eDataStorageType[j];
			m_sInputDataCache[i].pDataStorage[j] = pData->pDataStorage[j];
			
			DWORD dwDataSize = m_sInputDataCache[i].dwDataSize[j];

			switch(m_sInputDataCache[i].eDataType[j])
			{
				case DATA_NONE:
					break;
				case DATA_IMAGE:
				{
					Log(LOG_DATA, L"      %s %x allocating image cache for %x", GetFriendlyName(), this, pData->pData[j]);

					IplImage *pImage = (IplImage *) pData->pData[j];
					m_sInputDataCache[i].pData[j] = (void *) cvCloneImage(pImage);
					Log(LOG_DATA, L"          image cache at %x, i=%d, j=%d", m_sInputDataCache[i].pData[j], i, j);

				}
					break;
				case DATA_HOUGH_LINES_POLAR:
				case DATA_HOUGH_LINES_CART:
				case DATA_HOUGH_CIRCLES:
				{
					Log(LOG_DATA, L"      %s %x allocating seq storage cache for %x", GetFriendlyName(), this, pData->pData[j]);

					CvMemStorage* storage = cvCreateMemStorage(0);

					CvSeq *pLines = (CvSeq *) pData->pData[j];
					m_sInputDataCache[i].pData[j] = (void *) cvCloneSeq(pLines, storage);
					m_sInputDataCache[i].pDataStorage[j] = (void *) storage;
					m_sInputDataCache[i].eDataStorageType[j] = DATA_STORAGE_MEMSTORAGE;
					Log(LOG_DATA, L"          seq cache at %x, i=%d, j=%d", m_sInputDataCache[i].pData[j], i, j);

				}
					break;
				case DATA_KERNEL:
				{
					Log(LOG_DATA, L"      %s %x allocating kernel storage cache for %x", GetFriendlyName(), this, pData->pData[j]);

					KERNEL_TYPE *pKernel = new KERNEL_TYPE;
					
					if (pKernel)
					{
						KERNEL_TYPE *pSrcKernel = (KERNEL_TYPE *) pData->pData[j];
						
						m_sInputDataCache[i].pData[j] = (void *) pKernel;
						pKernel->pKernel = cvCloneMat(pSrcKernel->pKernel);
						pKernel->Anchor = pSrcKernel->Anchor;
					
					}
				}
					break;
				case DATA_HISTOGRAM:
				{
					Log(LOG_DATA, L"      %s %x allocating histogram storage cache for %x", GetFriendlyName(), this, pData->pData[j]);

					CvHistogram *pNewHist = NULL;
					cvCopyHist((CvHistogram *) pData->pData[j], &pNewHist);
					
					if (pNewHist)
						m_sInputDataCache[i].pData[j] = (void *) pNewHist;
					
				}
					break;
				case DATA_CONTOUR:
				{
					Log(LOG_DATA, L"      %s %x allocating contour storage cache for %x", GetFriendlyName(), this, pData->pData[j]);

					CvMemStorage* storage = cvCreateMemStorage(0);

					CvSeq *pContour = (CvSeq *) pData->pData[j];
					m_sInputDataCache[i].pData[j] = (void *) CopyContour(pContour, NULL, NULL, storage);

					m_sInputDataCache[i].pDataStorage[j] = (void *) storage;
					m_sInputDataCache[i].eDataStorageType[j] = DATA_STORAGE_MEMSTORAGE;
				}
					break;
				default:
				{
					Log(LOG_DATA, L"      %s %x allocating memory cache for %x, %d bytes", GetFriendlyName(), this, pData->pData[j], dwDataSize);

					m_sInputDataCache[i].pData[j] = new BYTE [dwDataSize];
					if (m_sInputDataCache[i].pData[j])
					{
						memcpy(m_sInputDataCache[i].pData[j], pData->pData[j], dwDataSize);
					}
				}
					break;
			} // switch
		}

			// Default to using the first input's data as our output-this may change by calling SetData
		BOOL fDisabled = m_fDisabled;
		m_fDisabled = FALSE;	// Fake SetData into actually setting these values
		for (int Index = 0; Index < m_sInputDataCache[0].bNumItems; Index++)
			SetData(Index, m_sInputDataCache[0].eDataType[Index], m_sInputDataCache[0].dwDataSize[Index], m_sInputDataCache[0].pData[Index]);	// Our output data is the cached version of the data
		m_fDisabled = fDisabled;

		Log(LOG_LOCK, L"NotifyNewData released mutex - 1");

		ReleaseMutex(m_hNewInputDataMutex);  // We're done updating the cache

		if (m_fErrorFlag) 
		{
			m_fErrorFlag = FALSE;	// Clear any previous errors
			InvalidateRect(NULL);
		}
		
		if (!m_fDisabled)
		{
			Log(LOG_DATA, L"      %s %x notifying for index %d", GetFriendlyName(), this,i);

			NotifyNewDataIndex(i);
		}
		else 
		{
			Log(LOG_DATA, L"      %s %x operator disabled, sending directly to outputs for index %d", GetFriendlyName(), this,i);

			COpBase::NotifyNewDataIndex(i);	// Send directly to outputs because this operator is disabled
		}

	}
	else 
	{
		Log(LOG_LOCK, L"NotifyNewData released mutex - normal");
		ReleaseMutex(m_hNewInputDataMutex);
	}

	Log(LOG_DATA, L"   NotifyNewData Done: %s", GetFriendlyName());

}

	// We got data from the given input index
void COpBase::NotifyNewDataIndex(int InputIndex)
{
	SendData(); // Default processing is to send data directly to outputs
}

	// Search for Index'th instance of the given type of data
void *COpBase::GetInputData(DataType eType, int Index)
{

	if (!m_fDisabled) 
	{
		int i, j;
		int Count = 0;

		Log(LOG_LOCK, L"GetInputData waiting for NewInputData mutex");
		WaitForSingleObject(m_hNewInputDataMutex, INFINITE);	// Wait until the input data is ours alone - no chance of new input data changing it until we're done
		Log(LOG_LOCK, L"GetInputData received mutex");

		for (i = 0; i < m_nNumInputs; i++)
		{
			if (m_pInputData[i])
			{
				for (j = 0; j < m_pInputData[i]->bNumItems; j++)
					if (m_pInputData[i]->eDataType[j] == eType && (Count++) == Index) break;

				if (j < m_pInputData[i]->bNumItems) break;
			}
		}

		if (i < m_nNumInputs) // Data found
		{
				// Deallocate old data
			if (m_sInputDataWorkingCopy[i].pData[j])
			{
				DeallocateData(m_sInputDataWorkingCopy[i].pData[j], m_sInputDataWorkingCopy[i].eDataType[j], 
					m_sInputDataWorkingCopy[i].eDataStorageType[j], m_sInputDataWorkingCopy[i].pDataStorage[j]);
			}

			m_sInputDataWorkingCopy[i].eDataType[j] = m_sInputDataCache[i].eDataType[j];
			m_sInputDataWorkingCopy[i].eDataStorageType[j] = m_sInputDataCache[i].eDataStorageType[j];
			m_sInputDataWorkingCopy[i].pDataStorage[j] = m_sInputDataCache[i].pDataStorage[j];

			// Refresh cache with clean data
			if (m_sInputDataCache[i].eDataType[j] == DATA_IMAGE)
			{
				m_sInputDataWorkingCopy[i].pData[j] = (IplImage *) cvCloneImage((IplImage *) m_sInputDataCache[i].pData[j]);
				IplImage *pImage = (IplImage *) m_sInputDataWorkingCopy[i].pData[j];
				Log(LOG_DATA, L"%s %x GetInputData recloning image %x at %x", GetFriendlyName(), this, m_sInputDataCache[i].pData[j], m_sInputDataWorkingCopy[i].pData[j]);
			}
			else if (m_sInputDataCache[i].eDataType[j] == DATA_KERNEL)
			{
				KERNEL_TYPE *pKernel = (KERNEL_TYPE *) m_sInputDataCache[i].pData[j];
				
				KERNEL_TYPE *pNewKernel = new KERNEL_TYPE;
				
				pNewKernel->pKernel = cvCloneMat(pKernel->pKernel);
				pNewKernel->Anchor = pKernel->Anchor;
				
				m_sInputDataWorkingCopy[i].pData[j] = pNewKernel;
				
				Log(LOG_DATA, L"%s %x GetInputData recloning kernel %x at %x", GetFriendlyName(), this, m_sInputDataCache[i].pData[j], m_sInputDataWorkingCopy[i].pData[j]);
			}
			else if (m_sInputDataCache[i].eDataType[j] == DATA_HISTOGRAM)
			{
				CvHistogram *pHist = (CvHistogram *) m_sInputDataCache[i].pData[j];
				
				CvHistogram *pNewHist = NULL;
				cvCopyHist(pHist, &pNewHist);
				
				m_sInputDataWorkingCopy[i].pData[j] = pNewHist;
				
				Log(LOG_DATA, L"%s %x GetInputData recloning histogram %x at %x", GetFriendlyName(), this, m_sInputDataCache[i].pData[j], m_sInputDataWorkingCopy[i].pData[j]);
			}

			else if (m_sInputDataCache[i].eDataType[j] == DATA_CONTOUR)
			{
				CvMemStorage *pStorage = cvCreateMemStorage();
				
				m_sInputDataWorkingCopy[i].pDataStorage[j] = (void *) pStorage;
				m_sInputDataWorkingCopy[i].eDataStorageType[j] = DATA_STORAGE_MEMSTORAGE;
				m_sInputDataWorkingCopy[i].pData[j] = CopyContour((CvSeq *) m_sInputDataCache[i].pData[j], NULL, NULL, pStorage);

				Log(LOG_DATA, L"%s %x GetInputData recloning contour %x at %x", GetFriendlyName(), this, m_sInputDataCache[i].pData[j], m_sInputDataWorkingCopy[i].pData[j]);
			}

				// NOTE: This one should be just before the ELSE because it overrides the eDataType - it's a default type
			else if (m_sInputDataCache[i].eDataStorageType[j] == DATA_STORAGE_MEMSTORAGE)
			{
				CvMemStorage *pStorage = cvCreateMemStorage();

				m_sInputDataWorkingCopy[i].pDataStorage[j] = (void *) pStorage;
				m_sInputDataWorkingCopy[i].eDataStorageType[j] = DATA_STORAGE_MEMSTORAGE;
				m_sInputDataWorkingCopy[i].pData[j] = cvCloneSeq((CvSeq *) m_sInputDataCache[i].pData[j], pStorage);
				Log(LOG_DATA, L"%s %x GetInputData recloning seq %x at %x", GetFriendlyName(), this, m_sInputDataCache[i].pData[j], m_sInputDataWorkingCopy[i].pData[j]);
			}

			else 
			{
					// Allocate space for new copy
				m_sInputDataWorkingCopy[i].pData[j] = new BYTE [m_sInputDataCache[i].dwDataSize[j]];
					// Copy new data
				if (m_sInputDataWorkingCopy[i].pData[j])
					memcpy(m_sInputDataWorkingCopy[i].pData[j], m_sInputDataCache[i].pData[j], m_sInputDataCache[i].dwDataSize[j]);
			}

			Log(LOG_LOCK, L"GetInputData released mutex");
			ReleaseMutex(m_hNewInputDataMutex);

			return m_sInputDataWorkingCopy[i].pData[j];
		}
		else // requested data not found
		{
			Log(LOG_LOCK, L"GetInputData released mutex");
			ReleaseMutex(m_hNewInputDataMutex);

			return NULL;
		}
		
	}
	else return NULL;	// Disabled
}

DataStruct *COpBase::GetData()
{
	return &m_sOutputData;
}

void COpBase::SetData(int Index, DataType eType, DWORD dwSizeInBytes, void *pData, BOOL fDeallocateWhenDone, StorageType eStorageType, void *pStoragePointer) { 

	if (!m_fDisabled) {
		if (m_fDeallocateWhenDone[Index])
		{

			DeallocateData(m_sOutputData.pData[Index], m_sOutputData.eDataType[Index], 
				m_sOutputData.eDataStorageType[Index], m_sOutputData.pDataStorage[Index]);

			
			m_fDeallocateWhenDone[Index] = FALSE;
		}

		if (Index >= m_sOutputData.bNumItems) m_sOutputData.bNumItems = Index + 1;
		m_sOutputData.eDataType[Index] = eType;
		m_sOutputData.pData[Index] = pData;
		m_sOutputData.dwDataSize[Index] = dwSizeInBytes;
		m_sOutputData.eDataStorageType[Index] = eStorageType;
		m_sOutputData.pDataStorage[Index] = pStoragePointer;

		m_fDeallocateWhenDone[Index] = fDeallocateWhenDone;
	}
}

void COpBase::OpDisable()
{
	if (!m_fDisabled)	// Before we disable, let's clear out our old cache of data
	{
		for (int i = 0; i < MAX_DATA_ITEMS; i++)
			SetData(i, DATA_NONE, 0,NULL);
	}

	m_fDisabled = !m_fDisabled;

		// Refresh all our input data
	if (m_nNumInputs) 
	{
		for (int i = 0; i < m_nNumInputs; i++)
			COpBase::NotifyNewData(m_pInputs[i]);  // Refresh our input data from all our inputs
	}

	if (m_fDisabled) {
		SendData();	// Pass that data to our outputs
	}
	else 
	{
		NotifyNewDataIndex(0);	// We're enabled, so recalc everything
	}
	
	InvalidateRect(NULL);
}	

void *COpBase::LockData(PTCHAR CallerName, DataType eType, int Index)
{

		Log(LOG_LOCK, L"     LockData waiting for mutex: %s", CallerName);

		DWORD Result = WaitForSingleObject(m_hDataInUseMutex, 2000);

		if (Result == WAIT_TIMEOUT)
		{
			TCHAR TStr[1024];
			Log(LOG_LOCK, L"LockData has failed to receive mutex - deadlock : %s", CallerName);

			_stprintf(TStr, L"LockData has failed to receive mutex - deadlock: %s", CallerName);

			MessageBox(TStr, L"Error");
			return NULL;
		}
		else 
		{
			Log(LOG_LOCK, L"     LockData mutex received: %s", CallerName);

			void *pData = GetInputData(eType, Index);

			if (!pData) {
				Log(LOG_LOCK, L"    %s: DataInUseMutex released - no data", CallerName);

				ReleaseMutex(m_hDataInUseMutex);  // No data, mutex not needed
			}

			return pData;
		}
}

void COpBase::MinimizeOperator(BOOL fMinimize)
{
	if (fMinimize)
	{
		RECT Rect;

		GetClientRect(&Rect);

		m_nMinimizeSavedWidth = Rect.right - Rect.left;
		m_nMinimizeSavedHeight = Rect.bottom - Rect.top;

			// Hide all the controls
		for (BYTE i = 0; i < m_bNumControls; i++)
			m_pControlList[i]->ShowWindow(SW_HIDE);

		SetWindowPos(NULL, 0, 0, MIN_BASE_OBJECT_WIDTH, MIN_BASE_OBJECT_HEIGHT, SWP_NOMOVE | SWP_NOZORDER);
		SetWindowPos(&wndBottom, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	}
	else
	{
					// Show all the controls
		for (BYTE i = 0; i < m_bNumControls; i++)
			m_pControlList[i]->ShowWindow(SW_SHOW);

		SetWindowPos(&wndTopMost, 0, 0, m_nMinimizeSavedWidth, m_nMinimizeSavedHeight, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
		SetWindowPos(NULL, 0, 0, m_nMinimizeSavedWidth, m_nMinimizeSavedHeight, SWP_NOMOVE | SWP_NOZORDER);
	}

	m_fMinimized = fMinimize;

	m_pMain->InvalidateRect(NULL);	// Redraw connections

}

	// Get the window position and size of the operator but without the minimized size, use regular size instead
void COpBase::GetMaximizedWindowRect(RECT *pRect)
{
	GetWindowRect(pRect);

	if (m_fMinimized)
	{
		pRect->right = pRect->left + m_nMinimizeSavedWidth;
		pRect->bottom = pRect->top + m_nMinimizeSavedHeight;
	}

}

	// Create a data-showing operator and attach it to this operator's output
void COpBase::OpShowData()
{
	if (m_nNumOutputs < MAX_OUTPUTS)
	{
		RECT Rect;

		GetWindowRect(&Rect);

		Rect.left += Rect.right-Rect.left;
		Rect.right = 0;
		Rect.bottom = 0;

		COpBase *pBase = m_pMain->CreateOperator(OP_IMAGE, Rect);

		pBase->ConnectBases(CONNECT_INPUT, this);
		ConnectBases(CONNECT_OUTPUT, pBase);
		SendData();

		m_pMain->InvalidateRect(NULL);
	
	}
}

void COpBase::OnShowError()
{
	if (m_fErrorFlag)
	{
		MessageBox(m_pErrorText, L"Error");
	}
}
void COpBase::OpChangeOperator()
{
	int OperatorNdx;

	RECT Rect, ParentRect;
	GetMaximizedWindowRect(&Rect);

	GetParent()->GetWindowRect(&ParentRect);

	Rect.left -= ParentRect.left + 1;
	Rect.right = 0; // ParentRect.left + 1;
	Rect.top -= ParentRect.top + 1;
	Rect.bottom = 0; // ParentRect.top + 1;

	CPoint DialogPoint(Rect.left, Rect.right);

//	tToScreen(&Rect);

	COpSelDlg *pDialog = new COpSelDlg(DialogPoint, &OperatorNdx, g_aOpList);

	if (pDialog->DoModal() == IDOK) {
		COpBase *pBase = m_pMain->CreateOperator(OperatorNdx, Rect);  

		if (pBase)
		{
			if (m_fMinimized) 
				pBase->MinimizeOperator(TRUE);

				// Connect all our inputs to this new base
			for (int i = 0; i < m_nNumInputs; i++)
			{
				m_pInputs[i]->ConnectBases(CONNECT_OUTPUT, pBase);
				pBase->ConnectBases(CONNECT_INPUT, m_pInputs[i]);
			}

				// Connect all our outputs to this new base
			for (int i = 0; i < m_nNumOutputs; i++)
			{
				m_pOutputs[i]->ConnectBases(CONNECT_INPUT, pBase);
				pBase->ConnectBases(CONNECT_OUTPUT, m_pOutputs[i]);
			}

				// Get all the data from the inputs
			for (int i = 0; i < m_nNumInputs; i++)
			{
				m_pInputs[i]->SendData();
			}

			StopBase();
			DeleteBase(FALSE);
		}
	}

	delete pDialog;
}

void COpBase::TitleError(PTCHAR pErrorText) 
{ 
	BOOL fCurErrorFlag = m_fErrorFlag;
	
	_tcscpy(m_pErrorText, pErrorText); 
	if (m_pErrorText[0]) m_fErrorFlag = TRUE; else m_fErrorFlag = FALSE;
	
	if (fCurErrorFlag != m_fErrorFlag) InvalidateRect(NULL, FALSE); 
}
