#pragma once
#include "opbase.h"
#include "Cv.h"

#define SMOOTH_DEFAULT_WIDTH (200)
#define SMOOTH_DEFAULT_HEIGHT (260)

class COpSmooth : public COpBase
{
public:
	COpSmooth(CMainView *pMain);
	~COpSmooth(void);

	virtual	PTCHAR GetFriendlyName() { return L"Smooth"; }
	virtual int	GetDefaultWidth() { return SMOOTH_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return SMOOTH_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pParam1;
	CSliderCtrl	*m_pParam2;
	CSliderCtrl	*m_pParam3;
	CSliderCtrl	*m_pParam4;
	CEdit		*m_pParamEdit1;
	CEdit		*m_pParamEdit2;
	CEdit		*m_pParamEdit3;
	CEdit		*m_pParamEdit4;
	CComboBox	*m_pSmoothType;
	TCHAR		m_aSmoothTypeStr[20];
	CSliderCtrl	*m_pIterationsSlider;
	CEdit		*m_pIterationsEdit;

		// Save data
	int			m_nSmoothType;
	BYTE		m_bBlurWidth;
	BYTE		m_bBlurHeight;
	float		m_fSigma1;
	float		m_fSigma2;
	int			m_nIterations;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


//	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters(int bBlurWidth, int bBlurHeight, float m_fSigma1, float m_fSigma2);

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void SmoothTypeChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnChangeParam1();
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );



//	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
//	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
//	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );

	DECLARE_MESSAGE_MAP()
};
