#pragma once
#include "opbase.h"
#include "Cv.h"

#define CmpS_DEFAULT_WIDTH (180)
#define CmpS_DEFAULT_HEIGHT (150)

class COpCmpS : public COpBase
{
public:
	COpCmpS(CMainView *pMain);
	~COpCmpS(void);

	virtual	PTCHAR GetFriendlyName() { return L"CompareS"; }
	virtual int	GetDefaultWidth() { return CmpS_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return CmpS_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pCmpSTypeCombo;
	CComboBox	*m_pChannelCombo;
	//TCHAR		m_aCmpSTypeStr[20];
	//TCHAR		m_aChannelTypeStr[20];
	CSliderCtrl	*m_pCmp_ValueSlider;
	CEdit		*m_pCmp_ValueEdit;

		// Save data
	int			m_nCmpSType;
	int			m_nChannel;
	double		m_gCmp_Value;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void CmpSTypeChange();
	afx_msg void ChannelChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
