#pragma once
#include "opbase.h"
#include "Cv.h"

#define Cmp_DEFAULT_WIDTH (180)
#define Cmp_DEFAULT_HEIGHT (100)

class COpCmp : public COpBase
{
public:
	COpCmp(CMainView *pMain);
	~COpCmp(void);

	virtual	PTCHAR GetFriendlyName() { return L"Compare"; }
	virtual int	GetDefaultWidth() { return Cmp_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Cmp_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pCmpTypeCombo;
	CComboBox	*m_pChannelCombo;
	//TCHAR		m_aCmpTypeStr[20];
	//TCHAR		m_aChannelTypeStr[20];

		// Save data
	int			m_nCmpType;
	int			m_nChannel;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void CmpTypeChange();
	afx_msg void ChannelChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
