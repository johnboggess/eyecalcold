#pragma once
#include "opbase.h"
#include "Cv.h"

#define HoughCircles_DEFAULT_WIDTH (200)
#define HoughCircles_DEFAULT_HEIGHT (270)

class COpHoughCircles : public COpBase
{
public:
	COpHoughCircles(CMainView *pMain);
	~COpHoughCircles(void);

	virtual	PTCHAR GetFriendlyName() { return L"HoughCircles"; }
	virtual int	GetDefaultWidth() { return HoughCircles_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return HoughCircles_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pDpSlider;
	CEdit		*m_pDpEdit;
	CSliderCtrl	*m_pMinDistSlider;
	CEdit		*m_pMinDistEdit;
	CSliderCtrl	*m_pParam1Slider;
	CEdit		*m_pParam1Edit;
	CSliderCtrl	*m_pParam2Slider;
	CEdit		*m_pParam2Edit;
	CSliderCtrl	*m_pMinRadiusSlider;
	CEdit		*m_pMinRadiusEdit;
	CSliderCtrl	*m_pMaxRadiusSlider;
	CEdit		*m_pMaxRadiusEdit;

		// Save data
	double	m_gDp;
	double	m_gMinDist;
	double	m_gParam1;
	double	m_gParam2;
	int		m_nMinRadius;
	int		m_nMaxRadius;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnMethodChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
