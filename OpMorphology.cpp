#include "StdAfx.h"
#include "OpMorphology.h"
#include "OpBase.h"
#include "Cv.h"

#define MIN_MORPH_ITERATIONS (1)
#define MAX_MORPH_ITERATIONS (20)

#define MIN_MORPH_SIZE (1)
#define MAX_MORPH_SIZE (20)

#define ID_MORPHTYPE (100)
#define ID_KERNELTYPE (101)


BYTE bMorphFiller[32];

BEGIN_MESSAGE_MAP(COpMorphology, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_MORPHTYPE, MorphTypeChange)
	ON_CBN_SELCHANGE(ID_KERNELTYPE, MorphKernelChange)
END_MESSAGE_MAP()

COpMorphology::COpMorphology(CMainView *pMain) : COpBase(pMain)
{
	m_pIterations = NULL;
	m_pMorpthType = NULL;
	m_pKernelType = NULL;
	m_pIterationsEdit = NULL;
	m_pSizeSlider = NULL;
	m_pSizeEdit = NULL;
	m_aMorphTypeStr[0] = 0;
	m_aMorphKernelStr[0] = 0;
	m_eMorphType = MORPH_DILATE;
	m_nKernelType = CV_SHAPE_RECT;
	m_nIterations = MIN_MORPH_ITERATIONS;
	m_pKernel = NULL;
	m_nSize = 3;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

	_tcscpy(m_aMorphTypeStr, L"Dilate");
	_tcscpy(m_aMorphKernelStr, L"Rectangle");

	m_hThread = NULL;

}

COpMorphology::~COpMorphology(void)
{
	if (m_pIterations)delete m_pIterations;
	if (m_pMorpthType)delete m_pMorpthType;
	if (m_pKernelType)delete m_pKernelType;
	if (m_pIterationsEdit)delete m_pIterationsEdit;
	if (m_pSizeSlider) delete m_pSizeSlider;
	if (m_pSizeEdit) delete m_pSizeEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpMorphology::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*md%
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpMorphology::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_pMorpthType = CreateOpCombo(0, ID_MORPHTYPE);

	if (m_pMorpthType)
	{
		int Ndx = m_pMorpthType->AddString(L"Dilate");
		m_pMorpthType->SetItemData(Ndx, MORPH_DILATE);
		Ndx = m_pMorpthType->AddString(L"Erode");
		m_pMorpthType->SetItemData(Ndx, MORPH_ERODE);
		Ndx = m_pMorpthType->AddString(L"Open");
		m_pMorpthType->SetItemData(Ndx, MORPH_OPEN);
		Ndx = m_pMorpthType->AddString(L"Close");
		m_pMorpthType->SetItemData(Ndx, MORPH_CLOSE);
		Ndx = m_pMorpthType->AddString(L"Gradient");
		m_pMorpthType->SetItemData(Ndx, MORPH_GRADIENT);
		Ndx = m_pMorpthType->AddString(L"TopHat");
		m_pMorpthType->SetItemData(Ndx, MORPH_TOPHAT);
		Ndx = m_pMorpthType->AddString(L"BlackHat");
		m_pMorpthType->SetItemData(Ndx, MORPH_BLACKHAT);
	}

	m_pKernelType = CreateOpCombo(0, ID_KERNELTYPE);

	if (m_pKernelType)
	{
		int Ndx = m_pKernelType->AddString(L"Rectangle");
		m_pKernelType->SetItemData(Ndx, CV_SHAPE_RECT);
		Ndx = m_pKernelType->AddString(L"Cross");
		m_pKernelType->SetItemData(Ndx, CV_SHAPE_CROSS);
		Ndx = m_pKernelType->AddString(L"Ellipse");
		m_pKernelType->SetItemData(Ndx, CV_SHAPE_ELLIPSE);
//		int Ndx = m_pKernelType->AddString(L"Custom");
//		m_pKernelType->SetItemData(Ndx, CV_SHAPE_CUSTOM);
	}

	CreateOpStatic(L"iterations", TRUE, 0);
	m_pIterationsEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pIterations = CreateOpSlider(0, MIN_MORPH_ITERATIONS, MAX_MORPH_ITERATIONS);

	CreateOpStatic(L"size", TRUE, 0);
	m_pSizeEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pSizeSlider = CreateOpSlider(0, MIN_MORPH_SIZE, MAX_MORPH_SIZE);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpMorphology::StartOpThread(void *pData)
{
	COpMorphology *pBase = (COpMorphology *) pData;

	pBase->OpThread();

	return 0;
}

void COpMorphology::UpdateSliders()
{
	m_pIterations->SetPos(m_nIterations);
	m_pSizeSlider->SetPos(m_nSize);

}
void COpMorphology::UpdateValues()
{
	TCHAR TStr[10];

	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pMorpthType->GetCount(); Ndx++)
		if (m_pMorpthType->GetItemData(Ndx) == m_eMorphType) break;

	m_pMorpthType->SetCurSel(Ndx);
	m_pMorpthType->GetLBText(Ndx, m_aMorphTypeStr);

	for (Ndx = 0; Ndx < m_pKernelType->GetCount(); Ndx++)
		if (m_pKernelType->GetItemData(Ndx) == (DWORD) m_nKernelType) break;

	m_pKernelType->SetCurSel(Ndx);
	m_pKernelType->GetLBText(Ndx, m_aMorphKernelStr);

	_itot(m_nIterations, TStr, 10);
	m_pIterationsEdit->SetWindowTextW(TStr);

	_itot(m_nSize, TStr, 10);
	m_pSizeEdit->SetWindowTextW(TStr);

}


void COpMorphology::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpMorphology::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_eMorphType, sizeof(m_eMorphType), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nKernelType, sizeof(m_nKernelType), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nSize, sizeof(m_nSize), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nIterations, sizeof(m_nIterations), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bMorphFiller, sizeof(bMorphFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpMorphology::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_eMorphType, sizeof(m_eMorphType), &dwBytesWritten, NULL);

		ReadFile(hFile, &m_nKernelType, sizeof(m_nKernelType), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nSize, sizeof(m_nSize), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nIterations, sizeof(m_nIterations), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bMorphFiller, sizeof(bMorphFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpMorphology::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpMorphology::ValidateParameters(MORPH_TYPE eMorphType, int nKernelType, int nIterations, int nSize)
{
	if (nKernelType != m_nKernelType || nSize != m_nSize)  // Changing kernels
	{
		if (m_pKernel) cvReleaseStructuringElement(&m_pKernel);

		m_pKernel = cvCreateStructuringElementEx(m_nSize, m_nSize, m_nSize / 2, m_nSize / 2, nKernelType);

		if (m_pKernel) 
		{
			m_nKernelType = nKernelType;
			m_nSize = nSize;
		}
	}

	m_eMorphType = eMorphType;
	m_nIterations = nIterations;


}

void COpMorphology::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	int nIterations = m_nIterations;
	int nSize = m_nSize;

	if (pSlider == m_pIterations) 
		nIterations = pSlider->GetPos();

	if (pSlider == m_pSizeSlider) 
		nSize = pSlider->GetPos();

	ValidateParameters(m_eMorphType, m_nKernelType, nIterations, nSize);

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpMorphology::MorphTypeChange()
{
	
	int Ndx = m_pMorpthType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		MORPH_TYPE eMorphType = (MORPH_TYPE) m_pMorpthType->GetItemData(Ndx);
		m_pMorpthType->GetLBText(Ndx, m_aMorphTypeStr);

		ValidateParameters(eMorphType, m_nKernelType, m_nIterations, m_nSize);

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}
void COpMorphology::MorphKernelChange()
{
	
	int Ndx = m_pKernelType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		int nKernelType = m_pKernelType->GetItemData(Ndx);
		m_pKernelType->GetLBText(Ndx, m_aMorphKernelStr);

		ValidateParameters(m_eMorphType, nKernelType, m_nIterations, m_nSize);

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpMorphology::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"MorphThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			int Iterations = m_nIterations;

			IplImage *pSrc = pImage;
			IplImage *pTemp = NULL;
			BOOL fDeallocateWhenDone = FALSE;

			if (m_pKernel)
				m_pKernel = cvCreateStructuringElementEx(m_nSize, m_nSize, m_nSize / 2, m_nSize / 2, m_nKernelType);

			switch (m_eMorphType)
			{
			case MORPH_ERODE:
					cvErode(pImage, pImage, m_pKernel, Iterations);
					break;
				case MORPH_DILATE:
					cvDilate(pImage, pImage, m_pKernel, Iterations);
					break;
				case MORPH_OPEN:
					cvMorphologyEx(pImage, pImage, pTemp, m_pKernel, CV_MOP_OPEN, Iterations);
					break;
				case MORPH_CLOSE:
					cvMorphologyEx(pImage, pImage, pTemp, m_pKernel, CV_MOP_CLOSE, Iterations);
					break;
				case MORPH_GRADIENT:
					pTemp = cvCloneImage(pImage);
					cvMorphologyEx(pImage, pImage, pTemp, m_pKernel, CV_MOP_GRADIENT, Iterations);
					break;
				case MORPH_TOPHAT:
					pTemp = cvCloneImage(pImage);
					cvMorphologyEx(pImage, pImage, pTemp, m_pKernel, CV_MOP_TOPHAT, Iterations);
					break;
				case MORPH_BLACKHAT:
					pTemp = cvCloneImage(pImage);
					cvMorphologyEx(pImage, pImage, pTemp, m_pKernel, CV_MOP_BLACKHAT, Iterations);
					break;
			}

			if (pTemp) cvReleaseImage(&pTemp);

			SetData(0, DATA_IMAGE, 0, pImage, fDeallocateWhenDone);
			SendData();

			ReleaseData(L"MorphThread");  // We're done with the data so it can change if needed

		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
