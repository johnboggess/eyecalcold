#include "StdAfx.h"
#include "OpFilter2D.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bFilter2DFiller[32];

BEGIN_MESSAGE_MAP(COpFilter2D, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()

COpFilter2D::COpFilter2D(CMainView *pMain) : COpBase(pMain)
{

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpFilter2D::~COpFilter2D(void)
{

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpFilter2D::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpFilter2D::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpFilter2D::StartOpThread(void *pData)
{
	COpFilter2D *pBase = (COpFilter2D *) pData;

	pBase->OpThread();

	return 0;
}

void COpFilter2D::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpFilter2D::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpFilter2D::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpFilter2D::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	return fRetValue;
}

BOOL COpFilter2D::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	return fRetValue;
}

	// We got data from the given input index
void COpFilter2D::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpFilter2D::ValidateParameters()
{
}

void COpFilter2D::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
}

void COpFilter2D::OnChange()
{
}

void COpFilter2D::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"Filter2DThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			KERNEL_TYPE *pKernel = (KERNEL_TYPE *) LockData(L"Filter2DThread", DATA_KERNEL, 0);
		
			if (pKernel) {	// Validate Src and Dst images, etc

				IplImage *pImage2 = cvCloneImage(pImage);

				cvFilter2D(pImage, pImage2, pKernel->pKernel, pKernel->Anchor);

				SetData(0, DATA_IMAGE, 0, pImage2, TRUE);
				SendData();
				
				ReleaseData(L"Filter2DThread");

			}
				else TitleError(L"Matrix kernel required");

			ReleaseData(L"Filter2DThread");  // We're done with the data so it can change if needed
		}
			else TitleError(L"Source image required");

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
