#pragma once
#include "opbase.h"
#include "Cv.h"

#define Tryout_DEFAULT_WIDTH (100)
#define Tryout_DEFAULT_HEIGHT (80)

class COpTryout : public COpBase
{
public:
	COpTryout(CMainView *pMain);
	~COpTryout(void);

	virtual	PTCHAR GetFriendlyName() { return L"Tryout"; }
	virtual int	GetDefaultWidth() { return Tryout_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Tryout_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
// (Hai)	virtual void StopBase();


protected:
// (Hai)	CSliderCtrl	*m_pSlider;
// (Hai)	CComboBox	*m_pCombo;
//	CEdit		*m_pEdit;
// (Hai)	TCHAR		m_aStr[20];
// (Hai)	CButton		*m_pButton;

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
// (Hai)	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
