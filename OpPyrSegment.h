#pragma once
#include "opbase.h"
#include "Cv.h"

#define PyrSegment_DEFAULT_WIDTH (200)
#define PyrSegment_DEFAULT_HEIGHT (160)

class COpPyrSegment : public COpBase
{
public:
	COpPyrSegment(CMainView *pMain);
	~COpPyrSegment(void);

	virtual	PTCHAR GetFriendlyName() { return L"PyrSegment"; }
	virtual int	GetDefaultWidth() { return PyrSegment_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return PyrSegment_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pLevelSlider;
	CEdit		*m_pLevelEdit;
	CSliderCtrl	*m_pThresh1Slider;
	CEdit		*m_pThresh1Edit;
	CSliderCtrl	*m_pThresh2Slider;
	CEdit		*m_pThresh2Edit;

		// Save data
	int		m_nLevel;
	double	m_nThreshold1;
	double	m_nThreshold2;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
