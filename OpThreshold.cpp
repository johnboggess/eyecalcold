#include "StdAfx.h"
#include "OpThreshold.h"
#include "Cv.h"

#define ID_THRESHOLDTYPE (104)

BYTE bThresholdFiller[24];

BEGIN_MESSAGE_MAP(COpThreshold, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_THRESHOLDTYPE, ThresholdTypeChange)

END_MESSAGE_MAP()

COpThreshold::COpThreshold(CMainView *pMain) : COpBase(pMain)
{
	m_pThresholdSlider = NULL;
	m_pMaxValueSlider = NULL;
	m_pThresholdEdit = NULL;
	m_pMaxValueEdit = NULL;
	m_pThresholdTypeCombo = NULL;
	m_aThresholdTypeStr[0] = 0;

	m_nThresholdType = CV_THRESH_BINARY;
	m_nMaxValue = 255;
	m_nThreshold = 128;

	m_hThread = NULL;

}

COpThreshold::~COpThreshold(void)
{
	if (m_pThresholdSlider) delete m_pThresholdSlider;
	if (m_pMaxValueSlider) delete m_pMaxValueSlider;
	if (m_pThresholdEdit) delete m_pThresholdEdit;
	if (m_pMaxValueEdit) delete m_pMaxValueEdit;
	if (m_pThresholdTypeCombo) delete m_pThresholdTypeCombo;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpThreshold::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aThresholdTypeStr, _tcslen(m_aThresholdTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpThreshold::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_pThresholdTypeCombo = CreateOpCombo(0, ID_THRESHOLDTYPE);

	if (m_pThresholdTypeCombo)
	{
		int Ndx = m_pThresholdTypeCombo->AddString(L"Binary");
		m_pThresholdTypeCombo->SetItemData(Ndx, CV_THRESH_BINARY);
		Ndx = m_pThresholdTypeCombo->AddString(L"Inverted");
		m_pThresholdTypeCombo->SetItemData(Ndx, CV_THRESH_BINARY_INV);
		Ndx = m_pThresholdTypeCombo->AddString(L"Truncated");
		m_pThresholdTypeCombo->SetItemData(Ndx, CV_THRESH_TRUNC);
		Ndx = m_pThresholdTypeCombo->AddString(L"ToZero");
		m_pThresholdTypeCombo->SetItemData(Ndx, CV_THRESH_TOZERO);
		Ndx = m_pThresholdTypeCombo->AddString(L"ToZeroInverted");
		m_pThresholdTypeCombo->SetItemData(Ndx, CV_THRESH_TOZERO_INV);
	}

	CreateOpStatic(L"threshold", TRUE, 0);
	m_pThresholdEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pThresholdSlider = CreateOpSlider(0, 0, 255);

	CreateOpStatic(L"max value", TRUE, 0);
	m_pMaxValueEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pMaxValueSlider = CreateOpSlider(0, 1, 255);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpThreshold::StartOpThread(void *pData)
{
	COpThreshold *pThreshold = (COpThreshold *) pData;

	pThreshold->OpThread();

	return 0;
}

void COpThreshold::UpdateSliders()
{
	m_pThresholdSlider->SetPos(m_nThreshold);
	m_pMaxValueSlider->SetPos(m_nMaxValue);
}

void COpThreshold::UpdateValues()
{
	TCHAR TStr[10];

	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pThresholdTypeCombo->GetCount(); Ndx++)
		if (m_pThresholdTypeCombo->GetItemData(Ndx) == m_nThresholdType) break;

	m_pThresholdTypeCombo->SetCurSel(Ndx);
	m_pThresholdTypeCombo->GetLBText(Ndx, m_aThresholdTypeStr);

	_itot(m_nThreshold, TStr, 10);
	m_pThresholdEdit->SetWindowTextW(TStr);
	_itot(m_nMaxValue, TStr, 10);
	m_pMaxValueEdit->SetWindowTextW(TStr);

}


void COpThreshold::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpThreshold::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nThresholdType, sizeof(m_nThresholdType), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_nThreshold, sizeof(m_nThreshold), &dwBytesWritten, NULL);
		
		if (WriteFile(hFile, &m_nMaxValue, sizeof(m_nMaxValue), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bThresholdFiller, sizeof(bThresholdFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpThreshold::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nThresholdType, sizeof(m_nThresholdType), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_nThreshold, sizeof(m_nThreshold), &dwBytesWritten, NULL);
		
		if (ReadFile(hFile, &m_nMaxValue, sizeof(m_nMaxValue), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bThresholdFiller, sizeof(bThresholdFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpThreshold::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpThreshold::ValidateParameters()
{
}

void COpThreshold::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	int nThreshold = m_nThreshold;
	int nMaxValue = m_nMaxValue;

	if (pSlider == m_pThresholdSlider) 
	{
		nThreshold = pSlider->GetPos();

	}
	else if (pSlider == m_pMaxValueSlider) 
	{
		nMaxValue = pSlider->GetPos();

	}

	ValidateParameters();

	m_nThreshold = nThreshold;
	m_nMaxValue = nMaxValue;

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpThreshold::ThresholdTypeChange()
{
	
	int Ndx = m_pThresholdTypeCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nThresholdType = m_pThresholdTypeCombo->GetItemData(Ndx);
		m_pThresholdTypeCombo->GetLBText(Ndx, m_aThresholdTypeStr);

//		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpThreshold::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"ThresholdThread", DATA_IMAGE, 0);

		if (pImage) 
		{
		
			if (pImage->nChannels == 1 && ((pImage->depth & IPL_DEPTH_8U) == IPL_DEPTH_8U))
			{
				BYTE nThreshold = m_nThreshold;
				BYTE nMaxValue = m_nMaxValue;

				IplImage *pSrc = pImage;

					// Need separate destination buffer
				pSrc = cvCloneImage(pImage);

				cvThreshold(pSrc, pImage, nThreshold, nMaxValue, m_nThresholdType);

				cvReleaseImage(&pSrc);

				SetData(0, DATA_IMAGE, 0, pImage);
				SendData();
			}
				else TitleError(L"Src image must have 1 channel and depth of 8 or 32");
				
			ReleaseData(L"ThresholdThread");  // We're done with the data so it can change if needed

		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
