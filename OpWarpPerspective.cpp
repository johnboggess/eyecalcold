#include "StdAfx.h"
#include "OpWarpPerspective.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bWarpPerspectiveFiller[32];

BEGIN_MESSAGE_MAP(COpWarpPerspective, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()

COpWarpPerspective::COpWarpPerspective(CMainView *pMain) : COpBase(pMain)
{

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpWarpPerspective::~COpWarpPerspective(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpWarpPerspective::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpWarpPerspective::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpWarpPerspective::StartOpThread(void *pData)
{
	COpWarpPerspective *pBase = (COpWarpPerspective *) pData;

	pBase->OpThread();

	return 0;
}

void COpWarpPerspective::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpWarpPerspective::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpWarpPerspective::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpWarpPerspective::SaveToStream(HANDLE hFile)
{
	return COpBase::SaveToStream(hFile);
}

BOOL COpWarpPerspective::LoadFromStream(HANDLE hFile)
{
	return COpBase::LoadFromStream(hFile);
}

	// We got data from the given input index
void COpWarpPerspective::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpWarpPerspective::ValidateParameters()
{
}

void COpWarpPerspective::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
}

void COpWarpPerspective::OnChange()
{
}

void COpWarpPerspective::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		POINT_LIST_TYPE *pSrcPoints = (POINT_LIST_TYPE *) LockData(L"WarpPerspectivePoints1", DATA_POINT_LIST, 0);
		POINT_LIST_TYPE *pDstPoints = (POINT_LIST_TYPE *) LockData(L"WarpPerspectivePoints1", DATA_POINT_LIST, 1);

		if (pSrcPoints && pSrcPoints->nNumPoints >= 4 && pDstPoints && pDstPoints->nNumPoints >= 4)
		{
			IplImage *pImage = (IplImage *) LockData(L"WarpPerspectiveThread", DATA_IMAGE, 0);
			IplImage *pDestImage = (IplImage *) LockData(L"WarpPerspectiveThread2", DATA_IMAGE, 1);

			if (pImage && pDestImage) 
			{
				CvPoint2D32f SrcQuad[4], DstQuad[4];
				CvMat *WarpMatrix = cvCreateMat(3,3, CV_32FC1);
				
				SrcQuad[0].x = (float) pSrcPoints->PointList[0].x;
				SrcQuad[0].y = (float) pSrcPoints->PointList[0].y;
				SrcQuad[1].x = (float) pSrcPoints->PointList[1].x;
				SrcQuad[1].y = (float) pSrcPoints->PointList[1].y;
				SrcQuad[2].x = (float) pSrcPoints->PointList[2].x;
				SrcQuad[2].y = (float) pSrcPoints->PointList[2].y;
				SrcQuad[3].x = (float) pSrcPoints->PointList[3].x;
				SrcQuad[3].y = (float) pSrcPoints->PointList[3].y;

				DstQuad[0].x = (float) pDstPoints->PointList[0].x;
				DstQuad[0].y = (float) pDstPoints->PointList[0].y;
				DstQuad[1].x = (float) pDstPoints->PointList[1].x;
				DstQuad[1].y = (float) pDstPoints->PointList[1].y;
				DstQuad[2].x = (float) pDstPoints->PointList[2].x;
				DstQuad[2].y = (float) pDstPoints->PointList[2].y;
				DstQuad[3].x = (float) pDstPoints->PointList[3].x;
				DstQuad[3].y = (float) pDstPoints->PointList[3].y;
				
				cvGetPerspectiveTransform(SrcQuad, DstQuad, WarpMatrix);
				if (1) {	// Validate Src and Dst images, etc

					cvWarpPerspective(pImage, pDestImage, WarpMatrix);

					SetData(0, DATA_IMAGE, 0, pDestImage);
					SendData();

				}
					else TitleError(L"");

				cvReleaseMat(&WarpMatrix);

				ReleaseData(L"WarpPerspectiveThread");  // We're done with the data so it can change if needed
				ReleaseData(L"WarpPerspectiveThread");  // We're done with the data so it can change if needed
				ReleaseData(L"WarpPerspectiveThread");  // We're done with the data so it can change if needed
				ReleaseData(L"WarpPerspectiveThread");  // We're done with the data so it can change if needed
			}
				else 
			{
				if (pImage) ReleaseData(L"WarpPerspectiveThread");  // We're done with the data so it can change if needed
				if (pDestImage) ReleaseData(L"WarpPerspectiveThread");  // We're done with the data so it can change if needed

				TitleError(L"Two image inputs are required");
			}

		}
			else 
		{
			if (pSrcPoints) ReleaseData(L"WarPerspectiveThread");
			if (pDstPoints) ReleaseData(L"WarPerspectiveThread");
			
			TitleError(L"Two sets for four points are required");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);
	}
}
