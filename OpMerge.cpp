#include "StdAfx.h"
#include "OpMerge.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bMergeFiller[32];

BEGIN_MESSAGE_MAP(COpMerge, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpMerge::COpMerge(CMainView *pMain) : COpBase(pMain)
{

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpMerge::~COpMerge(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpMerge::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpMerge::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CComboBox *pCombo = m_pCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
		Ndx = pCombo->AddString(L"");
		pCombo->SetItemData(Ndx, );
	}
*/
//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpMerge::StartOpThread(void *pData)
{
	COpMerge *pBase = (COpMerge *) pData;

	pBase->OpThread();

	return 0;
}

void COpMerge::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpMerge::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpMerge::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpMerge::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);
fRetValue = TRUE;
/*
		if (WriteFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bMergeFiller, sizeof(bMergeFiller), &dwBytesWritten, NULL);
		}
*/
	}

	return fRetValue;
}

BOOL COpMerge::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

fRetValue = TRUE;
//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);
/*
		if (ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bMergeFiller, sizeof(bMergeFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
*/
	}

	return fRetValue;
}

	// We got data from the given input index
void COpMerge::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpMerge::ValidateParameters()
{
}

void COpMerge::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpMerge::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpMerge::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpMerge::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpMerge::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage0 = (IplImage *) LockData(L"MergeThread", DATA_IMAGE, 0);
		IplImage *pImage1 = (IplImage *) LockData(L"MergeThread", DATA_IMAGE, 1);
		IplImage *pImage2 = (IplImage *) LockData(L"MergeThread", DATA_IMAGE, 2);
		IplImage *pImage3 = (IplImage *) LockData(L"MergeThread", DATA_IMAGE, 3);

		if (pImage0 && pImage1) 
		{
			if (1) {	// Validate Src and Dst images, etc
				int nChannels = 2;
				
				if (pImage2) nChannels++;
				if (pImage3) nChannels++;
			
				IplImage *pDestImage = cvCreateImage(cvGetSize(pImage0), pImage0->depth, nChannels);

				cvMerge(pImage0, pImage1, pImage2, pImage3, pDestImage);

				SetData(0, DATA_IMAGE, 0, pDestImage, TRUE);
				SendData();

			}
				else TitleError(L"");

			if (pImage0) ReleaseData(L"MergeThread");
			if (pImage1) ReleaseData(L"MergeThread");
			if (pImage2) ReleaseData(L"MergeThread");
			if (pImage3) ReleaseData(L"MergeThread");
		}
			else
		{
			TitleError(L"Requires at least two input channels");
			
			if (pImage0) ReleaseData(L"MergeThread");
			if (pImage1) ReleaseData(L"MergeThread");
			if (pImage2) ReleaseData(L"MergeThread");
			if (pImage3) ReleaseData(L"MergeThread");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
