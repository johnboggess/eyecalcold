#include "StdAfx.h"
#include "OpConvertColor.h"
#include "Cv.h"

#define ID_ConvertColorTYPE (104)

BYTE bConvertColorFiller[24];

BEGIN_MESSAGE_MAP(COpConvertColor, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_ConvertColorTYPE, ConvertColorTypeChange)

END_MESSAGE_MAP()

COpConvertColor::COpConvertColor(CMainView *pMain) : COpBase(pMain)
{
	m_pConvertColorTypeCombo = NULL;
	m_aConvertColorTypeStr[0] = 0;

	m_nConvertColorType = CV_BGR2GRAY;

	m_hThread = NULL;

}

COpConvertColor::~COpConvertColor(void)
{
	if (m_pConvertColorTypeCombo) delete m_pConvertColorTypeCombo;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpConvertColor::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aConvertColorTypeStr, _tcslen(m_aConvertColorTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpConvertColor::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	m_pConvertColorTypeCombo = CreateOpCombo(0, ID_ConvertColorTYPE);

	if (m_pConvertColorTypeCombo)
	{
		int Ndx = m_pConvertColorTypeCombo->AddString(L"rgb2gray");
		m_pConvertColorTypeCombo->SetItemData(Ndx, CV_RGB2GRAY);
		Ndx = m_pConvertColorTypeCombo->AddString(L"bgr2gray");
		m_pConvertColorTypeCombo->SetItemData(Ndx, CV_BGR2GRAY);
		Ndx = m_pConvertColorTypeCombo->AddString(L"gray2bgr");
		m_pConvertColorTypeCombo->SetItemData(Ndx, CV_GRAY2BGR);
		Ndx = m_pConvertColorTypeCombo->AddString(L"gray2rgb");
		m_pConvertColorTypeCombo->SetItemData(Ndx, CV_GRAY2RGB);
		Ndx = m_pConvertColorTypeCombo->AddString(L"rgb2bgr");
		m_pConvertColorTypeCombo->SetItemData(Ndx, CV_RGB2BGR);
		Ndx = m_pConvertColorTypeCombo->AddString(L"bgr2rgb");
		m_pConvertColorTypeCombo->SetItemData(Ndx, CV_BGR2RGB);
	}

	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpConvertColor::StartOpThread(void *pData)
{
	COpConvertColor *pConvertColor = (COpConvertColor *) pData;

	pConvertColor->OpThread();

	return 0;
}

void COpConvertColor::UpdateSliders()
{
}

void COpConvertColor::UpdateValues()
{
	TCHAR TStr[10];

	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pConvertColorTypeCombo->GetCount(); Ndx++)
		if (m_pConvertColorTypeCombo->GetItemData(Ndx) == m_nConvertColorType) break;

	m_pConvertColorTypeCombo->SetCurSel(Ndx);
	m_pConvertColorTypeCombo->GetLBText(Ndx, m_aConvertColorTypeStr);

}


void COpConvertColor::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpConvertColor::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (WriteFile(hFile, &m_nConvertColorType, sizeof(m_nConvertColorType), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bConvertColorFiller, sizeof(bConvertColorFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpConvertColor::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (ReadFile(hFile, &m_nConvertColorType, sizeof(m_nConvertColorType), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bConvertColorFiller, sizeof(bConvertColorFiller), &dwBytesWritten, NULL);

			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpConvertColor::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpConvertColor::ValidateParameters()
{
}

void COpConvertColor::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{

}

void COpConvertColor::ConvertColorTypeChange()
{
	
	int Ndx = m_pConvertColorTypeCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nConvertColorType = m_pConvertColorTypeCombo->GetItemData(Ndx);
		m_pConvertColorTypeCombo->GetLBText(Ndx, m_aConvertColorTypeStr);

//		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpConvertColor::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"ConvertColorThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			int nConvertColorType = m_nConvertColorType;

			IplImage *pDest = NULL;

			if (pImage->depth == IPL_DEPTH_8U || pImage->depth == IPL_DEPTH_16U || pImage->depth == IPL_DEPTH_32F)
			{
					// Need separate destination buffer
				if (nConvertColorType == CV_RGB2GRAY || nConvertColorType == CV_BGR2GRAY)
					pDest = cvCreateImage(cvGetSize(pImage), pImage->depth, 1);	// 1 channel - grayscale
				else
					pDest = cvCloneImage(pImage);
				
				cvCvtColor(pImage, pDest, nConvertColorType);

				SetData(0, DATA_IMAGE, 0, pDest, TRUE);
				SendData();
			}
				else TitleError(L"Invalid source image type");
				
			ReleaseData(L"ConvertColorThread");  // We're done with the data so it can change if needed

		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
