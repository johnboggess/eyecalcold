#pragma once
#include "opbase.h"
#include "Cv.h"

#define ConvertColor_DEFAULT_WIDTH (200)
#define ConvertColor_DEFAULT_HEIGHT (80)

class COpConvertColor : public COpBase
{
public:
	COpConvertColor(CMainView *pMain);
	~COpConvertColor(void);

	virtual	PTCHAR GetFriendlyName() { return L"ConvertClr"; }
	virtual int	GetDefaultWidth() { return ConvertColor_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return ConvertColor_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CComboBox	*m_pConvertColorTypeCombo;
	TCHAR		m_aConvertColorTypeStr[20];

		// Save data
	int			m_nConvertColorType;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();


	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void ConvertColorTypeChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );


	DECLARE_MESSAGE_MAP()
};
