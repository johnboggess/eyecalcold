#pragma once
#include "ChildView.h"
#include "Cv.h"
#include "Helper.h"

void Log(DWORD LogID, PTCHAR Format, ...);

#define LOG_DATA (1)
#define LOG_LOCK (2)
#define LOG_SOCKETS (3)

#define MAX_INPUTS (4)
#define MAX_OUTPUTS (4)
//#define OPBASE_BACKCOLOR RGB(200, 200, 200)
#define OPBASE_BACKCOLOR RGB(236, 233, 216)
//#define TITLE_BACK_COLOR (RGB(100, 100, 100))
#define TITLE_BACK_COLOR (RGB(170, 157, 83))
#define TITLE_DISABLED_COLOR (RGB(128, 128, 128))
#define TITLE_ERROR_COLOR (RGB(255, 0, 0))
#define CONNECTION_COLOR (RGB(0, 0, 0))
#define GOOD_CONNECTION_COLOR (RGB(0, 255, 0))
#define BORDER_COLOR (RGB(0, 0, 0))
#define LEFT_MARGIN (10)
#define RIGHT_MARGIN (10)
#define TITLE_HEIGHT (20)
#define BASE_OBJECT_WIDTH 100
#define BASE_OBJECT_HEIGHT 80
#define MIN_BASE_OBJECT_WIDTH 100
#define MIN_BASE_OBJECT_HEIGHT 80
#define MAX_DATA_ITEMS (10)
#define MAX_OPERATOR_CONTROLS (30)


#define CTRL_GAP (10)
#define TEXT_GAP (4)
#define SLIDER_HEIGHT (20)
#define COMBO_HEIGHT (20)
#define EDIT_HEIGHT (20)
#define TEXT_HEIGHT (16)
#define BUTTON_HEIGHT (20)
#define CONTROL_BORDER (5)

#define PARAM_EDIT_WIDTH (20)
#define PARAM_EDIT_WIDTH_3 (30)

typedef enum { CONNECT_INPUT, CONNECT_OUTPUT } ConnectionType;

typedef enum { DATA_NONE, DATA_IMAGE, DATA_POINT, DATA_POINT_LIST, DATA_HOUGH_LINES_POLAR, DATA_HOUGH_LINES_CART, 
		DATA_HOUGH_CIRCLES, DATA_KERNEL, DATA_HISTOGRAM, DATA_CONTOUR, DATA_CALIBRATION } DataType;

typedef enum { DATA_STORAGE_GENERIC, DATA_STORAGE_MEMSTORAGE } StorageType;

typedef struct {
	BYTE bNumItems;	// Number of data items
	DataType eDataType[MAX_DATA_ITEMS];
	StorageType eDataStorageType[MAX_DATA_ITEMS];
	DWORD dwDataSize[MAX_DATA_ITEMS];	// Size in bytes of data, if 0 then size is retrievable from data itself (eg. IplImage)
	void *pData[MAX_DATA_ITEMS];
	void *pDataStorage[MAX_DATA_ITEMS];	// In some cases, the data pointer itself is not the pointer used to deallocate the data
} DataStruct;

#define MAX_POINTS (100)

typedef struct {
	int nNumPoints;
	CvPoint	PointList[MAX_POINTS];
} POINT_LIST_TYPE;

typedef struct {
	CvMat *pKernel;
	CvPoint Anchor;
} KERNEL_TYPE;

typedef struct {
	int	   ImageWidth;
	int	   ImageHeight;
	double CamMatrix1[3][3];
	double CamMatrix2[3][3];
	double DistCoeff1[5];
	double DistCoeff2[5];
	double Rotation[3][3];
	double Translation[3];
	double Essential[3][3];
	double Fundamental[3][3];
	double Rectify1[3][3];
	double Rectify2[3][3];
	double Project1[3][4];
	double Project2[3][4];
	double Disparity[4][4];
} CALIBRATION_TYPE;
// COpBase

class COpBase : public CWnd
{
	DECLARE_DYNAMIC(COpBase)

public:
	COpBase(CMainView *pMain);
	virtual ~COpBase();
	BOOL	IsInput(CPoint point);
	BOOL	IsOutput(CPoint point);
	BOOL	ConnectBases(ConnectionType eConnection, COpBase *pNewBase);
	BOOL	DisconnectBases(COpBase *pCurBase);
	int		GetNumConnections(ConnectionType eConnectType) { if (eConnectType == CONNECT_INPUT) return m_nNumInputs; else return m_nNumOutputs; }
	COpBase *GetConnection(ConnectionType eConnectType, BYTE bConnectIndex);
	int		GetConnectionPoint(ConnectionType eConnectType, BYTE bConnectIndex, BYTE X);
	void	DeleteBase(BOOL fPrompt);
	void	OpShowData();
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	BOOL	ConnectOutputs();
	BYTE	GetNumInputs() { return m_nNumInputs; }
	virtual	PTCHAR GetFriendlyName() { return L"Unknown"; }
	int		GetOperator() { return m_nOperator; }
	void	SetOperator(int nOperator) { m_nOperator = nOperator; }
	DWORD	GetUniqueID() { return m_dwUniqueID; }
	void	SetUniqueID(DWORD dwID) { m_dwUniqueID = dwID; }
	virtual int	GetMinWidth() { return MIN_BASE_OBJECT_WIDTH; }
	virtual int	GetMinHeight() { return MIN_BASE_OBJECT_HEIGHT; }
	virtual int	GetDefaultWidth() { return BASE_OBJECT_WIDTH; }
	virtual int GetDefaultHeight() { return BASE_OBJECT_HEIGHT; }
	void MinimizeOperator(BOOL fMinimize);
	void GetMaximizedWindowRect(RECT *pRect);

	virtual void SendData();
	virtual void NotifyNewData(COpBase *pBase);
	virtual void NotifyNewDataIndex(int InputIndex);
	virtual DataStruct *GetData(); 
	void SetData(int Index, DataType eType, DWORD dwSizeInBytes, void *pData, BOOL fDeallocateWhenDone = FALSE, StorageType eStorageType = DATA_STORAGE_GENERIC, void *pStoragePointer = NULL);
	void *LockData(PTCHAR CallerName, DataType eType, int Index);  // Calls GetInputData but uses a mutex
	void ReleaseData(PTCHAR CallerName) { 
		Log(LOG_LOCK, L"    %s: DataInUseMutex released", CallerName);
		ReleaseMutex(m_hDataInUseMutex); 
	}
	virtual void StopBase() { };

protected:

		// Save data
	BYTE	m_nNumOutputs;
	int		m_nOperator;
	DWORD	m_dwUniqueID;
	COpBase *m_pOutputs[MAX_OUTPUTS];
	BOOL	m_fDisabled;
	BOOL	m_fMinimized;

		// Other data
	CMainView *m_pMain;
	BYTE	m_nNumInputs;
	BOOL	m_fDragging;
	BOOL	m_fConnecting;
	CPoint	m_oOldPoint;
	CPoint	m_oConnectPoint;
	CPoint	m_oOldEndPoint;
	COpBase *m_pInputs[MAX_INPUTS];
	DWORD	m_dwOutputIDs[MAX_OUTPUTS];
	void	*m_pOutputData[MAX_OUTPUTS];
	ConnectionType	m_eConnectingType;
//	HANDLE	m_hNewDataEvent;
	DataStruct m_sOutputData;
	BOOL	m_fDeallocateWhenDone[MAX_DATA_ITEMS];
	DataStruct *m_pInputData[MAX_INPUTS];
	DataStruct m_sInputDataCache[MAX_INPUTS];	// Copy of original data from input
	DataStruct m_sInputDataWorkingCopy[MAX_INPUTS];  // Working copy of data from cache
	HANDLE	m_hNewInputDataMutex;
	HANDLE	m_hDataInUseMutex;
	int		m_nCurControlY;
	int		m_nCurControlX;
	int		m_nMinimizeSavedWidth;
	int		m_nMinimizeSavedHeight;
	CWnd	*m_pControlList[MAX_OPERATOR_CONTROLS];
	BYTE	m_bNumControls;
	BOOL	m_fTitleBarShowing;
	DWORD	m_dwInputLockCount;
	TCHAR	m_pErrorText[1024];
	BOOL	m_fErrorFlag;

protected:
	void PaintBase(CPaintDC *pDc, BOOL fDisplayTitle);
	RECT GetBaseDrawingArea();
	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual void AddMenuItem(CMenu *pMenu, PTCHAR ItemStr, WORD wID, BOOL fSeparator = FALSE);
	virtual void AddMenuSubMenu(CMenu *pMenu, CMenu *pSubMenu, PTCHAR SubMenuNameStr);
	CComboBox *CreateOpCombo(int nWidth, UINT uID = 0);
	CStatic *CreateOpStatic(PTCHAR pText, BOOL fCenter, int nWidth);
	CSliderCtrl *CreateOpSlider(int nWidth, int RangeMin, int RangeMax, UINT uID = 0);
	CEdit *CreateOpEdit(int nWidth, BOOL fReadOnly, UINT uID = 0);
	CButton *CreateOpButton(PTCHAR pCaption, int nWidth, UINT uStyle, UINT uID = 0);
	void *GetInputData(DataType eType, int Index);  // This should only be called by LockData()
	void TitleError(PTCHAR pErrorText);
	void DeallocateData(void *pData, DataType eDataType, StorageType eDataStorageType = DATA_STORAGE_GENERIC, void *pDataStorage = NULL);
	afx_msg void OnPaint();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnMouseMove( UINT nFlags, CPoint point );
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
	afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
	afx_msg void OnLButtonDblClk( UINT nFlags, CPoint point );
	afx_msg	virtual void OpDisable();
	afx_msg void DeleteBasePrompt();
	afx_msg void OpChangeOperator();
	afx_msg void OnShowError();

	DECLARE_MESSAGE_MAP()
};


