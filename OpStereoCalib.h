#pragma once
#include "opbase.h"
#include "Cv.h"

#define StereoCalib_DEFAULT_WIDTH (200)
#define StereoCalib_DEFAULT_HEIGHT (220)

class COpStereoCalib : public COpBase
{
public:
	COpStereoCalib(CMainView *pMain);
	~COpStereoCalib(void);

	virtual	PTCHAR GetFriendlyName() { return L"StereoCalib"; }
	virtual int	GetDefaultWidth() { return StereoCalib_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return StereoCalib_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
//	CSliderCtrl	*m_pSlider;
//	CComboBox	*m_pCombo;
	CEdit		*m_pNumCapturesEdit;
	CEdit		*m_pCurCapturesEdit;
	CEdit		*m_pNumChessboardRowsEdit;
	CEdit		*m_pNumChessboardColsEdit;
	CButton		*m_pStartButton;
	CButton		*m_pLoadButton;
	CEdit		*m_pStatusEdit;

		// Save data
	int			m_nNumCaptures;
	int			m_nNumChessboardRows;
	int			m_nNumChessboardCols;
	
		// Other data
	std::vector<CvPoint2D32f> m_ChessboardPoints[2];
	int			m_nCurrentCapture;
	BOOL		m_fStarted;
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();
	void Calibrate(int NumImages, int nChessboardRows, int nChessboardCols, CvMat *pLeftCorners, CvMat *pRightCorners, CvSize ImageSize);

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnStartButton();
	afx_msg void OnLoadButton();

	DECLARE_MESSAGE_MAP()
};
