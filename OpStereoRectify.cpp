#include "StdAfx.h"
#include "OpStereoRectify.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bStereoRectifyFiller[32];

#define ID_RECTIFY_VIEW (101)

BEGIN_MESSAGE_MAP(COpStereoRectify, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_RECTIFY_VIEW, OnChangeView)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpStereoRectify::COpStereoRectify(CMainView *pMain) : COpBase(pMain)
{
	m_pViewCombo = NULL;

	m_nCurView = 0;	// Default to left camera

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpStereoRectify::~COpStereoRectify(void)
{
	if (m_pViewCombo)delete m_pViewCombo;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpStereoRectify::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpStereoRectify::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_pViewCombo = CreateOpCombo(0, ID_RECTIFY_VIEW);

	CComboBox *pCombo = m_pViewCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"Left camera");
		pCombo->SetItemData(Ndx, 0);
		Ndx = pCombo->AddString(L"Right camera");
		pCombo->SetItemData(Ndx, 1);
	}

//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpStereoRectify::StartOpThread(void *pData)
{
	COpStereoRectify *pBase = (COpStereoRectify *) pData;

	pBase->OpThread();

	return 0;
}

void COpStereoRectify::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpStereoRectify::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pViewCombo->GetCount(); Ndx++)
		if (m_pViewCombo->GetItemData(Ndx) == m_nCurView) break;

	m_pViewCombo->SetCurSel(Ndx);

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpStereoRectify::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpStereoRectify::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nCurView, sizeof(m_nCurView), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bStereoRectifyFiller, sizeof(bStereoRectifyFiller), &dwBytesWritten, NULL);
		}
	}

	return fRetValue;
}

BOOL COpStereoRectify::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nCurView, sizeof(m_nCurView), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bStereoRectifyFiller, sizeof(bStereoRectifyFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}

	}

	return fRetValue;
}

	// We got data from the given input index
void COpStereoRectify::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpStereoRectify::ValidateParameters()
{
}

void COpStereoRectify::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpStereoRectify::OnChangeView()
{
	
	int Ndx = m_pViewCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nCurView = (int) m_pViewCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}

}

void COpStereoRectify::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpStereoRectify::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpStereoRectify::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"StereoRectifyThread", DATA_IMAGE, 0);
		CALIBRATION_TYPE *pCalib = (CALIBRATION_TYPE *) LockData(L"StereoRectifyThread", DATA_CALIBRATION, 0);

		if (pImage && pCalib) 
		{
			CvMat *mx1 = cvCreateMat(pImage->height, pImage->width, CV_32F);
			CvMat *my1 = cvCreateMat(pImage->height, pImage->width, CV_32F);
            CvMat _M1 = cvMat(3, 3, CV_64F, pCalib->CamMatrix1);
            CvMat _M2 = cvMat(3, 3, CV_64F, pCalib->CamMatrix2);
            CvMat _D1 = cvMat(1, 5, CV_64F, pCalib->DistCoeff1);
            CvMat _D2 = cvMat(1, 5, CV_64F, pCalib->DistCoeff2);
            CvMat _R = cvMat(3, 3, CV_64F, pCalib->Rotation);
            CvMat _T = cvMat(3, 1, CV_64F, pCalib->Translation);
			CvMat _R1 = cvMat(3, 3, CV_64F, pCalib->Rectify1);
			CvMat _R2 = cvMat(3, 3, CV_64F, pCalib->Rectify2);
            CvMat _P1 = cvMat(3, 4, CV_64F, pCalib->Project1);
            CvMat _P2 = cvMat(3, 4, CV_64F, pCalib->Project2);

cvSave("M1.xml", &_M1);
cvSave("M2.xml", &_M2);
cvSave("D1.xml", &_D1);
cvSave("D2.xml", &_D2);
cvSave("R.xml", &_R);
cvSave("T.xml", &_T);
cvSave("R1.xml", &_R1);
cvSave("R2.xml", &_R2);
cvSave("P1.xml", &_P1);
cvSave("P2.xml", &_P2);

/*
			double R1[3][3], R2[3][3], P1[3][4], P2[3][4];
			CvMat _R1 = cvMat(3, 3, CV_64F, R1);
			CvMat _R2 = cvMat(3, 3, CV_64F, R2);
            CvMat _P1 = cvMat(3, 4, CV_64F, P1);
            CvMat _P2 = cvMat(3, 4, CV_64F, P2);
            CvMat _M1 = cvMat(3, 3, CV_64F, pCalib->CamMatrix1);
            CvMat _M2 = cvMat(3, 3, CV_64F, pCalib->CamMatrix2);
            CvMat _D1 = cvMat(1, 5, CV_64F, pCalib->DistCoeff1);
            CvMat _D2 = cvMat(1, 5, CV_64F, pCalib->DistCoeff2);
            CvMat _R = cvMat(3, 3, CV_64F, pCalib->Rotation);
            CvMat _T = cvMat(3, 1, CV_64F, pCalib->Translation);
            cvStereoRectify( &_M1, &_M2, &_D1, &_D2, cvGetSize(pImage),
                &_R, &_T,
                &_R1, &_R2, &_P1, &_P2, 0,
                0 );
*/                
                
//            isVerticalStereo = fabs(P2[1][3]) > fabs(P2[0][3]);
    //Precompute maps for cvRemap()
            if (m_nCurView == 0) //Left camera
            {
//Hartley's method				
//				cvInitUndistortRectifyMap(&_M1,&_D1,&_R1,&_M1,mx1,my1);
// Bouguet's method				
				cvInitUndistortRectifyMap(&_M1,&_D1,&_R1,&_P1,mx1,my1);
			}
            else // Right camera
            {
//Hartley's method	            
//				cvInitUndistortRectifyMap(&_M2,&_D2,&_R2,&_M2,mx1,my1);
// Bouguet's method	            
				cvInitUndistortRectifyMap(&_M2,&_D2,&_R2,&_P2,mx1,my1);
			}
			
			IplImage *pImageRect = cvCreateImage(cvGetSize(pImage), pImage->depth, pImage->nChannels);
			
			cvRemap(pImage, pImageRect, mx1, my1);

			SetData(0, DATA_IMAGE, 0, pImageRect, TRUE);
			SendData();

			ReleaseData(L"StereoRectifyThread");  // We're done with the data so it can change if needed
			ReleaseData(L"StereoRectifyThread");  // We're done with the data so it can change if needed
		}
			else
		{
			if (pImage) ReleaseData(L"StereoRectifyThread");  // We're done with the data so it can change if needed
			if (pCalib) ReleaseData(L"StereoRectifyThread");  // We're done with the data so it can change if needed
			
			TitleError(L"Requires image and calibration data");
		}
		
		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
