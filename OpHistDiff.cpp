#include "StdAfx.h"
#include "OpHistDiff.h"
#include "OpBase.h"
#include "Cv.h"

#define IDC_HIST_DIFF_METHOD_COMBO (101)

BYTE bHistDiffFiller[32];

BEGIN_MESSAGE_MAP(COpHistDiff, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(IDC_HIST_DIFF_METHOD_COMBO, OnMethodChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpHistDiff::COpHistDiff(CMainView *pMain) : COpBase(pMain)
{
	m_eDiffMethod = HIST_DIFF_CORREL;

	m_gDifferenceResult = 0;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpHistDiff::~COpHistDiff(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpHistDiff::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpHistDiff::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_pMethodCombo = CreateOpCombo(0, IDC_HIST_DIFF_METHOD_COMBO);

	CComboBox *pCombo = m_pMethodCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"correlation");
		pCombo->SetItemData(Ndx, HIST_DIFF_CORREL);
		Ndx = pCombo->AddString(L"chi-square");
		pCombo->SetItemData(Ndx, HIST_DIFF_CHISQR);
		Ndx = pCombo->AddString(L"intersection");
		pCombo->SetItemData(Ndx, HIST_DIFF_INTERSECT);
		Ndx = pCombo->AddString(L"Bhattacharyya");
		pCombo->SetItemData(Ndx, HIST_DIFF_BHAT);
		Ndx = pCombo->AddString(L"earth-movers");
		pCombo->SetItemData(Ndx, HIST_DIFF_EMD);
	}

	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"difference", TRUE, 0);
	m_pResultEdit = CreateOpEdit(PARAM_EDIT_WIDTH*4, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpHistDiff::StartOpThread(void *pData)
{
	COpHistDiff *pBase = (COpHistDiff *) pData;

	pBase->OpThread();

	return 0;
}

void COpHistDiff::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpHistDiff::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pMethodCombo->GetCount(); Ndx++)
		if (m_pMethodCombo->GetItemData(Ndx) == m_eDiffMethod) break;

	m_pMethodCombo->SetCurSel(Ndx);
//	m_pMethodCombo->GetLBText(Ndx, m_aStr);

	_stprintf(TStr, L"%6.2f", m_gDifferenceResult);

//	_itot(m_n, TStr, 10);
	m_pResultEdit->SetWindowTextW(TStr);


}


void COpHistDiff::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpHistDiff::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_eDiffMethod, sizeof(m_eDiffMethod), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bHistDiffFiller, sizeof(bHistDiffFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpHistDiff::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_eDiffMethod, sizeof(m_eDiffMethod), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bHistDiffFiller, sizeof(bHistDiffFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpHistDiff::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpHistDiff::ValidateParameters()
{
}

void COpHistDiff::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpHistDiff::OnMethodChange()
{
	
	int Ndx = m_pMethodCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_eDiffMethod = (HIST_DIFF_METHOD_TYPE) m_pMethodCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}

}

void COpHistDiff::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpHistDiff::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpHistDiff::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		CvHistogram *pHist1 = (CvHistogram *) LockData(L"HistDiffThread", DATA_HISTOGRAM, 0);
		CvHistogram *pHist2 = (CvHistogram *) LockData(L"HistDiffThread", DATA_HISTOGRAM, 1);

		if (pHist1 && pHist2) 
		{
			if (1) {	// Validate Src and Dst images, etc
				int nMethod = -1;

				switch (m_eDiffMethod)
				{
					case HIST_DIFF_CORREL:
						nMethod = CV_COMP_CORREL;
						break;
					case HIST_DIFF_CHISQR:
						nMethod = CV_COMP_CHISQR;
						break;
					case HIST_DIFF_INTERSECT:
						nMethod = CV_COMP_INTERSECT;
						break;
					case HIST_DIFF_BHAT:
						nMethod = CV_COMP_BHATTACHARYYA;
						break;
					case HIST_DIFF_EMD:
						nMethod = -1;
						break;

				}

				if (nMethod >= 0)
				{
					cvNormalizeHist(pHist1, 1);
					cvNormalizeHist(pHist2, 1);

					m_gDifferenceResult = cvCompareHist(pHist1, pHist2, nMethod);
				}
					else // Earth-movers distance
				{
					CvMat *pSig1, *pSig2;
					
					if (pHist1->mat.dims == 1)
					{
						int NumBins = pHist1->mat.dim[0].size;

						pSig1 = cvCreateMat(NumBins, 2, CV_32FC1); // 1 count + 1 coords = 2
						pSig2 = cvCreateMat(NumBins, 2, CV_32FC1); 
						
						for (int i = 0; i < NumBins; i++)
						{
							float bin_val = cvQueryHistValue_1D(pHist1, i);
							cvSet2D(pSig1, i, 0, cvScalar(bin_val));
							cvSet2D(pSig1, i, 1, cvScalar(i));
							bin_val = cvQueryHistValue_1D(pHist2, i);
							cvSet2D(pSig2, i, 0, cvScalar(bin_val));
							cvSet2D(pSig2, i, 1, cvScalar(i));
						}
						
						m_gDifferenceResult = cvCalcEMD2(pSig1, pSig2, CV_DIST_L2);
					}
				}				
				
				UpdateValues();

//				SetData(0, DATA_IMAGE, 0, pImage);
//				SendData();

			}
				else TitleError(L"");

			ReleaseData(L"HistDiffThread");  // We're done with the data so it can change if needed
			ReleaseData(L"HistDiffThread");  // We're done with the data so it can change if needed
		}
			else
		{
			TitleError(L"Input requires two histograms");
			
			if (pHist1) ReleaseData(L"HistDiffThread");
			if (pHist2) ReleaseData(L"HistDiffThread");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
