#pragma once
#include "opbase.h"
#include "Cv.h"

#define ConvertScale_DEFAULT_WIDTH (200)
#define ConvertScale_DEFAULT_HEIGHT (120)

class COpConvertScale : public COpBase
{
public:
	COpConvertScale(CMainView *pMain);
	~COpConvertScale(void);

	virtual	PTCHAR GetFriendlyName() { return L"ConvertScale"; }
	virtual int	GetDefaultWidth() { return ConvertScale_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return ConvertScale_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pScaleSlider;
	CSliderCtrl	*m_pShiftSlider;
	CEdit		*m_pScaleEdit;
	CEdit		*m_pShiftEdit;

		// Save data
	int m_nScaleShift;
	int m_nShiftShift;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
