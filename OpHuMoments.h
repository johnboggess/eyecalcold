#pragma once
#include "opbase.h"
#include "Cv.h"

#define HuMoments_DEFAULT_WIDTH (200)
#define HuMoments_DEFAULT_HEIGHT (250)

class COpHuMoments : public COpBase
{
public:
	COpHuMoments(CMainView *pMain);
	~COpHuMoments(void);

	virtual	PTCHAR GetFriendlyName() { return L"HuMoments"; }
	virtual int	GetDefaultWidth() { return HuMoments_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return HuMoments_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CEdit		*m_pHu01Edit;
	CEdit		*m_pHu02Edit;
	CEdit		*m_pHu03Edit;
	CEdit		*m_pHu04Edit;
	CEdit		*m_pHu05Edit;
	CEdit		*m_pHu06Edit;
	CEdit		*m_pHu07Edit;

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	CvMoments	m_CvMoments;
	CvHuMoments	m_CvHuMoments;


	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
