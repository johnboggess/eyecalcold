#pragma once
#include "opbase.h"
#include "Cv.h"

#define HistDiff_DEFAULT_WIDTH (150)
#define HistDiff_DEFAULT_HEIGHT (110)

typedef enum { HIST_DIFF_CORREL, HIST_DIFF_CHISQR, HIST_DIFF_INTERSECT, HIST_DIFF_BHAT, HIST_DIFF_EMD } HIST_DIFF_METHOD_TYPE;

class COpHistDiff : public COpBase
{
public:
	COpHistDiff(CMainView *pMain);
	~COpHistDiff(void);

	virtual	PTCHAR GetFriendlyName() { return L"HistDiff"; }
	virtual int	GetDefaultWidth() { return HistDiff_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return HistDiff_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CComboBox	*m_pMethodCombo;
	CEdit		*m_pResultEdit;

		// Save data
	HIST_DIFF_METHOD_TYPE m_eDiffMethod;
	
		// Other data
	double		m_gDifferenceResult;
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();
	afx_msg void OnMethodChange();

	DECLARE_MESSAGE_MAP()
};
