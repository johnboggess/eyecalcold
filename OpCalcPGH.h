#pragma once
#include "opbase.h"
#include "Cv.h"

#define CalcPGH_DEFAULT_WIDTH (100)
#define CalcPGH_DEFAULT_HEIGHT (100)

class COpCalcPGH : public COpBase
{
public:
	COpCalcPGH(CMainView *pMain);
	~COpCalcPGH(void);

	virtual	PTCHAR GetFriendlyName() { return L"Pairwise Hist"; }
	virtual int	GetDefaultWidth() { return CalcPGH_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return CalcPGH_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;



	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
