#include "StdAfx.h"
#include "OpMatchTemplate.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bMatchTemplateFiller[32];

#define ID_MATCH_METHOD_COMBO (101)

BEGIN_MESSAGE_MAP(COpMatchTemplate, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_MATCH_METHOD_COMBO, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpMatchTemplate::COpMatchTemplate(CMainView *pMain) : COpBase(pMain)
{

	m_pMatchMethodCombo = NULL;

	m_nMatchMethod = CV_TM_SQDIFF;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpMatchTemplate::~COpMatchTemplate(void)
{
	if (m_pMatchMethodCombo)delete m_pMatchMethodCombo;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpMatchTemplate::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpMatchTemplate::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	m_pMatchMethodCombo = CreateOpCombo(0, ID_MATCH_METHOD_COMBO);

	CComboBox *pCombo = m_pMatchMethodCombo;
	if (pCombo)
	{
		int Ndx = pCombo->AddString(L"square difference");
		pCombo->SetItemData(Ndx, CV_TM_SQDIFF);
		Ndx = pCombo->AddString(L"square difference - normalized ");
		pCombo->SetItemData(Ndx, CV_TM_SQDIFF_NORMED);
		Ndx = pCombo->AddString(L"correlation");
		pCombo->SetItemData(Ndx, CV_TM_CCORR);
		Ndx = pCombo->AddString(L"correlation - normalized");
		pCombo->SetItemData(Ndx, CV_TM_CCORR_NORMED);
		Ndx = pCombo->AddString(L"correlation coefficient");
		pCombo->SetItemData(Ndx, CV_TM_CCOEFF);
		Ndx = pCombo->AddString(L"correlation coefficient - normalized ");
		pCombo->SetItemData(Ndx, CV_TM_CCOEFF_NORMED);
	}

//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

//	m_pButton = CreateOpButton(L"", 80, BS_AUTOCHECKBOX, ID_FIXED_RANGE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpMatchTemplate::StartOpThread(void *pData)
{
	COpMatchTemplate *pBase = (COpMatchTemplate *) pData;

	pBase->OpThread();

	return 0;
}

void COpMatchTemplate::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpMatchTemplate::UpdateValues()
{
	TCHAR TStr[10];


	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pMatchMethodCombo->GetCount(); Ndx++)
		if (m_pMatchMethodCombo->GetItemData(Ndx) == m_nMatchMethod) break;

	m_pMatchMethodCombo->SetCurSel(Ndx);


//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpMatchTemplate::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpMatchTemplate::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (WriteFile(hFile, &m_nMatchMethod, sizeof(m_nMatchMethod), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bMatchTemplateFiller, sizeof(bMatchTemplateFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpMatchTemplate::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (ReadFile(hFile, &m_nMatchMethod, sizeof(m_nMatchMethod), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bMatchTemplateFiller, sizeof(bMatchTemplateFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpMatchTemplate::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpMatchTemplate::ValidateParameters()
{
}

void COpMatchTemplate::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpMatchTemplate::OnChange()
{
	
	int Ndx = m_pMatchMethodCombo->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nMatchMethod = (int) m_pMatchMethodCombo->GetItemData(Ndx);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}

}

void COpMatchTemplate::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpMatchTemplate::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpMatchTemplate::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"MatchTemplateThread", DATA_IMAGE, 0);
		IplImage *pTemplateImage = (IplImage *) LockData(L"MatchTemplateThread", DATA_IMAGE, 1);

		if (pImage && pTemplateImage) 
		{
			if (1) {	// Validate Src and Dst images, etc
				CvSize TemplateSize = cvGetSize(pTemplateImage);
				CvSize ResultSize = cvGetSize(pImage);
				
				ResultSize.width -= TemplateSize.width - 1;
				ResultSize.height -= TemplateSize.height - 1;
				
				IplImage *pResultImage = cvCreateImage(ResultSize, IPL_DEPTH_32F, 1);

				cvMatchTemplate(pImage, pTemplateImage, pResultImage, m_nMatchMethod);

				SetData(0, DATA_IMAGE, 0, pResultImage, TRUE);
				SendData();

			}
				else TitleError(L"");

			ReleaseData(L"MatchTemplateThread");  // We're done with the data so it can change if needed
			ReleaseData(L"MatchTemplateThread");  // We're done with the data so it can change if needed
		}
			else
		{
			TitleError(L"Search image and template image required");
			
			if (pImage) ReleaseData(L"MatchTemplateThread");
			if (pTemplateImage) ReleaseData(L"MatchTemplateThread");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
