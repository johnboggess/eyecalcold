#pragma once
#include "opbase.h"
#include "Cv.h"

#define AbsDiff_DEFAULT_WIDTH (100)
#define AbsDiff_DEFAULT_HEIGHT (80)

class COpAbsDiff : public COpBase
{
public:
	COpAbsDiff(CMainView *pMain);
	~COpAbsDiff(void);

	virtual	PTCHAR GetFriendlyName() { return L"AbsDiff"; }
	virtual int	GetDefaultWidth() { return AbsDiff_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return AbsDiff_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:

		// Save data

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
