#include "StdAfx.h"
#include "OpSubtract.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bSubtractFiller[32];

BEGIN_MESSAGE_MAP(COpSubtract, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
END_MESSAGE_MAP()

COpSubtract::COpSubtract(CMainView *pMain) : COpBase(pMain)
{

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpSubtract::~COpSubtract(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpSubtract::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpSubtract::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
/*
	m_pCombo = CreateOpCombo(0, ID_TYPE);

	CCombo *pCombo = m_pCombo;
	if (pCombo)
	{
//		int Ndx = pCombo->AddString(L"");
//		pCombo->SetItemData(Ndx, );
//		Ndx = pCombo->AddString(L"");
//		pCombo->SetItemData(Ndx, );
	}

//	CreateOpStatic(L"", TRUE, 0);
//	m_pEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
//	m_pSlider = CreateOpSlider(0, MIN, MAX);

	UpdateSliders();
	UpdateValues();
*/

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpSubtract::StartOpThread(void *pData)
{
	COpSubtract *pBase = (COpSubtract *) pData;

	pBase->OpThread();

	return 0;
}

void COpSubtract::UpdateSliders()
{
//	m_pSluder->SetPos(m_n);

}
void COpSubtract::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

//	_itot(m_n, TStr, 10);
//	m_pEdit->SetWindowTextW(TStr);


}


void COpSubtract::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpSubtract::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
/*	
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		WriteFile(hFile, &m_e, sizeof(m_e), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bSubtractFiller, sizeof(bSubtractFiller), &dwBytesWritten, NULL);
		}
*/
	}

	return fRetValue;
}

BOOL COpSubtract::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
/*	
		DWORD dwBytesWritten;

		fRetValue = FALSE;

//		ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_n, sizeof(m_n), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bSubtractFiller, sizeof(bSubtractFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
*/		
	}

	return fRetValue;
}

	// We got data from the given input index
void COpSubtract::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpSubtract::ValidateParameters()
{
}

void COpSubtract::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
/*
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

//	if (pSlider == m_pSlider) 
//		m_n = pSlider->GetPos();


	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
*/
}

void COpSubtract::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpSubtract::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"SubtractThread", DATA_IMAGE, 0);
		IplImage *pImage2 = NULL;

		if (pImage) 
		{
			pImage2 = cvCloneImage(pImage);
			ReleaseData(L"SubtractThread");
			
			IplImage *pImage = (IplImage *) LockData(L"SubtractThread", DATA_IMAGE, 1);
			
			if (pImage) {	// Validate Src and Dst images, etc

				if (pImage2->width == pImage->width && pImage2->height == pImage->height && pImage->depth == pImage2->depth &&
					pImage->nChannels == pImage2->nChannels)
				{
					IplImage *pDest = cvCloneImage(pImage);

					cvAbsDiff(pImage2, pImage, pDest);

					if (pDest)
					{
						SetData(0, DATA_IMAGE, 0, pDest, TRUE);
						SendData();
					}
				} else TitleError(L"Src and Dest images must be the same size and depth");
				
				ReleaseData(L"SubtractThread");  // We're done with the data so it can change if needed
			}
				else TitleError(L"Operation requires two images");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}


/*
	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"SubtractThread", DATA_IMAGE, 0);
		IplImage *pImage2 = NULL;

		if (pImage) 
		{
			pImage2 = cvCloneImage(pImage);
			ReleaseData(L"SubtractThread");
			
			IplImage *pImage = (IplImage *) LockData(L"SubtractThread", DATA_IMAGE, 1);
			
			if (pImage) {	// Validate Src and Dst images, etc

				if (pImage2->width == pImage->width && pImage2->height == pImage->height)
				{
					IplImage *pDest = cvCloneImage(pImage);

					cvSub(pImage2, pImage, pDest);

					if (pDest)
					{
						SetData(0, DATA_IMAGE, 0, pDest, TRUE);
						SendData();
					}
				} else TitleError(L"Src and Dest images must be the same size");
				
				ReleaseData(L"SubtractThread");  // We're done with the data so it can change if needed
			}
				else TitleError(L"Operation requires two images");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
*/

/*
	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"SubtractThread", DATA_IMAGE, 0);
		IplImage *pImage2 = (IplImage *) LockData(L"SubtractThread", DATA_IMAGE, 1);

		if (pImage && pImage2) 
		{
			if (pImage2->width == pImage->width && pImage2->height == pImage->height)
			{
				IplImage *pDest = cvCloneImage(pImage);

				cvSub(pImage2, pImage, pDest);

				if (pDest)
				{
					SetData(0, DATA_IMAGE, 0, pDest, TRUE);
					SendData();
				}
			} else TitleError(L"Src and Dest images must be the same size");
			
			ReleaseData(L"SubtractThread");  // We're done with the data so it can change if needed
			ReleaseData(L"SubtractThread");  // We're done with the data so it can change if needed
		}
			else   // Only got one image
		{
			if (pImage) ReleaseData(L"AddThread");

			TitleError(L"Operation requires two images");
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
*/