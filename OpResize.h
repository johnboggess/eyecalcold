#pragma once
#include "opbase.h"
#include "Cv.h"

#define Resize_DEFAULT_WIDTH (200)
#define Resize_DEFAULT_HEIGHT (160)

class COpResize : public COpBase
{
public:
	COpResize(CMainView *pMain);
	~COpResize(void);

	virtual	PTCHAR GetFriendlyName() { return L"Resize"; }
	virtual int	GetDefaultWidth() { return Resize_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Resize_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CEdit		*m_pWidthEdit;
	CEdit		*m_pHeightEdit;
	CComboBox	*m_pInterpolationCombo;
	TCHAR		m_aInterpolationStr[20];

		// Save data
	int		m_nWidth;
	int		m_nHeight;
	int		m_nInterpolation;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnResizeButton();
	afx_msg void OnInterpolationChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
