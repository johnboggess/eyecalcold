#include "StdAfx.h"
#include "OpSmooth.h"
#include "Cv.h"

#define ID_PARAM1 (100)
#define ID_PARAM2 (101)
#define ID_PARAM3 (102)
#define ID_PARAM4 (103)
#define ID_SMOOTHTYPE (104)

#define MIN_SMOOTH_WIDTH (2)
#define MAX_SMOOTH_WIDTH (100)
#define MIN_SMOOTH_SIGMA (0)
#define BILATERAL_MAX (500)

#define MIN_ITERATIONS (1)
#define MAX_ITERATIONS (20)

#define MAX_SMOOTH_SIGMA (50)

BYTE bSmoothFiller[64];

BEGIN_MESSAGE_MAP(COpSmooth, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(ID_SMOOTHTYPE, SmoothTypeChange)
	//	ON_WM_MOUSEMOVE()
//	ON_WM_LBUTTONDOWN()
//	ON_WM_LBUTTONUP()
//	ON_COMMAND(ID_LOAD_IMAGE, LoadImage)

END_MESSAGE_MAP()

COpSmooth::COpSmooth(CMainView *pMain) : COpBase(pMain)
{
	m_pParam1 = NULL;
	m_pParam2 = NULL;
	m_pParam3 = NULL;
	m_pParam4 = NULL;
	m_pParamEdit1 = NULL;
	m_pParamEdit2 = NULL;
	m_pParamEdit3 = NULL;
	m_pParamEdit4 = NULL;
	m_pSmoothType = NULL;
	m_pIterationsSlider = NULL;
	m_pIterationsEdit = NULL;

	m_nSmoothType = CV_BLUR;
	_tcscpy(m_aSmoothTypeStr, L"Blur");
	m_bBlurWidth = 3;
	m_bBlurHeight = MIN_SMOOTH_WIDTH;
	m_fSigma1 = 0;
	m_fSigma2 = 0;
	m_nIterations = MIN_ITERATIONS;

	m_hThread = NULL;

}

COpSmooth::~COpSmooth(void)
{
	if (m_pParam1)delete m_pParam1;
	if (m_pParam2)delete m_pParam2;
	if (m_pParam3)delete m_pParam3;
	if (m_pParam4)delete m_pParam4;
	if (m_pSmoothType) delete m_pSmoothType;
	if (m_pParamEdit1) delete m_pParamEdit1;
	if (m_pParamEdit2) delete m_pParamEdit2;
	if (m_pParamEdit3) delete m_pParamEdit3;
	if (m_pParamEdit4) delete m_pParamEdit4;
	if (m_pIterationsSlider) delete m_pIterationsSlider;
	if (m_pIterationsEdit) delete m_pIterationsEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpSmooth::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

			
	}

}

int COpSmooth::OnCreate( LPCREATESTRUCT lpCreateStruct )

{
	m_pSmoothType = CreateOpCombo(0, ID_SMOOTHTYPE);

	if (m_pSmoothType)
	{
		int Ndx = m_pSmoothType->AddString(L"blur");
		m_pSmoothType->SetItemData(Ndx, CV_BLUR);
		Ndx = m_pSmoothType->AddString(L"Gaussian");
		m_pSmoothType->SetItemData(Ndx, CV_GAUSSIAN);
		Ndx = m_pSmoothType->AddString(L"median");
		m_pSmoothType->SetItemData(Ndx, CV_MEDIAN);
		Ndx = m_pSmoothType->AddString(L"bilateral");
		m_pSmoothType->SetItemData(Ndx, CV_BILATERAL);
	}

	CreateOpStatic(L"width", TRUE, 0);
	m_pParamEdit1 = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pParam1 = CreateOpSlider(0, MIN_SMOOTH_WIDTH, MAX_SMOOTH_WIDTH);

	CreateOpStatic(L"height", TRUE, 0);
	m_pParamEdit2 = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pParam2 = CreateOpSlider(0, MIN_SMOOTH_WIDTH, MAX_SMOOTH_WIDTH);

	CreateOpStatic(L"sigma 1", TRUE, 0);
	m_pParamEdit3 = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pParam3 = CreateOpSlider(0, MIN_SMOOTH_SIGMA, MAX_SMOOTH_SIGMA);

	CreateOpStatic(L"sigma 2", TRUE, 0);
	m_pParamEdit4 = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pParam4 = CreateOpSlider(0, MIN_SMOOTH_SIGMA, MAX_SMOOTH_SIGMA);

	CreateOpStatic(L"iterations", TRUE, 0);
	m_pIterationsEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pIterationsSlider = CreateOpSlider(0, MIN_ITERATIONS, MAX_ITERATIONS);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpSmooth::StartOpThread(void *pData)
{
	COpSmooth *pSmooth = (COpSmooth *) pData;

	pSmooth->OpThread();

	return 0;
}

void COpSmooth::UpdateSliders()
{
	m_pParam1->SetPos(m_bBlurWidth);
	m_pParam2->SetPos(m_bBlurHeight);
	m_pIterationsSlider->SetPos(m_nIterations);

	if (m_nSmoothType == CV_BILATERAL)
	{
		m_pParam3->SetPos((int)(m_fSigma1));
		m_pParam4->SetPos((int)(m_fSigma2));
	}
	else
	{
		m_pParam3->SetPos((int)(m_fSigma1 * 10.0));
		m_pParam4->SetPos((int)(m_fSigma2 * 10.0));
	}


}
void COpSmooth::UpdateValues()
{
	TCHAR TStr[10];

	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_pSmoothType->GetCount(); Ndx++)
		if (m_pSmoothType->GetItemData(Ndx) == m_nSmoothType) break;

	m_pSmoothType->SetCurSel(Ndx);
	m_pSmoothType->GetLBText(Ndx, m_aSmoothTypeStr);

	_itot(m_bBlurWidth, TStr, 10);
	m_pParamEdit1->SetWindowTextW(TStr);
	_itot(m_bBlurHeight, TStr, 10);
	m_pParamEdit2->SetWindowTextW(TStr);

	_stprintf(TStr, L"%2.1f", m_fSigma1);
	m_pParamEdit3->SetWindowTextW(TStr);

	_stprintf(TStr, L"%2.1f", m_fSigma2);
	m_pParamEdit4->SetWindowTextW(TStr);

	_itot(m_nIterations, TStr, 10);
	m_pIterationsEdit->SetWindowTextW(TStr);

}


void COpSmooth::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpSmooth::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nSmoothType, sizeof(m_nSmoothType), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_bBlurWidth, sizeof(m_bBlurWidth), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_bBlurHeight, sizeof(m_bBlurHeight), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_fSigma1, sizeof(m_fSigma1), &dwBytesWritten, NULL);
		WriteFile(hFile, &m_fSigma2, sizeof(m_fSigma2), &dwBytesWritten, NULL);
		
		if (WriteFile(hFile, &m_nIterations, sizeof(m_nIterations), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bSmoothFiller, sizeof(bSmoothFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpSmooth::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nSmoothType, sizeof(m_nSmoothType), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_bBlurWidth, sizeof(m_bBlurWidth), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_bBlurHeight, sizeof(m_bBlurHeight), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_fSigma1, sizeof(m_fSigma1), &dwBytesWritten, NULL);
		ReadFile(hFile, &m_fSigma2, sizeof(m_fSigma2), &dwBytesWritten, NULL);
		
		if (ReadFile(hFile, &m_nIterations, sizeof(m_nIterations), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bSmoothFiller, sizeof(bSmoothFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
			SmoothTypeChange();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpSmooth::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpSmooth::ValidateParameters(int bBlurWidth, int bBlurHeight, float fSigma1, float fSigma2)
{
	if ((m_nSmoothType == CV_GAUSSIAN || m_nSmoothType == CV_MEDIAN || m_nSmoothType == CV_BILATERAL) && bBlurWidth % 2 == 0) { bBlurWidth++;  }

	if (m_nSmoothType == CV_MEDIAN || m_nSmoothType == CV_BILATERAL) // Must be square
	{
		if (bBlurWidth > 15) bBlurWidth = 15;
		
		if (bBlurHeight != m_bBlurHeight) bBlurWidth = bBlurHeight;
		else bBlurHeight = bBlurWidth;

		m_pParam2->SetPos(bBlurWidth);
	}

	if ((m_nSmoothType == CV_GAUSSIAN  || m_nSmoothType == CV_MEDIAN || m_nSmoothType == CV_BILATERAL) && bBlurHeight % 2 == 0) { bBlurHeight++;  }

	if (m_nSmoothType == CV_MEDIAN || m_nSmoothType == CV_BILATERAL) // Must be square
	{
		if (bBlurHeight > 15) bBlurHeight = 15;

		bBlurWidth = bBlurHeight;
		m_pParam1->SetPos(bBlurHeight);
	}

	m_bBlurWidth = bBlurWidth;
	m_bBlurHeight = bBlurHeight;
}

void COpSmooth::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	int bBlurWidth = m_bBlurWidth;
	int bBlurHeight = m_bBlurHeight;

	if (pSlider == m_pParam1) 
	{
		bBlurWidth = pSlider->GetPos();

	}
	else if (pSlider == m_pParam2) 
	{
		bBlurHeight = pSlider->GetPos();

	}
	else if (pSlider == m_pParam3) 
	{
		if (m_nSmoothType == CV_BILATERAL) m_fSigma1 = (float) pSlider->GetPos();
		else m_fSigma1 = pSlider->GetPos() / 10.0;
	}
	else if (pSlider == m_pParam4) 
	{
		if (m_nSmoothType == CV_BILATERAL) m_fSigma2 = (float) pSlider->GetPos();
		else m_fSigma2 = pSlider->GetPos() / 10.0;
	}
	else if (pSlider == m_pIterationsSlider)
		m_nIterations = pSlider->GetPos();

	ValidateParameters(bBlurWidth, bBlurHeight, m_fSigma1, m_fSigma2);

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpSmooth::SmoothTypeChange()
{
	
	int Ndx = m_pSmoothType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		m_nSmoothType = m_pSmoothType->GetItemData(Ndx);
		m_pSmoothType->GetLBText(Ndx, m_aSmoothTypeStr);

		if (m_nSmoothType == CV_BILATERAL)
		{
			m_pParam3->SetRangeMax(BILATERAL_MAX);
			m_pParam4->SetRangeMax(BILATERAL_MAX);
		}
		else
		{
			m_pParam3->SetRangeMax(MAX_SMOOTH_SIGMA);
			m_pParam4->SetRangeMax(MAX_SMOOTH_SIGMA);
		}

		ValidateParameters(m_bBlurWidth, m_bBlurHeight, m_fSigma1, m_fSigma2);

		UpdateValues();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
}

void COpSmooth::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"SmoothThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			BYTE bBlurWidth = m_bBlurWidth;
			BYTE bBlurHeight = m_bBlurHeight;

			IplImage *pSrc = pImage;
			BOOL fDeallocateWhenDone = FALSE;

				// Need separate destination buffer
			if (m_nIterations > 1 || (m_nSmoothType != CV_BLUR && m_nSmoothType != CV_GAUSSIAN))
				pSrc = cvCloneImage(pImage);

			if (m_nSmoothType == CV_BLUR_NO_SCALE)
			{
				pImage = cvCreateImage(cvSize(pImage->width, pImage->height), IPL_DEPTH_16U, pImage->nChannels);
				if (pImage) fDeallocateWhenDone = TRUE;
			}

			for (int i = 0; i < m_nIterations; i++)
			{
				cvSmooth(pSrc, pImage, m_nSmoothType, bBlurWidth, bBlurHeight, m_fSigma1, m_fSigma2);
				if (i < m_nIterations-1) // Reuse a copy of the output for the next input
				{
					IplImage *pOld = pSrc;
					
					pSrc = cvCloneImage(pImage);
					
					cvReleaseImage(&pOld);
				}
			}

				// Release separate destination buffer
			if (m_nSmoothType != CV_BLUR && m_nSmoothType != CV_GAUSSIAN)
				cvReleaseImage(&pSrc);

			SetData(0, DATA_IMAGE, 0, pImage, fDeallocateWhenDone);
			SendData();

			ReleaseData(L"SmoothThread");  // We're done with the data so it can change if needed

		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
