// OpSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "EyeCalc.h"
#include "OpSelDlg.h"


// COpSelDlg dialog

IMPLEMENT_DYNAMIC(COpSelDlg, CDialog)

COpSelDlg::COpSelDlg(CPoint DialogXy, int *pSelectionIndex, PTCHAR ItemList, CWnd* pParent /*=NULL*/)
	: CDialog(COpSelDlg::IDD, pParent)
{
	m_pItemList = ItemList;
	m_pSelectionIndex = pSelectionIndex;

	*m_pSelectionIndex = 0;

	m_oDialogXy = DialogXy;
}

COpSelDlg::~COpSelDlg()
{
}

void COpSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COpSelDlg, CDialog)
	ON_EN_CHANGE(IDC_OP_EDIT, &COpSelDlg::OnEnChangeOpEdit)
	ON_LBN_DBLCLK(IDC_OP_LIST, &COpSelDlg::OnLbnDblclkOpList)
END_MESSAGE_MAP()

BOOL COpSelDlg::OnInitDialog( )
{

	SetWindowPos(&wndTopMost, m_oDialogXy.x, m_oDialogXy.y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);


	CListBox *pList = (CListBox *) GetDlgItem(IDC_OP_LIST);

	TCHAR *pStr = m_pItemList;
	TCHAR NewStr[100];
	DWORD dwCount = 0;

	while (*pStr) {
		TCHAR *pStart = pStr;
		while (*pStr && *pStr != '|') pStr++;  // Find divider

		_tcsncpy(NewStr, pStart, pStr-pStart);
		*(NewStr + (pStr-pStart)) = 0;

		if (*NewStr) 
		{
			int Ndx = pList->AddString(NewStr);

			pList->SetItemData(Ndx, dwCount);
		}

		if (*pStr == '|') pStr++;

		dwCount++;
	}


	return TRUE;
}

// COpSelDlg message handlers
void COpSelDlg::OnEnChangeOpEdit()
{
	TCHAR TStr[100];

	CEdit *pEdit = (CEdit *) GetDlgItem(IDC_OP_EDIT);

	pEdit->GetWindowTextW(TStr, 100);

	CListBox *pList = (CListBox *) GetDlgItem(IDC_OP_LIST);

	int Ndx = pList->SelectString(-1, TStr);

	*m_pSelectionIndex = pList->GetItemData(Ndx);

}

void COpSelDlg::OnLbnDblclkOpList()
{
	CListBox *pList = (CListBox *) GetDlgItem(IDC_OP_LIST);

	*m_pSelectionIndex = pList->GetItemData(pList->GetCurSel());

	OnOK();

}
