#pragma once
#include "opbase.h"
#include "Cv.h"

#define Canny_DEFAULT_WIDTH (200)
#define Canny_DEFAULT_HEIGHT (150)

class COpCanny : public COpBase
{
public:
	COpCanny(CMainView *pMain);
	~COpCanny(void);

	virtual	PTCHAR GetFriendlyName() { return L"Canny"; }
	virtual int	GetDefaultWidth() { return Canny_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Canny_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pLowThreshSlider;
	CEdit		*m_pLowThreshEdit;
	CSliderCtrl	*m_pHiThreshSlider;
	CEdit		*m_pHiThreshEdit;
	CSliderCtrl	*m_pApertureSlider;
	CEdit		*m_pApertureEdit;

		// Save data
	int	m_nLowThresh;
	int	m_nHiThresh;
	int	m_nAperture;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
