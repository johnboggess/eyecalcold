#include "StdAfx.h"
#include "OpLaplace.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bLaplaceFiller[32];

BEGIN_MESSAGE_MAP(COpLaplace, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
END_MESSAGE_MAP()

COpLaplace::COpLaplace(CMainView *pMain) : COpBase(pMain)
{
	m_pApertureSlider = NULL;
	m_pApertureEdit = NULL;

		// Save data
	m_nAperture = 3;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpLaplace::~COpLaplace(void)
{
	if (m_pApertureSlider) delete m_pApertureSlider;
	if (m_pApertureEdit) delete m_pApertureEdit;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpLaplace::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpLaplace::OnCreate( LPCREATESTRUCT lpCreateStruct )
{

	CreateOpStatic(L"aperture", TRUE, 0);
	m_pApertureEdit = CreateOpEdit(PARAM_EDIT_WIDTH, TRUE);
	m_pApertureSlider = CreateOpSlider(0, 1, 7);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpLaplace::StartOpThread(void *pData)
{
	COpLaplace *pBase = (COpLaplace *) pData;

	pBase->OpThread();

	return 0;
}

void COpLaplace::UpdateSliders()
{
	m_pApertureSlider->SetPos(m_nAperture);

}
void COpLaplace::UpdateValues()
{
	TCHAR TStr[10];

	_itot(m_nAperture, TStr, 10);
	m_pApertureEdit->SetWindowTextW(TStr);

}


void COpLaplace::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpLaplace::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (WriteFile(hFile, &m_nAperture, sizeof(m_nAperture), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bLaplaceFiller, sizeof(bLaplaceFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpLaplace::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		if (ReadFile(hFile, &m_nAperture, sizeof(m_nAperture), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bLaplaceFiller, sizeof(bLaplaceFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpLaplace::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpLaplace::ValidateParameters()
{
	if (m_nAperture %2 == 0) m_nAperture++;	// must be odd
	
}

void COpLaplace::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pApertureSlider) 
		m_nAperture = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpLaplace::OnChange()
{
}

void COpLaplace::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		IplImage *pImage = (IplImage *) LockData(L"LaplaceThread", DATA_IMAGE, 0);

		if (pImage) 
		{
			if (1) {	// Validate Src and Dst images, etc

				int Depth = pImage->depth;
				if (Depth == IPL_DEPTH_8U)Depth = IPL_DEPTH_16S;  // to avoid overflow
				
				IplImage *pNewImage = cvCreateImage(cvGetSize(pImage), Depth, pImage->nChannels);
				
				if (pNewImage)
				{
					cvLaplace(pImage, pNewImage, m_nAperture);

					SetData(0, DATA_IMAGE, 0, pNewImage, TRUE);
					SendData();
				}
			}
				else TitleError(L"");

			ReleaseData(L"LaplaceThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
