#pragma once
#include "opbase.h"
#include "Cv.h"

#define Video_DEFAULT_WIDTH (200)
#define Video_DEFAULT_HEIGHT (200)

typedef enum { VIDEO_SOURCE_NONE, VIDEO_SOURCE_0, VIDEO_SOURCE_1, VIDEO_SOURCE_SVS_LEFT, VIDEO_SOURCE_SVS_RIGHT } VIDEO_SOURCE_TYPE;


class COpVideo : public COpBase
{
public:
	COpVideo(CMainView *pMain);
	~COpVideo(void);

	virtual	PTCHAR GetFriendlyName() { return L"Video"; }
	virtual int	GetDefaultWidth() { return Video_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Video_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);

	virtual void StopBase();

protected:
	CComboBox	*m_pSourceCombo;
	CEdit		*m_pFileNameEdit;
	CButton		*m_pBrowseButton;
	
	CSliderCtrl	*m_pDelaySlider;
	CEdit		*m_pDelayEdit;
	CButton		*m_pPauseButton;
	CButton		*m_pSaveButton;
	CEdit		*m_pFPSEdit;

		// Save data
	int			m_nDelay;
	BOOL		m_fPaused;
	BOOL		m_fSaveToFile;
	TCHAR		m_aSaveFileName[MAX_PATH];
	VIDEO_SOURCE_TYPE m_eVideoSource;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnVideoSourceChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnPauseButton();
	afx_msg void OnSaveButton();
	afx_msg void OnBrowseButton();

	DECLARE_MESSAGE_MAP()
};
