#include "StdAfx.h"
#include "OpApproxPoly.h"
#include "OpBase.h"
#include "Cv.h"

BYTE bApproxPolyFiller[32];

#define ID_RECURSIVE_BUTTON (101)

BEGIN_MESSAGE_MAP(COpApproxPoly, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
//	ON_COMMAND(ID_, OnButton)
END_MESSAGE_MAP()

COpApproxPoly::COpApproxPoly(CMainView *pMain) : COpBase(pMain)
{

	m_pParameterSlider = NULL;
	m_pParameterEdit = NULL;
	m_pRecursiveButton = NULL;

	m_nParameter = 10;
	m_nRecursive = 0;
	
	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpApproxPoly::~COpApproxPoly(void)
{
	if (m_pParameterSlider) delete m_pParameterSlider;
	if (m_pParameterEdit) delete m_pParameterEdit;
	if (m_pRecursiveButton) delete m_pRecursiveButton;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpApproxPoly::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpApproxPoly::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"parameter", TRUE, 0);
	m_pParameterEdit = CreateOpEdit(PARAM_EDIT_WIDTH_3, TRUE);
	m_pParameterSlider = CreateOpSlider(0, 1, 200);
	CreateOpStatic(L"", TRUE, 0);

	m_pRecursiveButton = CreateOpButton(L"Recursive", 90, BS_AUTOCHECKBOX, ID_RECURSIVE_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpApproxPoly::StartOpThread(void *pData)
{
	COpApproxPoly *pBase = (COpApproxPoly *) pData;

	pBase->OpThread();

	return 0;
}

void COpApproxPoly::UpdateSliders()
{
	m_pParameterSlider->SetPos(m_nParameter);

}
void COpApproxPoly::UpdateValues()
{
	TCHAR TStr[10];

/*
	int Ndx = LB_ERR;
	for (Ndx = 0; Ndx < m_p->GetCount(); Ndx++)
		if (m_pe->GetItemData(Ndx) == m_e) break;

	m_p->SetCurSel(Ndx);
	m_p->GetLBText(Ndx, m_aStr);
*/

	_itot(m_nParameter, TStr, 10);
	m_pParameterEdit->SetWindowTextW(TStr);

	if (m_nRecursive) m_pRecursiveButton->SetCheck(BST_CHECKED);
		else m_pRecursiveButton->SetCheck(BST_UNCHECKED);

}


void COpApproxPoly::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpApproxPoly::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nParameter, sizeof(m_nParameter), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_nRecursive, sizeof(m_nRecursive), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bApproxPolyFiller, sizeof(bApproxPolyFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpApproxPoly::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nParameter, sizeof(m_nParameter), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_nRecursive, sizeof(m_nRecursive), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bApproxPolyFiller, sizeof(bApproxPolyFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpApproxPoly::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpApproxPoly::ValidateParameters()
{
}

void COpApproxPoly::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pParameterSlider) 
		m_nParameter = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpApproxPoly::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpApproxPoly::OnButton()
{
	if (m_pRecursiveButton->GetCheck() == BST_CHECKED)
		m_nRecursive = 1;
	else
		m_nRecursive = 0;

	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpApproxPoly::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpApproxPoly::OpThread()
{
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		CvSeq *pSeq = (CvSeq *) LockData(L"ApproxPolyThread", DATA_CONTOUR, 0);

		if (pSeq) 
		{
			if (1) {	// Validate Src and Dst images, etc
				CvSeq *pNewContour = NULL;
				CvMemStorage *pStorage = cvCreateMemStorage();

				pNewContour = cvApproxPoly(pSeq, sizeof(CvContour), pStorage, CV_POLY_APPROX_DP, m_nParameter, m_nRecursive);

				SetData(0, DATA_CONTOUR, 0, pNewContour, TRUE, DATA_STORAGE_MEMSTORAGE, pStorage);
				SendData();

			}
				else TitleError(L"");

			ReleaseData(L"ApproxPolyThread");  // We're done with the data so it can change if needed
		}

		WaitForSingleObject(m_hThreadEvent, INFINITE);

	}
}
