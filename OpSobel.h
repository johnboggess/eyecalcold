#pragma once
#include "opbase.h"
#include "Cv.h"

#define Sobel_DEFAULT_WIDTH (200)
#define Sobel_DEFAULT_HEIGHT (150)

class COpSobel : public COpBase
{
public:
	COpSobel(CMainView *pMain);
	~COpSobel(void);

	virtual	PTCHAR GetFriendlyName() { return L"Sobel"; }
	virtual int	GetDefaultWidth() { return Sobel_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return Sobel_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);


protected:
	CSliderCtrl	*m_pXOrderSlider;
	CEdit		*m_pXOrderEdit;
	CSliderCtrl	*m_pYOrderSlider;
	CEdit		*m_pYOrderEdit;
	CSliderCtrl	*m_pApertureSlider;
	CEdit		*m_pApertureEdit;

		// Save data
	int	m_nXOrder;
	int	m_nYOrder;
	int	m_nAperture;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );

	DECLARE_MESSAGE_MAP()
};
