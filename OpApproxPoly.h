#pragma once
#include "opbase.h"
#include "Cv.h"

#define ApproxPoly_DEFAULT_WIDTH (200)
#define ApproxPoly_DEFAULT_HEIGHT (120)

class COpApproxPoly : public COpBase
{
public:
	COpApproxPoly(CMainView *pMain);
	~COpApproxPoly(void);

	virtual	PTCHAR GetFriendlyName() { return L"ApproxPoly"; }
	virtual int	GetDefaultWidth() { return ApproxPoly_DEFAULT_WIDTH; }
	virtual int GetDefaultHeight() { return ApproxPoly_DEFAULT_HEIGHT; }

	virtual void NotifyNewDataIndex(int InputIndex);
	virtual void StopBase();


protected:
	CSliderCtrl	*m_pParameterSlider;
	CEdit		*m_pParameterEdit;
	CButton		*m_pRecursiveButton;

		// Save data
	int	m_nParameter;
	int	m_nRecursive;

		// Other data
	HANDLE		m_hThread;
	BOOL		m_fThreadRunning;
	HANDLE		m_hThreadEvent;

	void	UpdateValues();
	void	UpdateSliders();

	virtual void AddItemsToContextMenu(CMenu *pMenu);
	virtual	BOOL SaveToStream(HANDLE hFile);
	virtual	BOOL LoadFromStream(HANDLE hFile);
	void ValidateParameters();

	static DWORD WINAPI StartOpThread(void *pData);
	void OpThread();

	afx_msg void OnChange();
	afx_msg void OnPaint();
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	afx_msg void OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar );
	afx_msg void OnButton();

	DECLARE_MESSAGE_MAP()
};
