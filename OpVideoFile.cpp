#include "StdAfx.h"
#include "OpVideoFile.h"
#include "OpBase.h"
#include "Cv.h"
#include "HighGui.h"

BYTE bVideoFileFiller[32];

#define MIN_DELAY (0)
#define MAX_DELAY (5000)

#define ID_VIDEO_FILE_BROWSE_BUTTON (101)
#define ID_VIDEO_FILE_PLAY_BUTTON (102)
#define ID_VIDEO_FILE_STOP_BUTTON (103)
#define ID_VIDEO_FILE_NEXT_BUTTON (104)
#define ID_VIDEO_FILE_PREV_BUTTON (105)

BEGIN_MESSAGE_MAP(COpVideoFile, COpBase)
	ON_WM_PAINT()
	ON_WM_CONTEXTMENU()
	ON_WM_CREATE()
	ON_WM_HSCROLL()
//	ON_CBN_SELCHANGE(ID_, OnChange)
	ON_COMMAND(ID_VIDEO_FILE_BROWSE_BUTTON, OnBrowseButton)
	ON_COMMAND(ID_VIDEO_FILE_PLAY_BUTTON, OnPlayButton)
	ON_COMMAND(ID_VIDEO_FILE_STOP_BUTTON, OnStopButton)
	ON_COMMAND(ID_VIDEO_FILE_NEXT_BUTTON, OnNextButton)
	ON_COMMAND(ID_VIDEO_FILE_PREV_BUTTON, OnPrevButton)
END_MESSAGE_MAP()

COpVideoFile::COpVideoFile(CMainView *pMain) : COpBase(pMain)
{

	m_aFileName[0] = 0;
	m_fPlayFlag = FALSE;
	m_fStopFlag = FALSE;
	m_nDelay = 0;
	m_pCapture = NULL;

	m_hThread = NULL;
	m_fThreadRunning = FALSE;
	m_hThreadEvent = NULL;

}

COpVideoFile::~COpVideoFile(void)
{
//	if (m_p)delete m_p;

	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
	}

	if (m_hThreadEvent) CloseHandle(m_hThreadEvent);

}

void COpVideoFile::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	PaintBase(&dc, TRUE); // Draw base with title

	if (m_fMinimized)
	{
/*
		RECT Rect;

		GetClientRect(&Rect);

		dc.SetBkColor(OPBASE_BACKCOLOR);

		Rect.top += 5;
		Rect.bottom += 5;

		dc.DrawText(m_aSmoothTypeStr, _tcslen(m_aSmoothTypeStr), &Rect, DT_CENTER | DT_TOP);

		TCHAR TStr[100];

		_stprintf(TStr, L"%d    %d", m_bBlurWidth, m_bBlurHeight);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);

		_stprintf(TStr, L"%2.1f    %2.1f", m_fSigma1, m_fSigma2);
		Rect.top += TEXT_HEIGHT;
		Rect.bottom += TEXT_HEIGHT;
		dc.DrawText(TStr, _tcslen(TStr), &Rect, DT_CENTER | DT_TOP);
*/
			
	}

}

int COpVideoFile::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	CreateOpStatic(L"Delay", TRUE, 0);
	m_pDelayEdit = CreateOpEdit(PARAM_EDIT_WIDTH*2, TRUE);
	m_pDelaySlider = CreateOpSlider(0, MIN_DELAY, MAX_DELAY);
	CreateOpStatic(L"", TRUE, 0);

	m_pBrowseButton = CreateOpButton(L"Browse", 0, 0, ID_VIDEO_FILE_BROWSE_BUTTON);
	m_pFileNameEdit = CreateOpEdit(0, TRUE); 
	CreateOpStatic(L"", TRUE, 0);

	CreateOpStatic(L" ", TRUE, 50);
	m_pPlayButton = CreateOpButton(L"Play", 60, 0, ID_VIDEO_FILE_PLAY_BUTTON);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L" ", TRUE, 10);
	m_pPrevButton = CreateOpButton(L"Prev", 60, 0, ID_VIDEO_FILE_PREV_BUTTON);
	CreateOpStatic(L" ", TRUE, 20);
	m_pNextButton = CreateOpButton(L"Next", 60, 0, ID_VIDEO_FILE_NEXT_BUTTON);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L"", TRUE, 0);
	CreateOpStatic(L" ", TRUE, 50);
	m_pStopButton = CreateOpButton(L"Stop", 60, 0, ID_VIDEO_FILE_STOP_BUTTON);

	UpdateSliders();
	UpdateValues();

	m_hThreadEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	DWORD dwThreadID;
	m_hThread = CreateThread(NULL, 0, StartOpThread, this, 0, &dwThreadID);

	return 0;	// Must be zero
}

DWORD WINAPI COpVideoFile::StartOpThread(void *pData)
{
	COpVideoFile *pBase = (COpVideoFile *) pData;

	pBase->OpThread();

	return 0;
}

void COpVideoFile::UpdateSliders()
{
//	m_pSlider->SetPos(m_n);

}
void COpVideoFile::UpdateValues()
{
	TCHAR TStr[10];


	_itot(m_nDelay, TStr, 10);
	m_pDelayEdit->SetWindowTextW(TStr);

	if (m_fPlayFlag) m_pPlayButton->SetWindowTextW(L"Pause");
		else m_pPlayButton->SetWindowTextW(L"Play");

	if (m_aFileName[0]) 
	{
		PTCHAR pChar = m_aFileName + _tcslen(m_aFileName) - 1;
		
		while (pChar > m_aFileName && *pChar && *pChar != '\\') pChar--;
		
		if (*pChar == '\\') pChar++;	// Move to first character of file name
		
		TCHAR FileName[MAX_PATH];
		
		_tcscpy(FileName, pChar);
		
		m_pFileNameEdit->SetWindowTextW(FileName);
	}
	else m_pFileNameEdit->SetWindowTextW(L"");

}


void COpVideoFile::AddItemsToContextMenu(CMenu *pMenu)
{
//	AddMenuItem(pMenu, L"Load Image", ID_LOAD_IMAGE);
}


BOOL COpVideoFile::SaveToStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::SaveToStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		WriteFile(hFile, &m_nDelay, sizeof(m_nDelay), &dwBytesWritten, NULL);

		if (WriteFile(hFile, &m_aFileName, sizeof(m_aFileName), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			WriteFile(hFile, bVideoFileFiller, sizeof(bVideoFileFiller), &dwBytesWritten, NULL);
		}

	}

	return fRetValue;
}

BOOL COpVideoFile::LoadFromStream(HANDLE hFile)
{
	BOOL fRetValue = FALSE;

	fRetValue = COpBase::LoadFromStream(hFile);

	if (fRetValue)
	{
		DWORD dwBytesWritten;

		fRetValue = FALSE;

		ReadFile(hFile, &m_nDelay, sizeof(m_nDelay), &dwBytesWritten, NULL);

		if (ReadFile(hFile, &m_aFileName, sizeof(m_aFileName), &dwBytesWritten, NULL))
		{		
			fRetValue = TRUE;

				// Make room for enhancements
			ReadFile(hFile, bVideoFileFiller, sizeof(bVideoFileFiller), &dwBytesWritten, NULL);

			UpdateSliders();
			UpdateValues();
		}
	}

	return fRetValue;
}

	// We got data from the given input index
void COpVideoFile::NotifyNewDataIndex(int InputIndex)
{
	SetEvent(m_hThreadEvent);	// Tell thread that new data is available

}

void COpVideoFile::ValidateParameters()
{
}

void COpVideoFile::OnHScroll( UINT nSBCode, UINT nPos, CScrollBar* pScrollBar )
{
	CSliderCtrl *pSlider = (CSliderCtrl *) pScrollBar;

	if (pSlider == m_pDelaySlider) 
		m_nDelay = pSlider->GetPos();

	ValidateParameters();

	UpdateValues();

	SetEvent(m_hThreadEvent);	// Tell thread to rework with changes

}

void COpVideoFile::OnChange()
{
/*	
	int Ndx = m_pType->GetCurSel();

	if (Ndx != LB_ERR)
	{
		TYPE eType = (TYPE) m_p->GetItemData(Ndx);
		m_p->GetLBText(Ndx, m_aStr);

		ValidateParameters();

		SetEvent(m_hThreadEvent);	// Tell thread to rework with changes
	}
*/
}

void COpVideoFile::OnButton()
{
//	if (m_p->GetCheck() == BST_CHECKED)

//	SetEvent(m_hThreadEvent);	// Tell thread that new data is available
}

void COpVideoFile::OnBrowseButton()
{
	CFileDialog *pDialog = new CFileDialog(FALSE, L".avi", NULL, 0, L"avi|*.avi||", NULL );

	if (pDialog->DoModal() == IDOK) {
		TCHAR TStr[MAX_PATH];

		_tcscpy(m_aFileName, pDialog->GetPathName());

		UpdateValues();	// Display new file name
	}

	delete pDialog;

}

void COpVideoFile::OnPlayButton()
{
	if (m_fPlayFlag) m_pPlayButton->SetWindowTextW(L"Play");
		else m_pPlayButton->SetWindowTextW(L"Pause");

	m_fPlayFlag = !m_fPlayFlag;

	SetEvent(m_hThreadEvent);
}

void COpVideoFile::OnNextButton()
{
	SetEvent(m_hThreadEvent);
}

void COpVideoFile::OnPrevButton()
{
	if (m_pCapture)
	{
		double gCurPos = cvGetCaptureProperty( m_pCapture, CV_CAP_PROP_POS_FRAMES);
		
		gCurPos --;
		
		if (gCurPos < 0) gCurPos = 0;
		
		cvSetCaptureProperty(m_pCapture, CV_CAP_PROP_POS_FRAMES, gCurPos);
		
		SetEvent(m_hThreadEvent);

	}
}

void COpVideoFile::OnStopButton()
{
	m_fStopFlag = TRUE;

	SetEvent(m_hThreadEvent);
}

void COpVideoFile::StopBase()
{
	if (m_hThread) {
		m_fThreadRunning = FALSE;
		SetEvent(m_hThreadEvent);
		WaitForSingleObject(m_hThread, 2000);
		CloseHandle(m_hThread);
		m_hThread = NULL;
	}
}

void COpVideoFile::OpThread()
{
	
	m_fThreadRunning = true;

	while (m_fThreadRunning)
	{
		if (m_fPlayFlag && !m_pCapture)
		{
			if (m_aFileName[0])
			{
				char FileName[MAX_PATH];
				
				wcstombs(FileName, m_aFileName, MAX_PATH);
				m_pCapture = cvCreateFileCapture(FileName);
				
				if (!m_pCapture) TitleError(L"Unable to create capture from file");
			}
				else TitleError(L"Source file name required");
		}
			
		IplImage *pImage = NULL;

		if (m_pCapture) {	// Validate Src and Dst images, etc

			pImage = cvQueryFrame(m_pCapture);

			if (pImage) 
			{

				SetData(0, DATA_IMAGE, 0, pImage);
				SendData();

			}
				else
			{
				if (m_fPlayFlag) 
				{
					cvReleaseCapture(&m_pCapture);
					m_pCapture = NULL;	
					OnPlayButton();	// We're done with the video so switch to pause
				}
			}
		}
		
		if (m_fStopFlag)
		{
			m_fPlayFlag = FALSE;

			m_fStopFlag = FALSE;
			
			UpdateValues();
			
			if (pImage)
			{
				IplImage *pBlankImage = cvCreateImage(cvGetSize(pImage), pImage->depth, pImage->nChannels);
				SetData(0, DATA_IMAGE, 0, pBlankImage, TRUE);
				SendData();
			}

			cvReleaseCapture(&m_pCapture);
			m_pCapture = NULL;	
			
		}
		
		if (m_fThreadRunning)
		{
			if (!m_fPlayFlag)
				WaitForSingleObject(m_hThreadEvent, INFINITE);
			else if (m_nDelay) 
				Sleep(m_nDelay);
		}

	}
}
